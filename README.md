# Connect2id Server SDK

Copyright (c) Connect2id Ltd., 2013 - 2024


# README

Toolkit for developing Connect2id Server plugins, such as a OpenID Connect
claims sources and OAuth 2.0 grant handlers.

## Documentation

Check the [SPI documentation](https://connect2id.com/products/server/docs/integration)
for the Connect2id server.

## Maven

Official releases of the Connect2id Server toolkit are pushed to Maven Central
under

* GroupId: com.nimbusds

* ArtifactId: c2id-server-sdk


These include the library’s source code, compiled JAR and JavaDocs.

To add the SDK to your Maven project use the following template:

```xml
<dependency>
    <groupId>com.nimbusds</groupId>
    <artifactId>c2id-server-sdk</artifactId>
    <version>[version]</version>
</dependency>
```
where `[version]` should match the expected by the particular Connect2id Server
version you're running.



## Questions or comments? 

Email [Connect2id tech support](https://connect2id.com/contact#support).

package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;


/**
 * Tests the optional token specification.
 */
public class OptionalTokenSpecTest {


        @Test
        public void testDoIssue()
		throws Exception {

		var tokenSpec = new OptionalTokenSpec(true, 600L, null, null);
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		jsonObject = JSONObjectUtils.parse(jsonObject.toJSONString());
		tokenSpec = OptionalTokenSpec.parse(jsonObject);
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
	}


        @Test
        public void testDontIssue()
		throws Exception {

		var tokenSpec = new OptionalTokenSpec(false, 0L, null, null);
		assertFalse(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		jsonObject = JSONObjectUtils.parse(jsonObject.toJSONString());
		tokenSpec = OptionalTokenSpec.parse(jsonObject);
		assertFalse(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
	}


        @Test
        public void testWithAudience()
		throws Exception {

		var tokenSpec = new OptionalTokenSpec(true, 600L, Arrays.asList(new Audience("alice"), new Audience("bob")), null);
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(Arrays.asList(new Audience("alice"), new Audience("bob")), tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());

		// {
		// "issue":true,
		// "lifetime":600,
		// "audience":["alice","bob"]
		// }
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(600L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals("alice", JSONObjectUtils.getStringList(jsonObject, "audience").get(0));
		assertEquals("bob", JSONObjectUtils.getStringList(jsonObject, "audience").get(1));
		assertEquals(2, JSONObjectUtils.getStringList(jsonObject, "audience").size());
		assertEquals(3, jsonObject.size());

		tokenSpec = OptionalTokenSpec.parse(JSONObjectUtils.parse(tokenSpec.toJSONObject().toJSONString()));
		assertEquals(2, tokenSpec.getAudience().size());
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(Arrays.asList(new Audience("alice"), new Audience("bob")), tokenSpec.getAudience());
		Assert.assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testZeroLifetimeToJSONObject() throws ParseException {

		JSONObject jsonObject = new OptionalTokenSpec(true, 0L, null, null).toJSONObject();
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(0L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals(2, jsonObject.size());
	}


        @Test
        public void testParseDefault()
		throws Exception {

		var jsonObject = new JSONObject();
		OptionalTokenSpec tokenSpec = OptionalTokenSpec.parse(jsonObject);
		assertFalse(tokenSpec.issue());
		assertEquals(-1L, tokenSpec.getLifetime());
	}
}

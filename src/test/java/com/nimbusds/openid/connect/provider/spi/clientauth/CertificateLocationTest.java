package com.nimbusds.openid.connect.provider.spi.clientauth;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CertificateLocationTest {


        @Test
        public void testConstants() {
		
		assertEquals("JWS_HEADER", CertificateLocation.JWS_HEADER.name());
		assertEquals("JWK", CertificateLocation.JWK.name());
		assertEquals(2, CertificateLocation.values().length);
	}
}

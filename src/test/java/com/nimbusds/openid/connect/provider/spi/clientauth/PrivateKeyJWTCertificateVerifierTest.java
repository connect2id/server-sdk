package com.nimbusds.openid.connect.provider.spi.clientauth;


import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.X509CertificateUtils;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Assert;
import org.junit.Test;

import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class PrivateKeyJWTCertificateVerifierTest {

	private static final Issuer CA = new Issuer("ca.example.com");
	
	private static final RSAKey CA_RSA_JWK;
	
	private static final Issuer OP = new Issuer("https://c2id.com");

	private static final ClientID CLIENT_ID = new ClientID("123");
	
	private static final RSAKey CLIENT_RSA_JWK;
	
	private static final X509Certificate CLIENT_CERT;
	
	private static final X509Certificate BAD_CLIENT_CERT;
	
	private static final ClientAuthenticationID CLIENT_AUTHENTICATION_ID = () -> UUID.randomUUID().toString();
	
	static {
		try {
			CA_RSA_JWK = new RSAKeyGenerator(2048)
				.generate();
			
			RSAKey basicClientJWK = new RSAKeyGenerator(2048)
				.keyUse(KeyUse.SIGNATURE)
				.keyID("1")
				.generate();
			
			CLIENT_CERT = X509CertificateUtils.generate(
				CA,
				new Subject(CLIENT_ID.getValue()),
				Date.from(Instant.now().minus(1, ChronoUnit.HOURS)),
				Date.from(Instant.now().plus(1, ChronoUnit.HOURS)),
				basicClientJWK.toPublicKey(),
				CA_RSA_JWK.toPrivateKey());
			
			List<Base64> x5c = Collections.singletonList(Base64.encode(CLIENT_CERT.getEncoded()));
			
			BAD_CLIENT_CERT = X509CertificateUtils.generate(
				CA,
				new Subject(CLIENT_ID.getValue()),
				Date.from(Instant.now().minus(2, ChronoUnit.HOURS)),
				Date.from(Instant.now().minus(1, ChronoUnit.HOURS)),
				basicClientJWK.toPublicKey(),
				CA_RSA_JWK.toPrivateKey());
			
			CLIENT_RSA_JWK = new RSAKey.Builder(basicClientJWK)
				.x509CertChain(x5c)
				.build();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	static class PrivateKeyJWTContextImpl implements PrivateKeyJWTContext {
		
		
		private final OIDCClientInformation clientInfo;
		private final JWSHeader jwsHeader;
		
		
		public PrivateKeyJWTContextImpl(final OIDCClientInformation clientInfo, final JWSHeader jwsHeader) {
			this.clientInfo = clientInfo;
			this.jwsHeader = jwsHeader;
		}
		
		
		@Override
		public Issuer getIssuer() {
			return OP;
		}
		
		
		@Override
		public OIDCClientInformation getOIDCClientInformation() {
			return clientInfo;
		}
		
		
		@Override
		public ClientAuthenticationID getID() {
			return CLIENT_AUTHENTICATION_ID;
		}
		
		
		@Override
		public JWSHeader getJWSHeader() {
			return jwsHeader;
		}
	}
	
	
	/**
	 * Plugin to always require {@code private_key_jwt} with x5c in the
	 * header to authenticate the client.
	 */
	static class AlwaysRequireX5C implements PrivateKeyJWTCertificateVerifier {
		
		
		@Override
		public void init(InitContext initContext) throws Exception {
			// nothing to do
		}
		
		
		@Override
		public boolean isEnabled() {
			return true;
		}
		
		
		@Override
		public void shutdown() {
			// nothing to do
		}
		
		
		@Override
		public Optional<CertificateVerification> checkCertificateRequirement(final PrivateKeyJWTContext ctx) {
			
			return Optional.of((x5c, certCtx) -> {
				
				if (! certCtx.getCertificateLocations().contains(CertificateLocation.JWS_HEADER)) {
					throw new InvalidClientException("Missing x5c in private_key_jwt");
				}
				
				try {
					x5c.get(0).checkValidity();
					x5c.get(0).verify(CA_RSA_JWK.toPublicKey());
					
				} catch (CertificateException | SignatureException e) {
					throw new InvalidClientException("Invalid x5c: " + e.getMessage());
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}


        @Test
        public void testCase_alwaysRequireX5C() throws InvalidClientException {
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setTokenEndpointAuthMethod(ClientAuthenticationMethod.PRIVATE_KEY_JWT);
		clientMetadata.setTokenEndpointAuthJWSAlg(JWSAlgorithm.RS256);
		clientMetadata.applyDefaults();
		
		var clientInfo = new OIDCClientInformation(CLIENT_ID, clientMetadata);
		
		JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
			.x509CertChain(CLIENT_RSA_JWK.getX509CertChain())
			.build();
		
		PrivateKeyJWTContext ctx = new PrivateKeyJWTContextImpl(clientInfo, jwsHeader);
		
		Optional<CertificateVerification> certVerification = new AlwaysRequireX5C().checkCertificateRequirement(ctx);
		
		assertTrue("Client cert required", certVerification.isPresent());
		
		// Success
		certVerification.get().verify(Collections.singletonList(CLIENT_CERT), () -> Collections.singleton(CertificateLocation.JWS_HEADER));
		
		// Bad certificate
		try {
			certVerification.get().verify(Collections.singletonList(BAD_CLIENT_CERT), () -> Collections.singleton(CertificateLocation.JWS_HEADER));
			fail();
		} catch (InvalidClientException e) {
			assertTrue(e.getMessage().startsWith("Invalid x5c: NotAfter:"));
		}
		
		// Certificate found in registered client JWKs, but not in private_key_jwt header
		try {
			certVerification.get().verify(Collections.singletonList(BAD_CLIENT_CERT), () -> Collections.singleton(CertificateLocation.JWK));
			fail();
		} catch (InvalidClientException e) {
			Assert.assertEquals("Missing x5c in private_key_jwt", e.getMessage());
		}
	}
	
	
	/**
	 * Plugin to require {@code private_key_jwt} with "kid" xor "x5c" in
	 * the header to authenticate the client.
	 */
	static class RequireKeyIDxorX5C implements PrivateKeyJWTCertificateVerifier {
		
		
		@Override
		public void init(InitContext initContext) throws Exception {
			// nothing to do
		}
		
		
		@Override
		public boolean isEnabled() {
			return true;
		}
		
		
		@Override
		public void shutdown() {
			// nothing to do
		}
		
		
		@Override
		public Optional<CertificateVerification> checkCertificateRequirement(final PrivateKeyJWTContext ctx)
			throws InvalidClientException {
			
			JWSHeader jwsHeader = ctx.getJWSHeader();
			
			if (jwsHeader.getKeyID() != null && jwsHeader.getX509CertChain() != null ||
			    jwsHeader.getKeyID() == null && jwsHeader.getX509CertChain() == null) {
				
				throw new InvalidClientException("Illegal JWS header");
			}
			
			if (jwsHeader.getKeyID() !=  null) {
				// Let the Connect2id server continue with regular private_key_jwt
				return Optional.empty();
			}
			
			// Verify the x5c in the private_key_jwt
			return Optional.of((x5c, certCtx) -> {
				
				if (! certCtx.getCertificateLocations().contains(CertificateLocation.JWS_HEADER)) {
					throw new InvalidClientException("Missing x5c in private_key_jwt");
				}
				
				try {
					x5c.get(0).checkValidity();
					x5c.get(0).verify(CA_RSA_JWK.toPublicKey());
					
				} catch (CertificateException | SignatureException e) {
					throw new InvalidClientException("Invalid x5c: " + e.getMessage());
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
		}
	}


        @Test
        public void testCase_requireKeyIDxorX5C() throws InvalidClientException {
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setTokenEndpointAuthMethod(ClientAuthenticationMethod.PRIVATE_KEY_JWT);
		clientMetadata.setTokenEndpointAuthJWSAlg(JWSAlgorithm.RS256);
		clientMetadata.setJWKSet(new JWKSet(CLIENT_RSA_JWK.toPublicJWK()));
		clientMetadata.applyDefaults();
		
		var clientInfo = new OIDCClientInformation(CLIENT_ID, clientMetadata);
		
		
		PrivateKeyJWTCertificateVerifier plugin = new RequireKeyIDxorX5C();
		
		
		// private_key_jwt with kid
		
		JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
			.keyID(CLIENT_RSA_JWK.getKeyID())
			.build();
		
		PrivateKeyJWTContext ctx = new PrivateKeyJWTContextImpl(clientInfo, jwsHeader);
		
		Assert.assertFalse(plugin.checkCertificateRequirement(ctx).isPresent());
		
		
		// private_key_jwt with x5c
		
		jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
			.x509CertChain(CLIENT_RSA_JWK.getX509CertChain())
			.build();
		
		ctx = new PrivateKeyJWTContextImpl(clientInfo, jwsHeader);
		
		plugin.checkCertificateRequirement(ctx).ifPresent(certVerification -> {
			// Success
			try {
				certVerification.verify(Collections.singletonList(CLIENT_CERT), () -> Collections.singleton(CertificateLocation.JWS_HEADER));
			} catch (InvalidClientException e) {
				fail();
			}
		});
		
		
		// private_key_jwt w/o kid and x5c
		
		jwsHeader = new JWSHeader.Builder(JWSAlgorithm.RS256)
			.build();
		
		ctx = new PrivateKeyJWTContextImpl(clientInfo, jwsHeader);
		
		try {
			plugin.checkCertificateRequirement(ctx);
			fail();
		} catch (InvalidClientException e) {
			Assert.assertEquals("Illegal JWS header", e.getMessage());
		}
	}
}

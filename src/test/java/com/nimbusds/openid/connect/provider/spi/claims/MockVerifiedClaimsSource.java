package com.nimbusds.openid.connect.provider.spi.claims;


import java.util.List;
import java.util.Set;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.assurance.IdentityVerification;
import com.nimbusds.openid.connect.sdk.assurance.claims.VerifiedClaimsSet;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import com.nimbusds.openid.connect.sdk.claims.PersonClaims;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;


public class MockVerifiedClaimsSource extends MockClaimsSource implements AdvancedClaimsSource{
	
	
	@Override
	public UserInfo getClaims(final Subject subject,
				  final Set<String> claims,
				  final List<LangTag> claimsLocales,
				  final ClaimsSourceRequestContext requestContext) throws ParseException {
		
		assert AdvancedClaimsSourceTest.ISSUER.equals(requestContext.getIssuer());
		assert AdvancedClaimsSourceTest.CLIENT_ID.equals(requestContext.getClientID());
		assert ClaimsTransport.USERINFO.equals(requestContext.getClaimsTransport());
		assert AdvancedClaimsSourceTest.CLIENT_INFO.equals(requestContext.getOIDCClientInformation());
		assert AdvancedClaimsSourceTest.CLIENT_IP.equals(requestContext.getClientIPAddress());
		
		UserInfo userInfo = getClaims(subject, claims, claimsLocales);
		
		if (userInfo == null) {
			return null;
		}
		
		if (requestContext.getClaimsData() == null) {
			return userInfo;
		}
		
		// The identity verification element is passed via the claims fulfillment data
		IdentityVerification verification = IdentityVerification.parse(requestContext.getClaimsData());
		
		PersonClaims verifiedClaims = new PersonClaims();
		for (String claimName: claims) {
			if ("verified:email".equals(claimName)) {
				verifiedClaims.setEmailAddress("alice@wonderland.net");
			}
			if ("verified:name".equals(claimName)) {
				verifiedClaims.setName("Alice Adams");
			}
		}
		
		if (verifiedClaims.toJSONObject().isEmpty()) {
			// No verified claims requested
			return userInfo;
		}
		
		VerifiedClaimsSet verifiedClaimsSet = new VerifiedClaimsSet(verification, verifiedClaims);
		userInfo.setVerifiedClaims(verifiedClaimsSet);
		return userInfo;
	}
}

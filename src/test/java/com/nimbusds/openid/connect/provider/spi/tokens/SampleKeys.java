package com.nimbusds.openid.connect.provider.spi.tokens;


import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import com.nimbusds.jose.jwk.RSAKey;


public class SampleKeys {
	
	
	public static final RSAPublicKey RSA_PUBLIC_2048;
	
	
	public static final RSAPrivateKey RSA_PRIVATE_2048;
	
	
	public static final RSAKey RSA_JWK_2048;
	
	
	static {
		try {
			KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
			gen.initialize(2048);
			KeyPair keyPair = gen.generateKeyPair();
			
			RSA_PUBLIC_2048 = (RSAPublicKey)keyPair.getPublic();
			RSA_PRIVATE_2048 = (RSAPrivateKey)keyPair.getPrivate();
			
			RSA_JWK_2048 = new RSAKey.Builder(RSA_PUBLIC_2048)
				.privateKey(RSA_PRIVATE_2048)
				.keyID("1")
				.build();
			
		} catch (Exception e) {
			throw new RuntimeException("Couldn't generate key: " + e.getMessage(), e);
		}
	}
	
}

package com.nimbusds.openid.connect.provider.spi.clientauth;


import com.nimbusds.oauth2.sdk.OAuth2Error;
import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class ExposedInvalidClientExceptionTest {


        @Test
        public void testMinimalConstructor_nullMessage_nullErrorDescription() {
		
		var exception = new ExposedInvalidClientException(null, null);
		assertNull(exception.getMessage());
		
		assertEquals(OAuth2Error.INVALID_CLIENT.getCode(), exception.getErrorObject().getCode());
		assertNull(exception.getErrorObject().getDescription());
		assertNull(exception.getErrorObject().getURI());
	}


        @Test
        public void testMinimalConstructor() {
		
		String message = "Expired certificate";
		String errorDescription = "Invalid client: Expired certificate";
		
		var exception = new ExposedInvalidClientException(message, errorDescription);
		assertEquals(message, exception.getMessage());
		
		assertEquals(OAuth2Error.INVALID_CLIENT.getCode(), exception.getErrorObject().getCode());
		assertEquals(errorDescription, exception.getErrorObject().getDescription());
		assertNull(exception.getErrorObject().getURI());
	}


        @Test
        public void testMinimalConstructor_nullMessage_nullErrorDescription_nullErrorURI() {
		
		var exception = new ExposedInvalidClientException(null, null, null);
		assertNull(exception.getMessage());
		
		assertEquals(OAuth2Error.INVALID_CLIENT.getCode(), exception.getErrorObject().getCode());
		assertNull(exception.getErrorObject().getDescription());
		assertNull(exception.getErrorObject().getURI());
	}


        @Test
        public void testFullConstructor() {
		
		var message = "Expired certificate";
		var errorDescription = "Invalid client: Expired certificate";
		URI errorURI = URI.create("https://errors.c2id.com/expired-x5c");
		
		var exception = new ExposedInvalidClientException(message, errorDescription, errorURI);
		assertEquals(message, exception.getMessage());
		
		assertEquals(OAuth2Error.INVALID_CLIENT.getCode(), exception.getErrorObject().getCode());
		assertEquals(errorDescription, exception.getErrorObject().getDescription());
		assertEquals(errorURI, exception.getErrorObject().getURI());
	}
}

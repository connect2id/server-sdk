package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.assertions.saml2.*;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.NameID;
import org.opensaml.security.credential.BasicCredential;
import org.opensaml.xmlsec.signature.support.SignatureConstants;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.net.URI;
import java.util.*;

import static org.junit.Assert.*;


/**
 * Tests the third-party SAML 2.0 handler interface.
 */
public class ThirdPartySAML2GrantHandlerTest {


	private static final SecretKey HMAC_KEY = new SecretKeySpec(new Secret().getValueBytes(), "HmacSha256");


	private static final Issuer EXPECTED_ISSUER = new Issuer("https://secure-token-service.com");


	private static final URI TOKEN_ENDPOINT = URI.create("https://c2id.com/token");


	static class ExampleHandler implements ThirdPartySAML2GrantHandler {


		SAML2AssertionDetailsVerifier detailsVerifier = new SAML2AssertionDetailsVerifier(
			new HashSet<>(Collections.singletonList(new Audience(TOKEN_ENDPOINT))));


		@Override
		public void init(InitContext initContext) throws Exception {

		}

		@Override
		public boolean isEnabled() {
			return true;
		}


		@Override
		public ThirdPartyAssertionAuthorization processThirdPartyGrant(final Assertion assertion,
									       final Scope scope,
									       final ClientID clientID,
									       final boolean confidentialClient,
									       final OIDCClientMetadata clientMetadata)
			throws GeneralException {

			// Permit only confidential clients
			if (! confidentialClient) {
				throw new GeneralException(OAuth2Error.INVALID_CLIENT.setDescription("Clients must be confidential"));
			}

			// Parse assertion and check audience and expiration
			SAML2AssertionDetails assertionDetails;
			try {
				assertionDetails = SAML2AssertionDetails.parse(assertion);
				detailsVerifier.verify(assertionDetails);
			} catch (com.nimbusds.oauth2.sdk.ParseException | BadSAML2AssertionException e) {
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription(e.getMessage()));
			}

			// Check the issuer
			if (! EXPECTED_ISSUER.equals(assertionDetails.getIssuer())) {
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Unexpected issuer, must be " + EXPECTED_ISSUER));
			}

			// The authorised scope is set in the SAML 2.0 assertion as an attribute statement
			Scope authzScope = Scope.parse(assertionDetails.getAttributeStatement().get("scope"));

			// Verify the HMAC
			if (assertion.getSignature() == null) {
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Missing HMAC / signature"));
			}

			if (! SignatureConstants.ALGO_ID_MAC_HMAC_SHA256.equals(assertion.getSignature().getSignatureAlgorithm())) {
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Unexpected XML digsig algorithm"));
			}

			try {
				SAML2AssertionValidator.verifySignature(assertion.getSignature(), HMAC_KEY);
			} catch (BadSAML2AssertionException e) {
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription(e.getMessage()));
			}

			return new ThirdPartyAssertionAuthorization(assertionDetails.getSubject(), clientID, authzScope);
		}


		@Override
		public void shutdown() throws Exception {

		}
	}


        @Test
        public void testAuthorizationSuccess()
		throws Exception {

		final Date now = new Date();

		Map<String,List<String>> attrs = new HashMap<>();
		attrs.put("scope", Scope.parse("read write").toStringList());

		var assertionDetails = new SAML2AssertionDetails(
			EXPECTED_ISSUER,
			new Subject("alice"),
			NameID.UNSPECIFIED,
			now,
			new ACR("http://acr.c2id.com/highsec"),
			new Audience(TOKEN_ENDPOINT).toSingleAudienceList(),
			new Date(now.getTime() + 6*60*1000L),
			now,
			now,
			new Identifier(),
			null,

			attrs);

		var credential = new BasicCredential(HMAC_KEY);
		Assertion assertion = SAML2AssertionFactory.create(assertionDetails, SignatureConstants.ALGO_ID_MAC_HMAC_SHA256, credential);

		ThirdPartySAML2GrantHandler handler = new ExampleHandler();
		assertTrue(handler.isEnabled());

		var clientID = new ClientID("123");

		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setGrantTypes(new HashSet<>(Collections.singletonList(GrantType.SAML2_BEARER)));
		clientMetadata.setResponseTypes(Collections.emptySet());
		clientMetadata.applyDefaults();

		ThirdPartyAssertionAuthorization authz = handler.processThirdPartyGrant(assertion, null, clientID, true, clientMetadata);

		assertEquals("alice", authz.getSubject().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime()); // implies default to server setting
		assertFalse(authz.getIDTokenSpec().issue());
		assertNull(authz.getData());
	}
}

package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;


public class TokenScopeShaper extends BaseTokenIntrospectionResponseComposer {
	
	
	@Override
	public TokenIntrospectionSuccessResponse compose(final AccessTokenAuthorization tokenAuthz,
							 final TokenIntrospectionContext context) {
		
		TokenIntrospectionSuccessResponse response = super.compose(tokenAuthz, context);
		
		if (! response.isActive() || context.getOIDCClientInformation() == null) {
			// Invalid / expired token, or invalid client, nothing to shape
			return response;
		}
		
		Scope registeredScopes = context.getOIDCClientInformation().getMetadata().getScope();
		
		final Scope shapedScope;
		
		if (CollectionUtils.isEmpty(registeredScopes)) {
			shapedScope = new Scope(); // resource server not registered for any scopes
		} else if (CollectionUtils.isEmpty(response.getScope())) {
			shapedScope = new Scope(); // token with no scope values
		} else {
			shapedScope = new Scope();
			shapedScope.addAll(response.getScope());
			shapedScope.retainAll(registeredScopes);
		}
		
		return new TokenIntrospectionSuccessResponse.Builder(response)
			.scope(shapedScope)
			.build();
	}
}

package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.*;


/**
 * Tests the third-party assertion authorisation.
 */
public class ThirdPartyAssertionAuthorizationTest {


        @Test
        public void testMinimalOnBehalfOfUser()
		throws Exception {

		var authz = new ThirdPartyAssertionAuthorization(new Subject("alice"), new ClientID("123"), Scope.parse("read write"));
		assertEquals("alice", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
		assertNull(authz.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getIDTokenSpec().issue());
		assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(authz.getData());

		String json = authz.toJSONObject().toJSONString();

		authz = ThirdPartyAssertionAuthorization.parse(json);

		assertEquals("alice", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
		assertNull(authz.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getIDTokenSpec().issue());
		assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(authz.getData());
	}


        @Test
        public void testFullOnBehalfOfUser()
		throws Exception {

		var authz = new ThirdPartyAssertionAuthorization(
			new Subject("alice"),
			new ClientID("123"),
			Scope.parse("read write"),
			new AccessTokenSpec(600L, Collections.singletonList(new Audience("A")), TokenEncoding.IDENTIFIER, false),
			new IDTokenSpec(true, 900L, null),
			new ClaimsSpec(new HashSet<>(Arrays.asList("openid", "email", "email_verified"))),
			null);
		assertEquals("alice", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals("A", authz.getAccessTokenSpec().getAudience().get(0).getValue());
		assertEquals(1, authz.getAccessTokenSpec().getAudience().size());
		assertEquals(TokenEncoding.IDENTIFIER, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertTrue(authz.getIDTokenSpec().issue());
		assertEquals(900L, authz.getIDTokenSpec().getLifetime());
		assertNull(authz.getIDTokenSpec().getImpersonatedSubject());
		assertTrue(authz.getClaimsSpec().getNames().equals(new HashSet<>(Arrays.asList("openid", "email", "email_verified"))));
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(authz.getData());

		String json = authz.toJSONObject().toJSONString();

		authz = ThirdPartyAssertionAuthorization.parse(json);

		assertEquals("alice", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals("A", authz.getAccessTokenSpec().getAudience().get(0).getValue());
		assertEquals(1, authz.getAccessTokenSpec().getAudience().size());
		assertEquals(TokenEncoding.IDENTIFIER, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertTrue(authz.getIDTokenSpec().issue());
		assertEquals(900L, authz.getIDTokenSpec().getLifetime());
		assertNull(authz.getIDTokenSpec().getImpersonatedSubject());
		assertTrue(authz.getClaimsSpec().getNames().equals(new HashSet<>(Arrays.asList("openid", "email", "email_verified"))));
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(authz.getData());
	}


        @Test
        public void testMinimalOnOwnBehalf()
		throws Exception {

		var authz = new ThirdPartyAssertionAuthorization(new ClientID("123"), Scope.parse("read"));
		assertEquals("123", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read"), authz.getScope());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
		assertNull(authz.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getIDTokenSpec().issue());
		assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(authz.getData());

		String json = authz.toJSONObject().toJSONString();

		authz = ThirdPartyAssertionAuthorization.parse(json);

		assertEquals("123", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read"), authz.getScope());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
		assertNull(authz.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getIDTokenSpec().issue());
		assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(authz.getData());
	}


        @Test
        public void testFullOnOwnBehalf()
		throws Exception {

		var authz = new ThirdPartyAssertionAuthorization(
			new ClientID("123"),
			Scope.parse("read write"),
			new AccessTokenSpec(600L, Collections.singletonList(new Audience("A")), TokenEncoding.IDENTIFIER, false),
			JSONObjectUtils.parse("{\"items\":10}"));
		assertEquals("123", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals("A", authz.getAccessTokenSpec().getAudience().get(0).getValue());
		assertEquals(1, authz.getAccessTokenSpec().getAudience().size());
		assertEquals(TokenEncoding.IDENTIFIER, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getIDTokenSpec().issue());
		assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertEquals(10L, authz.getData().get("items"));

		String json = authz.toJSONObject().toJSONString();

		authz = ThirdPartyAssertionAuthorization.parse(json);

		assertEquals("123", authz.getSubject().getValue());
		assertEquals("123", authz.getClientID().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals("A", authz.getAccessTokenSpec().getAudience().get(0).getValue());
		assertEquals(1, authz.getAccessTokenSpec().getAudience().size());
		assertEquals(TokenEncoding.IDENTIFIER, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getIDTokenSpec().issue());
		assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertTrue(authz.getClaimsSpec().getPresetClaims().isEmpty());
		assertEquals(10L, authz.getData().get("items"));
	}
}

package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagUtils;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.assurance.IdentityTrustFramework;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


/**
 * Tests the claims specification.
 */
public class ClaimsSpecTest {


        @Test
        public void testDefault()
		throws Exception {

		var spec = new ClaimsSpec();
		assertTrue(spec.getNames().isEmpty());
		assertNull(spec.getLocales());
		assertNull(spec.getData());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertEquals(ClaimsTransport.getDefault(), spec.getTransport());
		JSONObject jsonObject = spec.toJSONObject();
		assertEquals("USERINFO", (String)jsonObject.get("claims_transport"));
		assertEquals(1, jsonObject.size());
		spec = ClaimsSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertTrue(spec.getNames().isEmpty());
		assertNull(spec.getLocales());
		assertNull(spec.getData());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertEquals(ClaimsTransport.getDefault(), spec.getTransport());
	}


        @Test
        public void testNoneConstant()
		throws Exception {

		ClaimsSpec spec = ClaimsSpec.NONE;
		assertTrue(spec.getNames().isEmpty());
		assertNull(spec.getLocales());
		assertNull(spec.getData());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertEquals(ClaimsTransport.getDefault(), spec.getTransport());
		JSONObject jsonObject = spec.toJSONObject();
		assertEquals("USERINFO", (String)jsonObject.get("claims_transport"));
		assertEquals(1, jsonObject.size());
		spec = ClaimsSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertTrue(spec.getNames().isEmpty());
		assertNull(spec.getLocales());
		assertNull(spec.getData());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertEquals(ClaimsTransport.getDefault(), spec.getTransport());
	}


        @Test
        public void testMinimalConstructor()
		throws Exception {

		Set<String> names = Set.of("sub", "email", "email_verified");

		var spec = new ClaimsSpec(names);

		assertEquals(names, spec.getNames());
		assertNull(spec.getLocales());
		assertNull(spec.getData());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetUserInfoClaims());
		assertEquals(ClaimsTransport.getDefault(), spec.getTransport());

		JSONObject jsonObject = spec.toJSONObject();

		spec = ClaimsSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));

		assertTrue(spec.getNames().containsAll(Arrays.asList("sub", "email", "email_verified")));
		assertEquals(3, spec.getNames().size());
		assertNull(spec.getLocales());
		assertNull(spec.getData());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetUserInfoClaims());
		assertEquals(ClaimsTransport.getDefault(), spec.getTransport());
	}


        @Test
        public void testFullConstructor()
		throws Exception {

		Set<String> names = Set.of("sub", "email", "email_verified");
		List<LangTag> locales = LangTagUtils.parseLangTagList("en", "es");
		JSONObject data = new JSONObject();
		data.put("trust_framework", IdentityTrustFramework.EIDAS_IAL_SUBSTANTIAL.getValue());
		JSONObject presetIDToken = new JSONObject();
		presetIDToken.put("ip", "192.168.0.1");
		JSONObject presetUserInfo = new JSONObject();
		presetUserInfo.put("admin", true);
		ClaimsTransport transport = ClaimsTransport.ID_TOKEN;

		var spec = new ClaimsSpec(names, locales, data, presetIDToken, presetUserInfo, transport);

		assertEquals(names, spec.getNames());
		assertEquals(locales, spec.getLocales());
		assertEquals(data, spec.getData());
		assertEquals(presetIDToken, spec.getPresetIDTokenClaims());
		assertEquals(presetUserInfo, spec.getPresetUserInfoClaims());
		assertEquals(transport, spec.getTransport());

		JSONObject jsonObject = spec.toJSONObject();

		spec = ClaimsSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));

		assertTrue(spec.getNames().containsAll(Arrays.asList("sub", "email", "email_verified")));
		assertEquals(3, spec.getNames().size());
		assertTrue(spec.getLocales().containsAll(LangTagUtils.parseLangTagList("en", "es")));
		assertEquals(2, spec.getLocales().size());
		assertEquals(data, spec.getData());
		assertEquals("192.168.0.1", (String)spec.getPresetIDTokenClaims().get("ip"));
		assertEquals(1, spec.getPresetIDTokenClaims().size());
		assertTrue((Boolean)spec.getPresetUserInfoClaims().get("admin"));
		assertEquals(1, spec.getPresetUserInfoClaims().size());
		assertEquals(transport, spec.getTransport());
	}
}

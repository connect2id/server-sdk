package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.id.*;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertNull;


public class AccessTokenAuthorizationTest {
	
	
	static class AccessTokenAuthorizationImpl implements AccessTokenAuthorization {
		
		
		@Override
		public Subject getSubject() {
			return null;
		}
		
		
		@Override
		public Actor getActor() {
			return null;
		}
		
		
		@Override
		public ClientID getClientID() {
			return null;
		}
		
		
		@Override
		public Scope getScope() {
			return null;
		}
		
		
		@Override
		public Instant getExpirationTime() {
			return null;
		}
		
		
		@Override
		public Instant getIssueTime() {
			return null;
		}
		
		
		@Override
		public Issuer getIssuer() {
			return null;
		}
		
		
		@Override
		public List<Audience> getAudienceList() {
			return null;
		}
		
		
		@Override
		public JWTID getJWTID() {
			return null;
		}
		
		
		@Override
		public Set<String> getClaimNames() {
			return null;
		}
		
		
		@Override
		public List<LangTag> getClaimsLocales() {
			return null;
		}
		
		
		@Override
		public JSONObject getPresetClaims() {
			return null;
		}
		
		
		@Override
		public JSONObject getData() {
			return null;
		}
		
		
		@Override
		public X509CertificateConfirmation getClientCertificateConfirmation() {
			return null;
		}
	}


        @Test
        public void testDefaultMethods() {
	
		AccessTokenAuthorization authz = new AccessTokenAuthorizationImpl();
		assertNull(authz.getSubjectType());
		assertNull(authz.getLocalSubject());
		assertNull(authz.getJWKThumbprintConfirmation());
		assertNull(authz.getOtherTopLevelParameters());
		assertNull(authz.getClaimsData());
		assertNull(authz.getSubjectSessionKey());
	}
}

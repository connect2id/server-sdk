package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Tests the ID token spec.
 */
public class IDTokenSpecTest {


        @Test
        public void testDefaultConstructor()
		throws Exception {

		var tokenSpec = new IDTokenSpec();
		assertFalse(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
		
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertFalse((Boolean)jsonObject.get("issue"));
		assertEquals(1, jsonObject.size());
		
		tokenSpec = IDTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		
		assertFalse(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testNoneConstant()
		throws Exception {

		IDTokenSpec tokenSpec = IDTokenSpec.NONE;
		assertFalse(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
		
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertFalse((Boolean)jsonObject.get("issue"));
		assertEquals(1, jsonObject.size());
		
		tokenSpec = IDTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		
		assertFalse(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testIssue()
		throws Exception {

		var tokenSpec = new IDTokenSpec(true, 0L, null);
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		
		assertTrue((Boolean) jsonObject.get("issue"));
		assertEquals(1, jsonObject.size());
		
		tokenSpec = IDTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testIssueWithSpecificLifetime()
		throws Exception {

		var tokenSpec = new IDTokenSpec(true, 900L, null);
		assertTrue(tokenSpec.issue());
		assertEquals(900L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
		
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertTrue((Boolean) jsonObject.get("issue"));
		assertEquals(900L, ((Long)jsonObject.get("lifetime")).longValue());
		assertEquals(2, jsonObject.size());
		
		tokenSpec = IDTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		
		assertTrue(tokenSpec.issue());
		assertEquals(900L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testWithImpersonatedSubject()
		throws Exception {

		var tokenSpec = new IDTokenSpec(true, 600L, new Subject("bob"));
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertEquals(new Subject("bob"), tokenSpec.getImpersonatedSubject());
		
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(600L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals("bob", JSONObjectUtils.getString(jsonObject, "impersonated_sub"));
		assertEquals(3, jsonObject.size());
		
		tokenSpec = IDTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
		assertFalse(tokenSpec.isAllowRefresh());
		assertEquals(new Subject("bob"), tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testFullConstructor() throws ParseException {
		
		final Date authTime = new Date(123* 1000L);
		
		var tokenSpec = new IDTokenSpec(true, 600L, authTime, new ACR("0"), Collections.singletonList(new AMR("pwd")), true, new Subject("bob"));
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(authTime, tokenSpec.getAuthTime());
		assertEquals("0", tokenSpec.getACR().getValue());
		assertEquals("pwd", tokenSpec.getAMRList().get(0).getValue());
		assertTrue(tokenSpec.isAllowRefresh());
		assertEquals("bob", tokenSpec.getImpersonatedSubject().getValue());
		
		JSONObject jsonObject = tokenSpec.toJSONObject();
		
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(600L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals(authTime.toInstant().getEpochSecond(), JSONObjectUtils.getLong(jsonObject, "auth_time"));
		assertEquals("0", JSONObjectUtils.getString(jsonObject, "acr"));
		assertEquals(List.of("pwd"), JSONObjectUtils.getStringList(jsonObject, "amr"));
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "allow_refresh"));
		assertEquals("bob", JSONObjectUtils.getString(jsonObject, "impersonated_sub"));
		assertEquals(7, jsonObject.size());
		
		tokenSpec = IDTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		
		assertTrue(tokenSpec.issue());
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(authTime, tokenSpec.getAuthTime());
		assertEquals("0", tokenSpec.getACR().getValue());
		assertEquals("pwd", tokenSpec.getAMRList().get(0).getValue());
		assertEquals("bob", tokenSpec.getImpersonatedSubject().getValue());
	}


        @Test
        public void testParseMinimal()
		throws Exception {

		var jsonObject = new JSONObject();
		IDTokenSpec tokenSpec = IDTokenSpec.parse(jsonObject);
		assertFalse(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testParseEmptyImpersonatedSubject() {

		var jsonObject = new JSONObject();
		jsonObject.put("impersonated_sub", "");
		try {
			IDTokenSpec.parse(jsonObject);
			Assert.fail();
		} catch (ParseException e) {
			assertEquals("The value must not be null or empty string", e.getMessage());
		}
	}
}

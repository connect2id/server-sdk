package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSource;
import com.nimbusds.openid.connect.provider.spi.claims.CommonClaimsSource;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;


public class MockTokenEncoderContext extends MockTokenCodecContext implements TokenEncoderContext {
	
	
	@Override
	public OIDCClientInformation getOIDCClientInformation() {
		return null;
	}
	
	
	@Override
	public ClaimsSource getClaimsSource() {
		return null;
	}
	
	
	@Override
	public CommonClaimsSource getCommonClaimsSource() {
		return null;
	}
}

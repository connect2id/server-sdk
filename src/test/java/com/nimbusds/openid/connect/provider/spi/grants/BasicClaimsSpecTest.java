package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.assurance.IdentityTrustFramework;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;


/**
 * Tests the basic claims specification.
 */
public class BasicClaimsSpecTest {


        @Test
        public void testDefault()
		throws Exception {

		var spec = new BasicClaimsSpec();
		assertTrue(spec.getNames().isEmpty());
		assertNull(spec.getData());
		assertNull(spec.getPresetClaims().getPresetIDTokenClaims());
		assertNull(spec.getPresetClaims().getPresetUserInfoClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetIDTokenClaims());
		JSONObject jsonObject = spec.toJSONObject();
		assertTrue(jsonObject.isEmpty());
		spec = BasicClaimsSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertTrue(spec.getNames().isEmpty());
		assertNull(spec.getData());
		assertNull(spec.getPresetClaims().getPresetIDTokenClaims());
		assertNull(spec.getPresetClaims().getPresetUserInfoClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetIDTokenClaims());
	}


        @Test
        public void testMinimalConstructor()
		throws Exception {

		Set<String> names = new HashSet<>(Arrays.asList("sub", "email", "email_verified"));

		var spec = new BasicClaimsSpec(names);

		assertEquals(names, spec.getNames());
		assertNull(spec.getData());
		assertNull(spec.getPresetClaims().getPresetIDTokenClaims());
		assertNull(spec.getPresetClaims().getPresetUserInfoClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetUserInfoClaims());

		JSONObject jsonObject = spec.toJSONObject();
		assertEquals(1, jsonObject.size());

		spec = BasicClaimsSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));

		assertTrue(spec.getNames().containsAll(Arrays.asList("sub", "email", "email_verified")));
		assertEquals(3, spec.getNames().size());
		assertNull(spec.getData());
		assertNull(spec.getPresetClaims().getPresetIDTokenClaims());
		assertNull(spec.getPresetClaims().getPresetUserInfoClaims());
		assertNull(spec.getPresetIDTokenClaims());
		assertNull(spec.getPresetUserInfoClaims());
	}


        @Test
        public void testFullConstructor()
		throws Exception {

		Set<String> names = new HashSet<>(Arrays.asList("sub", "email", "email_verified"));
		JSONObject data = new JSONObject();
		data.put("trust_framework", IdentityTrustFramework.EIDAS_IAL_SUBSTANTIAL.getValue());
		JSONObject presetIDToken = new JSONObject();
		presetIDToken.put("ip", "192.168.0.1");
		JSONObject presetUserInfo = new JSONObject();
		presetUserInfo.put("admin", true);

		var spec = new BasicClaimsSpec(names, data, new PresetClaims(presetIDToken, presetUserInfo));

		assertEquals(names, spec.getNames());
		assertEquals(data, spec.getData());
		assertEquals(presetIDToken, spec.getPresetClaims().getPresetIDTokenClaims());
		assertEquals(presetUserInfo, spec.getPresetClaims().getPresetUserInfoClaims());
		assertEquals(presetIDToken, spec.getPresetIDTokenClaims());
		assertEquals(presetUserInfo, spec.getPresetUserInfoClaims());

		JSONObject jsonObject = spec.toJSONObject();

		spec = BasicClaimsSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));

		assertTrue(spec.getNames().containsAll(Arrays.asList("sub", "email", "email_verified")));
		assertEquals(3, spec.getNames().size());
		assertEquals(data, spec.getData());
		assertEquals("192.168.0.1", (String)spec.getPresetClaims().getPresetIDTokenClaims().get("ip"));
		assertEquals(1, spec.getPresetClaims().getPresetIDTokenClaims().size());
		assertEquals("192.168.0.1", (String) spec.getPresetIDTokenClaims().get("ip"));
		assertEquals(1, spec.getPresetIDTokenClaims().size());
		assertTrue((Boolean) spec.getPresetClaims().getPresetUserInfoClaims().get("admin"));
		assertEquals(1, spec.getPresetClaims().getPresetUserInfoClaims().size());
		assertTrue((Boolean) spec.getPresetUserInfoClaims().get("admin"));
		assertEquals(1, spec.getPresetUserInfoClaims().size());
	}


        @Test
        public void testEmptyPresetClaims()
		throws Exception {

		Set<String> names = new HashSet<>(Arrays.asList("sub", "email", "email_verified"));
		var spec = new BasicClaimsSpec(names, new PresetClaims(new JSONObject(), new JSONObject()));
		assertTrue(spec.getPresetClaims().isEmpty());
		assertTrue(spec.getPresetIDTokenClaims().isEmpty());
		assertTrue(spec.getPresetUserInfoClaims().isEmpty());

		String json = spec.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parse(json);
		assertTrue(jsonObject.containsKey("claims"));
		assertEquals(1, jsonObject.size());
	}
}

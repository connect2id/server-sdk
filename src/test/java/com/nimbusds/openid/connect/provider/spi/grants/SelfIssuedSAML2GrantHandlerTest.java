package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.assertions.saml2.SAML2AssertionDetails;
import com.nimbusds.oauth2.sdk.assertions.saml2.SAML2AssertionFactory;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.security.credential.BasicCredential;
import org.opensaml.security.credential.UsageType;
import org.opensaml.xmlsec.signature.support.SignatureConstants;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.*;


/**
 * Tests the self-issued SAML 2.0 assertion grant handler interface.
 */
public class SelfIssuedSAML2GrantHandlerTest {


	private static final Issuer ISSUER = new Issuer("https://c2id.com");

	private static final ClientID CLIENT_ID = new ClientID("123");

	private static final Subject SUBJECT = new Subject("alice");
	

	static class ExampleDeprecatedHandler_1 implements SelfIssuedSAML2GrantHandler {


		@Override
		public boolean isEnabled() {
			return true;

		}

		@Override
		public SelfIssuedAssertionAuthorization processSelfIssuedGrant(final Assertion assertion,
									       final TokenRequestParameters tokenRequestParams,
									       final ClientID clientID,
									       final OIDCClientMetadata clientMetadata,
									       final InvocationContext invocationCtx)
			throws GeneralException {
			
			assertEquals(ISSUER, invocationCtx.getIssuer());

			Scope allowedScopeValues = clientMetadata.getScope();

			if (CollectionUtils.isEmpty(allowedScopeValues)) {
				throw new GeneralException("No registered scopes for client " + clientID, OAuth2Error.SERVER_ERROR);
			}

			// Compose the authorised scope
			Scope authorizedScope = new Scope();

			if (CollectionUtils.isEmpty(tokenRequestParams.getScope())) {
				// If no requested scope default to registered
				authorizedScope.addAll(allowedScopeValues);
			} else {
				// Set to requested
				authorizedScope.addAll(tokenRequestParams.getScope());
				// and limit to registered values in client metadata
				authorizedScope.retainAll(allowedScopeValues);
			}

			return new SelfIssuedAssertionAuthorization(
				new Subject(assertion.getSubject().getNameID().getValue()),
				authorizedScope);
		}
	}
	

	static class ExampleDeprecatedHandler_2 implements SelfIssuedSAML2GrantHandler {


		@Override
		public boolean isEnabled() {
			return true;

		}

		@Override
		public SelfIssuedAssertionAuthorization processSelfIssuedGrant(final Assertion assertion,
									       final Scope scope,
									       final ClientID clientID,
									       final OIDCClientMetadata clientMetadata)
			throws GeneralException {

			Scope allowedScopeValues = clientMetadata.getScope();

			if (CollectionUtils.isEmpty(allowedScopeValues)) {
				throw new GeneralException("No registered scopes for client " + clientID, OAuth2Error.SERVER_ERROR);
			}

			// Compose the authorised scope
			Scope authorizedScope = new Scope();

			if (CollectionUtils.isEmpty(scope)) {
				// If no requested scope default to registered
				authorizedScope.addAll(allowedScopeValues);
			} else {
				// Set to requested
				authorizedScope.addAll(scope);
				// and limit to registered values in client metadata
				authorizedScope.retainAll(allowedScopeValues);
			}

			return new SelfIssuedAssertionAuthorization(
				new Subject(assertion.getSubject().getNameID().getValue()),
				authorizedScope);
		}
	}


        @Test
        public void testAuthorizationSuccess()
		throws Exception {

		var assertionDetails = new SAML2AssertionDetails(
			new Issuer(CLIENT_ID),
			SUBJECT,
			new Audience("https://c2id.com/token"));

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();

		var credential = new BasicCredential(keyPair.getPublic(), keyPair.getPrivate());
		credential.setUsageType(UsageType.SIGNING);

		Assertion assertion = SAML2AssertionFactory.create(
			assertionDetails,
			SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256,
			credential);

		for (SelfIssuedSAML2GrantHandler handler: Arrays.asList(new ExampleDeprecatedHandler_1(), new ExampleDeprecatedHandler_2())) {
			
			assertTrue(handler.isEnabled());
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.applyDefaults();
			clientMetadata.setScope(Scope.parse("read write"));
			
			SelfIssuedAssertionAuthorization authz = handler.processSelfIssuedGrant(
				assertion,
				new TokenRequestParameters() {
				},
				CLIENT_ID,
				clientMetadata,
				new GrantHandlerContext() {
					@Override
					public Set<String> resolveClaimNames(@Nullable Scope scope) {
						return OIDCScopeValue.resolveClaimNames(scope);
					}

					@Override
					public Issuer getIssuer() {
						return ISSUER;
					}
				});
			
			assertEquals(SUBJECT, authz.getSubject());
			assertEquals(clientMetadata.getScope(), authz.getScope());
			assertEquals(0L, authz.getAccessTokenSpec().getLifetime()); // implies default to server setting
			assertFalse(authz.getIDTokenSpec().issue());
			assertNull(authz.getData());
		}
	}


        @Test
        public void testServerErrorDueToNoRegisteredScopes()
		throws Exception {

		var assertionDetails = new SAML2AssertionDetails(
			new Issuer(CLIENT_ID),
			SUBJECT,
			new Audience("https://c2id.com/token"));

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		keyPairGenerator.initialize(2048);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();

		var credential = new BasicCredential(keyPair.getPublic(), keyPair.getPrivate());
		credential.setUsageType(UsageType.SIGNING);

		Assertion assertion = SAML2AssertionFactory.create(
			assertionDetails,
			SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA256,
			credential);
		
		for (SelfIssuedSAML2GrantHandler handler: Arrays.asList(new ExampleDeprecatedHandler_1(), new ExampleDeprecatedHandler_2())) {
			
			Assert.assertTrue(handler.isEnabled());
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.applyDefaults();
			
			try {
				handler.processSelfIssuedGrant(
					assertion,
					new TokenRequestParameters() {},
					CLIENT_ID,
					clientMetadata,
					new GrantHandlerContext() {
						@Override
						public Set<String> resolveClaimNames(@Nullable Scope scope) {
							return OIDCScopeValue.resolveClaimNames(scope);
						}

						@Override
						public Issuer getIssuer() {
							return new Issuer("https://c2id.com");
						}
					});
				fail();
			} catch (GeneralException e) {
				assertEquals(OAuth2Error.SERVER_ERROR.getCode(), e.getErrorObject().getCode());
			}
		}
	}
}

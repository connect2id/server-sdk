package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.oauth2.sdk.id.Identifier;
import org.junit.Test;

import static org.junit.Assert.*;


public class IdentifierAccessTokenTest {


        @Test
        public void testSimpleConstructor() {
		
		var identifier = new Identifier();
		var token = new IdentifierAccessToken(identifier);
		assertEquals(identifier, token.getIdentifier());
		assertEquals(identifier.getValue(), token.getTokenValue());
		assertEquals(identifier.getValue(), token.toString());
	}


        @Test
        public void testFullConstructor() {
		
		var identifier = new Identifier();
		String value = "id:" + identifier;
		var token = new IdentifierAccessToken(identifier, value);
		assertEquals(identifier, token.getIdentifier());
		assertEquals(value, token.getTokenValue());
		assertEquals(identifier.getValue(), token.toString());
	}


        @Test
        public void testEquality() {
		
		var identifier = new Identifier();
		
		assertTrue(new IdentifierAccessToken(identifier).equals(new IdentifierAccessToken(identifier)));
		
		assertTrue(new IdentifierAccessToken(identifier, "abc").equals(new IdentifierAccessToken(identifier, "def")));
	}


        @Test
        public void testInequality() {
		
		assertFalse(new IdentifierAccessToken(new Identifier("123")).equals(new IdentifierAccessToken(new Identifier("456"))));
		assertFalse(new IdentifierAccessToken(new Identifier("123"), "abc").equals(new IdentifierAccessToken(new Identifier("456"), "abc")));
	}


        @Test
        public void testHashCode() {
		
		var identifier = new Identifier();
		
		assertEquals(new IdentifierAccessToken(identifier).hashCode(), new IdentifierAccessToken(identifier).hashCode());
		assertEquals(new IdentifierAccessToken(identifier, "abc").hashCode(), new IdentifierAccessToken(identifier, "def").hashCode());
	}
}

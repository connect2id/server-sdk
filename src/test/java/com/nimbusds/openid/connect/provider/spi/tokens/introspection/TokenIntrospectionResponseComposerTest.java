package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.claims.MockAdvancedClaimsSource;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;
import com.nimbusds.openid.connect.provider.spi.tokens.MutableAccessTokenAuthorization;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;

import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.*;


public class TokenIntrospectionResponseComposerTest {
	
	
	private static OIDCClientInformation createClientInfo(final Scope scope) {
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setTokenEndpointAuthMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC);
		clientMetadata.setGrantTypes(Collections.emptySet());
		clientMetadata.setScope(scope);
		return new OIDCClientInformation(new ClientID(), new Date(), clientMetadata, new Secret());
	}


        @Test
        public void testShape_retainAll() {
		
		TokenIntrospectionResponseComposer shaper = new TokenScopeShaper();
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withScope(new Scope("read", "write"));
		
		TokenIntrospectionSuccessResponse out = shaper.compose(
			tokenAuthz,
			new MockTokenIntrospectionContext(
				new Issuer("https://c2id.com"),
				createClientInfo(new Scope("read", "write")),
				new MockAdvancedClaimsSource(),
				null)
		);
		
		assertTrue(out.isActive());
		assertEquals(tokenAuthz.getScope(), out.getScope());
	}


        @Test
        public void testShape_retainSome() {
		
		TokenIntrospectionResponseComposer shaper = new TokenScopeShaper();
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withScope(new Scope("read", "write"));
		
		TokenIntrospectionSuccessResponse out = shaper.compose(
			tokenAuthz,
			new MockTokenIntrospectionContext(
				new Issuer("https://c2id.com"),
				createClientInfo(new Scope("read", "write", "email")),
				new MockAdvancedClaimsSource(),
				null)
		);
		
		assertTrue(out.isActive());
		assertEquals(new Scope("read", "write"), out.getScope());
	}


        @Test
        public void testShape_retainNone() {
		
		TokenIntrospectionResponseComposer shaper = new TokenScopeShaper();
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withScope(new Scope("read", "write"));
		
		TokenIntrospectionSuccessResponse out = shaper.compose(
			tokenAuthz,
			new MockTokenIntrospectionContext(
				new Issuer("https://c2id.com"),
				createClientInfo(new Scope("email")),
				new MockAdvancedClaimsSource(),
				null)
		);
		
		assertTrue(out.isActive());
		assertTrue(out.getScope().isEmpty());
	}


        @Test
        public void testShape_noRegisteredScope() {
		
		TokenIntrospectionResponseComposer shaper = new TokenScopeShaper();
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withScope(new Scope("read", "write"));
		
		TokenIntrospectionSuccessResponse out = shaper.compose(
			tokenAuthz,
			new MockTokenIntrospectionContext(
				new Issuer("https://c2id.com"),
				createClientInfo(null),
				new MockAdvancedClaimsSource(),
				null)
		);
		
		assertTrue(out.isActive());
		assertTrue(out.getScope().isEmpty());
	}


        @Test
        public void testShape_emptyRegisteredScope() {
		
		TokenIntrospectionResponseComposer shaper = new TokenScopeShaper();
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withScope(new Scope("read", "write"));
		
		TokenIntrospectionSuccessResponse out = shaper.compose(
			tokenAuthz,
			new MockTokenIntrospectionContext(
				new Issuer("https://c2id.com"),
				createClientInfo(new Scope()),
				new MockAdvancedClaimsSource(),
				null)
		);
		
		assertTrue(out.isActive());
		assertTrue(out.getScope().isEmpty());
	}


        @Test
        public void testShape_invalidToken() {
		
		TokenIntrospectionResponseComposer shaper = new TokenScopeShaper();
		
		TokenIntrospectionSuccessResponse out = shaper.compose(
			null,
			new MockTokenIntrospectionContext(
				new Issuer("https://c2id.com"),
				createClientInfo(new Scope("read", "write")),
				new MockAdvancedClaimsSource(),
				null)
		);
		
		assertFalse(out.isActive());
		assertNull(out.getScope());
	}


        @Test
        public void testShape_invalidClient() {
		
		TokenIntrospectionResponseComposer shaper = new TokenScopeShaper();
		
		TokenIntrospectionSuccessResponse out = shaper.compose(
			null,
			new MockTokenIntrospectionContext(
				new Issuer("https://c2id.com"),
				null,
				new MockAdvancedClaimsSource(),
				null)
		);
		
		assertFalse(out.isActive());
		assertNull(out.getScope());
	}
}

package com.nimbusds.openid.connect.provider.spi.crypto;


import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.id.Issuer;


public class MockJWTSignerAndSignatureVerifier implements JWTSigner, JWSVerifier {
	
	
	public static final RSAKey RSA_JWK;
	
	
	public static final Issuer ISSUER = new Issuer("https://c2id.com");
	
	
	static {
		try {
			RSA_JWK = new RSAKeyGenerator(2048)
				.keyID("1")
				.algorithm(JWSAlgorithm.RS256)
				.generate();
		} catch (JOSEException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	public SignedJWT sign(final JWTClaimsSet jwtClaimsSet) {
		
		return sign(null, jwtClaimsSet);
	}
	
	
	@Override
	public SignedJWT sign(@Nullable JOSEObjectType typ, JWTClaimsSet jwtClaimsSet) {
		
		JWTClaimsSet sanitizedClaimsSet = new JWTClaimsSet.Builder(jwtClaimsSet)
			.issuer(ISSUER.getValue())
			.build();
		
		SignedJWT jwt = new SignedJWT(
			new JWSHeader.Builder((JWSAlgorithm)RSA_JWK.getAlgorithm())
				.type(typ)
				.keyID(RSA_JWK.getKeyID())
				.build(),
			sanitizedClaimsSet
		);
		
		try {
			jwt.sign(new RSASSASigner(RSA_JWK));
		} catch (JOSEException e) {
			throw new RuntimeException(e);
		}
		
		return jwt;
	}
	
	
	@Override
	public boolean verifySignature(final JWSObject jwsObject) {
		
		if (! jwsObject.getHeader().getAlgorithm().equals(RSA_JWK.getAlgorithm())) {
			return false;
		}
		
		String keyID = jwsObject.getHeader().getKeyID();
		
		if (! RSA_JWK.getKeyID().equals(keyID)) {
			return false;
		}
		
		try {
			return jwsObject.verify(new RSASSAVerifier(RSA_JWK));
		} catch (JOSEException e) {
			throw new RuntimeException(e);
		}
	}
}

package com.nimbusds.openid.connect.provider.spi.par;


import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class InvalidPushedAuthorizationRequestExceptionTest {


        @Test
        public void testMinimalConstructor() {
		
		String message = "message";
		
		var e = new InvalidPushedAuthorizationRequestException(message);
		assertEquals(message, e.getMessage());
		assertEquals(400, e.getErrorObject().getHTTPStatusCode());
		assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), e.getErrorObject().getCode());
		assertEquals(OAuth2Error.INVALID_REQUEST.getDescription(), e.getErrorObject().getDescription());
		assertNull(e.getErrorObject().getURI());
	}


        @Test
        public void testFullConstructor() {
		
		var message = "message";
		ErrorObject errorObject = OAuth2Error.INVALID_SCOPE.setDescription("Scope not accepted: Some scope");
		
		var e = new InvalidPushedAuthorizationRequestException(message, errorObject);
		assertEquals(message, e.getMessage());
		assertEquals(errorObject, e.getErrorObject());
		assertNull(e.getErrorObject().getURI());
	}


        @Test
        public void testNullMessage() {
		
		var e = new InvalidPushedAuthorizationRequestException(null);
		assertNull(e.getMessage());
		assertEquals(400, e.getErrorObject().getHTTPStatusCode());
		assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), e.getErrorObject().getCode());
		assertEquals(OAuth2Error.INVALID_REQUEST.getDescription(), e.getErrorObject().getDescription());
		assertNull(e.getErrorObject().getURI());
	}


        @Test(expected = NullPointerException.class)
        public void testIllegalArgumentExceptionOnNullErrorObject() {
		new InvalidPushedAuthorizationRequestException("Message", null);
	}
}

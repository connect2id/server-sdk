package com.nimbusds.openid.connect.provider.spi.tokens;


/**
 * For testing the base abstract codec class.
 */
class ConcreteBaseSelfContainedAccessTokenCodec extends BaseSelfContainedAccessTokenClaimsCodec {
}

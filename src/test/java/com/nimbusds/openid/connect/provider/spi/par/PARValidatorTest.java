package com.nimbusds.openid.connect.provider.spi.par;


import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.op.ReadOnlyOIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.*;


public class PARValidatorTest {
	
	
	private static final Issuer ISSUER = new Issuer("https://c2id.com");
	
	
	/**
	 * Sample PAR validator that checks if the received authorisation
	 * request scope values are present in the OAuth 2.0 client
	 * registration.
	 */
	static class LegacyScopeValidator implements PARValidator {
		
		
		@Override
		public void validate(final AuthorizationRequest authzRequest,
				     final ValidatorContext validatorCtx)
			throws GeneralException {
			
			assertEquals(ISSUER, validatorCtx.getIssuer());
			
			assertEquals(ISSUER, validatorCtx.getReadOnlyOIDCProviderMetadata().getIssuer());
			
			assertNotNull(validatorCtx.getRawRequest());
			
			OIDCClientInformation clientInfo = validatorCtx.getOIDCClientInformation();
			
			if (clientInfo.getMetadata().getScope() == null || ! clientInfo.getMetadata().getScope().containsAll(authzRequest.getScope())) {
				
				Scope unacceptedScope = new Scope(authzRequest.getScope());
				unacceptedScope.removeAll(clientInfo.getMetadata().getScope());
				
				String msg = "Scope not accepted: " + unacceptedScope;
				
				throw new GeneralException(
					msg, // will be logged
					OAuth2Error.INVALID_SCOPE.setHTTPStatusCode(400).setDescription(msg));
			}
		}
	}
	
	
	/**
	 * Sample PAR validator that checks if the received authorisation
	 * request scope values are present in the OAuth 2.0 client
	 * registration.
	 */
	static class ScopeValidator implements PARValidator {
		
		
		@Override
		public boolean isEnabled() {
			return true;
		}
		
		
		@Override
		public AuthorizationRequest validatePushedAuthorizationRequest(final AuthorizationRequest authzRequest,
									       final ValidatorContext validatorCtx)
			throws InvalidPushedAuthorizationRequestException {
			
			assertEquals(ISSUER, validatorCtx.getIssuer());
			
			assertEquals(ISSUER, validatorCtx.getReadOnlyOIDCProviderMetadata().getIssuer());
			
			OIDCClientInformation clientInfo = validatorCtx.getOIDCClientInformation();
			
			if (clientInfo.getMetadata().getScope() == null || ! clientInfo.getMetadata().getScope().containsAll(authzRequest.getScope())) {
				
				Scope unacceptedScope = new Scope(authzRequest.getScope());
				unacceptedScope.removeAll(clientInfo.getMetadata().getScope());
				
				String msg = "Scope not accepted: " + unacceptedScope;
				
				throw new InvalidPushedAuthorizationRequestException(
					msg, // will be logged
					OAuth2Error.INVALID_SCOPE.setHTTPStatusCode(400).setDescription(msg)
				);
			}
			
			return authzRequest;
		}
	}
	
	
	private static OIDCClientInformation createOIDCClientInformation() {
		var metadata = new OIDCClientMetadata();
		metadata.setRedirectionURI(URI.create("https://example.com/cb"));
		metadata.setScope(new Scope("openid", "email", "profile"));
		metadata.applyDefaults();
		return new OIDCClientInformation(
			new ClientID("123"),
			new Date(),
			metadata,
			new Secret());
	}


        @Test
        public void testUsage_pass() throws InvalidPushedAuthorizationRequestException {
		
		OIDCClientInformation clientInfo = createOIDCClientInformation();
		
		AuthorizationRequest authzRequest = new AuthenticationRequest.Builder(
			new ResponseType("code"),
			new Scope("openid", "email"),
			clientInfo.getID(),
			clientInfo.getMetadata().getRedirectionURI())
			.state(new State())
			.build();
		
		PARValidator validator = new ScopeValidator();
		assertTrue(validator.isEnabled());
		
		AuthorizationRequest validated = validator.validatePushedAuthorizationRequest(
			authzRequest,
			new ValidatorContext() {
				
				@Override
				public ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata() {
					return new OIDCProviderMetadata(
						ISSUER,
						Collections.singletonList(SubjectType.PUBLIC),
						URI.create(ISSUER + "/jwks.json"));
				}
				
				@Override
				public OIDCClientInformation getOIDCClientInformation() {
					return clientInfo;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(ClientID clientID) {
					return null;
				}

				@Override
				public Issuer getIssuer() {
				return ISSUER;
			}
				
				
				@Override
				public AuthorizationRequest getRawRequest() {
					return authzRequest;
				}
			}
		);
		
		assertEquals(validated, authzRequest);
	}


        @Test
        public void testUsage_fail() {
		
		OIDCClientInformation clientInfo = createOIDCClientInformation();
		
		AuthorizationRequest authzRequest = new AuthenticationRequest.Builder(
			new ResponseType("code"),
			new Scope("openid", "email", "address"),
			clientInfo.getID(),
			clientInfo.getMetadata().getRedirectionURI())
			.state(new State())
			.build();
		
		PARValidator validator = new ScopeValidator();
		assertTrue(validator.isEnabled());
		
		try {
			validator.validatePushedAuthorizationRequest(
				authzRequest,
				new ValidatorContext() {
					
					@Override
					public ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata() {
						return new OIDCProviderMetadata(
							ISSUER,
							Collections.singletonList(SubjectType.PUBLIC),
							URI.create(ISSUER + "/jwks.json"));
					}
					
					@Override
					public OIDCClientInformation getOIDCClientInformation() {
						return clientInfo;
					}


					@Override
					public @Nullable OIDCClientInformation getOIDCClientInformation(ClientID clientID) {
						return null;
					}

					@Override
					public Issuer getIssuer() {
					return ISSUER;
				}
					
					
					@Override
					public AuthorizationRequest getRawRequest() {
						return authzRequest;
					}
				}
			);
			Assert.fail();
		} catch (InvalidPushedAuthorizationRequestException e) {
			assertEquals("Scope not accepted: address", e.getMessage());
			assertEquals(400, e.getErrorObject().getHTTPStatusCode());
			assertEquals(OAuth2Error.INVALID_SCOPE, e.getErrorObject());
			assertEquals("Scope not accepted: address", e.getErrorObject().getDescription());
		}
	}


        @Test
        public void testLegacyUsage_pass() throws InvalidPushedAuthorizationRequestException {
		
		OIDCClientInformation clientInfo = createOIDCClientInformation();
		
		AuthorizationRequest authzRequest = new AuthenticationRequest.Builder(
			new ResponseType("code"),
			new Scope("openid", "email"),
			clientInfo.getID(),
			clientInfo.getMetadata().getRedirectionURI())
			.state(new State())
			.build();
		
		PARValidator validator = new LegacyScopeValidator();
		assertTrue(validator.isEnabled());
		
		AuthorizationRequest validated = validator.validatePushedAuthorizationRequest(
			authzRequest,
			new ValidatorContext() {
				
				@Override
				public ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata() {
					return new OIDCProviderMetadata(
						ISSUER,
						Collections.singletonList(SubjectType.PUBLIC),
						URI.create(ISSUER + "/jwks.json"));
				}
				
				@Override
				public OIDCClientInformation getOIDCClientInformation() {
					return clientInfo;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(ClientID clientID) {
					return null;
				}

				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
				
				
				@Override
				public AuthorizationRequest getRawRequest() {
					return authzRequest;
				}
			}
		);
		
		assertEquals(validated, authzRequest);
	}


        @Test
        public void testLegacyUsage_fail() {
		
		OIDCClientInformation clientInfo = createOIDCClientInformation();
		
		AuthorizationRequest authzRequest = new AuthenticationRequest.Builder(
			new ResponseType("code"),
			new Scope("openid", "email", "address"),
			clientInfo.getID(),
			clientInfo.getMetadata().getRedirectionURI())
			.state(new State())
			.build();
		
		PARValidator validator = new LegacyScopeValidator();
		assertTrue(validator.isEnabled());
		
		try {
			validator.validatePushedAuthorizationRequest(
				authzRequest,
				new ValidatorContext() {
					
					@Override
					public ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata() {
						return new OIDCProviderMetadata(
							ISSUER,
							Collections.singletonList(SubjectType.PUBLIC),
							URI.create(ISSUER + "/jwks.json"));
					}
					
					@Override
					public OIDCClientInformation getOIDCClientInformation() {
						return clientInfo;
					}


					@Override
					public @Nullable OIDCClientInformation getOIDCClientInformation(ClientID clientID) {
						return null;
					}

					@Override
					public Issuer getIssuer() {
						return ISSUER;
					}
					
					
					@Override
					public AuthorizationRequest getRawRequest() {
						return authzRequest;
					}
				}
			);
			Assert.fail();
		} catch (InvalidPushedAuthorizationRequestException e) {
			assertEquals("Scope not accepted: address", e.getMessage());
			assertEquals(400, e.getErrorObject().getHTTPStatusCode());
			assertEquals(OAuth2Error.INVALID_SCOPE, e.getErrorObject());
			assertEquals("Scope not accepted: address", e.getErrorObject().getDescription());
		}
	}
}

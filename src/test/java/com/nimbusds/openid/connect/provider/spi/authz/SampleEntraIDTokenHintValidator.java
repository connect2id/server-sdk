package com.nimbusds.openid.connect.provider.spi.authz;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.source.JWKSourceBuilder;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.DefaultJOSEObjectTypeVerifier;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTClaimsVerifier;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;

import java.net.URL;
import java.util.List;
import java.util.Set;


/**
 * Validator of Entra ID {@code id_token_hint} JWTs in requests for EAM.
 *
 * <p>See https://connect2id.com/products/server/docs/guides/microsoft-entra-eam-provider#id_token_hint_validation
 */
public class SampleEntraIDTokenHintValidator implements AuthorizationRequestValidator {


        private boolean enabled;

        private URL entraJWKSetURL;

        private ClientID expectedClientID;

        private Issuer expectedIssuer;

        private Audience expectedAudience;

        private ConfigurableJWTProcessor idTokenHintValidator;


        @Override
        public void init(final InitContext initContext)
                throws Exception {

                enabled = "true".equals(System.getProperty("op.entraID.eam.enable", "false"));

                if (! enabled) {
                        return;
                }

                entraJWKSetURL = new URL(System.getProperty("op.entraID.jwkSetURL"));
                expectedClientID = new ClientID(System.getProperty("op.entraID.eam.clientID"));
                expectedIssuer = new Issuer(System.getProperty("op.entraID.eam.idTokenHint.issuer"));
                expectedAudience = new Audience(System.getProperty("op.entraID.eam.idTokenHint.audience"));

                idTokenHintValidator = createIDTokenHintValidator();
        }


        private ConfigurableJWTProcessor createIDTokenHintValidator() {

                ConfigurableJWTProcessor processor = new DefaultJWTProcessor<>();

                // The id_token_hint from Entra ID has a "typ":"JWT" header
                processor.setJWSTypeVerifier(new DefaultJOSEObjectTypeVerifier(JOSEObjectType.JWT));

                // JWK set URL selector with caching support
                JWSKeySelector jwkSelector = new JWSVerificationKeySelector(
                        JWSAlgorithm.RS256,
                        JWKSourceBuilder.create(entraJWKSetURL)
                                .build());

                processor.setJWSKeySelector(jwkSelector);
                processor.setJWTClaimsSetVerifier(new DefaultJWTClaimsVerifier(
                        new JWTClaimsSet.Builder()
                                .issuer(expectedIssuer.getValue())
                                .audience(expectedAudience.getValue())
                                .build(),
                        // required JWT claims that must be present
                        Set.of("exp", "iat", "tid", "oid", "sub")
                ));

                return processor;
        }


        @Override
        public boolean isEnabled() {

                return enabled;
        }


        @Override
        public AuthorizationRequest validateAuthorizationRequest(final AuthorizationRequest authzRequest,
                                                                 final ValidatorContext validatorCtx)
                throws InvalidAuthorizationRequestException {

                if (! enabled) {
                        return authzRequest;
                }

                if (! (authzRequest instanceof AuthenticationRequest)) {
                        // Not an OpenID request
                        return authzRequest;
                }

                if (! expectedClientID.equals(authzRequest.getClientID())) {
                        // Not a request from Entra ID
                        return authzRequest;
                }

                var authRequest = (AuthenticationRequest)authzRequest;

                // The Entra id_token_hint parameter name is rewritten to
                // prevent its processing as regular id_token_hint
                List<String> param = authRequest.getCustomParameter("entra_id_token_hint");

                if (param == null || param.size() != 1) {
                        // Not an Entra ID EAM request with id_token_hint
                        return authzRequest;
                }

                String jwtString = param.get(0);

                JWTClaimsSet idTokenHintClaims;
                try {
                        idTokenHintClaims = idTokenHintValidator.process(jwtString, null);

                } catch (java.text.ParseException | BadJOSEException e) {

                        throw new InvalidAuthorizationRequestException(
                                "Invalid Entra ID id_token_hint: " + e.getMessage(), // WIll be logged by the c2id server
                                OAuth2Error.INVALID_REQUEST.setDescription("Invalid id_token_hint"),
                                false);

                } catch (JOSEException e) {
                        throw new RuntimeException("Internal error: " + e.getMessage(), e);
                }

                // Append selected id_token_hint claims to the request as top-level parameters
                AuthenticationRequest finalRequest;

                try {
                        finalRequest = new AuthenticationRequest.Builder(authRequest)
                                .customParameter("entra_iss", idTokenHintClaims.getIssuer())
                                .customParameter("entra_tid", idTokenHintClaims.getStringClaim("tid"))
                                .customParameter("entra_oid", idTokenHintClaims.getStringClaim("oid"))
                                .customParameter("entra_preferred_username", idTokenHintClaims.getStringClaim("preferred_username"))
                                .customParameter("entra_sub", idTokenHintClaims.getSubject())
                                .build();

                } catch (java.text.ParseException e) {
                        throw new InvalidAuthorizationRequestException(
                                "Invalid Entra ID id_token_hint claim: " + e.getMessage(),
                                OAuth2Error.INVALID_REQUEST.setDescription("Invalid id_token_hint"),
                                false);
                }

                return finalRequest;
        }
}

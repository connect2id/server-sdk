package com.nimbusds.openid.connect.provider.spi.crypto;


import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class JWTSignerTest {


        @Test
        public void testWithType() {
		
		JWTSigner signer = new MockJWTSignerAndSignatureVerifier();
		
		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
			.subject("alice")
			.build();
		
		var typ = new JOSEObjectType("token-introspection+jwt");
		
		SignedJWT jwt = signer.sign(typ, claimsSet);
		
		assertEquals(typ, jwt.getHeader().getType());
	}
}

package com.nimbusds.openid.connect.provider.spi.secrets;


import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ClientSecretStoreCodecTest {
	
	
	private final static Issuer ISSUER = new Issuer("https://c2id.com");
	
	
	private final static OIDCClientMetadata CLIENT_METADATA;
	
	
	static {
		CLIENT_METADATA = new OIDCClientMetadata();
		CLIENT_METADATA.applyDefaults();
	}
	
	
	private static final SecretCodecContext SECRET_CODEC_CONTEXT = new SecretCodecContext() {
		
		@Override
		public Issuer getIssuer() {
			return ISSUER;
		}
		
		@Override
		public JWKSet getIssuerJWKSet() {
			return new JWKSet();
		}
		
		@Override
		public OIDCClientMetadata getOIDCClientMetadata() {
			return CLIENT_METADATA;
		}
	};


        @Test
        public void testDefaultImplementation() {
		
		var codec = new ClientSecretStoreCodec() {};
		
		var secret = new Secret();
		
		String valueToStore = codec.encode(secret, SECRET_CODEC_CONTEXT);
		
		assertEquals(valueToStore, secret.getValue());
		
		assertEquals(secret, codec.decode(valueToStore, SECRET_CODEC_CONTEXT));
		
		assertEquals(valueToStore, codec.encodeImported(secret, SECRET_CODEC_CONTEXT));
	}
}

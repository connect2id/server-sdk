package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.assertEquals;


/**
 * Tests the self-issued JWT assertion grant handler interface.
 */
public class SelfIssuedJWTGrantHandlerTest {


	static class ExampleHandler implements SelfIssuedJWTGrantHandler {


		@Override
		public boolean isEnabled() {
			return true;
		}


		@Override
		public SelfIssuedAssertionAuthorization processSelfIssuedGrant(final JWTClaimsSet jwtClaimsSet,
									       final TokenRequestParameters tokenRequestParams,
									       final ClientID clientID,
									       final OIDCClientMetadata clientMetadata,
									       final InvocationContext invocationCtx)
			throws GeneralException {
			
			assertEquals(new Issuer("https://c2id.com"), invocationCtx.getIssuer());

			Scope allowedScopeValues = clientMetadata.getScope();

			if (CollectionUtils.isEmpty(allowedScopeValues)) {
				throw new GeneralException("No registered scopes for client " + clientID, OAuth2Error.SERVER_ERROR);
			}

			// Compose the authorised scope
			Scope authorizedScope = new Scope();

			if (CollectionUtils.isEmpty(tokenRequestParams.getScope())) {
				// If no requested scope default to registered
				authorizedScope.addAll(allowedScopeValues);
			} else {
				// Set to requested
				authorizedScope.addAll(tokenRequestParams.getScope());
				// and limit to registered values in client metadata
				authorizedScope.retainAll(allowedScopeValues);
			}

			return new SelfIssuedAssertionAuthorization(new Subject(jwtClaimsSet.getSubject()), authorizedScope);
		}
	}


	static class ExampleDeprecatedHandler implements SelfIssuedJWTGrantHandler {


		@Override
		public boolean isEnabled() {
			return true;
		}


		@Override
		public SelfIssuedAssertionAuthorization processSelfIssuedGrant(final JWTClaimsSet jwtClaimsSet,
									       final Scope scope,
									       final ClientID clientID,
									       final OIDCClientMetadata clientMetadata)
			throws GeneralException {

			Scope allowedScopeValues = clientMetadata.getScope();

			if (CollectionUtils.isEmpty(allowedScopeValues)) {
				throw new GeneralException("No registered scopes for client " + clientID, OAuth2Error.SERVER_ERROR);
			}

			// Compose the authorised scope
			Scope authorizedScope = new Scope();

			if (CollectionUtils.isEmpty(scope)) {
				// If no requested scope default to registered
				authorizedScope.addAll(allowedScopeValues);
			} else {
				// Set to requested
				authorizedScope.addAll(scope);
				// and limit to registered values in client metadata
				authorizedScope.retainAll(allowedScopeValues);
			}

			return new SelfIssuedAssertionAuthorization(new Subject(jwtClaimsSet.getSubject()), authorizedScope);
		}
	}


        @Test
        public void testAuthorizationSuccess()
		throws Exception {

		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
			.issuer("123")
			.audience("https://c2id.com/token")
			.subject("alice")
			.expirationTime(new Date(new Date().getTime() + 5*60* 1000L))
			.build();

		Scope scope = Scope.parse("read");

		ClientID clientID = new ClientID("123");

		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();
		clientMetadata.applyDefaults();
		clientMetadata.setScope(Scope.parse("read write"));

		for (SelfIssuedJWTGrantHandler handler: Arrays.asList(new ExampleHandler(), new ExampleDeprecatedHandler())) {
			
			SelfIssuedAssertionAuthorization authz = handler.processSelfIssuedGrant(
				claimsSet,
				new TokenRequestParameters() {
					@Override
					public @Nullable Scope getScope() {
						return scope;
					}
				},
				clientID,
				clientMetadata,
				() -> new Issuer("https://c2id.com"));
			
			assertEquals("alice", authz.getSubject().getValue());
			assertEquals(scope, authz.getScope());
			assertEquals(0L, authz.getAccessTokenSpec().getLifetime()); // implies default to server setting
			Assert.assertFalse(authz.getIDTokenSpec().issue());
			Assert.assertNull(authz.getData());
		}
	}


        @Test
        public void testDefaultToRegisteredScope()
		throws Exception {

		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
			.issuer("123")
			.audience("https://c2id.com/token")
			.subject("alice")
			.expirationTime(new Date(new Date().getTime() + 5*60* 1000L))
			.build();

		ClientID clientID = new ClientID("123");

		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();
		clientMetadata.applyDefaults();
		clientMetadata.setScope(Scope.parse("read write"));
		
		for (SelfIssuedJWTGrantHandler handler: Arrays.asList(new ExampleHandler(), new ExampleDeprecatedHandler())) {
			
			SelfIssuedAssertionAuthorization authz = handler.processSelfIssuedGrant(
				claimsSet,
				new TokenRequestParameters() {},
				clientID,
				clientMetadata,
				() -> new Issuer("https://c2id.com"));
			
			assertEquals("alice", authz.getSubject().getValue());
			assertEquals(clientMetadata.getScope(), authz.getScope());
			assertEquals(0L, authz.getAccessTokenSpec().getLifetime()); // implies default to server setting
			Assert.assertFalse(authz.getIDTokenSpec().issue());
			Assert.assertNull(authz.getData());
		}
	}


        @Test
        public void testServerErrorDueToNoRegisteredScopes() {

		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
			.issuer("123")
			.audience("https://c2id.com/token")
			.subject("alice")
			.expirationTime(new Date(new Date().getTime() + 5*60* 1000L))
			.build();
		
		ClientID clientID = new ClientID("123");

		OIDCClientMetadata clientMetadata = new OIDCClientMetadata();
		clientMetadata.applyDefaults();

		
		for (SelfIssuedJWTGrantHandler handler: Arrays.asList(new ExampleHandler(), new ExampleDeprecatedHandler())) {
			
			try {
				handler.processSelfIssuedGrant(
					claimsSet,
					new TokenRequestParameters() {},
					clientID,
					clientMetadata,
					() -> new Issuer("https://c2id.com"));
				Assert.fail();
			} catch (GeneralException e) {
				assertEquals(OAuth2Error.SERVER_ERROR.getCode(), e.getErrorObject().getCode());
			}
			
		}
	}
}

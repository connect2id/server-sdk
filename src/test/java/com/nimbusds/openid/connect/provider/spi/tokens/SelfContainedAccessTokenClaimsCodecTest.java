package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.jwt.JWTClaimsSet;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class SelfContainedAccessTokenClaimsCodecTest {


        @Test
        public void testMinimalImpl() throws TokenDecodeException {
		
		var codec = new SelfContainedAccessTokenClaimsCodec() {
			
			
			@Override
			public JWTClaimsSet encode(AccessTokenAuthorization tokenAuthz, TokenEncoderContext context) {
				return new JWTClaimsSet.Builder().build();
			}
			
			
			@Override
			public AccessTokenAuthorization decode(JWTClaimsSet claimsSet, TokenCodecContext context) throws TokenDecodeException {
				return new MutableAccessTokenAuthorization();
			}
		};
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization();
		
		JWTClaimsSet jwtClaimsSet = codec.encode(tokenAuthz, new MockTokenEncoderContext());
		
		assertTrue(jwtClaimsSet.getClaims().isEmpty());
		
		assertNotNull(codec.decode(jwtClaimsSet, new MockTokenCodecContext()));
	}
}

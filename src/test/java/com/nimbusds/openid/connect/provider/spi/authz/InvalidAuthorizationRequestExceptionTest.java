package com.nimbusds.openid.connect.provider.spi.authz;


import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class InvalidAuthorizationRequestExceptionTest {


        @Test
        public void testMinimalConstructor() {
		
		String message = "message";
		
		InvalidAuthorizationRequestException e = new InvalidAuthorizationRequestException(message);
		assertEquals(message, e.getMessage());
		assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), e.getErrorObject().getCode());
		assertEquals(OAuth2Error.INVALID_REQUEST.getDescription(), e.getErrorObject().getDescription());
		assertNull(e.getErrorObject().getURI());
		assertFalse(e.isRedirectDisabled());
	}


        @Test
        public void testFullConstructor() {
		
		String message = "message";
		ErrorObject errorObject = OAuth2Error.INVALID_SCOPE.setDescription("Scope not accepted: Some scope");
		
		InvalidAuthorizationRequestException e = new InvalidAuthorizationRequestException(message, errorObject, true);
		assertEquals(message, e.getMessage());
		assertEquals(errorObject, e.getErrorObject());
		assertNull(e.getErrorObject().getURI());
		Assert.assertTrue(e.isRedirectDisabled());
	}


        @Test
        public void testNullMessage() {
		
		InvalidAuthorizationRequestException e = new InvalidAuthorizationRequestException(null);
		assertNull(e.getMessage());
		assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), e.getErrorObject().getCode());
		assertEquals(OAuth2Error.INVALID_REQUEST.getDescription(), e.getErrorObject().getDescription());
		assertNull(e.getErrorObject().getURI());
		assertFalse(e.isRedirectDisabled());
	}


        @Test(expected = NullPointerException.class)
        public void testIllegalArgumentExceptionOnNullErrorObject() {
		new InvalidAuthorizationRequestException("Message", null, false);
	}
}

package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;


/**
 * Tests the password grant handler interface.
 */
public class PasswordGrantHandlerTest {


	private static final Issuer ISSUER = new Issuer("https://c2id.com");

	private static final ClientID CLIENT_ID = new ClientID("123");

	private static final Subject SUBJECT = new Subject("alice");

	private static final Secret SECRET = new Secret();


        @Test
        public void testGrantTypeConstant() {

		assertEquals(GrantType.PASSWORD, PasswordGrantHandler.GRANT_TYPE);
	}


	static class ExampleDeprecatedHandler_1 implements PasswordGrantHandler {


		@Override
		public void init(final InitContext initContext)
			throws Exception {
			
			assertNotNull(initContext);
		}


		@Override
		public boolean isEnabled() {

			return true;
		}


		@Override
		public PasswordGrantAuthorization processGrant(final ResourceOwnerPasswordCredentialsGrant grant,
							       final TokenRequestParameters tokenRequestParams,
							       final ClientID clientID,
							       final boolean confidentialClient,
							       final OIDCClientMetadata clientMetadata,
							       final InvocationContext invocationCtx)
			throws GeneralException {
			
			assertEquals(ISSUER, invocationCtx.getIssuer());

			if (grant.getUsername().equals(SUBJECT.getValue()) && grant.getPassword().equals(SECRET)) {
				// Good credentials
			} else {
				// Bad credentials
				throw new GeneralException("Invalid user credentials", OAuth2Error.INVALID_GRANT);
			}

			Scope allowedScopeValuesForAlice = Scope.parse("openid email profile offline_access");

			Scope requestedScope = tokenRequestParams.getScope();

			if (requestedScope == null) {
				requestedScope = allowedScopeValuesForAlice;
			} else {
				requestedScope.retainAll(allowedScopeValuesForAlice);
			}

			return new PasswordGrantAuthorization(SUBJECT, requestedScope);
		}
	}


	static class ExampleDeprecatedHandler_2 implements PasswordGrantHandler {


		@Override
		public void init(final InitContext initContext)
			throws Exception {
			
			assertNotNull(initContext);
		}


		@Override
		public boolean isEnabled() {

			return true;
		}


		@Override
		public PasswordGrantAuthorization processGrant(final ResourceOwnerPasswordCredentialsGrant grant,
							       final Scope scope,
							       final ClientID clientID,
							       final boolean confidentialClient,
							       final OIDCClientMetadata clientMetadata)
			throws GeneralException {

			if (grant.getUsername().equals(SUBJECT.getValue()) && grant.getPassword().equals(SECRET)) {
				// Good credentials
			} else {
				// Bad credentials
				throw new GeneralException("Invalid user credentials", OAuth2Error.INVALID_GRANT);
			}

			Scope allowedScopeValuesForAlice = Scope.parse("openid email profile offline_access");

			Scope requestedScope = scope;

			if (requestedScope == null) {
				requestedScope = allowedScopeValuesForAlice;
			} else {
				requestedScope.retainAll(allowedScopeValuesForAlice);
			}

			return new PasswordGrantAuthorization(SUBJECT, requestedScope);
		}
	}


        @Test
        public void testExampleHandlers()
		throws Exception {

		for (PasswordGrantHandler handler: Arrays.asList(new ExampleDeprecatedHandler_1(), new ExampleDeprecatedHandler_2())) {
			
			handler.init(mock(InitContext.class));
			assertEquals(GrantType.PASSWORD, handler.getGrantType());
			
			var grant = new ResourceOwnerPasswordCredentialsGrant(SUBJECT.getValue(), SECRET);

			var clientMetadata = new OIDCClientMetadata();
			
			PasswordGrantAuthorization authz = handler.processGrant(
				grant,
				new TokenRequestParameters() {
					@Override
					public Scope getScope() {
						return new Scope("openid", "email", "admin");
					}
				},
				CLIENT_ID,
				true,
				clientMetadata,
				new GrantHandlerContext() {
					@Override
					public Set<String> resolveClaimNames(@Nullable Scope scope) {
						return OIDCScopeValue.resolveClaimNames(scope);
					}

					@Override
					public Issuer getIssuer() {
						return ISSUER;
					}
				});
			
			assertEquals(SUBJECT, authz.getSubject());
			assertTrue(Scope.parse("openid email").containsAll(authz.getScope()));
			assertEquals(2, authz.getScope().size());
			assertNull(authz.getAudience());
			assertFalse(authz.isLongLived());
			assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
			assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
			assertFalse(authz.getAccessTokenSpec().encrypt());
			assertFalse(authz.getRefreshTokenSpec().issue());
			assertEquals(-1L, authz.getRefreshTokenSpec().getLifetime());
			assertNull(authz.getData());
		}
	}
}

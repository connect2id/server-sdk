package com.nimbusds.openid.connect.provider.spi.tokens.response;


import com.nimbusds.oauth2.sdk.AccessTokenResponse;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.TokenErrorResponse;
import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;


public class RARTokenResponseComposer implements CustomTokenResponseComposer {
	
	
	@Override
	public TokenResponse compose(final TokenResponse originalResponse, final TokenResponseContext context) {
		
		if (originalResponse instanceof TokenErrorResponse) {
			return originalResponse;
		}
		
		JSONObject authzData = context.getAuthorizationData();
		
		if (authzData == null) {
			// No data / RAR
			return originalResponse;
		}
		
		JSONArray rarDetails;
		try {
			rarDetails = JSONObjectUtils.getJSONArray(authzData, "rar_details", null);
		} catch (ParseException e) {
			throw new RuntimeException("Internal error: " + e.getMessage(), e);
		}
		
		if (rarDetails == null) {
			// No RAR
			return originalResponse;
		}
		
		AccessTokenResponse successResponse = originalResponse.toSuccessResponse();
		
		try {
			JSONObject tokensObject = successResponse.toJSONObject();
			tokensObject.put("authorization_details", rarDetails);
			return AccessTokenResponse.parse(tokensObject);
		} catch (ParseException e) {
			throw new RuntimeException("Internal error: " + e.getMessage(), e);
		}
	}
}

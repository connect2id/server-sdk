package com.nimbusds.openid.connect.provider.spi.events;


import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import com.nimbusds.openid.connect.provider.spi.crypto.MockJWTSignerAndSignatureVerifier;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class AccessTokenIssueEventTest {
	
	
	static class Listener implements AccessTokenIssueEventListener {
		
		
		AccessTokenIssueEvent lastEvent;
		
		
		@Override
		public void accessTokenIssued(AccessTokenIssueEvent event, EventContext eventContext) {
		
			lastEvent = event;
			
			assertEquals(MockJWTSignerAndSignatureVerifier.ISSUER, eventContext.getIssuer());
			assertNotNull(eventContext.getOIDCClientInformation());
			assertNotNull(eventContext.getJWTSigner());
		}
	}
	
	
	private static JWTClaimsSet generateTokenClaims() {
		
		return new JWTClaimsSet.Builder()
			.issuer("https://c2id.com")
			.subject("alice")
			.claim("scp", Arrays.asList("read", "write"))
			.expirationTime(Date.from(Instant.now().plus(1, ChronoUnit.HOURS)))
			.build();
	}


        @Test
        public void testCreate() {
		
		JWTClaimsSet jwtClaimsSet = generateTokenClaims();
		
		var source = new Object();
		
		var event = new AccessTokenIssueEvent(source, jwtClaimsSet);
		
		assertEquals(source, event.getSource());
		assertEquals(jwtClaimsSet, event.getJWTClaimsSet());
	}


        @Test
        public void testListen() {
		
		JWTClaimsSet jwtClaimsSet = generateTokenClaims();
		
		var source = new Object();
		
		var event = new AccessTokenIssueEvent(source, jwtClaimsSet);
		
		var clientID = new ClientID("123");
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.CLIENT_CREDENTIALS));
		clientMetadata.setTokenEndpointAuthMethod(ClientAuthenticationMethod.TLS_CLIENT_AUTH);
		clientMetadata.applyDefaults();
		var clientInfo = new OIDCClientInformation(clientID, clientMetadata);
		
		var listener = new Listener();
		listener.accessTokenIssued(event, new EventContext() {

			@Override
			public Issuer getIssuer() {
				return MockJWTSignerAndSignatureVerifier.ISSUER;
			}

			@Override
			public OIDCClientInformation getOIDCClientInformation() {
				return clientInfo;
			}


			@Override
			public JWTSigner getJWTSigner() {
				return new MockJWTSignerAndSignatureVerifier();
			}

		});
		
		assertEquals(source, listener.lastEvent.getSource());
		assertEquals(jwtClaimsSet, listener.lastEvent.getJWTClaimsSet());
	}
}

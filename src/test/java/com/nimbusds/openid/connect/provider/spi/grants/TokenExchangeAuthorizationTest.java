package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagException;
import com.nimbusds.langtag.LangTagUtils;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.token.TokenTypeURI;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;


public class TokenExchangeAuthorizationTest {

	
	private static final Subject SUBJECT = new Subject("alice");
	
	private static final Scope SCOPE = new Scope("read", "write");

        @Test
        public void testMinimal() throws ParseException {
		
		var accessTokenSpec = new AccessTokenSpec(
			60L,
			new Audience("https://rs1.example.com").toSingleAudienceList(),
			TokenEncoding.SELF_CONTAINED,
			null,
			Optional.of(false),
			SubjectType.PAIRWISE
		);
		
		var claimsSpec = new ClaimsSpec();

		var authorization = new TokenExchangeAuthorization(
			SUBJECT,
			SCOPE,
			accessTokenSpec,
			claimsSpec,
			null);
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(SUBJECT, authorization.getSubject());
		assertEquals(SCOPE, authorization.getScope());
		assertFalse(authorization.isLongLived());
		assertEquals(accessTokenSpec, authorization.getAccessTokenSpec());
		assertFalse(authorization.getRefreshTokenSpec().issue());
		assertFalse(authorization.getIDTokenSpec().issue());
		assertEquals(claimsSpec, authorization.getClaimsSpec());
		assertNull(authorization.getData());
		
		JSONObject jsonObject = authorization.toJSONObject();
		assertEquals(TokenTypeURI.ACCESS_TOKEN.toString(), jsonObject.get("issued_token_type"));
		assertEquals(SUBJECT.getValue(), jsonObject.get("sub"));
		assertEquals(SCOPE.toStringList(), jsonObject.get("scope"));
		assertFalse(JSONObjectUtils.getBoolean(jsonObject, "long_lived"));
		assertEquals(Audience.toStringList(accessTokenSpec.getAudience()), jsonObject.get("audience"));
		assertEquals(accessTokenSpec.toJSONObject(), JSONObjectUtils.getJSONObject(jsonObject, "access_token"));
		assertEquals(ClaimsTransport.USERINFO.toString(), jsonObject.get("claims_transport"));
		assertEquals(7, jsonObject.size());
		
		authorization = TokenExchangeAuthorization.parse(jsonObject.toJSONString());
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(SUBJECT, authorization.getSubject());
		assertEquals(SCOPE, authorization.getScope());
		assertFalse(authorization.isLongLived());
		assertEquals(accessTokenSpec.toJSONObject(), authorization.getAccessTokenSpec().toJSONObject());
		assertFalse(authorization.getRefreshTokenSpec().issue());
		assertFalse(authorization.getIDTokenSpec().issue());
		assertEquals(claimsSpec.toJSONObject(), authorization.getClaimsSpec().toJSONObject());
		assertNull(authorization.getData());
	}


        @Test
        public void testWithOptionalRefreshToken() throws ParseException {
		
		var accessTokenSpec = new AccessTokenSpec(
			60L,
			new Audience("https://rs1.example.com").toSingleAudienceList(),
			TokenEncoding.SELF_CONTAINED,
			null,
			Optional.of(false),
			SubjectType.PAIRWISE
		);
		
		var refreshTokenSpec = new RefreshTokenSpec(true, 3600 * 24 * 30);
		
		var claimsSpec = new ClaimsSpec();
		
		var authorization = new TokenExchangeAuthorization(
			SUBJECT,
			SCOPE,
			true,
			accessTokenSpec,
			refreshTokenSpec,
			claimsSpec,
			null);
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(SUBJECT, authorization.getSubject());
		assertEquals(SCOPE, authorization.getScope());
		assertTrue(authorization.isLongLived());
		assertEquals(accessTokenSpec, authorization.getAccessTokenSpec());
		assertEquals(refreshTokenSpec, authorization.getRefreshTokenSpec());
		assertFalse(authorization.getIDTokenSpec().issue());
		assertEquals(claimsSpec, authorization.getClaimsSpec());
		assertNull(authorization.getData());
		
		JSONObject jsonObject = authorization.toJSONObject();
		assertEquals(TokenTypeURI.ACCESS_TOKEN.toString(), jsonObject.get("issued_token_type"));
		assertEquals(SUBJECT.getValue(), jsonObject.get("sub"));
		assertEquals(SCOPE.toStringList(), jsonObject.get("scope"));
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "long_lived"));
		assertEquals(Audience.toStringList(accessTokenSpec.getAudience()), jsonObject.get("audience"));
		assertEquals(accessTokenSpec.toJSONObject(), JSONObjectUtils.getJSONObject(jsonObject, "access_token"));
		assertEquals(refreshTokenSpec.toJSONObject(), JSONObjectUtils.getJSONObject(jsonObject, "refresh_token"));
		assertEquals(ClaimsTransport.USERINFO.toString(), jsonObject.get("claims_transport"));
		assertEquals(8, jsonObject.size());
		
		authorization = TokenExchangeAuthorization.parse(jsonObject.toJSONString());
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(SUBJECT, authorization.getSubject());
		assertEquals(SCOPE, authorization.getScope());
		assertTrue(authorization.isLongLived());
		assertEquals(accessTokenSpec.toJSONObject(), authorization.getAccessTokenSpec().toJSONObject());
		assertEquals(authorization.getRefreshTokenSpec().toJSONObject(), authorization.getRefreshTokenSpec().toJSONObject());
		assertFalse(authorization.getIDTokenSpec().issue());
		assertEquals(claimsSpec.toJSONObject(), authorization.getClaimsSpec().toJSONObject());
		assertNull(authorization.getData());
		
		// long_lived defaults to false
		jsonObject.remove("long_lived");
		
		authorization = TokenExchangeAuthorization.parse(jsonObject.toJSONString());
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(SUBJECT, authorization.getSubject());
		assertEquals(SCOPE, authorization.getScope());
		assertFalse(authorization.isLongLived());
		assertEquals(accessTokenSpec.toJSONObject(), authorization.getAccessTokenSpec().toJSONObject());
		assertEquals(authorization.getRefreshTokenSpec().toJSONObject(), authorization.getRefreshTokenSpec().toJSONObject());
		assertFalse(authorization.getIDTokenSpec().issue());
		assertEquals(claimsSpec.toJSONObject(), authorization.getClaimsSpec().toJSONObject());
		assertNull(authorization.getData());
	}


        @Test
        public void testAllParams() throws ParseException, LangTagException {
		
		var accessTokenSpec = new AccessTokenSpec(
			60L,
			new Audience("https://rs1.example.com").toSingleAudienceList(),
			TokenEncoding.SELF_CONTAINED,
			new Subject("bob"),
			Optional.of(true),
			SubjectType.PAIRWISE
		);
		
		var refreshTokenSpec = new RefreshTokenSpec(true, 3600 * 24 * 30);
		
		var now = new Date();
		
		var idTokenSpec = new IDTokenSpec(
			true,
			120L,
			now,
			new ACR("https://loa.c2id.com/high"),
			null,
			null
		);
		
		var claimsSpec = new ClaimsSpec(
			new HashSet<>(Arrays.asList("email", "email_verified", "name")),
			Arrays.asList(LangTag.parse("en"), LangTag.parse("bg")),
			null,
			null,
			null,
			ClaimsTransport.USERINFO
		);
		
		var data = new JSONObject();
		data.put("xyz", "123");
		
		var authorization = new TokenExchangeAuthorization(
			SUBJECT,
			SCOPE,
			false,
			accessTokenSpec,
			refreshTokenSpec,
			idTokenSpec,
			claimsSpec,
			data);
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(SUBJECT, authorization.getSubject());
		assertEquals(SCOPE, authorization.getScope());
		assertFalse(authorization.isLongLived());
		assertEquals(accessTokenSpec, authorization.getAccessTokenSpec());
		assertEquals(refreshTokenSpec, authorization.getRefreshTokenSpec());
		assertEquals(idTokenSpec, authorization.getIDTokenSpec());
		assertEquals(claimsSpec, authorization.getClaimsSpec());
		assertEquals(data, authorization.getData());
		
		JSONObject jsonObject = authorization.toJSONObject();
		assertEquals(TokenTypeURI.ACCESS_TOKEN.toString(), jsonObject.get("issued_token_type"));
		assertEquals(SUBJECT.getValue(), jsonObject.get("sub"));
		assertEquals(SCOPE.toStringList(), jsonObject.get("scope"));
		assertFalse(JSONObjectUtils.getBoolean(jsonObject, "long_lived"));
		assertEquals(Audience.toStringList(accessTokenSpec.getAudience()), jsonObject.get("audience"));
		assertEquals(accessTokenSpec.toJSONObject(), JSONObjectUtils.getJSONObject(jsonObject, "access_token"));
		assertEquals(refreshTokenSpec.toJSONObject(), JSONObjectUtils.getJSONObject(jsonObject, "refresh_token"));
		
		JSONObject idTokenObject = JSONObjectUtils.getJSONObject(jsonObject, "id_token");
		assertTrue(JSONObjectUtils.getBoolean(idTokenObject, "issue"));
		assertEquals(idTokenSpec.getLifetime(), JSONObjectUtils.getLong(idTokenObject, "lifetime"));
		assertEquals(2, idTokenObject.size());
		assertEquals(DateUtils.toSecondsSinceEpoch(idTokenSpec.getAuthTime()), JSONObjectUtils.getLong(jsonObject, "auth_time"));
		assertEquals(idTokenSpec.getACR().getValue(), JSONObjectUtils.getString(jsonObject, "acr"));
		
		assertEquals(ClaimsTransport.USERINFO.toString(), jsonObject.get("claims_transport"));
		assertEquals(claimsSpec.getNames(), JSONObjectUtils.getStringSet(jsonObject, "claims"));
		assertEquals(LangTagUtils.toStringList(claimsSpec.getLocales()), JSONObjectUtils.getStringList(jsonObject, "claims_locales"));
		assertEquals(data, jsonObject.get("data"));
		assertEquals(14, jsonObject.size());
		
		authorization = TokenExchangeAuthorization.parse(jsonObject.toJSONString());
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(SUBJECT, authorization.getSubject());
		assertEquals(SCOPE, authorization.getScope());
		assertFalse(authorization.isLongLived());
		assertEquals(accessTokenSpec.toJSONObject(), authorization.getAccessTokenSpec().toJSONObject());
		assertEquals(refreshTokenSpec.toJSONObject(), authorization.getRefreshTokenSpec().toJSONObject());
		assertEquals(idTokenSpec.toJSONObject(), authorization.getIDTokenSpec().toJSONObject());
		assertEquals(claimsSpec.toJSONObject(), authorization.getClaimsSpec().toJSONObject());
		assertEquals(data, authorization.getData());
	}


        @Test
        public void testRejectIssuedTokenTypeOtherThanAccessToken() {
		
		var jsonObject = new JSONObject();
		jsonObject.put("issued_token_type", TokenTypeURI.JWT.toString());
		
		try {
			TokenExchangeAuthorization.parse(jsonObject.toJSONString());
			Assert.fail();
		} catch (ParseException e) {
			assertEquals("The issued_token_type must be urn:ietf:params:oauth:token-type:access_token", e.getMessage());
		}
	}


	@Test
	public void testWebSSOAuthorizationUseCase() throws ParseException {

		var authorization = new TokenExchangeAuthorization(
			new Subject("alice"),
			new Scope("web_sso"),
			new AccessTokenSpec(
				60,
				Audience.create("https://login.c2id.com", "rp123"),
				TokenEncoding.IDENTIFIER,
				Optional.empty()
			),
			new ClaimsSpec(),
			null
		);

		JSONObject jsonObject = authorization.toJSONObject();

		assertEquals(TokenTypeURI.ACCESS_TOKEN.toString(), jsonObject.get("issued_token_type"));
		assertEquals(authorization.getSubject().getValue(), jsonObject.get("sub"));
		assertEquals(authorization.getScope().toStringList(), jsonObject.get("scope"));
		assertFalse((Boolean) jsonObject.get("long_lived"));

		var accessTokenSpec = (JSONObject) jsonObject.get("access_token");
		assertEquals(authorization.getAccessTokenSpec().getLifetime(), accessTokenSpec.get("lifetime"));
		assertEquals(Audience.toStringList(authorization.getAccessTokenSpec().getAudience()), accessTokenSpec.get("audience"));
		assertEquals(TokenEncoding.IDENTIFIER.toString(), accessTokenSpec.get("encoding"));
		assertEquals(SubjectType.PUBLIC.name(), accessTokenSpec.get("sub_type"));
		assertEquals(4, accessTokenSpec.size());

		assertEquals(ClaimsTransport.USERINFO.toString(), jsonObject.get("claims_transport"));

		// TODO deprecated
		assertEquals(Audience.toStringList(authorization.getAccessTokenSpec().getAudience()), jsonObject.get("audience"));

		assertEquals(7, jsonObject.size());

		assertEquals(authorization.toJSONObject(), TokenExchangeAuthorization.parse(jsonObject.toJSONString()).toJSONObject());
	}


	@Test
	public void testWebSSOAuthorizationUseCase_parseMinimalExample() throws ParseException {

		String json =
			"""
			{
			  "sub": "alice",
			  "issued_token_type": "urn:ietf:params:oauth:token-type:access_token",
			  "scope": [ "web_sso" ],
			  "access_token": {
			    "lifetime": 60,
			    "audience": [
			      "rp123",
			      "https://login.c2id.com"
			    ],
			    "encoding": "IDENTIFIER"
			  }
			}
			""";

		TokenExchangeAuthorization authorization = TokenExchangeAuthorization.parse(json);
		assertEquals(new Subject("alice"), authorization.getSubject());
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authorization.getTokenType());
		assertEquals(new Scope("web_sso"), authorization.getScope());
		assertEquals(60L, authorization.getAccessTokenSpec().getLifetime());
		assertEquals(Audience.create("rp123", "https://login.c2id.com"), authorization.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, authorization.getAccessTokenSpec().getEncoding());
		assertEquals(SubjectType.PUBLIC, authorization.getAccessTokenSpec().getSubjectType());
		assertFalse(authorization.getAccessTokenSpec().getEncryptSelfContained().isPresent());
		assertTrue(authorization.getClaimsSpec().getNames().isEmpty());
		assertEquals(ClaimsTransport.USERINFO, authorization.getClaimsSpec().getTransport());
	}
}

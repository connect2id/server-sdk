package com.nimbusds.openid.connect.provider.spi.internal.sessionstore;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class SubjectSessionIDTest {


        record ImmutableSubjectSessionID(String value) implements SubjectSessionID {

                @Override
                public String getValue() {
                        return value;
                }
        }


        @Test
        public void testGetValue() {

                String value = "WYqFXK7Q4HFnJv0hiT3Fgw.-oVkvSXgalUuMQDfEsh1lw";

                var sid = new ImmutableSubjectSessionID(value);

                assertEquals(value, sid.getValue());
        }
}

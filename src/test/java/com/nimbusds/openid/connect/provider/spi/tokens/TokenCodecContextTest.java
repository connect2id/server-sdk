package com.nimbusds.openid.connect.provider.spi.tokens;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TokenCodecContextTest {

        @Test
        public void testCodecPrefix() {
		
		assertEquals("authzStore.accessToken.codec.", TokenEncoderContext.CODEC_PROPERTIES_PREFIX);
	}
}

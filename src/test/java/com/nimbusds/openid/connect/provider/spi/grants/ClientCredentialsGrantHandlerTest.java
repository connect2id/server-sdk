package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Test;

import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;


/**
 * Tests the client credentials grant handler interface.
 */
public class ClientCredentialsGrantHandlerTest {


	private static final Issuer ISSUER = new Issuer("https://c2id.com");

	private static final ClientID CLIENT_ID = new ClientID("123");


        @Test
        public void testGrantTypeConstant() {

		assertEquals(GrantType.CLIENT_CREDENTIALS, ClientCredentialsGrantHandler.GRANT_TYPE);
	}


	static class ExampleDeprecatedHandler_1 implements ClientCredentialsGrantHandler {


		@Override
		public void init(final InitContext initContext)
			throws Exception {
			
			assertNotNull(initContext);
		}


		@Override
		public boolean isEnabled() {

			return true;
		}


		@Override
		public GrantAuthorization processGrant(final TokenRequestParameters tokenRequestParams,
						       final ClientID clientID,
						       final ClientMetadata clientMetadata,
						       final InvocationContext invocationCtx)
			throws GeneralException {

			assertEquals(ISSUER, invocationCtx.getIssuer());
			
			Scope allowedScopeValues = clientMetadata.getScope();

			if (CollectionUtils.isEmpty(allowedScopeValues)) {
				throw new GeneralException("No registered scopes for client " + clientID, OAuth2Error.SERVER_ERROR);
			}

			// Compose the authorised scope
			Scope authorizedScope = new Scope();

			if (CollectionUtils.isEmpty(tokenRequestParams.getScope())) {
				// If no requested scope default to registered
				authorizedScope.addAll(allowedScopeValues);
			} else {
				// Set to requested
				authorizedScope.addAll(tokenRequestParams.getScope());
				// and limit to registered values in client metadata
				authorizedScope.retainAll(allowedScopeValues);
			}

			return new GrantAuthorization(
				authorizedScope,
				null, // audience
				new AccessTokenSpec(600L, TokenEncoding.SELF_CONTAINED, false),
				null // data
			);
		}
	}


	static class ExampleDeprecatedHandler_2 implements ClientCredentialsGrantHandler {


		@Override
		public void init(final InitContext initContext)
			throws Exception {
			
			assertNotNull(initContext);
		}


		@Override
		public boolean isEnabled() {

			return true;
		}


		@Override
		public GrantAuthorization processGrant(final Scope scope,
						       final ClientID clientID,
						       final ClientMetadata clientMetadata)
			throws GeneralException {

			Scope allowedScopeValues = clientMetadata.getScope();

			if (CollectionUtils.isEmpty(allowedScopeValues)) {
				throw new GeneralException("No registered scopes for client " + clientID, OAuth2Error.SERVER_ERROR);
			}

			// Compose the authorised scope
			var authorizedScope = new Scope();

			if (CollectionUtils.isEmpty(scope)) {
				// If no requested scope default to registered
				authorizedScope.addAll(allowedScopeValues);
			} else {
				// Set to requested
				authorizedScope.addAll(scope);
				// and limit to registered values in client metadata
				authorizedScope.retainAll(allowedScopeValues);
			}

			return new GrantAuthorization(
				authorizedScope,
				null, // audience
				new AccessTokenSpec(600L, TokenEncoding.SELF_CONTAINED, false),
				null // data
			);
		}
	}


        @Test
        public void testExampleHandlers()
		throws Exception {

		for (ClientCredentialsGrantHandler handler: Arrays.asList(new ExampleDeprecatedHandler_1(), new ExampleDeprecatedHandler_2())) {
			handler.init(mock(InitContext.class));
			assertEquals(GrantType.CLIENT_CREDENTIALS, handler.getGrantType());

			var clientMetadata = new ClientMetadata();
			clientMetadata.setScope(new Scope("read", "write"));
			
			GrantAuthorization authz = handler.processGrant(
				new TokenRequestParameters() {
					@Override
					public Scope getScope() {
						return new Scope("read", "write", "admin");
					}
				},
				CLIENT_ID,
				clientMetadata,
				new GrantHandlerContext() {
					@Override
					public Set<String> resolveClaimNames(@Nullable Scope scope) {
						return OIDCScopeValue.resolveClaimNames(scope);
					}

					@Override
					public Issuer getIssuer() {
						return ISSUER;
					}
				});
			
			assertTrue(Scope.parse("read write").containsAll(authz.getScope()));
			assertEquals(2, authz.getScope().size());
			assertNull(authz.getAudience());
			assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
			assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
			assertFalse(authz.getAccessTokenSpec().encrypt());
			assertNull(authz.getData());
		}
	}
}


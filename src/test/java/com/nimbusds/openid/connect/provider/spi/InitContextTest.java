package com.nimbusds.openid.connect.provider.spi;


import jakarta.servlet.ServletContext;
import org.infinispan.manager.DefaultCacheManager;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Tests the init context interface.
 */
public class InitContextTest {


        @Test
        public void testMock() {

		InitContext initContext = mock(InitContext.class);
		when(initContext.getServletContext()).thenReturn(mock(ServletContext.class));
		when(initContext.getInfinispanCacheManager()).thenReturn(new DefaultCacheManager());
		
		assertTrue(initContext.getServletContext() != null);
		assertTrue(initContext.getInfinispanCacheManager() != null);
	}
}

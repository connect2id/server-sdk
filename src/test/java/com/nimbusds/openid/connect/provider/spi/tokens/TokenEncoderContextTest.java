package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSource;
import com.nimbusds.openid.connect.provider.spi.crypto.HMACComputer;
import com.nimbusds.openid.connect.provider.spi.crypto.JWSVerifier;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;

import java.net.URI;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class TokenEncoderContextTest {
	
	
	private static final Subject SUBJECT = new Subject("alice");
	
	private static final String EMAIL_ADDRESS = "alice@example.com";

	private static final ClientID CLIENT_ID = new ClientID("123");
	
	private static final OIDCClientInformation CLIENT_INFO;
	
	static {
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://rp.example.com"));
		clientMetadata.applyDefaults();
		CLIENT_INFO = new OIDCClientInformation(CLIENT_ID, clientMetadata);
	}


        @Test
        public void testGetCommonClaimsSourceDefault() throws Exception {
		
		var context = new TokenEncoderContext() {
			
			
			@Override
			public OIDCClientInformation getOIDCClientInformation() {
				return CLIENT_INFO;
			}
			
			
			@Override
			public ClaimsSource getClaimsSource() {
				return new ClaimsSource() {
					@Override
					public UserInfo getClaims(Subject subject, Set<String> claims, List<LangTag> claimsLocales) {
						
						if (SUBJECT.equals(subject)) {
							UserInfo userInfo = new UserInfo(SUBJECT);
							if (claims.contains("email")) {
								userInfo.setEmailAddress(EMAIL_ADDRESS);
							}
							return userInfo;
						}
						
						return null;
					}
					
					
					@Override
					public Set<String> supportedClaims() {
						return Collections.singleton("email");
					}
				};
			}
			
			
			@Override
			public Issuer getIssuer() {
				return null;
			}
			
			
			@Override
			public SecureRandom getSecureRandom() {
				return null;
			}
			
			
			@Override
			public JWTSigner getJWTSigner() {
				return null;
			}
			
			
			@Override
			public JWSVerifier getJWSVerifier() {
				return null;
			}
			
			
			@Override
			public HMACComputer getHMACComputer() {
				return null;
			}
			
			
			@Override
			public Properties getCodecProperties() {
				return null;
			}


			@Override
			public ClaimNamesCompressor getClaimNamesCompressor() {
				return new NoOpClaimNamesCompressor();
			}
		};
		
		assertEquals(CLIENT_INFO, context.getOIDCClientInformation());
		
		assertEquals(Collections.singleton("email"), context.getCommonClaimsSource().supportedClaims());

		assertNull(context.getClaimNamesCompressor().compress(null));
		assertEquals(Collections.emptyList(), context.getClaimNamesCompressor().compress(Collections.emptySet()));
		assertEquals(List.of("email"), context.getClaimNamesCompressor().compress(Set.of("email")));

		assertNull(context.getClaimNamesCompressor().decompress(null));
		assertEquals(Collections.emptyList(), context.getClaimNamesCompressor().decompress(Collections.emptyList()));
		assertEquals(List.of("email"), context.getClaimNamesCompressor().decompress(List.of("email")));

		UserInfo userInfo = ((ClaimsSource)context.getCommonClaimsSource()).getClaims(SUBJECT, Collections.singleton("email"), null);
		
		assertEquals(SUBJECT, userInfo.getSubject());
		assertEquals(EMAIL_ADDRESS, userInfo.getEmailAddress());
	}
}

package com.nimbusds.openid.connect.provider.spi.clientauth;


import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.ClientSecretBasic;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class ClientAuthenticationInterceptorTest {
	
	
	private static final Issuer ISSUER = new Issuer("https://c2id.com");
	
	private static final ClientID CLIENT_ID = new ClientID("123");
	
	
	private ClientAuthenticationContext ctx;
	
	
	private OIDCClientInformation clientInfo;
	
	
	static class ClientAuthenticationCounter implements ClientAuthenticationInterceptor {
		
		private final AtomicInteger successCounter = new AtomicInteger();
		
		private final AtomicInteger errorCounter = new AtomicInteger();
		
		
		public AtomicInteger getSuccessCounter() {
			return successCounter;
		}
		
		
		public AtomicInteger getErrorCounter() {
			return errorCounter;
		}
		
		
		@Override
		public boolean isEnabled() {
			return true;
		}
		
		
		@Override
		public void interceptSuccess(ClientAuthentication clientAuth,
					     ClientAuthenticationContext ctx) {
			
			assertEquals(ISSUER, ctx.getIssuer());
			assertEquals(CLIENT_ID, ctx.getOIDCClientInformation().getID());
			assertNotNull(ctx.getID());
			
			successCounter.incrementAndGet();
		}
		
		
		@Override
		public void interceptError(ClientAuthentication clientAuth,
					   InvalidClientException exception,
					   ClientAuthenticationContext ctx) {
			
			assertEquals(CLIENT_ID, ctx.getOIDCClientInformation().getID());
			
			errorCounter.incrementAndGet();
		}
	}
	
	
	static class DenySuccessfulClientAuthentication implements ClientAuthenticationInterceptor {
		
		
		@Override
		public boolean isEnabled() {
			return true;
		}
		
		
		@Override
		public void interceptSuccess(ClientAuthentication clientAuth,
					     ClientAuthenticationContext ctx)
			throws InvalidClientException {
			
			throw new InvalidClientException("Client authentication rejected");
		}
	}
	
	
	@Before
	public void setUp() {

		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://example.com/cb"));
		clientMetadata.applyDefaults();
		clientInfo = new OIDCClientInformation(
			CLIENT_ID,
			new Date(),
			clientMetadata,
			new Secret());
		
		ctx = new ClientAuthenticationContext() {
			@Override
			public OIDCClientInformation getOIDCClientInformation() {
				return clientInfo;
			}
			
			
			@Override
			public ClientAuthenticationID getID() {
				return () -> UUID.randomUUID().toString();
			}
			
			
			@Override
			public Issuer getIssuer() {
				return ISSUER;
			}
		};
	}


        @Test
        public void testIntercept_authCounterPlugin() {
		
		ClientAuthenticationCounter clientAuthCounter = new ClientAuthenticationCounter();
		
		assertEquals(0, clientAuthCounter.getSuccessCounter().get());
		assertEquals(0, clientAuthCounter.getErrorCounter().get());
		
		clientAuthCounter.interceptSuccess(
			new ClientSecretBasic(clientInfo.getID(), clientInfo.getSecret()),
			ctx
		);
		
		assertEquals(1, clientAuthCounter.getSuccessCounter().get());
		assertEquals(0, clientAuthCounter.getErrorCounter().get());
		
		clientAuthCounter.interceptError(
			new ClientSecretBasic(clientInfo.getID(), clientInfo.getSecret()),
			new InvalidClientException("Bad client secret"),
			ctx
		);
		
		assertEquals(1, clientAuthCounter.getSuccessCounter().get());
		assertEquals(1, clientAuthCounter.getErrorCounter().get());
	}


        @Test
        public void testIntercept_denyClientAuthSuccess() {
		
		DenySuccessfulClientAuthentication denySuccess = new DenySuccessfulClientAuthentication();
		
		try {
			denySuccess.interceptSuccess(
				new ClientSecretBasic(clientInfo.getID(), clientInfo.getSecret()),
				ctx
			);
			Assert.fail();
		} catch (InvalidClientException e) {
			assertEquals("Client authentication rejected", e.getMessage());
			assertEquals(OAuth2Error.INVALID_CLIENT.getCode(), e.getErrorObject().getCode());
			assertEquals(OAuth2Error.INVALID_CLIENT.getDescription(), e.getErrorObject().getDescription());
		}
	}
}

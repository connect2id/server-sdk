package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.Scope;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.util.*;

import static org.junit.Assert.*;


public class DefaultTokenRequestParametersTest {


        @Test
        public void testMinimal() {
		
		var params = new DefaultTokenRequestParameters(null, null, Collections.emptyMap());
		
		assertNull(params.getScope());
		assertNull(params.getResources());
		assertTrue(params.getCustomParameters().isEmpty());
	}


        @Test
        public void testScopeConstructor_null() {
		
		var params = new DefaultTokenRequestParameters(null);
		assertNull(params.getScope());
		assertNull(params.getResources());
		assertTrue(params.getCustomParameters().isEmpty());
	}


        @Test
        public void testScopeConstructor() {
		
		var scope = new Scope("read", "write");
		var params = new DefaultTokenRequestParameters(scope);
		assertEquals(scope, params.getScope());
		assertNull(params.getResources());
		assertTrue(params.getCustomParameters().isEmpty());
	}


        @Test
        public void testAllSet() {

		var scope = new Scope("read", "write");
		List<URI> resources = Arrays.asList(URI.create("https://api-1.example.com"), URI.create("https://api-2.example.com"));
		Map<String,List<String>> customParams = new HashMap<>();
		customParams.put("x-param", Collections.singletonList("x-value"));
		
		var params = new DefaultTokenRequestParameters(scope, resources, customParams);
		
		assertEquals(scope, params.getScope());
		assertEquals(resources, params.getResources());
		assertEquals(customParams, params.getCustomParameters());
	}


        @Test
        public void testRejectNullCustomParams() {
		
		try {
			new DefaultTokenRequestParameters(null, null, null);
			Assert.fail();
		} catch (NullPointerException e) {
			assertNull(e.getMessage());
		}
	}
}

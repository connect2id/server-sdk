package com.nimbusds.openid.connect.provider.spi.secrets;


import at.favre.lib.crypto.bcrypt.BCrypt;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Sample BCrypt codec test.
 */
public class BCryptSecretCodecTest {
	
	
	private final static Issuer ISSUER = new Issuer("https://c2id.com");
	
	
	private final static JWKSet JWK_SET = new JWKSet();
	
	
	private final static OIDCClientMetadata CLIENT_METADATA;
	
	
	static {
		CLIENT_METADATA = new OIDCClientMetadata();
		CLIENT_METADATA.applyDefaults();
	}
	
	
	private static final SecretCodecContext SECRET_CODEC_CONTEXT = new SecretCodecContext() {
		
		@Override
		public Issuer getIssuer() {
			return ISSUER;
		}
		
		@Override
		public JWKSet getIssuerJWKSet() {
			return JWK_SET;
		}
		
		@Override
		public OIDCClientMetadata getOIDCClientMetadata() {
			return CLIENT_METADATA;
		}
	};
	
	
	private static final ClientSecretStoreCodec BCRYPT_SECRET_CODEC = new ClientSecretStoreCodec() {
		
		@Override
		public String encode(final Secret secret, final SecretCodecContext ctx) {
			
			assertEquals(ISSUER, ctx.getIssuer());
			assertEquals(JWK_SET, ctx.getIssuerJWKSet());
			assertEquals(CLIENT_METADATA, ctx.getOIDCClientMetadata());
			
			return BCrypt.withDefaults().hashToString(4, secret.getValue().toCharArray());
		}
		
		
		@Override
		public DecodedSecret decode(final String storedValue, final SecretCodecContext ctx) {
			
			assertEquals(ISSUER, ctx.getIssuer());
			assertEquals(JWK_SET, ctx.getIssuerJWKSet());
			assertEquals(CLIENT_METADATA, ctx.getOIDCClientMetadata());
			
			return DecodedSecret.createForHashedSecret(
				otherSecret -> {
					BCrypt.Result result = BCrypt.verifyer().verify(otherSecret.getValue().toCharArray(), storedValue.toCharArray());
					return result.verified;
				}
			);
		}
	};


        @Test
        public void testLifeCycle() {
	
		var secret = new Secret("me4ieb0poh6Boh9n");
		
		String valueToStore = BCRYPT_SECRET_CODEC.encode(secret, SECRET_CODEC_CONTEXT);
		
//		System.out.println("Secret value to store: " + valueToStore);
		
		assertTrue(valueToStore.startsWith("$2a$04$"));
		
		DecodedSecret decoded = BCRYPT_SECRET_CODEC.decode(valueToStore, SECRET_CODEC_CONTEXT);
		
		assertNull("Cannot be decoded", decoded.getValue());
		assertNull("Cannot be decoded", decoded.getValueBytes());
		
		assertTrue("Verifies", decoded.equals(secret));
		
		assertFalse("Doesn't verify", decoded.equals(new Secret("zie4nieweChohcoh")));
	}


        @Test
        public void testImportLifeCycle() {
	
		var importedSecret = new Secret("$2a$04$uHeeUt1sX7Ja.gaF7HL20OA9FD.IUeQAryCi1616JP5maCa5mvL1G");
		
		String valueToStore = BCRYPT_SECRET_CODEC.encodeImported(importedSecret, SECRET_CODEC_CONTEXT);
		
		assertEquals(importedSecret.getValue(), valueToStore);
		
		DecodedSecret decoded = BCRYPT_SECRET_CODEC.decode(valueToStore, SECRET_CODEC_CONTEXT);
		
		assertNull("Cannot be decoded", decoded.getValue());
		assertNull("Cannot be decoded", decoded.getValueBytes());
		
		assertTrue("Verifies", decoded.equals(new Secret("me4ieb0poh6Boh9n")));
		
		assertFalse("Doesn't verify", decoded.equals(new Secret("zie4nieweChohcoh")));
	}
}

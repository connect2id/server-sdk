package com.nimbusds.openid.connect.provider.spi.reg;


import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.assertEquals;


public class FinalMetadataValidatorTest {
	
	
	private static final Issuer ISSUER = new Issuer("https://c2id.com");
	
	
	private static final OIDCClientMetadata RECEIVED_METADATA;
	
	
	private static final OIDCClientMetadata PROVISIONED_METADATA;
	
	
	static {
		RECEIVED_METADATA = new OIDCClientMetadata();
		RECEIVED_METADATA.setRedirectionURI(URI.create("https://example.com/cb"));
		
		PROVISIONED_METADATA = new OIDCClientMetadata(RECEIVED_METADATA);
		PROVISIONED_METADATA.applyDefaults();
	}


        @Test
        public void testPassThroughValidator() throws InvalidRegistrationException {
		
		FinalMetadataValidator validator = new FinalMetadataValidator() {
			@Override
			public OIDCClientMetadata validate(final OIDCClientMetadata metadata, final ValidatorContext validatorCtx) {
				
				assertEquals(AuthorizationCredentialType.MASTER_TOKEN, validatorCtx.getAuthorizationCredentialType());
				assertEquals(RECEIVED_METADATA, validatorCtx.getReceivedMetadata());
				assertEquals(ISSUER, validatorCtx.getIssuer());
				
				return metadata;
			}
			
			
			@Override
			public void init(InitContext initContext) {
			
			}
			
			
			@Override
			public boolean isEnabled() {
				return true;
			}
			
			
			@Override
			public void shutdown() {
			
			}
		};
		
		ValidatorContext validatorCtx = new ValidatorContext() {
			@Override
			public AuthorizationCredentialType getAuthorizationCredentialType() {
				return AuthorizationCredentialType.MASTER_TOKEN;
			}
			
			
			@Override
			public OIDCClientMetadata getReceivedMetadata() {
				return RECEIVED_METADATA;
			}
			
			
			@Override
			public Issuer getIssuer() {
				return ISSUER;
			}
		};
		
		assertEquals(PROVISIONED_METADATA, validator.validate(PROVISIONED_METADATA, validatorCtx));
	}
}

package com.nimbusds.openid.connect.provider.spi.events;

import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import com.nimbusds.openid.connect.provider.spi.crypto.MockJWTSignerAndSignatureVerifier;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectAuthentication;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import com.nimbusds.openid.connect.sdk.claims.IDTokenClaimsSet;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class SubjectAuthAndConsentEventTest {


        static class Listener implements SubjectAuthAndConsentEventListener {


                SubjectAuthAndConsentEvent lastEvent;

                @Override
                public void subjectAuthenticatedAndConsented(SubjectAuthAndConsentEvent event, EventContext eventContext) {

                        lastEvent = event;

                        assertEquals(ISSUER, eventContext.getIssuer());
                        assertNotNull(eventContext.getOIDCClientInformation());
                        assertNotNull(eventContext.getJWTSigner());
                }
        }


        private static final Issuer ISSUER = new Issuer("https://c2id.com");
        private static final Subject SUBJECT = new Subject("alice");
        private static final ClientID CLIENT_ID = new ClientID("123");
        private static final IDTokenClaimsSet ID_TOKEN_CLAIMS_SET;
        private static final Scope SCOPE = new Scope("openid", "email");
        private static final Set<String> CLAIM_NAMES = Set.of("email", "email_verified");
        private static final JSONObject AUTHZ_DATA = new JSONObject();
        private static final SubjectSession SUBJECT_SESSION = new SubjectSession() {
                @Override
                public Subject getSubject() {
                        return SUBJECT;
                }

                @Override
                public SubjectAuthentication getSubjectAuthentication() {
                        return new SubjectAuthentication() {
                                @Override
                                public Subject getSubject() {
                                        return SUBJECT;
                                }

                                @Override
                                public Instant getTime() {
                                        return Instant.now();
                                }

                                @Override
                                public @Nullable ACR getACR() {
                                        return new ACR("platinum");
                                }

                                @Override
                                public @Nullable List<AMR> getAMRList() {
                                        return List.of(AMR.FACE, AMR.OTP, AMR.MFA);
                                }
                        };
                }

                @Override
                public Instant getCreationTime() {
                        return Instant.now();
                }

                @Override
                public long getMaxLifetime() {
                        return 24 * 60;
                }

                @Override
                public long getAuthLifetime() {
                        return 24 * 60;
                }

                @Override
                public long getMaxIdleTime() {
                        return 8 * 60;
                }

                @Override
                public Set<ClientID> getRelyingParties() {
                        return Set.of(CLIENT_ID);
                }

                @Override
                public @Nullable JSONObject getClaims() {
                        return null;
                }

                @Override
                public @Nullable JSONObject getData() {
                        return null;
                }
        };

        static {
                ID_TOKEN_CLAIMS_SET = new IDTokenClaimsSet(
                        ISSUER,
                        SUBJECT,
                        new Audience(CLIENT_ID).toSingleAudienceList(),
                        Date.from(Instant.now().plus(5, ChronoUnit.MINUTES)),
                        Date.from(Instant.now())
                );
                AUTHZ_DATA.put("consent_id", UUID.randomUUID().toString());
        }

        @Test
        public void testCreateMinimal() {

                var source = new Object();

                var event = new SubjectAuthAndConsentEvent(
                        source,
                        SUBJECT,
                        CLIENT_ID,
                        null,
                        null,
                        null,
                        null,
                        null
                );

                assertEquals(source, event.getSource());
                assertEquals(SUBJECT, event.getSubject());
                assertEquals(CLIENT_ID, event.getClientID());
                Assert.assertNull(event.getIDTokenClaimsSet());
                Assert.assertNull(event.getScope());
                Assert.assertNull(event.getClaimNames());
                Assert.assertNull(event.getAuthorizationData());
                Assert.assertNull(event.getSubjectSession());
        }

        @Test
        public void testCreateAllSet() {

                var source = new Object();

                var event = new SubjectAuthAndConsentEvent(
                        source,
                        SUBJECT,
                        CLIENT_ID,
                        ID_TOKEN_CLAIMS_SET,
                        SCOPE,
                        CLAIM_NAMES,
                        AUTHZ_DATA,
                        SUBJECT_SESSION
                );

                assertEquals(source, event.getSource());
                assertEquals(SUBJECT, event.getSubject());
                assertEquals(CLIENT_ID, event.getClientID());
                assertEquals(ID_TOKEN_CLAIMS_SET, event.getIDTokenClaimsSet());
                assertEquals(SCOPE, event.getScope());
                assertEquals(CLAIM_NAMES, event.getClaimNames());
                assertEquals(AUTHZ_DATA, event.getAuthorizationData());
                assertEquals(SUBJECT_SESSION, event.getSubjectSession());
        }


        @Test
        public void testListen() {

                var source = new Object();

                var event = new SubjectAuthAndConsentEvent(
                        source,
                        SUBJECT,
                        CLIENT_ID,
                        ID_TOKEN_CLAIMS_SET,
                        SCOPE,
                        CLAIM_NAMES,
                        AUTHZ_DATA,
                        SUBJECT_SESSION
                );

                var clientMetadata = new OIDCClientMetadata();
                clientMetadata.setRedirectionURI(URI.create("https://rp.example.com"));
                clientMetadata.applyDefaults();
                var clientInfo = new OIDCClientInformation(CLIENT_ID, clientMetadata);

                var listener = new Listener();

                listener.subjectAuthenticatedAndConsented(
                        event,
                        new EventContext() {
                                @Override
                                public Issuer getIssuer() {
                                        return ISSUER;
                                }

                                @Override
                                public OIDCClientInformation getOIDCClientInformation() {
                                        return clientInfo;
                                }

                                @Override
                                public JWTSigner getJWTSigner() {
                                        return new MockJWTSignerAndSignatureVerifier();
                                }
                        }
                );

                assertEquals(event, listener.lastEvent);
        }
}

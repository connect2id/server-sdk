package com.nimbusds.openid.connect.provider.spi.tokens;


import java.text.ParseException;
import java.util.Map;

import net.jcip.annotations.ThreadSafe;
import net.minidev.json.JSONObject;

import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.id.Identifier;


/**
 * Identifier-based access token codec which may be useful in multi-AS setups.
 * The access token is a minimal signed JWT which includes the following
 * claims:
 *
 * <ul>
 *     <li>iss - the token issuer
 *     <li>jti - the token identifier (or key)
 *     <li>cnf.x5t#S256 - client X.509 certificate thumbprint if mutual TLS is
 *         used
 * </ul>
 */
@ThreadSafe
public class HybridIdentifierAccessTokenCodec implements IdentifierAccessTokenCodec {
	
	
	@Override
	public IdentifierAccessToken generate(final AccessTokenAuthorization tokenAuthz, final TokenEncoderContext context) {
		
		// Generate 256 bit ID
		byte[] idBytes = new byte[32];
		context.getSecureRandom().nextBytes(idBytes);
		
		// Encode the ID bytes
		Identifier id = new Identifier(Base64URL.encode(idBytes).toString());
		
		JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder()
			.issuer(context.getIssuer().getValue())
			.jwtID(id.getValue());
		
		if (tokenAuthz.getClientCertificateConfirmation() != null) {
			Map.Entry<String,JSONObject> cnfClaim = tokenAuthz.getClientCertificateConfirmation().toJWTClaim();
			builder.claim(cnfClaim.getKey(), cnfClaim.getValue());
		}
		
		JWTClaimsSet jwtClaimsSet = builder.build();
		
		SignedJWT jwt = context.getJWTSigner().sign(jwtClaimsSet);
		
		return new IdentifierAccessToken(id, jwt.serialize());
	}
	
	
	@Override
	public Identifier decode(final String tokenValue, final TokenCodecContext context) throws TokenDecodeException {
		
		SignedJWT jwt;
		JWTClaimsSet jwtClaimsSet;
		
		try {
			jwt = SignedJWT.parse(tokenValue);
			jwtClaimsSet = jwt.getJWTClaimsSet();
		} catch (ParseException e) {
			throw new TokenDecodeException(e.getMessage(), e);
		}
		
		String issuer = jwtClaimsSet.getIssuer();
		
		// This issuer check is redundant from a validation perspective,
		// it is only intended to detect early issuer mismatches and
		// log them under AS0212. Without this check the token
		// introspection endpoint will simply log the token as invalid
		// or expired.
		if (! context.getIssuer().getValue().equals(issuer)) {
			throw new TokenDecodeException("Missing or invalid issuer: " + issuer);
		}
		
		// Optional signature validation
		if (! context.getJWSVerifier().verifySignature(jwt)) {
			throw new TokenDecodeException("Invalid JWT signature");
		}
		
		
		// Extract access token ID
		String id = jwtClaimsSet.getJWTID();
		
		if (id == null || id.trim().isEmpty()) {
			throw new TokenDecodeException("Missing or invalid JWT ID: " + id);
		}
		
		// The access token ID will now be validated by the Connect2id server
		return new Identifier(id);
	}
}

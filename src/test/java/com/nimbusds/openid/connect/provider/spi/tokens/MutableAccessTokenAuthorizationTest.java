package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.dpop.JWKThumbprintConfirmation;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.oauth2.sdk.util.date.DateWithTimeZoneOffset;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.assurance.IdentityTrustFramework;
import com.nimbusds.openid.connect.sdk.assurance.IdentityVerification;
import com.nimbusds.openid.connect.sdk.assurance.VerificationProcess;
import com.nimbusds.openid.connect.sdk.assurance.evidences.QESEvidence;
import com.nimbusds.openid.connect.sdk.id.HashBasedPairwiseSubjectCodec;
import com.nimbusds.openid.connect.sdk.id.PairwiseSubjectCodec;
import com.nimbusds.openid.connect.sdk.id.SectorID;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class MutableAccessTokenAuthorizationTest {


        @Test
        public void testDefaultConstructorWithSettersAndGetters()
		throws Exception {
		
		var authz = new MutableAccessTokenAuthorization();
		assertNull(authz.getSubject());
		assertNull(authz.getActor());
		assertNull(authz.getClientID());
		assertNull(authz.getScope());
		assertNull(authz.getExpirationTime());
		assertNull(authz.getIssueTime());
		assertNull(authz.getIssuer());
		assertNull(authz.getAudienceList());
		assertNull(authz.getSubjectType());
		assertNull(authz.getLocalSubject());
		assertNull(authz.getJWTID());
		assertNull(authz.getClaimNames());
		assertNull(authz.getClaimsLocales());
		assertNull(authz.getPresetClaims());
		assertNull(authz.getClaimsData());
		assertNull(authz.getData());
		assertNull(authz.getClientCertificateConfirmation());
		assertNull(authz.getJWKThumbprintConfirmation());
		
		var sub = new Subject("alice");
		var act = new Actor(new Subject("bob"));
		var clientID = new ClientID("123");
		var scope = new Scope("openid", "email");
		Instant exp = Instant.ofEpochSecond(123000L);
		Instant iat = Instant.ofEpochSecond(100000L);
		var iss = new Issuer("https://c2id.com");
		List<Audience> audList = new Audience("resource-server-1").toSingleAudienceList();
		var subjectType = SubjectType.PUBLIC;
		var jti = new JWTID();
		Set<String> claimNames = new HashSet<>(Arrays.asList("email", "email_verified"));
		List<LangTag> claimsLocales = Arrays.asList(LangTag.parse("en-GB"), LangTag.parse("de-DE"));
		var presetClaims = new JSONObject();
		presetClaims.put("name", "Alice Adams");
		var verification = new IdentityVerification(
			IdentityTrustFramework.EIDAS_IAL_SUBSTANTIAL,
			new DateWithTimeZoneOffset(new java.util.Date()),
			new VerificationProcess(UUID.randomUUID().toString()),
			new QESEvidence(
				new Issuer("https://eidas.c2id.com"),
				UUID.randomUUID().toString(),
				new DateWithTimeZoneOffset(new java.util.Date())
			)
		);
		var sessionKey = "WYqFXK7Q4HFnJv0hiT3Fgw";
		var data = new JSONObject();
		data.put("ip", "192.168.0.1");
		var otherTopLevelParams = new JSONObject();
		otherTopLevelParams.put("patientId", "p123");
		var cnfX5t = new X509CertificateConfirmation(SampleX509Certificate.X5T_SHA256);
		var cnfJkt = new JWKThumbprintConfirmation(SampleKeys.RSA_JWK_2048.computeThumbprint());
		authz.withSubject(sub)
			.withActor(act)
			.withClientID(clientID)
			.withScope(scope)
			.withExpirationTime(exp)
			.withIssueTime(iat)
			.withIssuer(iss)
			.withAudienceList(audList)
			.withSubjectType(subjectType)
			.withJWTID(jti)
			.withClaimNames(claimNames)
			.withClaimsLocales(claimsLocales)
			.withPresetClaims(presetClaims)
			.withClaimsData(verification.toJSONObject())
			.withSubjectSessionkey(sessionKey)
			.withData(data)
			.withOtherTopLevelParameters(otherTopLevelParams)
			.withClientCertificateConfirmation(cnfX5t)
			.withJWKThumbprintConfirmation(cnfJkt);
		
		assertEquals(sub, authz.getSubject());
		assertEquals(act, authz.getActor());
		assertEquals(clientID, authz.getClientID());
		assertEquals(scope, authz.getScope());
		assertEquals(exp, authz.getExpirationTime());
		assertEquals(iat, authz.getIssueTime());
		assertEquals(iss, authz.getIssuer());
		assertEquals(audList, authz.getAudienceList());
		assertEquals(subjectType, authz.getSubjectType());
		assertEquals(sub, authz.getLocalSubject());
		assertEquals(jti, authz.getJWTID());
		assertEquals(claimNames, authz.getClaimNames());
		assertEquals(claimsLocales, authz.getClaimsLocales());
		assertEquals(presetClaims, authz.getPresetClaims());
		assertEquals(verification.toJSONObject(), authz.getClaimsData());
		assertEquals(sessionKey, authz.getSubjectSessionKey());
		assertEquals(data, authz.getData());
		assertEquals(otherTopLevelParams, authz.getOtherTopLevelParameters());
		assertEquals(cnfX5t, authz.getClientCertificateConfirmation());
		assertEquals(cnfJkt, authz.getJWKThumbprintConfirmation());
	}


        @Test
        public void testCopyConstructor()
		throws Exception {
		
		var sub = new Subject("alice");
		var act = new Actor(new Subject("bob"));
		var clientID = new ClientID("123");
		var scope = new Scope("openid", "email");
		Instant exp = Instant.ofEpochSecond(123000L);
		Instant iat = Instant.ofEpochSecond(100000L);
		var iss = new Issuer("https://c2id.com");
		List<Audience> audList = new Audience("resource-server-1").toSingleAudienceList();
		var subjectType = SubjectType.PUBLIC;
		var jti = new JWTID();
		Set<String> claimNames = new HashSet<>(Arrays.asList("email", "email_verified"));
		List<LangTag> claimsLocales = Arrays.asList(LangTag.parse("en-GB"), LangTag.parse("de-DE"));
		var presetClaims = new JSONObject();
		presetClaims.put("name", "Alice Adams");
		var verification = new IdentityVerification(
			IdentityTrustFramework.EIDAS_IAL_SUBSTANTIAL,
			new DateWithTimeZoneOffset(new java.util.Date()),
			new VerificationProcess(UUID.randomUUID().toString()),
			new QESEvidence(
				new Issuer("https://eidas.c2id.com"),
				UUID.randomUUID().toString(),
				new DateWithTimeZoneOffset(new java.util.Date())
			)
		);
		var sessionKey = "WYqFXK7Q4HFnJv0hiT3Fgw";
		var data = new JSONObject();
		data.put("ip", "192.168.0.1");
		var otherTopLevelParams = new JSONObject();
		otherTopLevelParams.put("patientId", "p123");
		var cnfX5t = new X509CertificateConfirmation(SampleX509Certificate.X5T_SHA256);
		var cnfJkt = new JWKThumbprintConfirmation(SampleKeys.RSA_JWK_2048.computeThumbprint());
		
		AccessTokenAuthorization authz = new MutableAccessTokenAuthorization()
			.withSubject(sub)
			.withActor(act)
			.withClientID(clientID)
			.withScope(scope)
			.withExpirationTime(exp)
			.withIssueTime(iat)
			.withIssuer(iss)
			.withAudienceList(audList)
			.withSubjectType(subjectType)
			.withJWTID(jti)
			.withClaimNames(claimNames)
			.withClaimsLocales(claimsLocales)
			.withPresetClaims(presetClaims)
			.withClaimsData(verification.toJSONObject())
			.withSubjectSessionkey(sessionKey)
			.withData(data)
			.withOtherTopLevelParameters(otherTopLevelParams)
			.withClientCertificateConfirmation(cnfX5t)
			.withJWKThumbprintConfirmation(cnfJkt);
		
		AccessTokenAuthorization copy = new MutableAccessTokenAuthorization(authz);
		
		assertEquals(sub, copy.getSubject());
		assertEquals(act, copy.getActor());
		assertEquals(clientID, copy.getClientID());
		assertEquals(scope, copy.getScope());
		assertEquals(exp, copy.getExpirationTime());
		assertEquals(iat, copy.getIssueTime());
		assertEquals(iss, copy.getIssuer());
		assertEquals(audList, copy.getAudienceList());
		assertEquals(subjectType, copy.getSubjectType());
		assertEquals(sub, copy.getLocalSubject());
		assertEquals(jti, copy.getJWTID());
		assertEquals(claimNames, copy.getClaimNames());
		assertEquals(claimsLocales, copy.getClaimsLocales());
		assertEquals(presetClaims, copy.getPresetClaims());
		assertEquals(verification.toJSONObject(), copy.getClaimsData());
		assertEquals(sessionKey, copy.getSubjectSessionKey());
		assertEquals(data, copy.getData());
		assertEquals(otherTopLevelParams, copy.getOtherTopLevelParameters());
		assertEquals(cnfX5t, copy.getClientCertificateConfirmation());
		assertEquals(cnfJkt, copy.getJWKThumbprintConfirmation());
	}


        @Test
        public void testCopyConstructor_NPE() {
		
		try {
			new MutableAccessTokenAuthorization(null);
			Assert.fail();
		} catch (NullPointerException e) {
			// ok
		}
	}


        @Test
        public void testWithPairwiseSubject() {
		
		var localSubject = new Subject("alice");
		PairwiseSubjectCodec codec = new HashBasedPairwiseSubjectCodec(new Secret(32).getValueBytes());
		Subject pairwiseSubject = codec.encode(new SectorID("example.com"), localSubject);
		
		AccessTokenAuthorization authz = new MutableAccessTokenAuthorization()
			.withSubject(pairwiseSubject)
			.withSubjectType(SubjectType.PAIRWISE)
			.withLocalSubject(localSubject);
		
		assertEquals(pairwiseSubject, authz.getSubject());
		assertEquals(SubjectType.PAIRWISE, authz.getSubjectType());
		assertEquals(localSubject, authz.getLocalSubject());
		
		AccessTokenAuthorization copy = new MutableAccessTokenAuthorization(authz);
		
		assertEquals(pairwiseSubject, copy.getSubject());
		assertEquals(SubjectType.PAIRWISE, copy.getSubjectType());
		assertEquals(localSubject, copy.getLocalSubject());
	}


        @Test
        public void testWithPairwiseSubject_subjectTypeAndLocalSubjectNotSet() {
		
		var localSubject = new Subject("alice");
		PairwiseSubjectCodec codec = new HashBasedPairwiseSubjectCodec(new Secret(32).getValueBytes());
		Subject pairwiseSubject = codec.encode(new SectorID("example.com"), localSubject);
		
		AccessTokenAuthorization authz = new MutableAccessTokenAuthorization()
			.withSubject(pairwiseSubject)
			.withSubjectType(null);
		
		assertEquals(pairwiseSubject, authz.getSubject());
		assertNull(authz.getSubjectType());
		assertNull(authz.getLocalSubject());
		
		AccessTokenAuthorization copy = new MutableAccessTokenAuthorization(authz);
		
		assertEquals(pairwiseSubject, copy.getSubject());
		assertNull(authz.getSubjectType());
		assertNull(authz.getLocalSubject());
	}


        @Test
        public void testWithPairwiseSubject_localSubjectNotSet() {
		
		var localSubject = new Subject("alice");
		PairwiseSubjectCodec codec = new HashBasedPairwiseSubjectCodec(new Secret(32).getValueBytes());
		Subject pairwiseSubject = codec.encode(new SectorID("example.com"), localSubject);
		
		AccessTokenAuthorization authz = new MutableAccessTokenAuthorization()
			.withSubject(pairwiseSubject)
			.withSubjectType(SubjectType.PAIRWISE);
		
		assertEquals(pairwiseSubject, authz.getSubject());
		assertEquals(SubjectType.PAIRWISE, authz.getSubjectType());
		assertNull(authz.getLocalSubject());
		
		AccessTokenAuthorization copy = new MutableAccessTokenAuthorization(authz);
		
		assertEquals(pairwiseSubject, copy.getSubject());
		assertEquals(SubjectType.PAIRWISE, copy.getSubjectType());
		assertNull(authz.getLocalSubject());
	}
}

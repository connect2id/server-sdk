package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSource;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import org.checkerframework.checker.nullness.qual.Nullable;


public class MockTokenIntrospectionContext implements TokenIntrospectionContext {
	
	
	private final Issuer issuer;
	
	
	private final OIDCClientInformation clientInfo;
	
	
	private final ClaimsSource claimsSource;


	private final SubjectSession session;
	
	
	public MockTokenIntrospectionContext(final Issuer issuer,
										 final OIDCClientInformation clientInfo,
										 final ClaimsSource claimsSource,
										 final SubjectSession session) {
		assert issuer != null;
		this.issuer = issuer;
		this.clientInfo = clientInfo;
		this.claimsSource = claimsSource;
		this.session = session;
	}
	
	
	@Override
	public OIDCClientInformation getOIDCClientInformation() {
		return clientInfo;
	}


	@Override
	public @Nullable OIDCClientInformation getOIDCClientInformation(ClientID clientID) {
		return null;
	}

	@Override
	public Issuer getIssuer() {
		return issuer;
	}
	
	
	@Override
	public ClaimsSource getClaimsSource() {
		return claimsSource;
	}


	@Override
	public @Nullable SubjectSession getSubjectSession() {
		return session;
	}
}

package com.nimbusds.openid.connect.provider.spi.reg;


import com.nimbusds.oauth2.sdk.client.RegistrationError;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class InvalidRegistrationExceptionTest {


        @Test
        public void testDefaultConstructor() {
		
		var e = new InvalidRegistrationException();
		
		assertEquals(RegistrationError.INVALID_CLIENT_METADATA, e.getErrorObject());
		
		assertNull(e.getMessage());
		assertNull(e.getCause());
	}


        @Test
        public void testErrorObjectConstructor_null() {

		var e = new InvalidRegistrationException(null);

		assertEquals(RegistrationError.INVALID_CLIENT_METADATA, e.getErrorObject());

		assertNull(e.getMessage());
		assertNull(e.getCause());
	}


        @Test
        public void testErrorObjectConstructor() {

		var e = new InvalidRegistrationException(RegistrationError.INVALID_REDIRECT_URI);

		assertEquals(RegistrationError.INVALID_REDIRECT_URI, e.getErrorObject());

		assertNull(e.getMessage());
		assertNull(e.getCause());
	}


        @Test
        public void testErrorObjectConstructor_JavaDocExample() {

		var e = new InvalidRegistrationException(RegistrationError.INVALID_CLIENT_METADATA.setDescription("The policy_uri must be on a redirect_uris domain"));

		assertEquals(RegistrationError.INVALID_CLIENT_METADATA, e.getErrorObject());
		assertEquals(RegistrationError.INVALID_CLIENT_METADATA.getCode(), e.getErrorObject().getCode());
		assertEquals("The policy_uri must be on a redirect_uris domain", e.getErrorObject().getDescription());
		assertNull(e.getErrorObject().getURI());

		assertNull(e.getMessage());
		assertNull(e.getCause());
	}


        @Test
        public void testInvalidClientMetadataConstructor_field() {

		var e = new InvalidRegistrationException("jwks_uri", null);

		assertEquals(RegistrationError.INVALID_CLIENT_METADATA.getCode(), e.getErrorObject().getCode());
		assertEquals("Invalid client metadata field jwks_uri", e.getErrorObject().getDescription());
	}


        @Test
        public void testInvalidClientMetadataConstructor_field_description() {

		var e = new InvalidRegistrationException("jwks_uri", "No RSA key(s) found");

		assertEquals(RegistrationError.INVALID_CLIENT_METADATA.getCode(), e.getErrorObject().getCode());
		assertEquals("Invalid client metadata field jwks_uri: No RSA key(s) found", e.getErrorObject().getDescription());
	}
}

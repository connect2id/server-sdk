package com.nimbusds.openid.connect.provider.spi.config;


import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertEquals;


public class SystemPropertiesSourceTest {
	
	
	static class SampleSystemPropertiesSource implements SystemPropertiesSource {
		
		
		@Override
		public Properties getProperties() {
			var properties = new Properties();
			properties.setProperty("op.issuer", "https://c2id.com");
			return properties;
		}
	}


        @Test
        public void testLoad() {
		
		SystemPropertiesSource source = new SampleSystemPropertiesSource();
		
		Properties properties = source.getProperties();
		
		assertEquals("https://c2id.com", properties.getProperty("op.issuer"));
		assertEquals(1, properties.size());
	}
}

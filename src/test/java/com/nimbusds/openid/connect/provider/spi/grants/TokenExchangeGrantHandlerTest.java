package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.token.TokenTypeURI;
import com.nimbusds.oauth2.sdk.tokenexchange.TokenExchangeGrant;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;
import com.nimbusds.openid.connect.provider.spi.tokens.MutableAccessTokenAuthorization;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Test;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;


public class TokenExchangeGrantHandlerTest {
	
	
	private static final Issuer ISSUER = new Issuer("https://c2id.com");

	
	static class SampleHandlerDeprecatedHandler implements TokenExchangeGrantHandler {
		
		static final Scope.Value SCOPE_IN = new Scope.Value("api-read");
		static final Scope.Value SCOPE_OUT = new Scope.Value("api-downstream-read");
		
		
		@Override
		public TokenExchangeAuthorization processGrant(
			TokenExchangeGrant grant,
			TokenRequestParameters tokenRequestParams,
			ClientID clientID,
			boolean confidentialClient,
			OIDCClientMetadata clientMetadata,
			TokenIntrospection tokenIntrospection,
			TokenIssueHelpers tokenIssueHelpers,
			InvocationContext invocationCtx)
			throws GeneralException {
			
			assertEquals(ISSUER, invocationCtx.getIssuer());
			
			AccessTokenAuthorization tokenAuthz = tokenIntrospection.getAccessTokenAuthorization();
			
			if (tokenAuthz == null) {
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Invalid subject_token"));
			}
			
			if (tokenAuthz.getScope() != null && tokenAuthz.getScope().contains(SCOPE_IN)) {
				return new TokenExchangeAuthorization(
					tokenAuthz.getSubject(),
					new Scope(SCOPE_OUT),
					new AccessTokenSpec(
						60L,
						new Audience("https://api.example.com").toSingleAudienceList(),
						TokenEncoding.SELF_CONTAINED,
						Optional.of(false)
					),
					new ClaimsSpec(),
					null);
			}
			
			throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Insufficient scope for exchange"));
		}
	}
	
	
	private static final ClientID CLIENT_ID = new ClientID("123");
	
	private static final OIDCClientMetadata CLIENT_METADATA = new OIDCClientMetadata();
	
	static {
		CLIENT_METADATA.setGrantTypes(Collections.singleton(GrantType.TOKEN_EXCHANGE));
		CLIENT_METADATA.setResponseTypes(Collections.emptySet());
		CLIENT_METADATA.applyDefaults();
	}


        @Test
        public void testInvocation() throws GeneralException {
		
		TokenExchangeGrantHandler handler = new SampleHandlerDeprecatedHandler();
		
		assertEquals(GrantType.TOKEN_EXCHANGE, handler.getGrantType());
		
		TokenExchangeAuthorization authz = handler.processGrant(
			new TokenExchangeGrant(
				new BearerAccessToken(),
				TokenTypeURI.ACCESS_TOKEN
			),
			new TokenRequestParameters() {
				@Override
				public Scope getScope() {
					return new Scope(SampleHandlerDeprecatedHandler.SCOPE_OUT);
				}
			},
			CLIENT_ID,
			true,
			CLIENT_METADATA,
			new TokenIntrospection() {
				@Override
				public AccessTokenAuthorization getAccessTokenAuthorization() {
					return new MutableAccessTokenAuthorization()
						.withSubject(new Subject("alice"))
						.withScope(new Scope(SampleHandlerDeprecatedHandler.SCOPE_IN));
				}

				@Override
				public OIDCClientMetadata getOIDCClientMetadata() {
					return CLIENT_METADATA;
				}
			},
			() -> null,
			new GrantHandlerContext() {
				@Override
				public Set<String> resolveClaimNames(@Nullable Scope scope) {
					return OIDCScopeValue.resolveClaimNames(scope);
				}

				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
			}
		);
		
		assertEquals(TokenTypeURI.ACCESS_TOKEN, authz.getTokenType());
		assertEquals(new Subject("alice"), authz.getSubject());
		assertEquals(new Scope(SampleHandlerDeprecatedHandler.SCOPE_OUT), authz.getScope());
		assertEquals(60L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().getEncryptSelfContained().get());
		assertNull(authz.getData());
	}
}

package com.nimbusds.openid.connect.provider.spi.claims;


import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.DistributedClaims;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;


public class MockDistributedClaimsSource extends MockClaimsSource implements AdvancedClaimsSource {
	
	
	@Override
	public Set<String> supportedClaims() {
		Set<String> supportedClaims = super.supportedClaims();
		supportedClaims.add("credit_score");
		return supportedClaims;
	}
	
	
	@Override
	public UserInfo getClaims(final Subject subject,
				  final Set<String> claims,
				  final List<LangTag> claimsLocales,
				  final ClaimsSourceRequestContext requestContext) {
		
		if (! isEnabled()) {
			return null;
		}
		
		if (! subject.equals(new Subject("alice"))) {
			return null;
		}
		
		UserInfo alice = new UserInfo(subject);
		
		if (claims.contains("email")) {
			alice.setEmailAddress("alice@wonderland.net");
		}
		
		if (claims.contains("name")) {
			alice.setName("Alice Adams");
		}
		
		if (claims.contains("credit_score") && requestContext.getUserInfoAccessToken() != null) {
			
			// Real handler may pass access token to upstream claims provider
			DistributedClaims distClaims = new DistributedClaims(
				Collections.singleton("credit_score"),
				URI.create("https://credit-agency.com/claims"),
				requestContext.getUserInfoAccessToken()
			);
			alice.addDistributedClaims(distClaims);
		}
		
		return alice;
	}
}

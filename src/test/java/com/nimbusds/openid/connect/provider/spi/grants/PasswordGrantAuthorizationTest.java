package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagUtils;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


/**
 * Tests the password grant authorisation class.
 */
public class PasswordGrantAuthorizationTest {


        @Test
        public void testOAuthConstructorMinimal()
		throws Exception {

		var subject = new Subject("alice");
		var scope = new Scope("openid", "email");
		boolean longLived = false;

		var authz = new PasswordGrantAuthorization(
			subject, scope, null, longLived, new AccessTokenSpec(), new RefreshTokenSpec(), null);

		assertEquals(subject, authz.getSubject());
		assertEquals(scope, authz.getScope());
		assertNull(authz.getAudience());
		assertFalse(authz.isLongLived());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getRefreshTokenSpec().issue());
		assertEquals(-1L, authz.getRefreshTokenSpec().getLifetime());
		assertNull(authz.getData());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parse(json);

		authz = PasswordGrantAuthorization.parse(jsonObject);

		assertEquals(subject, authz.getSubject());
		assertEquals(scope, authz.getScope());
		assertNull(authz.getAudience());
		assertFalse(authz.isLongLived());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getRefreshTokenSpec().issue());
		assertEquals(-1L, authz.getRefreshTokenSpec().getLifetime());
		assertNull(authz.getData());
	}


        @Test
        public void testTransientWithRefreshToken()
		throws Exception {

		var subject = new Subject("alice");
		var scope = new Scope("read", "write");
		boolean longLived = false;
		var accessTokenSpec = new AccessTokenSpec();
		var refreshTokenSpec = new RefreshTokenSpec(true, 3600*24*30);

		var authz = new PasswordGrantAuthorization(
			subject, scope, longLived, accessTokenSpec, refreshTokenSpec, null);

		assertEquals(subject, authz.getSubject());
		assertEquals(scope, authz.getScope());
		assertNull(authz.getAudience());
		assertFalse(authz.isLongLived());
		assertEquals(accessTokenSpec.toJSONObject(), authz.getAccessTokenSpec().toJSONObject());
		assertEquals(refreshTokenSpec, authz.getRefreshTokenSpec());
		assertNull(authz.getData());

		String json = authz.toJSONObject().toJSONString();
		
		JSONObject jsonObject = JSONObjectUtils.parse(json);

		authz = PasswordGrantAuthorization.parse(jsonObject);

		assertEquals(subject, authz.getSubject());
		assertEquals(scope, authz.getScope());
		assertNull(authz.getAudience());
		assertFalse(authz.isLongLived());
		assertEquals(accessTokenSpec.toJSONObject(), authz.getAccessTokenSpec().toJSONObject());
		assertEquals(refreshTokenSpec.toJSONObject(), authz.getRefreshTokenSpec().toJSONObject());
		assertNull(authz.getData());
	}


        @Test
        public void testOAuthConstructorFull()
		throws Exception {

		var subject = new Subject("alice");
		var scope = new Scope("openid", "email");
		List<Audience> aud = List.of(new Audience("a"), new Audience("b"));
		boolean longLived = true;
		var accessTokenSpec = new AccessTokenSpec(600L, aud, TokenEncoding.SELF_CONTAINED, null, true, SubjectType.PAIRWISE);
		var refreshTokenSpec = new RefreshTokenSpec(true, 72000L);
		var data = new JSONObject();
		data.put("realm", "internal");

		var authz = new PasswordGrantAuthorization(subject, scope, aud, longLived, accessTokenSpec, refreshTokenSpec, data);

		assertEquals(subject, authz.getSubject());
		assertEquals(scope, authz.getScope());
		assertTrue(authz.getAudience().containsAll(Arrays.asList(new Audience("a"), new Audience("b"))));
		assertEquals(2, authz.getAudience().size());
		assertTrue(authz.isLongLived());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertTrue(authz.getAccessTokenSpec().encrypt());
		assertEquals(SubjectType.PAIRWISE, authz.getAccessTokenSpec().getSubjectType());
		assertTrue(authz.getRefreshTokenSpec().issue());
		assertEquals(72000L, authz.getRefreshTokenSpec().getLifetime());
		assertEquals("internal", authz.getData().get("realm"));
		assertEquals(1, authz.getData().size());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parse(json);

		authz = PasswordGrantAuthorization.parse(jsonObject);

		assertEquals(subject, authz.getSubject());
		assertEquals(scope, authz.getScope());
		assertTrue(authz.getAudience().containsAll(Arrays.asList(new Audience("a"), new Audience("b"))));
		assertEquals(2, authz.getAudience().size());
		assertTrue(authz.isLongLived());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertTrue(authz.getAccessTokenSpec().encrypt());
		assertEquals(SubjectType.PAIRWISE, authz.getAccessTokenSpec().getSubjectType());
		assertTrue(authz.getRefreshTokenSpec().issue());
		assertEquals(72000L, authz.getRefreshTokenSpec().getLifetime());
		assertEquals("internal", authz.getData().get("realm"));
		assertEquals(1, authz.getData().size());
	}


        @Test
        public void testOIDCConstructor()
		throws Exception {

		var subject = new Subject("alice");
		var authTime = new Date(123L * 1000L);
		var acr = new ACR("urn:c2id:acr:strong-auth");
		List<AMR> amrList = List.of(new AMR("ldap"), new AMR("token"));
		var scope = new Scope("openid", "email");
		List<Audience> aud = List.of(new Audience("a"), new Audience("b"));
		var accessTokenSpec = new AccessTokenSpec(600L, TokenEncoding.IDENTIFIER, false);
		var refreshTokenSpec = new RefreshTokenSpec(true, 0L);
		var idTokenSpec = new IDTokenSpec(true, 900L, new Subject("bob"));
		Set<String> claims = Set.of("openid", "email", "email_verified");
		List<LangTag> claimsLocales = LangTagUtils.parseLangTagList("en", "es");
		var presetIDTokenClaims = new JSONObject();
		presetIDTokenClaims.put("group", "admin");
		var presetUserInfoClaims = new JSONObject();
		presetUserInfoClaims.put("office", "15");
		var claimsSpec = new ClaimsSpec(claims, claimsLocales, presetIDTokenClaims, presetUserInfoClaims, ClaimsTransport.USERINFO);
		var data = new JSONObject();
		data.put("realm", "internal");

		var authz = new PasswordGrantAuthorization(
			subject, authTime, acr, amrList, scope, aud, true,
			accessTokenSpec, refreshTokenSpec, idTokenSpec, claimsSpec, data);

		assertEquals(subject.getValue(), authz.getSubject().getValue());
		assertEquals(authTime.getTime(), authz.getAuthTime().getTime());
		assertEquals(acr.getValue(), authz.getACR().getValue());
		assertEquals(amrList.get(0).getValue(), authz.getAMRList().get(0).getValue());
		assertEquals(amrList.get(1).getValue(), authz.getAMRList().get(1).getValue());
		assertEquals(amrList.size(), authz.getAMRList().size());
		assertEquals(scope, authz.getScope());
		assertEquals(aud.get(0).getValue(), authz.getAudience().get(0).getValue());
		assertEquals(aud.get(1).getValue(), authz.getAudience().get(1).getValue());
		assertEquals(aud.size(), authz.getAudience().size());
		assertTrue(authz.isLongLived());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.IDENTIFIER, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertTrue(authz.getRefreshTokenSpec().issue());
		assertEquals(0L, authz.getRefreshTokenSpec().getLifetime());
		assertTrue(authz.getIDTokenSpec().issue());
		assertEquals(900L, authz.getIDTokenSpec().getLifetime());
		assertEquals(new Subject("bob"), authz.getIDTokenSpec().getImpersonatedSubject());
		assertTrue(claims.containsAll(authz.getClaimsSpec().getNames()));
		assertEquals(claims.size(), authz.getClaimsSpec().getNames().size());
		assertTrue(claimsLocales.containsAll(authz.getClaimsSpec().getLocales()));
		assertEquals(claimsLocales.size(), authz.getClaimsSpec().getLocales().size());
		assertEquals("admin", (String) authz.getClaimsSpec().getPresetIDTokenClaims().get("group"));
		assertEquals(1, authz.getClaimsSpec().getPresetIDTokenClaims().size());
		assertEquals("15", (String) authz.getClaimsSpec().getPresetUserInfoClaims().get("office"));
		assertEquals(1, authz.getClaimsSpec().getPresetUserInfoClaims().size());
		assertEquals("internal", authz.getData().get("realm"));
		assertEquals(1, authz.getData().size());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parse(json);

		authz = PasswordGrantAuthorization.parse(jsonObject);

		assertEquals(subject.getValue(), authz.getSubject().getValue());
		assertEquals(authTime.getTime(), authz.getAuthTime().getTime());
		assertEquals(acr.getValue(), authz.getACR().getValue());
		assertEquals(amrList.get(0).getValue(), authz.getAMRList().get(0).getValue());
		assertEquals(amrList.get(1).getValue(), authz.getAMRList().get(1).getValue());
		assertEquals(amrList.size(), authz.getAMRList().size());
		assertEquals(scope, authz.getScope());
		assertEquals(aud.get(0).getValue(), authz.getAudience().get(0).getValue());
		assertEquals(aud.get(1).getValue(), authz.getAudience().get(1).getValue());
		assertEquals(aud.size(), authz.getAudience().size());
		assertTrue(authz.isLongLived());
		assertEquals(600L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.IDENTIFIER, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertTrue(authz.getRefreshTokenSpec().issue());
		assertEquals(0L, authz.getRefreshTokenSpec().getLifetime());
		assertTrue(authz.getIDTokenSpec().issue());
		assertEquals(900L, authz.getIDTokenSpec().getLifetime());
		assertEquals(new Subject("bob"), authz.getIDTokenSpec().getImpersonatedSubject());
		assertTrue(claims.containsAll(authz.getClaimsSpec().getNames()));
		assertEquals(claims.size(), authz.getClaimsSpec().getNames().size());
		assertTrue(claimsLocales.containsAll(authz.getClaimsSpec().getLocales()));
		assertEquals(claimsLocales.size(), authz.getClaimsSpec().getLocales().size());
		assertEquals("admin", (String) authz.getClaimsSpec().getPresetIDTokenClaims().get("group"));
		assertEquals(1, authz.getClaimsSpec().getPresetIDTokenClaims().size());
		assertEquals("15", (String) authz.getClaimsSpec().getPresetUserInfoClaims().get("office"));
		assertEquals(1, authz.getClaimsSpec().getPresetUserInfoClaims().size());
		assertEquals("internal", authz.getData().get("realm"));
		assertEquals(1, authz.getData().size());
	}


        @Test
        public void testParseMinimal()
		throws Exception {

		var o = new JSONObject();
		o.put("scope", Arrays.asList("openid", "email"));
		o.put("sub", "alice");

		String json = o.toJSONString();

		PasswordGrantAuthorization authz = PasswordGrantAuthorization.parse(JSONObjectUtils.parse(json));

		assertTrue(Scope.parse("openid email").containsAll(authz.getScope()));
		assertEquals(2, authz.getScope().size());
		assertEquals("alice", authz.getSubject().getValue());

		assertNull(authz.getAudience());
		assertNull(authz.getAuthTime());
		assertNull(authz.getACR());
		assertNull(authz.getAMRList());
		assertFalse(authz.isLongLived());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authz.getAccessTokenSpec().getEncoding());
		assertFalse(authz.getAccessTokenSpec().encrypt());
		assertFalse(authz.getRefreshTokenSpec().issue());
		assertEquals(-1L, authz.getRefreshTokenSpec().getLifetime());
		assertFalse(authz.getIDTokenSpec().issue());
		assertEquals(0L, authz.getIDTokenSpec().getLifetime());
		assertNull(authz.getIDTokenSpec().getImpersonatedSubject());
		assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertNull(authz.getClaimsSpec().getLocales());
		assertNull(authz.getClaimsSpec().getPresetIDTokenClaims());
		assertNull(authz.getClaimsSpec().getPresetUserInfoClaims());
		assertEquals(ClaimsTransport.getDefault(), authz.getClaimsSpec().getTransport());
		assertNull(authz.getData());
	}
}


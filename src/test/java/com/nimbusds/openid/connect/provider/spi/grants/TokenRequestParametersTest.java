package com.nimbusds.openid.connect.provider.spi.grants;


import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class TokenRequestParametersTest {


        @Test
        public void testDefaults() {
		
		var params = new TokenRequestParameters() {};
		
		assertNull(params.getScope());
		assertNull(params.getResources());
		assertTrue(params.getCustomParameters().isEmpty());
	}
}

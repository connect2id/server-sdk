package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.proc.BadJWTException;
import com.nimbusds.jwt.proc.DefaultJWTClaimsVerifier;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;

import java.net.URI;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;


/**
 * Tests the third-party JWT handler interface.
 */
public class ThirdPartyJWTGrantHandlerTest {


	private static final Secret MAC_KEY = new Secret();


	private static final Issuer JWT_ISSUER = new Issuer("https://secure-token-service.com");


	private static final URI TOKEN_ENDPOINT = URI.create("https://c2id.com/token");


	static class ExampleHandler implements ThirdPartyJWTGrantHandler {


		MACVerifier macVerifier;


		URI tokenEndpoint;


		public ExampleHandler() {

			try {
				macVerifier = new MACVerifier(MAC_KEY.getValueBytes());
			} catch (JOSEException e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		}


		@Override
		public ThirdPartyAssertionAuthorization processThirdPartyGrant(final JOSEObject jwtAssertion,
									       final Scope scope,
									       final ClientID clientID,
									       final boolean confidentialClient,
									       final OIDCClientMetadata clientMetadata)
			throws GeneralException {

			// Permit only confidential clients
			if (! confidentialClient) {
				throw new GeneralException(OAuth2Error.INVALID_CLIENT.setDescription("Clients must be confidential"));
			}

			// Verify JWT MAC
			if (! (jwtAssertion instanceof SignedJWT)) {
				// Encrypted JWT rejected
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Signed JWT assertions accepted only"));
			}

			SignedJWT signedJWT = (SignedJWT)jwtAssertion;
			try {
				if (! signedJWT.verify(macVerifier)) {
					throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Invalid JWT MAC"));
				}
			} catch (JOSEException e) {
				// Error must be logged, then indicate server error
				throw new GeneralException(OAuth2Error.SERVER_ERROR);
			}

			// Validate JWT claims, see http://tools.ietf.org/html/rfc7523#section-3
			DefaultJWTClaimsVerifier<?> jwtClaimsVerifier = new DefaultJWTClaimsVerifier<>(
				TOKEN_ENDPOINT.toString(),
				new JWTClaimsSet.Builder()
					.issuer(JWT_ISSUER.getValue())
					.build(),
				new HashSet<>(Arrays.asList("exp", "sub"))
			);

			Subject sub;
			Scope authzScope;
			try {
				JWTClaimsSet jwtClaimsSet = signedJWT.getJWTClaimsSet();
				jwtClaimsVerifier.verify(jwtClaimsSet, null);
				sub = new Subject(jwtClaimsSet.getSubject());
				authzScope = Scope.parse(jwtClaimsSet.getStringClaim("scope"));
			} catch (ParseException | BadJWTException e) {
				throw new GeneralException(OAuth2Error.INVALID_GRANT.setDescription("Invalid JWT claims set: " + e.getMessage()));
			}

			return new ThirdPartyAssertionAuthorization(sub, clientID, authzScope);
		}


		@Override
		public boolean isEnabled() {
			return true;
		}


		@Override
		public void shutdown()
			throws Exception {
		}
	}


        @Test
        public void testAuthorizationSuccess()
		throws Exception {

		ThirdPartyJWTGrantHandler handler = new ExampleHandler();
		
		InitContext mockInitContext = mock(InitContext.class);
		handler.init(mockInitContext);

		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
			.issuer(JWT_ISSUER.getValue())
			.audience(TOKEN_ENDPOINT.toString())
			.subject("alice")
			.expirationTime(new Date(new Date().getTime() + 5*60*1000L))
			.claim("scope", "read write")
			.build();

		var signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);
		signedJWT.sign(new MACSigner(MAC_KEY.getValueBytes()));

		var clientID = new ClientID("123");

		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.applyDefaults();
		clientMetadata.setScope(Scope.parse("read write"));

		ThirdPartyAssertionAuthorization authz = handler.processThirdPartyGrant(signedJWT, null, clientID, true, clientMetadata);

		assertEquals("alice", authz.getSubject().getValue());
		assertEquals(Scope.parse("read write"), authz.getScope());
		assertEquals(0L, authz.getAccessTokenSpec().getLifetime()); // default to server setting
		assertFalse(authz.getIDTokenSpec().issue());
		assertNull(authz.getData());
	}
}

package com.nimbusds.openid.connect.provider.spi.claims;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSessionID;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Claims source request context.
 */
public class RequestCtx implements ClaimsSourceRequestContext {
	
	
	final Issuer issuer;


	final OIDCClientInformation clientInfo;
	
	
	final ClaimsTransport claimsTransport;
	
	
	final JSONObject verification;
	
	
	final String clientIPAddress;
	
	
	final AccessToken accessToken;


	final SubjectSessionID subjectSessionID;


	final SubjectSession subjectSession;


	final Scope consentedScope;


	public RequestCtx(final Issuer issuer,
			  final OIDCClientInformation clientInfo,
			  final ClaimsTransport claimsTransport,
			  final JSONObject verification,
			  final String clientIPAddress,
			  final AccessToken accessToken,
			  final SubjectSessionID subjectSessionID,
			  final SubjectSession subjectSession,
			  final Scope consentedScope) {

		assert issuer != null;
		this.issuer = issuer;
		
		assert clientInfo != null;
		this.clientInfo = clientInfo;
		
		this.claimsTransport = claimsTransport;
		
		this.verification = verification;
		
		this.clientIPAddress = clientIPAddress;
		
		this.accessToken = accessToken;

		this.subjectSessionID = subjectSessionID;

		this.subjectSession = subjectSession;

		this.consentedScope = consentedScope;
	}
	
	
	@Override
	public Issuer getIssuer() {
		
		return issuer;
	}
	
	
	@Override
	public ClaimsTransport getClaimsTransport() {
		
		return claimsTransport;
	}
	
	
	@Override
	public JSONObject getClaimsData() {
		
		return verification;
	}
	
	
	@Override
	public ClientID getClientID() {

		return clientInfo.getID();
	}


	@Override
	public OIDCClientInformation getOIDCClientInformation() {
		return clientInfo;
	}


	@Override
	public @Nullable OIDCClientInformation getOIDCClientInformation(ClientID clientID) {
		return null;
	}


	@Override
	public String getClientIPAddress() {
		
		return clientIPAddress;
	}
	
	
	@Override
	public AccessToken getUserInfoAccessToken() {
		
		return accessToken;
	}


	@Override
	public SubjectSessionID getSubjectSessionID() {

		return subjectSessionID;
	}

	@Override
	public SubjectSession getSubjectSession() {

		return subjectSession;
	}


	@Override
	public @Nullable Scope getScope() {

		return consentedScope;
	}
}

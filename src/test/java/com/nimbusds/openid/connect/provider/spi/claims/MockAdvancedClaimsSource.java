package com.nimbusds.openid.connect.provider.spi.claims;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import java.util.List;
import java.util.Set;


/**
 * Mock advanced claims source.
 */
public class MockAdvancedClaimsSource extends MockClaimsSource implements AdvancedClaimsSource {


	@Override
	public UserInfo getClaims(final Subject subject,
				  final Set<String> claims,
				  final List<LangTag> claimsLocales,
				  final ClaimsSourceRequestContext requestContext) {

		assert AdvancedClaimsSourceTest.ISSUER.equals(requestContext.getIssuer());
		assert AdvancedClaimsSourceTest.CLIENT_ID.equals(requestContext.getClientID());
		assert AdvancedClaimsSourceTest.CLIENT_INFO.equals(requestContext.getOIDCClientInformation());
		assert ClaimsTransport.USERINFO.equals(requestContext.getClaimsTransport());
		assert AdvancedClaimsSourceTest.CLIENT_IP.equals(requestContext.getClientIPAddress());
		assert AdvancedClaimsSourceTest.SID.getValue().equals(requestContext.getSubjectSessionID().getValue());
		
		return getClaims(subject, claims, claimsLocales);
	}
}

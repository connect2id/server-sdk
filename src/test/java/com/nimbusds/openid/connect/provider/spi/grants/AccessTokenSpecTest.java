package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.SubjectType;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;


public class AccessTokenSpecTest {
	
	
	private static void verifyDefaultConstant(final AccessTokenSpec tokenSpec)
		throws ParseException {
		
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals("SELF_CONTAINED", (String)jsonObject.get("encoding"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(2, jsonObject.size());
		
		AccessTokenSpec parsedSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(0L, parsedSpec.getLifetime());
		assertNull(parsedSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, parsedSpec.getEncoding());
		assertFalse(parsedSpec.encrypt());
		assertFalse(parsedSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, parsedSpec.getSubjectType());
	}


        @Test
        public void testDefaultConstructor()
		throws Exception {

		verifyDefaultConstant(new AccessTokenSpec());
	}


        @Test
        public void testDefaultConstant()
		throws Exception {

		verifyDefaultConstant(AccessTokenSpec.DEFAULT);
	}


        @Test
        public void testIdentifierEncoded()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(0L, null, TokenEncoding.IDENTIFIER, Optional.empty());
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals("IDENTIFIER", (String) jsonObject.get("encoding"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(2, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testIdentifierEncoded_ignoreEncryptPreference() {

		// encrypt false
		AccessTokenSpec tokenSpec = new AccessTokenSpec(0L, null, TokenEncoding.IDENTIFIER, Optional.of(false));
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
		
		// encrypt true
		tokenSpec = new AccessTokenSpec(0L, null, TokenEncoding.IDENTIFIER, Optional.of(true));
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testIdentifierEncoded_deprecatedConstructor()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(0L, null, TokenEncoding.IDENTIFIER, false);
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals("IDENTIFIER", (String) jsonObject.get("encoding"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(2, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testIdentifierEncodedWithAudience()
		throws Exception {

		List<Audience> audList = Arrays.asList(new Audience("A"), new Audience("B"));
		AccessTokenSpec tokenSpec = new AccessTokenSpec(0L, audList, TokenEncoding.IDENTIFIER, Optional.empty());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals("A", ((List) jsonObject.get("audience")).get(0));
		assertEquals("B", ((List)jsonObject.get("audience")).get(1));
		assertEquals(2, ((List)jsonObject.get("audience")).size());
		assertEquals("IDENTIFIER", (String) jsonObject.get("encoding"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(3, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testPairwiseSubjectType()
		throws Exception {

		List<Audience> audList = Arrays.asList(new Audience("A"), new Audience("B"));
		AccessTokenSpec tokenSpec = new AccessTokenSpec(0L, audList, TokenEncoding.IDENTIFIER, null, Optional.empty(), SubjectType.PAIRWISE);
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals("A", ((List) jsonObject.get("audience")).get(0));
		assertEquals("B", ((List)jsonObject.get("audience")).get(1));
		assertEquals(2, ((List)jsonObject.get("audience")).size());
		assertEquals("IDENTIFIER", (String) jsonObject.get("encoding"));
		assertEquals(SubjectType.PAIRWISE.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(3, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PAIRWISE, tokenSpec.getSubjectType());
	}


        @Test
        public void testPairwiseSubjectType_deprecatedConstructor()
		throws Exception {

		List<Audience> audList = Arrays.asList(new Audience("A"), new Audience("B"));
		AccessTokenSpec tokenSpec = new AccessTokenSpec(0L, audList, TokenEncoding.IDENTIFIER, null, false, SubjectType.PAIRWISE);
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals("A", ((List) jsonObject.get("audience")).get(0));
		assertEquals("B", ((List)jsonObject.get("audience")).get(1));
		assertEquals(2, ((List)jsonObject.get("audience")).size());
		assertEquals("IDENTIFIER", (String) jsonObject.get("encoding"));
		assertEquals(SubjectType.PAIRWISE.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(3, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(TokenEncoding.IDENTIFIER, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertEquals(SubjectType.PAIRWISE, tokenSpec.getSubjectType());
	}


        @Test
        public void testPairwiseSubjectType_requiresAudience() {

		String excMsg = null;
		try {
			new AccessTokenSpec(0L, null, TokenEncoding.IDENTIFIER, null, Optional.empty(), SubjectType.PAIRWISE);
			Assert.fail();
		} catch (IllegalArgumentException e) {
			excMsg = e.getMessage();
		}
		
		assertEquals("The pairwise token subject type requires an explicit token audience", excMsg);
	}


        @Test
        public void testPairwiseSubjectType_requiresAudience_deprecatedConstructor() {

		String excMsg = null;
		try {
			new AccessTokenSpec(0L, null, TokenEncoding.IDENTIFIER, null, false, SubjectType.PAIRWISE);
			Assert.fail();
		} catch (IllegalArgumentException e) {
			excMsg = e.getMessage();
		}
		
		assertEquals("The pairwise token subject type requires an explicit token audience", excMsg);
	}


        @Test
        public void testEncrypted()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(600L, null, TokenEncoding.SELF_CONTAINED, Optional.of(true));
		assertEquals(600L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		Assert.assertTrue(tokenSpec.encrypt());
		Assert.assertTrue(tokenSpec.getEncryptSelfContained().get());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals(600L, ((Long)jsonObject.get("lifetime")).longValue());
		assertEquals("SELF_CONTAINED", (String) jsonObject.get("encoding"));
		Assert.assertTrue((Boolean) jsonObject.get("encrypt"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(4, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertNull(tokenSpec.getAudience());
		Assert.assertTrue(tokenSpec.encrypt());
		Assert.assertTrue(tokenSpec.getEncryptSelfContained().get());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testEncrypted_deprecatedConstructor()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(600L, null, TokenEncoding.SELF_CONTAINED, true);
		assertEquals(600L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		Assert.assertTrue(tokenSpec.encrypt());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals(600L, ((Long)jsonObject.get("lifetime")).longValue());
		assertEquals("SELF_CONTAINED", (String) jsonObject.get("encoding"));
		Assert.assertTrue((Boolean) jsonObject.get("encrypt"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(4, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertNull(tokenSpec.getAudience());
		Assert.assertTrue(tokenSpec.encrypt());
		Assert.assertTrue(tokenSpec.getEncryptSelfContained().get());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testEncryptedWithAudience()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(600L, Collections.singletonList(new Audience("A")), TokenEncoding.SELF_CONTAINED, Optional.of(true));
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(Collections.singletonList(new Audience("A")), tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		Assert.assertTrue(tokenSpec.encrypt());
		Assert.assertTrue(tokenSpec.getEncryptSelfContained().get());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals(600L, ((Long)jsonObject.get("lifetime")).longValue());
		assertEquals("A", ((List)jsonObject.get("audience")).get(0));
		assertEquals(1, ((List)jsonObject.get("audience")).size());
		assertEquals("SELF_CONTAINED", (String) jsonObject.get("encoding"));
		Assert.assertTrue((Boolean) jsonObject.get("encrypt"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(5, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertEquals(Collections.singletonList(new Audience("A")), tokenSpec.getAudience());
		Assert.assertTrue(tokenSpec.encrypt());
		Assert.assertTrue(tokenSpec.getEncryptSelfContained().get());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testEncryptedWithAudience_deprecatedConstructor()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(600L, Collections.singletonList(new Audience("A")), TokenEncoding.SELF_CONTAINED, true);
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(Collections.singletonList(new Audience("A")), tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		Assert.assertTrue(tokenSpec.encrypt());
		Assert.assertTrue(tokenSpec.getEncryptSelfContained().get());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertEquals(600L, ((Long)jsonObject.get("lifetime")).longValue());
		assertEquals("A", ((List)jsonObject.get("audience")).get(0));
		assertEquals(1, ((List)jsonObject.get("audience")).size());
		assertEquals("SELF_CONTAINED", (String) jsonObject.get("encoding"));
		Assert.assertTrue((Boolean) jsonObject.get("encrypt"));
		assertEquals(SubjectType.PUBLIC.toString().toUpperCase(), jsonObject.get("sub_type"));
		assertEquals(5, jsonObject.size());
		tokenSpec = AccessTokenSpec.parse(JSONObjectUtils.parse(jsonObject.toJSONString()));
		assertEquals(600L, tokenSpec.getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertEquals(Collections.singletonList(new Audience("A")), tokenSpec.getAudience());
		Assert.assertTrue(tokenSpec.encrypt());
		Assert.assertTrue(tokenSpec.getEncryptSelfContained().get());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testParseDefault()
		throws Exception {

		JSONObject jsonObject = new JSONObject();
		AccessTokenSpec tokenSpec = AccessTokenSpec.parse(jsonObject);
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testWithActor()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(900L, null, TokenEncoding.SELF_CONTAINED, new Subject("claire"), Optional.empty(), SubjectType.PUBLIC);
		assertEquals(900L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertEquals(new Subject("claire"), tokenSpec.getImpersonatedSubject());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());

		JSONObject jsonObject = JSONObjectUtils.parse(tokenSpec.toJSONObject().toJSONString());

		assertEquals(900L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals("SELF_CONTAINED", JSONObjectUtils.getString(jsonObject, "encoding"));
		assertEquals("claire", JSONObjectUtils.getString(jsonObject, "impersonated_sub"));
		assertEquals(SubjectType.PUBLIC.name(), JSONObjectUtils.getString(jsonObject, "sub_type"));
		assertEquals(4, jsonObject.size());

		tokenSpec = AccessTokenSpec.parse(jsonObject);

		assertEquals(900L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertEquals(new Subject("claire"), tokenSpec.getImpersonatedSubject());
		assertFalse(tokenSpec.encrypt());
		assertFalse(tokenSpec.getEncryptSelfContained().isPresent());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}


        @Test
        public void testWithActor_deprecatedConstructor()
		throws Exception {

		AccessTokenSpec tokenSpec = new AccessTokenSpec(900L, null, TokenEncoding.SELF_CONTAINED, new Subject("claire"), false);
		assertEquals(900L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertEquals(new Subject("claire"), tokenSpec.getImpersonatedSubject());
		assertFalse(tokenSpec.encrypt());

		JSONObject jsonObject = JSONObjectUtils.parse(tokenSpec.toJSONObject().toJSONString());

		assertEquals(900L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals("SELF_CONTAINED", JSONObjectUtils.getString(jsonObject, "encoding"));
		assertEquals("claire", JSONObjectUtils.getString(jsonObject, "impersonated_sub"));
		assertFalse(JSONObjectUtils.getBoolean(jsonObject, "encrypt"));

		tokenSpec = AccessTokenSpec.parse(jsonObject);

		assertEquals(900L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, tokenSpec.getEncoding());
		assertEquals(new Subject("claire"), tokenSpec.getImpersonatedSubject());
		assertFalse(tokenSpec.encrypt());
		assertEquals(SubjectType.PUBLIC, tokenSpec.getSubjectType());
	}
}

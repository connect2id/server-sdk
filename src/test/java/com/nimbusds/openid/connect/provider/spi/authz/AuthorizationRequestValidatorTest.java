package com.nimbusds.openid.connect.provider.spi.authz;


import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.ResponseType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.State;
import com.nimbusds.openid.connect.sdk.AuthenticationRequest;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.op.ReadOnlyOIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class AuthorizationRequestValidatorTest {
	
	
	private static final Issuer ISSUER = new Issuer("https://c2id.com");
	
	
	/**
	 * Sample validator that checks if the received authorisation request
	 * scope values are present in the OAuth 2.0 client registration.
	 */
	static class ScopeValidator implements AuthorizationRequestValidator {
		
		
		@Override
		public boolean isEnabled() {
			return true;
		}
		
		
		@Override
		public AuthorizationRequest validateAuthorizationRequest(final AuthorizationRequest authzRequest,
									 final ValidatorContext validatorCtx)
			throws InvalidAuthorizationRequestException {
			
			assertEquals(ISSUER, validatorCtx.getIssuer());
			
			assertEquals(ISSUER, validatorCtx.getReadOnlyOIDCProviderMetadata().getIssuer());
			
			OIDCClientInformation clientInfo = validatorCtx.getOIDCClientInformation();
			
			assertNotNull(validatorCtx.getRawRequest());
			
			if (clientInfo.getMetadata().getScope() == null || ! clientInfo.getMetadata().getScope().containsAll(authzRequest.getScope())) {
				
				Scope unacceptedScope = new Scope(authzRequest.getScope());
				unacceptedScope.removeAll(clientInfo.getMetadata().getScope());
				
				String msg = "Scope not accepted: " + unacceptedScope;
				
				throw new InvalidAuthorizationRequestException(
					msg, // will be logged
					OAuth2Error.INVALID_SCOPE.setDescription(msg),
					false // redirection not disabled
				);
			}
			
			return authzRequest;
		}
	}
	
	
	private static OIDCClientInformation createOIDCClientInformation() {
		OIDCClientMetadata metadata = new OIDCClientMetadata();
		metadata.setRedirectionURI(URI.create("https://example.com/cb"));
		metadata.setScope(new Scope("openid", "email", "profile"));
		metadata.applyDefaults();
		return new OIDCClientInformation(
			new ClientID("123"),
			new Date(),
			metadata,
			new Secret());
	}


        @Test
        public void testUsage_pass() throws InvalidAuthorizationRequestException {
		
		OIDCClientInformation clientInfo = createOIDCClientInformation();
		
		AuthorizationRequest authzRequest = new AuthenticationRequest.Builder(
			new ResponseType("code"),
			new Scope("openid", "email"),
			clientInfo.getID(),
			clientInfo.getMetadata().getRedirectionURI())
			.state(new State())
			.build();
		
		AuthorizationRequestValidator validator = new ScopeValidator();
		Assert.assertTrue(validator.isEnabled());
		
		AuthorizationRequest validated = validator.validateAuthorizationRequest(
			authzRequest,
			new ValidatorContext() {
				
				@Override
				public ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata() {
					return new OIDCProviderMetadata(
						ISSUER,
						Collections.singletonList(SubjectType.PUBLIC),
						URI.create(ISSUER + "/jwks.json"));
				}
				
				
				@Override
				public OIDCClientInformation getOIDCClientInformation() {
					return clientInfo;
				}
				
				
				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
				
				
				@Override
				public AuthorizationRequest getRawRequest() {
					return authzRequest;
				}
			}
		);
		
		assertEquals(validated, authzRequest);
	}


        @Test
        public void testUsage_fail() {
		
		OIDCClientInformation clientInfo = createOIDCClientInformation();
		
		AuthorizationRequest authzRequest = new AuthenticationRequest.Builder(
			new ResponseType("code"),
			new Scope("openid", "email", "address"),
			clientInfo.getID(),
			clientInfo.getMetadata().getRedirectionURI())
			.state(new State())
			.build();
		
		AuthorizationRequestValidator validator = new ScopeValidator();
		Assert.assertTrue(validator.isEnabled());
		
		try {
			validator.validateAuthorizationRequest(
				authzRequest,
				new ValidatorContext() {
					
					@Override
					public ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata() {
						return new OIDCProviderMetadata(
							ISSUER,
							Collections.singletonList(SubjectType.PUBLIC),
							URI.create(ISSUER + "/jwks.json"));
					}
					
					@Override
					public OIDCClientInformation getOIDCClientInformation() {
						return clientInfo;
					}
					
					
					@Override
					public Issuer getIssuer() {
					return ISSUER;
				}
					
					
					@Override
					public AuthorizationRequest getRawRequest() {
						return authzRequest;
					}
				}
			);
			Assert.fail();
		} catch (InvalidAuthorizationRequestException e) {
			assertEquals("Scope not accepted: address", e.getMessage());
			assertEquals(OAuth2Error.INVALID_SCOPE, e.getErrorObject());
			assertEquals("Scope not accepted: address", e.getErrorObject().getDescription());
			Assert.assertNull(e.getErrorObject().getURI());
			Assert.assertFalse(e.isRedirectDisabled());
		}
	}
}

package com.nimbusds.openid.connect.provider.spi.reg;


import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class WrappedHTTPResponseExceptionTest {


        @Test
        public void testConstructor() {
		
		var httpResponse = new HTTPResponse(HTTPResponse.SC_UNAUTHORIZED);
		
		var exception = new WrappedHTTPResponseException(null, httpResponse);
		assertNull(exception.getMessage());
		assertEquals(httpResponse, exception.getHTTPResponse());
	}


        @Test
        public void testConstructor_withMessage() {
		
		var message = "Bad software statement JWT";
		var httpResponse = new HTTPResponse(HTTPResponse.SC_UNAUTHORIZED);
		
		var exception = new WrappedHTTPResponseException(message, httpResponse);
		assertEquals(message, exception.getMessage());
		assertEquals(httpResponse, exception.getHTTPResponse());
	}


        @Test(expected = NullPointerException.class)
        public void testHTTPResponseMustBeSet() {
		new WrappedHTTPResponseException(null, null);
	}
}

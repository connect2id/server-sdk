package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.minidev.json.JSONObject;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Tests the preset claims spec.
 */
public class PresetClaimsTest {


        @Test
        public void testDefaultConstructor()
		throws ParseException {

		var presetClaims = new PresetClaims();

		assertTrue(presetClaims.isEmpty());

		assertNull(presetClaims.getPresetIDTokenClaims());
		assertNull(presetClaims.getPresetUserInfoClaims());

		JSONObject o = presetClaims.toJSONObject();

		assertTrue(o.isEmpty());

		presetClaims = PresetClaims.parse(o);

		assertTrue(presetClaims.isEmpty());

		assertNull(presetClaims.getPresetIDTokenClaims());
		assertNull(presetClaims.getPresetUserInfoClaims());
	}


        @Test
        public void testParamConstructor()
		throws ParseException {

		var idTokenClaims = new JSONObject();
		idTokenClaims.put("login_ip", "10.20.30.40");

		var userInfoClaims = new JSONObject();
		userInfoClaims.put("name", "Alice");

		var presetClaims = new PresetClaims(idTokenClaims, userInfoClaims);
		assertFalse(presetClaims.isEmpty());
		assertEquals(idTokenClaims, presetClaims.getPresetIDTokenClaims());
		assertEquals(userInfoClaims, presetClaims.getPresetUserInfoClaims());

		JSONObject o = presetClaims.toJSONObject();

		idTokenClaims = JSONObjectUtils.getJSONObject(o, "id_token");
		assertEquals("10.20.30.40", idTokenClaims.get("login_ip"));
		assertEquals(1, idTokenClaims.size());

		userInfoClaims = JSONObjectUtils.getJSONObject(o, "userinfo");
		assertEquals("Alice", userInfoClaims.get("name"));
		assertEquals(1, userInfoClaims.size());

		presetClaims = PresetClaims.parse(o);

		assertFalse(presetClaims.isEmpty());

		assertEquals("10.20.30.40", presetClaims.getPresetIDTokenClaims().get("login_ip"));
		assertEquals(1, presetClaims.getPresetIDTokenClaims().size());

		assertEquals("Alice", presetClaims.getPresetUserInfoClaims().get("name"));
		assertEquals(1, presetClaims.getPresetUserInfoClaims().size());
	}


        @Test
        public void testToString()
		throws ParseException {

		var idTokenClaims = new JSONObject();
		idTokenClaims.put("login_ip", "10.20.30.40");

		var userInfoClaims = new JSONObject();
		userInfoClaims.put("name", "Alice");

		var presetClaims = new PresetClaims(idTokenClaims, userInfoClaims);

		presetClaims = PresetClaims.parse(JSONObjectUtils.parse(presetClaims.toString()));

		assertEquals("10.20.30.40", presetClaims.getPresetIDTokenClaims().get("login_ip"));
		assertEquals(1, presetClaims.getPresetIDTokenClaims().size());

		assertEquals("Alice", presetClaims.getPresetUserInfoClaims().get("name"));
		assertEquals(1, presetClaims.getPresetUserInfoClaims().size());
	}


        @Test
        public void testEmpty()
		throws ParseException {

		var presetClaims = new PresetClaims(new JSONObject(), new JSONObject());
		assertTrue(presetClaims.isEmpty());
		assertTrue(presetClaims.getPresetIDTokenClaims().isEmpty());
		assertTrue(presetClaims.getPresetUserInfoClaims().isEmpty());

		String json = presetClaims.toJSONObject().toJSONString();

		assertEquals("{}", json);

		presetClaims = PresetClaims.parse(JSONObjectUtils.parse(json));
		assertTrue(presetClaims.isEmpty());
		assertNull(presetClaims.getPresetIDTokenClaims());
		assertNull(presetClaims.getPresetUserInfoClaims());
	}
}

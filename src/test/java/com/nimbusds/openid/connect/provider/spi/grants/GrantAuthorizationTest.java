package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;


public class GrantAuthorizationTest {


        @Test
        public void testMinimal()
		throws Exception {

		var basicAuthz = new GrantAuthorization(Scope.parse("read"));

		assertEquals(Scope.parse("read"), basicAuthz.getScope());
		assertNull(basicAuthz.getAccessTokenSpec().getAudience());
		assertNull(basicAuthz.getAudience());
		assertEquals(0L, basicAuthz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, basicAuthz.getAccessTokenSpec().getEncoding());
		assertFalse(basicAuthz.getAccessTokenSpec().encrypt());
		assertNull(basicAuthz.getData());

		String json = basicAuthz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parse(json);
		assertEquals(new Scope("read"), new Scope(JSONObjectUtils.getStringArray(jsonObject, "scope")));
		assertEquals("SELF_CONTAINED", JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("encoding"));
		assertEquals("PUBLIC", JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("sub_type"));
		assertEquals(2, JSONObjectUtils.getJSONObject(jsonObject, "access_token").size());
		assertEquals(ClaimsTransport.USERINFO, JSONObjectUtils.getEnum(jsonObject, "claims_transport", ClaimsTransport.class));

		assertEquals(3, jsonObject.size());

		basicAuthz = GrantAuthorization.parse(json);

		assertEquals(Scope.parse("read"), basicAuthz.getScope());
		assertNull(basicAuthz.getAccessTokenSpec().getAudience());
		assertNull(basicAuthz.getAudience());
		assertEquals(0L, basicAuthz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, basicAuthz.getAccessTokenSpec().getEncoding());
		assertFalse(basicAuthz.getAccessTokenSpec().encrypt());
		Assert.assertTrue(basicAuthz.getClaimsSpec().getNames().isEmpty());
		assertNull(basicAuthz.getClaimsSpec().getLocales());
		assertNull(basicAuthz.getClaimsSpec().getPresetClaims().getPresetIDTokenClaims());
		assertNull(basicAuthz.getClaimsSpec().getPresetClaims().getPresetUserInfoClaims());
		assertNull(basicAuthz.getClaimsSpec().getData());
		assertEquals(ClaimsTransport.USERINFO, basicAuthz.getClaimsSpec().getTransport());
		assertNull(basicAuthz.getData());
	}


        @Test
        public void testDeprecatedConstructorWithAudienceList()
		throws Exception {

		var scope = new Scope("openid", "email");

		List<Audience> audList = Arrays.asList(new Audience("1"), new Audience("2"));

		var accessTokenSpec = new AccessTokenSpec();

		JSONObject data = new JSONObject();
		data.put("realm", "internal");

		var authz = new GrantAuthorization(scope, audList, accessTokenSpec, data);
		
		assertEquals(scope, authz.getScope());
		
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(audList, authz.getAudience());
		assertEquals(accessTokenSpec.getLifetime(), authz.getAccessTokenSpec().getLifetime());
		assertEquals(accessTokenSpec.getEncoding(), authz.getAccessTokenSpec().getEncoding());
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(accessTokenSpec.encrypt(), authz.getAccessTokenSpec().encrypt());
		
		Assert.assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertNull(authz.getClaimsSpec().getLocales());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetIDTokenClaims());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetUserInfoClaims());
		assertNull(authz.getClaimsSpec().getData());
		assertEquals(ClaimsTransport.USERINFO, authz.getClaimsSpec().getTransport());
		
		assertEquals(data, authz.getData());

		String json = authz.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parse(json);

		authz = GrantAuthorization.parse(jsonObject);
		
		assertEquals(scope, authz.getScope());
		
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(audList, authz.getAudience());
		assertEquals(accessTokenSpec.getLifetime(), authz.getAccessTokenSpec().getLifetime());
		assertEquals(accessTokenSpec.getEncoding(), authz.getAccessTokenSpec().getEncoding());
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(accessTokenSpec.encrypt(), authz.getAccessTokenSpec().encrypt());
		
		Assert.assertTrue(authz.getClaimsSpec().getNames().isEmpty());
		assertNull(authz.getClaimsSpec().getLocales());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetIDTokenClaims());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetUserInfoClaims());
		assertNull(authz.getClaimsSpec().getData());
		assertEquals(ClaimsTransport.USERINFO, authz.getClaimsSpec().getTransport());
		
		assertEquals(data, authz.getData());
	}


        @Test
        public void testFull()
		throws Exception {
		
		var scope = new Scope("openid", "email");
		
		List<Audience> audList = Arrays.asList(new Audience("1"), new Audience("2"));
		var accessTokenSpec = new AccessTokenSpec(60L, audList, TokenEncoding.IDENTIFIER, Optional.empty());
		
		var claimsSpec = new ClaimsSpec(new HashSet<>(Arrays.asList("email", "email_verified")));
		
		var data = new JSONObject();
		data.put("realm", "internal");
		
		var authz = new GrantAuthorization(scope, accessTokenSpec, claimsSpec, data);
		
		assertEquals(scope, authz.getScope());
		
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(audList, authz.getAudience());
		assertEquals(accessTokenSpec.getLifetime(), authz.getAccessTokenSpec().getLifetime());
		assertEquals(accessTokenSpec.getEncoding(), authz.getAccessTokenSpec().getEncoding());
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(accessTokenSpec.encrypt(), authz.getAccessTokenSpec().encrypt());
		
		assertEquals(claimsSpec.getNames(), authz.getClaimsSpec().getNames());
		assertNull(authz.getClaimsSpec().getLocales());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetIDTokenClaims());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetUserInfoClaims());
		assertNull(authz.getClaimsSpec().getData());
		assertEquals(claimsSpec.getTransport(), authz.getClaimsSpec().getTransport());
		
		assertEquals(data, authz.getData());
		
		String json = authz.toJSONObject().toJSONString();
		
		JSONObject jsonObject = JSONObjectUtils.parse(json);
		
		authz = GrantAuthorization.parse(jsonObject);
		
		assertEquals(scope, authz.getScope());
		
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(audList, authz.getAudience());
		assertEquals(accessTokenSpec.getLifetime(), authz.getAccessTokenSpec().getLifetime());
		assertEquals(accessTokenSpec.getEncoding(), authz.getAccessTokenSpec().getEncoding());
		assertEquals(audList, authz.getAccessTokenSpec().getAudience());
		assertEquals(accessTokenSpec.encrypt(), authz.getAccessTokenSpec().encrypt());
		
		assertEquals(claimsSpec.getNames(), authz.getClaimsSpec().getNames());
		assertNull(authz.getClaimsSpec().getLocales());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetIDTokenClaims());
		assertNull(authz.getClaimsSpec().getPresetClaims().getPresetUserInfoClaims());
		assertNull(authz.getClaimsSpec().getData());
		assertEquals(claimsSpec.getTransport(), authz.getClaimsSpec().getTransport());
		
		assertEquals(data, authz.getData());
	}


        @Test
        public void testFullAlt()
		throws Exception {

		var basicAuthz = new GrantAuthorization(
			Scope.parse("read write"),
			new AccessTokenSpec(600L, Arrays.asList(new Audience("a"), new Audience("b")), TokenEncoding.IDENTIFIER, false),
			new ClaimsSpec(),
			JSONObjectUtils.parse("{\"a\":\"b\"}"));

		assertEquals(Scope.parse("read write"), basicAuthz.getScope());
		assertEquals(Arrays.asList(new Audience("a"), new Audience("b")), basicAuthz.getAccessTokenSpec().getAudience());
		assertEquals(600L, basicAuthz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.IDENTIFIER, basicAuthz.getAccessTokenSpec().getEncoding());
		assertFalse(basicAuthz.getAccessTokenSpec().encrypt());
		assertEquals("b", basicAuthz.getData().get("a"));
		assertEquals(1, basicAuthz.getData().size());
		
		assertTrue(basicAuthz.getClaimsSpec().getNames().isEmpty());
		assertNull(basicAuthz.getClaimsSpec().getLocales());
		assertNull(basicAuthz.getClaimsSpec().getPresetClaims().getPresetIDTokenClaims());
		assertNull(basicAuthz.getClaimsSpec().getPresetClaims().getPresetUserInfoClaims());
		assertNull(basicAuthz.getClaimsSpec().getData());
		assertEquals(ClaimsTransport.USERINFO, basicAuthz.getClaimsSpec().getTransport());

		String json = basicAuthz.toJSONObject().toJSONString();

		// {"scope":["read","write"],"audience":["a","b"],"access_token":{"lifetime":600,"encoding":"IDENTIFIER"},"data":{"a":"b"}}

		JSONObject jsonObject = JSONObjectUtils.parse(json);
		assertEquals(Scope.parse("read write"), new Scope(JSONObjectUtils.getStringArray(jsonObject, "scope")));
		assertEquals("a", JSONObjectUtils.getJSONArray(jsonObject, "audience").get(0));
		assertEquals("b", JSONObjectUtils.getJSONArray(jsonObject, "audience").get(1));
		assertEquals(2, JSONObjectUtils.getJSONArray(jsonObject, "audience").size());
		assertEquals("IDENTIFIER", JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("encoding"));
		assertNull(JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("encrypt"));
		assertEquals(ClaimsTransport.USERINFO, JSONObjectUtils.getEnum(jsonObject, "claims_transport", ClaimsTransport.class));
		assertEquals(5, jsonObject.size());

		basicAuthz = GrantAuthorization.parse(json);
		
		assertEquals(Scope.parse("read write"), basicAuthz.getScope());
		assertEquals(Arrays.asList(new Audience("a"), new Audience("b")), basicAuthz.getAccessTokenSpec().getAudience());
		assertEquals(600L, basicAuthz.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.IDENTIFIER, basicAuthz.getAccessTokenSpec().getEncoding());
		assertFalse(basicAuthz.getAccessTokenSpec().encrypt());
		assertEquals("b", basicAuthz.getData().get("a"));
		assertEquals(1, basicAuthz.getData().size());
		
		assertTrue(basicAuthz.getClaimsSpec().getNames().isEmpty());
		assertNull(basicAuthz.getClaimsSpec().getLocales());
		assertNull(basicAuthz.getClaimsSpec().getPresetClaims().getPresetIDTokenClaims());
		assertNull(basicAuthz.getClaimsSpec().getPresetClaims().getPresetUserInfoClaims());
		assertNull(basicAuthz.getClaimsSpec().getData());
		assertEquals(ClaimsTransport.USERINFO, basicAuthz.getClaimsSpec().getTransport());
	}


        @Test(expected = NullPointerException.class)
        public void testRejectNullScope() {
		new GrantAuthorization(null, new AccessTokenSpec(), new ClaimsSpec(), null);
	}


        @Test
        public void testAllowEmptyScope() {

		var basicAuthz = new GrantAuthorization(new Scope(), new AccessTokenSpec(),  new ClaimsSpec(), null);
		assertTrue(basicAuthz.getScope().isEmpty());
	}


        @Test(expected = NullPointerException.class)
        public void testRejectNullAccessTokenSpec() {
		new GrantAuthorization(new Scope(), null, null);
	}


        @Test(expected = NullPointerException.class)
        public void testRejectNullClaimsSpec() {
		new GrantAuthorization(new Scope(), new AccessTokenSpec(), null, null);
	}
}

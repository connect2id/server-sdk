package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;


public class SubjectAuthorizationTest {


        @Test
        public void testMinimal()
		throws Exception {

		var a = new SubjectAuthorization(
			new Subject("alice"),
			new Scope("openid"),
			new AccessTokenSpec(),
			new IDTokenSpec(),
			new ClaimsSpec(),
			null);

		assertEquals("alice", a.getSubject().getValue());
		assertEquals("openid", a.getScope().toString());
		assertEquals(0, a.getAccessTokenSpec().getLifetime());
		assertNull(a.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, a.getAccessTokenSpec().getEncoding());
		assertFalse(a.getAccessTokenSpec().encrypt());
		assertEquals(SubjectType.PUBLIC, a.getAccessTokenSpec().getSubjectType());
		assertNull(a.getAudience());
		assertFalse(a.getIDTokenSpec().issue());
		assertEquals(0, a.getIDTokenSpec().getLifetime());
		assertNull(a.getIDTokenSpec().getAudience());
		assertNull(a.getIDTokenSpec().getAuthTime());
		assertNull(a.getIDTokenSpec().getACR());
		assertNull(a.getIDTokenSpec().getAMRList());
		assertNull(a.getIDTokenSpec().getImpersonatedSubject());
		assertNull(a.getAuthTime());
		assertNull(a.getACR());
		assertNull(a.getAMRList());
		assertTrue(a.getClaimsSpec().getNames().isEmpty());
		assertEquals(ClaimsTransport.USERINFO, a.getClaimsSpec().getTransport());
		assertNull(a.getClaimsSpec().getLocales());
		assertTrue(a.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(a.getData());

		String json = a.toJSONObject().toJSONString();

		JSONObject jsonObject = JSONObjectUtils.parse(json);
		assertEquals("alice", jsonObject.get("sub"));
		assertEquals("openid", ((List)jsonObject.get("scope")).get(0));
		assertEquals(1, ((List)jsonObject.get("scope")).size());
		assertEquals("SELF_CONTAINED", ((JSONObject) jsonObject.get("access_token")).get("encoding"));
		assertEquals("PUBLIC", ((JSONObject) jsonObject.get("access_token")).get("sub_type"));
		assertEquals(2, ((JSONObject) jsonObject.get("access_token")).size());
		assertEquals(4, jsonObject.size());

		// { "sub":"alice",
		// "scope":["openid"],
		// "access_token":{"encrypt":false,"encoding":"SELF_CONTAINED"},
		// "claims_transport":"USERINFO"}

		a = SubjectAuthorization.parse(json);

		assertEquals("alice", a.getSubject().getValue());
		assertEquals("openid", a.getScope().toString());
		assertEquals(0, a.getAccessTokenSpec().getLifetime());
		assertNull(a.getAccessTokenSpec().getAudience());
		assertEquals(TokenEncoding.SELF_CONTAINED, a.getAccessTokenSpec().getEncoding());
		assertFalse(a.getAccessTokenSpec().encrypt());
		assertEquals(SubjectType.PUBLIC, a.getAccessTokenSpec().getSubjectType());
		assertNull(a.getAudience());
		assertFalse(a.getIDTokenSpec().issue());
		assertEquals(0, a.getIDTokenSpec().getLifetime());
		assertNull(a.getIDTokenSpec().getAudience());
		assertNull(a.getIDTokenSpec().getAuthTime());
		assertNull(a.getIDTokenSpec().getACR());
		assertNull(a.getIDTokenSpec().getAMRList());
		assertNull(a.getIDTokenSpec().getImpersonatedSubject());
		assertNull(a.getAuthTime());
		assertNull(a.getACR());
		assertNull(a.getAMRList());
		assertTrue(a.getClaimsSpec().getNames().isEmpty());
		assertEquals(ClaimsTransport.USERINFO, a.getClaimsSpec().getTransport());
		assertNull(a.getClaimsSpec().getLocales());
		assertTrue(a.getClaimsSpec().getPresetClaims().isEmpty());
		assertNull(a.getData());
	}


        @Test
        public void testFull()
		throws Exception {

		final Date TIMESTAMP = new Date(1234 * 1000L);

		var a = new SubjectAuthorization(
			new Subject("alice"),
			new Scope("openid", "email"),
			new AccessTokenSpec(600L, new Audience("a").toSingleAudienceList(), TokenEncoding.SELF_CONTAINED, null, true, SubjectType.PAIRWISE),
			new IDTokenSpec(true, 900L, TIMESTAMP, new ACR("0"), Collections.singletonList(new AMR("pwd")), new Subject("bob")),
			new ClaimsSpec(new HashSet<>(Arrays.asList("email", "email_verified"))),
			JSONObjectUtils.parse("{\"a\":\"b\"}"));

		assertEquals("alice", a.getSubject().getValue());
		assertEquals(TIMESTAMP, a.getAuthTime());
		assertEquals(new ACR("0"), a.getACR());
		assertEquals("pwd", a.getAMRList().get(0).getValue());
		assertEquals(1, a.getAMRList().size());
		assertEquals("openid email", a.getScope().toString());
		assertEquals(new Audience("a").toSingleAudienceList(), a.getAudience());
		assertEquals(600L, a.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, a.getAccessTokenSpec().getEncoding());
		assertTrue(a.getAccessTokenSpec().encrypt());
		assertEquals(SubjectType.PAIRWISE, a.getAccessTokenSpec().getSubjectType());
		assertTrue(a.getIDTokenSpec().issue());
		assertEquals("bob", a.getIDTokenSpec().getImpersonatedSubject().getValue());
		assertEquals(new LinkedHashSet<>(Arrays.asList("email", "email_verified")), a.getClaimsSpec().getNames());
		assertNull(a.getClaimsSpec().getLocales());
		assertTrue(a.getClaimsSpec().getPresetClaims().isEmpty());
		assertEquals("b", a.getData().get("a"));
		assertEquals(1, a.getData().size());

		String json = a.toJSONObject().toJSONString();

		// {
		// "sub":"alice",
		// "scope":["openid email"],
		// "auth_time":1234,
		// "acr":"0",
		// "amr":["pwd"],
		// "audience":["a"],
		// "access_token":{"lifetime":600,"encrypt":true,"encoding":"SELF_CONTAINED"},
		// "claims":["email","email_verified"],
		// "claims_transport":"USERINFO",
		// "id_token":{"impersonated_sub":"bob","lifetime":900,"issue":true},
		// "data":{"a":"b"}
		// }

		JSONObject jsonObject = JSONObjectUtils.parse(json);
		assertEquals("alice", jsonObject.get("sub"));
		assertEquals("openid", JSONObjectUtils.getStringList(jsonObject, "scope").get(0));
		assertEquals("email", JSONObjectUtils.getStringList(jsonObject, "scope").get(1));
		assertEquals(2, JSONObjectUtils.getStringList(jsonObject, "scope").size());
		assertEquals(1234L, JSONObjectUtils.getLong(jsonObject, "auth_time"));
		assertEquals("0", jsonObject.get("acr"));
		assertEquals("pwd", JSONObjectUtils.getStringList(jsonObject, "amr").get(0));
		assertEquals(1, JSONObjectUtils.getStringList(jsonObject, "amr").size());
		assertEquals(Collections.singletonList("a"), JSONObjectUtils.getStringList(jsonObject, "audience"));
		assertEquals(1, JSONObjectUtils.getStringList(jsonObject, "audience").size());
		assertEquals(600L, JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("lifetime"));
		assertEquals(Collections.singletonList("a"), JSONObjectUtils.getStringList(JSONObjectUtils.getJSONObject(jsonObject, "access_token"), ("audience")));
		assertEquals(true, JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("encrypt"));
		assertEquals("SELF_CONTAINED", JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("encoding"));
		assertEquals("PAIRWISE", JSONObjectUtils.getJSONObject(jsonObject, "access_token").get("sub_type"));
		assertEquals(5, JSONObjectUtils.getJSONObject(jsonObject, "access_token").size());
		assertTrue(JSONObjectUtils.getStringList(jsonObject, "claims").contains("email"));
		assertTrue(JSONObjectUtils.getStringList(jsonObject, "claims").contains("email_verified"));
		assertEquals(2, JSONObjectUtils.getStringList(jsonObject, "claims").size());
		assertEquals(true, JSONObjectUtils.getJSONObject(jsonObject, "id_token").get("issue"));
		assertEquals(900L, JSONObjectUtils.getJSONObject(jsonObject, "id_token").get("lifetime"));
		assertEquals("bob", JSONObjectUtils.getString(JSONObjectUtils.getJSONObject(jsonObject, "id_token"), "impersonated_sub"));
		assertEquals(3, JSONObjectUtils.getJSONObject(jsonObject, "id_token").size());
		assertEquals("USERINFO", jsonObject.get("claims_transport"));


		a = SubjectAuthorization.parse(json);

		assertEquals("alice", a.getSubject().getValue());
		assertEquals(TIMESTAMP, a.getAuthTime());
		assertEquals(new ACR("0"), a.getACR());
		assertEquals("pwd", a.getAMRList().get(0).getValue());
		assertEquals(1, a.getAMRList().size());
		assertEquals("openid email", a.getScope().toString());
		assertEquals(new Audience("a").toSingleAudienceList(), a.getAudience());
		assertEquals(600L, a.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, a.getAccessTokenSpec().getEncoding());
		assertTrue(a.getAccessTokenSpec().encrypt());
		assertEquals(SubjectType.PAIRWISE, a.getAccessTokenSpec().getSubjectType());
		assertTrue(a.getIDTokenSpec().issue());
		assertEquals("bob", a.getIDTokenSpec().getImpersonatedSubject().getValue());
		assertEquals(new HashSet<>(Arrays.asList("email", "email_verified")), a.getClaimsSpec().getNames());
		assertNull(a.getClaimsSpec().getLocales());
		assertTrue(a.getClaimsSpec().getPresetClaims().isEmpty());
		assertEquals("b", a.getData().get("a"));
		assertEquals(1, a.getData().size());
	}


        @Test(expected = NullPointerException.class)
        public void testRejectNullSubject() {

		new SubjectAuthorization(
			null, // subject
			new Scope("openid"),
			new AccessTokenSpec(),
			new IDTokenSpec(),
			new ClaimsSpec(),
			null);
	}


        @Test(expected = NullPointerException.class)
        public void testRejectNullScope() {

		new SubjectAuthorization(
			new Subject("alice"),
			null,
			new AccessTokenSpec(),
			new IDTokenSpec(),
			new ClaimsSpec(),
			null);
	}


        @Test(expected = NullPointerException.class)
        public void testRejectNullAccessTokenTokenSpec() {

		new SubjectAuthorization(
			new Subject("alice"),
			new Scope("openid"),
			null, // access_token spec
			new IDTokenSpec(),
			new ClaimsSpec(),
			null);
	}


        @Test(expected = NullPointerException.class)
        public void testRejectNullIDTokenTokenSpec() {

		new SubjectAuthorization(
			new Subject("alice"),
			new Scope("openid"),
			new AccessTokenSpec(),
			null, // id_token spec
			new ClaimsSpec(),
			null);
	}


        @Test(expected = NullPointerException.class)
        public void testRejectClaimsSpec() {

		new SubjectAuthorization(
			new Subject("alice"),
			new Scope("openid"),
			new AccessTokenSpec(),
			new IDTokenSpec(),
			null, // claims spec
			null);
	}
}

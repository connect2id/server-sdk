package com.nimbusds.openid.connect.provider.spi.secrets;


import com.nimbusds.jose.crypto.utils.ConstantTimeUtils;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.oauth2.sdk.auth.Secret;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.junit.Assert.*;


public class DecodedSecretTest {


        @Test
        public void testForPlainSecret() {
		
		var plain = "quahs8ua0ieGhoh5";
		var encoded = "CuiVohvahPho3boe";
		
		DecodedSecret decodedSecret = DecodedSecret.createForPlainSecret(plain);
		assertEquals(plain, decodedSecret.getValue());
		assertNull(decodedSecret.getExpirationDate());
		assertNull(decodedSecret.getEncodedValue());
		assertTrue(decodedSecret.equals(new Secret(plain)));
		assertFalse(decodedSecret.equals(new Secret("eiY2eijuheshie4e")));
		
		decodedSecret = decodedSecret.withEncodedValue(encoded);
		
		assertEquals(plain, decodedSecret.getValue());
		assertNull(decodedSecret.getExpirationDate());
		assertEquals(encoded, decodedSecret.getEncodedValue());
		assertTrue(decodedSecret.equals(new Secret(plain)));
		assertFalse(decodedSecret.equals(new Secret("eiY2eijuheshie4e")));
		
		Date expDate = Date.from(Instant.now().plus(1, ChronoUnit.HOURS));
		decodedSecret = decodedSecret.withExpiration(expDate);
		
		assertEquals(plain, decodedSecret.getValue());
		assertEquals(expDate, decodedSecret.getExpirationDate());
		assertEquals(encoded, decodedSecret.getEncodedValue());
		assertTrue(decodedSecret.equals(new Secret(plain)));
		assertFalse(decodedSecret.equals(new Secret("eiY2eijuheshie4e")));
	}


        @Test
        public void testForPlainSecret_rejectNullValue() {
		
		try {
			DecodedSecret.createForPlainSecret(null);
			Assert.fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The plain secret value must not be null or empty", e.getMessage());
		}
	}


        @Test
        public void testForPlainSecret_rejectEmptyValue() {
		
		try {
			DecodedSecret.createForPlainSecret("");
			Assert.fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The plain secret value must not be null or empty", e.getMessage());
		}
	}


        @Test
        public void testForHashedSecret() {
		
		var plain = "quahs8ua0ieGhoh5";
		var hashed = Base64URL.encode(new Secret(plain).getSHA256()).toString();
		
		DecodedSecret decodedSecret = DecodedSecret.createForHashedSecret(
			secret -> ConstantTimeUtils.areEqual(secret.getSHA256(), new Base64URL(hashed).decode())
		);
		
		assertNull(decodedSecret.getValue());
		assertNull(decodedSecret.getValueBytes());
		assertNull(decodedSecret.getExpirationDate());
		assertNull(decodedSecret.getEncodedValue());
		assertTrue(decodedSecret.equals(new Secret(plain)));
		assertFalse(decodedSecret.equals(new Secret("eiY2eijuheshie4e")));
		
		decodedSecret = decodedSecret.withEncodedValue(hashed);
		
		assertNull(decodedSecret.getValue());
		assertNull(decodedSecret.getValueBytes());
		assertNull(decodedSecret.getExpirationDate());
		assertEquals(hashed, decodedSecret.getEncodedValue());
		assertTrue(decodedSecret.equals(new Secret(plain)));
		assertFalse(decodedSecret.equals(new Secret("eiY2eijuheshie4e")));
		
		Date expDate = Date.from(Instant.now().plus(1, ChronoUnit.HOURS));
		decodedSecret = decodedSecret.withExpiration(expDate);
		
		assertNull(decodedSecret.getValue());
		assertNull(decodedSecret.getValueBytes());
		assertEquals(expDate, decodedSecret.getExpirationDate());
		assertEquals(hashed, decodedSecret.getEncodedValue());
		assertTrue(decodedSecret.equals(new Secret(plain)));
		assertFalse(decodedSecret.equals(new Secret("eiY2eijuheshie4e")));
	}


        @Test(expected = NullPointerException.class)
        public void testForHashedSecret_rejectNullVerifier() {
		DecodedSecret.createForHashedSecret(null);
	}
}

package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.dpop.JWKThumbprintConfirmation;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.oauth2.sdk.util.date.DateWithTimeZoneOffset;
import com.nimbusds.openid.connect.sdk.assurance.IdentityTrustFramework;
import com.nimbusds.openid.connect.sdk.assurance.IdentityVerification;
import com.nimbusds.openid.connect.sdk.assurance.VerificationProcess;
import com.nimbusds.openid.connect.sdk.assurance.evidences.QESEvidence;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.sql.Date;
import java.text.ParseException;
import java.time.Instant;
import java.util.*;

import static org.junit.Assert.*;


public class BaseSelfContainedAccessTokenClaimsCodecTest {


        @Test
        public void testSupportedClaimNamesConstant() {
		
		assertEquals(Set.of("sub", "act", "exp", "iat", "iss", "aud", "jti", "cnf"), BaseSelfContainedAccessTokenClaimsCodec.SUPPORTED_CLAIM_NAMES);
	}


        @Test
        public void testCodecMinimal()
		throws TokenDecodeException {
		
		SelfContainedAccessTokenClaimsCodec codec = new ConcreteBaseSelfContainedAccessTokenCodec();
		
		AccessTokenAuthorization authz = new MutableAccessTokenAuthorization();
		
		JWTDetails jwtDetails = codec.advancedEncode(authz, new MockTokenEncoderContext());
		
		JWTClaimsSet jwtClaimsSet = jwtDetails.getJWTClaimsSet();
		assertNull(jwtDetails.getType());
		
		assertTrue(jwtClaimsSet.toJSONObject().isEmpty());
		
		authz = codec.advancedDecode(jwtDetails, new MockTokenCodecContext());
		
		assertNull(authz.getSubject());
		assertNull(authz.getActor());
		assertNull(authz.getClientID());
		assertNull(authz.getScope());
		assertNull(authz.getExpirationTime());
		assertNull(authz.getIssueTime());
		assertNull(authz.getIssuer());
		assertNull(authz.getAudienceList());
		assertNull(authz.getJWTID());
		assertNull(authz.getClaimNames());
		assertNull(authz.getClaimsLocales());
		assertNull(authz.getPresetClaims());
		assertNull(authz.getData());
		assertNull(authz.getClientCertificateConfirmation());
		assertNull(authz.getJWKThumbprintConfirmation());
		assertNull(authz.getOtherTopLevelParameters());
		assertNull(authz.getClaimsData());
		assertNull(authz.getSubjectSessionKey());
	}


        @Test
        public void testCodecFull()
		throws TokenDecodeException, LangTagException, ParseException, JOSEException {
		
		SelfContainedAccessTokenClaimsCodec codec = new ConcreteBaseSelfContainedAccessTokenCodec();
		
		var in = new MutableAccessTokenAuthorization();
		
		var sub = new Subject("alice");
		var act = new Actor(new Subject("bob"));
		var clientID = new ClientID("123");
		var scope = new Scope("openid", "email");
		Instant exp = Instant.ofEpochSecond(123000L);
		Instant iat = Instant.ofEpochSecond(100000L);
		var iss = new Issuer("https://c2id.com");
		List<Audience> audList = new Audience("resource-server-1").toSingleAudienceList();
		var jti = new JWTID();
		Set<String> claimNames = new HashSet<>(Arrays.asList("email", "email_verified"));
		List<LangTag> claimsLocales = Arrays.asList(LangTag.parse("en-GB"), LangTag.parse("de-DE"));
		var presetClaims = new JSONObject();
		presetClaims.put("name", "Alice Adams");
		var sessionKey = "WYqFXK7Q4HFnJv0hiT3Fgw";
		var data = new JSONObject();
		data.put("ip", "192.168.0.1");
		var cnfX5t = new X509CertificateConfirmation(SampleX509Certificate.X5T_SHA256);
		var cnfJkt = new JWKThumbprintConfirmation(SampleKeys.RSA_JWK_2048.computeThumbprint());
		Map<String,Object> otherTopLevelParams = new HashMap<>();
		otherTopLevelParams.put("patientId", "p123");
		otherTopLevelParams.put("hospitalId", "h123");
		var verification = new IdentityVerification(
			IdentityTrustFramework.EIDAS_IAL_SUBSTANTIAL,
			new DateWithTimeZoneOffset(new java.util.Date()),
			new VerificationProcess(UUID.randomUUID().toString()),
			new QESEvidence(
				new Issuer("https://eidas.c2id.com"),
				UUID.randomUUID().toString(),
				new DateWithTimeZoneOffset(new java.util.Date())
			)
		);
		
		in.withSubject(sub)
			.withActor(act)
			.withClientID(clientID)
			.withScope(scope)
			.withExpirationTime(exp)
			.withIssueTime(iat)
			.withIssuer(iss)
			.withAudienceList(audList)
			.withJWTID(jti)
			.withClaimNames(claimNames)
			.withClaimsLocales(claimsLocales)
			.withPresetClaims(presetClaims)
			.withClaimsData(verification.toJSONObject())
			.withSubjectSessionkey(sessionKey)
			.withData(data)
			.withClientCertificateConfirmation(cnfX5t)
			.withJWKThumbprintConfirmation(cnfJkt)
			.withOtherTopLevelParameters(otherTopLevelParams);
		
		JWTDetails jwtDetails = codec.advancedEncode(in, new MockTokenEncoderContext());
		
		JWTClaimsSet jwtClaimsSet = jwtDetails.getJWTClaimsSet();
		assertNull(jwtDetails.getType());
		
		assertTrue(jwtClaimsSet.toJSONObject().keySet().containsAll(BaseSelfContainedAccessTokenClaimsCodec.SUPPORTED_CLAIM_NAMES));
		
		assertEquals(sub.getValue(), jwtClaimsSet.getSubject());
		assertEquals("bob", jwtClaimsSet.getJSONObjectClaim("act").get("sub"));
		assertEquals(1, jwtClaimsSet.getJSONObjectClaim("act").size());
		assertEquals(Date.from(exp), jwtClaimsSet.getExpirationTime());
		assertEquals(Date.from(iat), jwtClaimsSet.getIssueTime());
		assertEquals(iss.getValue(), jwtClaimsSet.getIssuer());
		assertEquals(Collections.singletonList(audList.get(0).getValue()), jwtClaimsSet.getAudience());
		assertEquals(jti.getValue(), jwtClaimsSet.getJWTID());
		assertEquals(cnfX5t, X509CertificateConfirmation.parse(jwtClaimsSet));
		assertEquals(cnfJkt, JWKThumbprintConfirmation.parse(jwtClaimsSet));
		assertEquals(8, jwtClaimsSet.toJSONObject().size());
		
		AccessTokenAuthorization out = codec.advancedDecode(jwtDetails, new MockTokenCodecContext());
		
		assertEquals(sub, out.getSubject());
		assertEquals(act, out.getActor());
		assertNull(out.getClientID());
		assertNull(out.getScope());
		assertEquals(exp, out.getExpirationTime());
		assertEquals(iat, out.getIssueTime());
		assertEquals(iss, out.getIssuer());
		assertEquals(audList, out.getAudienceList());
		assertEquals(jti, out.getJWTID());
		assertNull(out.getClaimNames());
		assertNull(out.getClaimsLocales());
		assertNull(out.getPresetClaims());
		assertNull(out.getSubjectSessionKey());
		assertNull(out.getData());
		assertEquals(cnfX5t, out.getClientCertificateConfirmation());
		assertEquals(cnfJkt, out.getJWKThumbprintConfirmation());
		assertNull(out.getOtherTopLevelParameters());
	}
}

package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.oauth2.sdk.token.AccessTokenType;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSource;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;
import com.nimbusds.openid.connect.provider.spi.tokens.MutableAccessTokenAuthorization;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class DefaultTokenIntrospectionResponseComposerTest {
	
	
	static class MockContext implements TokenIntrospectionContext {


		@Override
		public Issuer getIssuer() {
			return null;
		}

		@Override
		public OIDCClientInformation getOIDCClientInformation() {
			return null;
		}


		@Override
		public @Nullable OIDCClientInformation getOIDCClientInformation(ClientID clientID) {
			return null;
		}

		@Override
		public ClaimsSource getClaimsSource() {
			return null;
		}


		@Override
		public SubjectSession getSubjectSession() {
			return null;
		}
	}


        @Test
        public void testComposeEmptyFictional() {
		
		TokenIntrospectionSuccessResponse response = new DefaultTokenIntrospectionResponseComposer().compose(new MutableAccessTokenAuthorization(), new MockContext());
		
		assertTrue((Boolean)response.toJSONObject().get("active"));
		assertEquals("Bearer", response.toJSONObject().get("token_type"));
		assertEquals(2, response.toJSONObject().size());
	}


        @Test
        public void testComposeWithAllParams()
		throws Exception {
		
		var uip = new JSONObject();
		uip.put("group", "admin");
		
		var dat = new JSONObject();
		dat.put("ip", "10.20.30.40");
		
		var oth = new JSONObject();
		oth.put("patientId", "p123");
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withIssuer(new Issuer("https://c2id.com"))
			.withSubject(new Subject("alice"))
			.withIssueTime(Instant.ofEpochSecond(102030L))
			.withExpirationTime(Instant.ofEpochSecond(203040L))
			.withAudienceList(Arrays.asList(new Audience("A"), new Audience("B")))
			.withJWTID(new JWTID("loh3nei2ja3pai2queilas7Aech5veih"))
			.withClientID(new ClientID("123"))
			.withScope(new Scope("read", "write"))
			.withClaimNames(Set.of("openid", "email"))
			.withClaimsLocales(List.of(LangTag.parse("en"), LangTag.parse("de")))
			.withPresetClaims(uip)
			.withSubjectSessionkey("WYqFXK7Q4HFnJv0hiT3Fgw")
			.withData(dat)
			.withActor(new Actor(new Subject("bob")))
			.withClientCertificateConfirmation(new X509CertificateConfirmation(new Base64URL("bwcK0esc3ACC3DB2Y5_lESsXE8o9ltc05O89jdN-dg2")))
			.withOtherTopLevelParameters(oth);
		
		TokenIntrospectionSuccessResponse response = new DefaultTokenIntrospectionResponseComposer().compose(tokenAuthz, new MockContext());
		
		assertTrue(response.isActive());
		assertEquals(AccessTokenType.BEARER, response.getTokenType());
		assertEquals(tokenAuthz.getIssuer(), response.getIssuer());
		assertEquals(tokenAuthz.getSubject(), response.getSubject());
		assertEquals(tokenAuthz.getIssueTime(), response.getIssueTime().toInstant());
		assertEquals(tokenAuthz.getExpirationTime(), response.getExpirationTime().toInstant());
		assertEquals(tokenAuthz.getAudienceList(), response.getAudience());
		assertEquals(tokenAuthz.getJWTID(), response.getJWTID());
		assertEquals(tokenAuthz.getClientID(), response.getClientID());
		assertEquals(tokenAuthz.getScope(), response.getScope());
		assertEquals("10.20.30.40", ((JSONObject)response.toJSONObject().get("dat")).get("ip"));
		assertEquals(tokenAuthz.getActor().getSubject(), Actor.parseTopLevel(response.toJSONObject()).getSubject());
		assertEquals(tokenAuthz.getClientCertificateConfirmation().getValue(), response.getX509CertificateConfirmation().getValue());
		assertEquals("p123", response.getStringParameter("patientId"));
		assertEquals(14, response.toJSONObject().size());
	}
}

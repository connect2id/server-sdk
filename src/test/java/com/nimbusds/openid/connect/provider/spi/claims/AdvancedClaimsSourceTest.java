package com.nimbusds.openid.connect.provider.spi.claims;


import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.util.date.DateWithTimeZoneOffset;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSessionID;
import com.nimbusds.openid.connect.sdk.assurance.IdentityTrustFramework;
import com.nimbusds.openid.connect.sdk.assurance.IdentityVerification;
import com.nimbusds.openid.connect.sdk.assurance.VerificationProcess;
import com.nimbusds.openid.connect.sdk.assurance.evidences.QESEvidence;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import com.nimbusds.openid.connect.sdk.claims.DistributedClaims;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Tests the advanced claims source interface.
 */
public class AdvancedClaimsSourceTest {


        static final Issuer ISSUER = new Issuer("https://c2id.com");


        static final ClientID CLIENT_ID = new ClientID("123");


        static final OIDCClientInformation CLIENT_INFO;


        static final String CLIENT_IP = "192.168.0.1";


        static final SubjectSessionID SID = () -> "WYqFXK7Q4HFnJv0hiT3Fgw.-oVkvSXgalUuMQDfEsh1lw";


        static {
                var clientMetadata = new OIDCClientMetadata();
                clientMetadata.setRedirectionURI(URI.create("https://example.com/cb"));
                clientMetadata.applyDefaults();
                CLIENT_INFO = new OIDCClientInformation(CLIENT_ID, clientMetadata);
        }


        @Test
        public void testPrefix() {

                assertEquals("verified:", AdvancedClaimsSource.VERIFIED_CLAIM_NAME_PREFIX);
        }


        @Test
        public void testWithNormalClaims()
                throws Exception {

                var props = new Properties();
                props.setProperty("mockClaimsSource.enabled", "true");
                var stream = new ByteArrayOutputStream();
                props.store(stream, null);

                InitContext mockInitContext = mock(InitContext.class);
                when(mockInitContext.getResourceAsStream("/config.properties")).thenReturn(new ByteArrayInputStream(stream.toByteArray()));

                AdvancedClaimsSource source = new MockAdvancedClaimsSource();

                assertTrue(source instanceof CommonClaimsSource);

                source.init(mockInitContext);

                assertTrue(source.isEnabled());

                assertTrue(source.supportedClaims().contains("email"));
                assertTrue(source.supportedClaims().contains("name"));
                assertEquals(2, source.supportedClaims().size());

                assertNull(source.getClaims(new Subject("no-such-user"),
                        Set.of("name", "email"),
                        null,
                        new RequestCtx(ISSUER, CLIENT_INFO, ClaimsTransport.USERINFO, null, CLIENT_IP, null, SID, null, null)));

                UserInfo alice = source.getClaims(new Subject("alice"),
                        Set.of("name", "email"),
                        null,
                        new RequestCtx(ISSUER, CLIENT_INFO, ClaimsTransport.USERINFO, null, CLIENT_IP, null, SID, null, null));

                assertEquals("alice", alice.getSubject().getValue());
                assertEquals("Alice Adams", alice.getName());
                assertEquals("alice@wonderland.net", alice.getEmailAddress());

                source.shutdown();
        }


        @Test
        public void testWithVerifiedClaims()
                throws Exception {

                var props = new Properties();
                props.setProperty("mockClaimsSource.enabled", "true");
                var stream = new ByteArrayOutputStream();
                props.store(stream, null);

                InitContext mockInitContext = mock(InitContext.class);
                when(mockInitContext.getResourceAsStream("/config.properties")).thenReturn(new ByteArrayInputStream(stream.toByteArray()));

                AdvancedClaimsSource source = new MockVerifiedClaimsSource();

                assertTrue(source instanceof CommonClaimsSource);

                source.init(mockInitContext);

                assertTrue(source.isEnabled());

                assertTrue(source.supportedClaims().contains("email"));
                assertTrue(source.supportedClaims().contains("name"));
                assertEquals(2, source.supportedClaims().size());

                var verification = new IdentityVerification(
                        IdentityTrustFramework.EIDAS_IAL_SUBSTANTIAL,
                        new DateWithTimeZoneOffset(new Date()),
                        new VerificationProcess(UUID.randomUUID().toString()),
                        new QESEvidence(
                                new Issuer("https://eidas.c2id.com"),
                                UUID.randomUUID().toString(),
                                new DateWithTimeZoneOffset(new Date())
                        )
                );

                assertNull(source.getClaims(new Subject("no-such-user"),
                        Set.of("verified:name", "verified:email"),
                        null,
                        new RequestCtx(ISSUER, CLIENT_INFO, ClaimsTransport.USERINFO, verification.toJSONObject(), CLIENT_IP, null, null, null, null)));

                UserInfo alice = source.getClaims(new Subject("alice"),
                        Set.of("verified:name", "verified:email"),
                        null,
                        new RequestCtx(ISSUER, CLIENT_INFO, ClaimsTransport.USERINFO, verification.toJSONObject(), CLIENT_IP, null, null, null, null));

                assertEquals(verification.toJSONObject(), alice.getVerifiedClaims().get(0).getVerification().toJSONObject());

                assertEquals("alice", alice.getSubject().getValue());
                assertEquals("Alice Adams", alice.getVerifiedClaims().get(0).getClaimsSet().getName());
                assertEquals("alice@wonderland.net", alice.getVerifiedClaims().get(0).getClaimsSet().getEmailAddress());

                source.shutdown();
        }


        @Test
        public void testWithDistributedClaims()
                throws Exception {

                var props = new Properties();
                props.setProperty("mockClaimsSource.enabled", "true");
                var stream = new ByteArrayOutputStream();
                props.store(stream, null);

                InitContext mockInitContext = mock(InitContext.class);
                when(mockInitContext.getResourceAsStream("/config.properties")).thenReturn(new ByteArrayInputStream(stream.toByteArray()));

                AdvancedClaimsSource source = new MockDistributedClaimsSource();

                assertTrue(source instanceof CommonClaimsSource);

                source.init(mockInitContext);

                assertTrue(source.isEnabled());

                assertTrue(source.supportedClaims().contains("email"));
                assertTrue(source.supportedClaims().contains("name"));
                assertTrue(source.supportedClaims().contains("credit_score"));
                assertEquals(3, source.supportedClaims().size());

                assertNull(source.getClaims(new Subject("no-such-user"),
                        Set.of("name", "email", "credit_score"),
                        null,
                        new RequestCtx(ISSUER, CLIENT_INFO, ClaimsTransport.USERINFO, null, CLIENT_IP, new BearerAccessToken(), null, null, null)));

                AccessToken accessToken = new BearerAccessToken();

                UserInfo alice = source.getClaims(new Subject("alice"),
                        Set.of("name", "email", "credit_score"),
                        null,
                        new RequestCtx(ISSUER, CLIENT_INFO, ClaimsTransport.USERINFO, null, CLIENT_IP, accessToken, null, null, null));

                assertEquals("alice", alice.getSubject().getValue());
                assertEquals("Alice Adams", alice.getName());
                assertEquals("alice@wonderland.net", alice.getEmailAddress());

                Set<DistributedClaims> distClaimsSet = alice.getDistributedClaims();

                DistributedClaims distClaims = distClaimsSet.iterator().next();
                assertEquals(URI.create("https://credit-agency.com/claims"), distClaims.getSourceEndpoint());
                assertEquals(accessToken.getValue(), distClaims.getAccessToken().getValue()); // same token used upstream
                assertEquals(Collections.singleton("credit_score"), distClaims.getNames());

                assertEquals(1, distClaimsSet.size());

                source.shutdown();
        }
}

package com.nimbusds.openid.connect.provider.spi.events;


import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import com.nimbusds.openid.connect.provider.spi.crypto.MockJWTSignerAndSignatureVerifier;
import com.nimbusds.openid.connect.sdk.claims.IDTokenClaimsSet;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class IDTokenIssueEventTest {
	
	
	static class Listener implements IDTokenIssueEventListener {

		IDTokenIssueEvent lastEvent;

		@Override
		public void idTokenIssued(IDTokenIssueEvent event, EventContext eventContext) {
			
			lastEvent = event;
			
			assertEquals(MockJWTSignerAndSignatureVerifier.ISSUER, eventContext.getIssuer());
			assertNotNull(eventContext.getOIDCClientInformation());
			assertNotNull(eventContext.getJWTSigner());
		}
	}
	
	
	private static JWTClaimsSet generateTokenClaims() {
		
		var idTokenClaimsSet = new IDTokenClaimsSet(
			new Issuer("https://c2id.com"),
			new Subject("alice"),
			new Audience("123").toSingleAudienceList(),
			Date.from(Instant.now().plus(1L, ChronoUnit.MINUTES)),
			new Date());
		
		try {
			return idTokenClaimsSet.toJWTClaimsSet();
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}


        @Test
        public void testConstructor() {

		var source = new Object();
		JWTClaimsSet jwtClaimsSet = generateTokenClaims();
		var localSubject = new Subject("alice");

		var event = new IDTokenIssueEvent(source, jwtClaimsSet, localSubject);
		
		assertEquals(source, event.getSource());
		assertEquals(jwtClaimsSet, event.getJWTClaimsSet());
		assertEquals(localSubject, event.getLocalSubject());
	}


        @Test
        public void testDeprecatedConstructor() {

		var source = new Object();
		JWTClaimsSet jwtClaimsSet = generateTokenClaims();

		var event = new IDTokenIssueEvent(source, jwtClaimsSet);

		assertEquals(source, event.getSource());
		assertEquals(jwtClaimsSet, event.getJWTClaimsSet());
		Assert.assertNull(event.getLocalSubject());
	}


        @Test
        public void testListen() {

		var source = new Object();
		JWTClaimsSet jwtClaimsSet = generateTokenClaims();
		var localSubject = new Subject("alice");

		var event = new IDTokenIssueEvent(source, jwtClaimsSet, localSubject);
		
		var clientID = new ClientID("123");
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://rp.example.com"));
		clientMetadata.applyDefaults();
		var clientInfo = new OIDCClientInformation(clientID, clientMetadata);
		
		var listener = new Listener();
		listener.idTokenIssued(event, new EventContext() {

			@Override
			public Issuer getIssuer() {
				return MockJWTSignerAndSignatureVerifier.ISSUER;
			}

			@Override
			public OIDCClientInformation getOIDCClientInformation() {
				return clientInfo;
			}


			@Override
			public JWTSigner getJWTSigner() {
				return new MockJWTSignerAndSignatureVerifier();
			}

		});
		
		assertEquals(source, listener.lastEvent.getSource());
		assertEquals(jwtClaimsSet, listener.lastEvent.getJWTClaimsSet());
		assertEquals(localSubject, listener.lastEvent.getLocalSubject());
	}


	@Test(expected = NullPointerException.class)
	public void testConstructor_rejectNullClaimsSet() {

		new IDTokenIssueEvent(new Object(), null, new Subject("alice"));
	}


	@Test(expected = NullPointerException.class)
	public void testConstructor_rejectNullSubject() {

		new IDTokenIssueEvent(new Object(), generateTokenClaims(), null);
	}
}

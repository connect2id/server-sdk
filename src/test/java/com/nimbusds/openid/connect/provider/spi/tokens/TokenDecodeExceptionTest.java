package com.nimbusds.openid.connect.provider.spi.tokens;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class TokenDecodeExceptionTest {


        @Test
        public void testMessageConstructor() {
		
		var exception = new TokenDecodeException("message");
		assertEquals("message", exception.getMessage());
		assertNull(exception.getCause());
	}


        @Test
        public void testMessageAndCauseConstructor() {
		
		var cause = new Exception();
		
		var exception = new TokenDecodeException("message", cause);
		assertEquals("message", exception.getMessage());
		assertEquals(cause, exception.getCause());
	}
}

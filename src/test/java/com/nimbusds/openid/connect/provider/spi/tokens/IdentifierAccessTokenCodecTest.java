package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.ByteUtils;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.id.Identifier;
import com.nimbusds.oauth2.sdk.id.Issuer;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class IdentifierAccessTokenCodecTest {


        @Test
        public void testSampleHybridCodec()
		throws Exception {
		
		var issuer = new Issuer("https://c2id.com");
		
		var codec = new HybridIdentifierAccessTokenCodec();
		
		var ctx = new MockTokenEncoderContext();
		ctx.setIssuer(issuer);
		ctx.createJWTSigner(SampleKeys.RSA_JWK_2048);
		ctx.createJWSVerifier(SampleKeys.RSA_JWK_2048);
		
		IdentifierAccessToken t = codec.generate(new MutableAccessTokenAuthorization(), ctx);
		
		assertEquals(ByteUtils.byteLength(256), new Base64URL(t.getIdentifier().getValue()).decode().length);
//		System.out.println("ID: " + t.getIdentifier().getValue());
//		System.out.println("Token value: " + t.getTokenValue());
		
		SignedJWT jwt = SignedJWT.parse(t.getTokenValue());
		
//		System.out.println("JWT header: " + jwt.getHeader().toJSONObject());
//		System.out.println("JWT claims: " + jwt.getJWTClaimsSet().toJSONObject());
		
		assertEquals(JWSAlgorithm.RS256, jwt.getHeader().getAlgorithm());
		assertEquals(SampleKeys.RSA_JWK_2048.getKeyID(), jwt.getHeader().getKeyID());
		
		assertEquals(issuer.getValue(), jwt.getJWTClaimsSet().getIssuer());
		assertEquals(t.getIdentifier().getValue(), jwt.getJWTClaimsSet().getJWTID());
		assertEquals(2, jwt.getJWTClaimsSet().getClaims().size());
		
		Identifier out = codec.decode(t.getTokenValue(), ctx);
		assertEquals(t.getIdentifier(), out);
		
		// Decode invalid ID token
		try {
			codec.decode("invalid-token", ctx);
			Assert.fail();
		} catch (TokenDecodeException e) {
			assertEquals("Invalid serialized unsecured/JWS/JWE object: Missing part delimiters", e.getMessage());
		}
	}


        @Test
        public void testSampleHybridCodec_cnfX5tSHA256()
		throws Exception {
		
		var issuer = new Issuer("https://c2id.com");
		
		var codec = new HybridIdentifierAccessTokenCodec();
		
		var ctx = new MockTokenEncoderContext();
		ctx.setIssuer(issuer);
		ctx.createJWTSigner(SampleKeys.RSA_JWK_2048);
		ctx.createJWSVerifier(SampleKeys.RSA_JWK_2048);
		
		IdentifierAccessToken t = codec.generate(
			new MutableAccessTokenAuthorization()
				.withClientCertificateConfirmation(X509CertificateConfirmation.of(SampleX509Certificate.X_509)), ctx);
		
		assertEquals(ByteUtils.byteLength(256), new Base64URL(t.getIdentifier().getValue()).decode().length);
//		System.out.println("ID: " + t.getIdentifier().getValue());
//		System.out.println("Token value: " + t.getTokenValue());
		
		SignedJWT jwt = SignedJWT.parse(t.getTokenValue());
		
//		System.out.println("JWT header: " + jwt.getHeader().toJSONObject());
//		System.out.println("JWT claims: " + jwt.getJWTClaimsSet().toJSONObject());
		
		assertEquals(JWSAlgorithm.RS256, jwt.getHeader().getAlgorithm());
		assertEquals(SampleKeys.RSA_JWK_2048.getKeyID(), jwt.getHeader().getKeyID());
		
		assertEquals(issuer.getValue(), jwt.getJWTClaimsSet().getIssuer());
		assertEquals(t.getIdentifier().getValue(), jwt.getJWTClaimsSet().getJWTID());
		assertEquals(SampleX509Certificate.X5T_SHA256, X509CertificateConfirmation.parse(jwt.getJWTClaimsSet()).getValue());
		assertEquals(3, jwt.getJWTClaimsSet().getClaims().size());
		
		Identifier out = codec.decode(t.getTokenValue(), ctx);
		assertEquals(t.getIdentifier(), out);
		
		// Decode invalid ID token
		try {
			codec.decode("invalid-token", ctx);
			Assert.fail();
		} catch (TokenDecodeException e) {
			assertEquals("Invalid serialized unsecured/JWS/JWE object: Missing part delimiters", e.getMessage());
		}
	}
}
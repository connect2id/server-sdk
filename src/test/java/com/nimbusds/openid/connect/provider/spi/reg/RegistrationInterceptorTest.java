package com.nimbusds.openid.connect.provider.spi.reg;


import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.id.Issuer;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;


public class RegistrationInterceptorTest {
	
	
	private static final URL URL;
	private static final HTTPRequest POST_REQUEST;
	private static final HTTPRequest GET_REQUEST;
	private static final HTTPRequest PUT_REQUEST;
	private static final HTTPRequest DELETE_REQUEST;
	
	
	static {
		try {
			URL = new URL("https://c2id.com/clients");
			POST_REQUEST = new HTTPRequest(HTTPRequest.Method.POST, URL);
			GET_REQUEST = new HTTPRequest(HTTPRequest.Method.GET, URL);
			PUT_REQUEST = new HTTPRequest(HTTPRequest.Method.DELETE, URL);
			DELETE_REQUEST = new HTTPRequest(HTTPRequest.Method.DELETE, URL);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}


        @Test
        public void testDefaultImplementation() throws WrappedHTTPResponseException {
		
		var defaultInterceptor = new RegistrationInterceptor() {
		};
		
		var ctx = new InterceptorContext() {
			@Override
			public boolean openRegistrationIsAllowed() {
				return false;
			}
			
			
			@Override
			public Issuer getIssuer() {
				return new Issuer("https://c2id.com");
			}
		};
		
		
		assertEquals(POST_REQUEST, defaultInterceptor.interceptPostRequest(POST_REQUEST, ctx));
		assertEquals(GET_REQUEST, defaultInterceptor.interceptGetRequest(GET_REQUEST, ctx));
		assertEquals(PUT_REQUEST, defaultInterceptor.interceptPutRequest(PUT_REQUEST, ctx));
		assertEquals(DELETE_REQUEST, defaultInterceptor.interceptDeleteRequest(DELETE_REQUEST, ctx));
	}
}

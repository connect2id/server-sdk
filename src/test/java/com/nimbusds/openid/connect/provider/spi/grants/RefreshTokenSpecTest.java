package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;


/**
 * Tests the refresh token spec.
 */
public class RefreshTokenSpecTest {


        @Test
        public void testDefault()
		throws Exception {

		RefreshTokenSpec tokenSpec = RefreshTokenSpec.DEFAULT;
		assertFalse(tokenSpec.issue());
		assertEquals(-1L, tokenSpec.getLifetime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertEquals("{\"issue\":false}", tokenSpec.toString());
		tokenSpec = RefreshTokenSpec.parse(tokenSpec.toJSONObject());
		assertNull(tokenSpec.getAudience());
		assertFalse(tokenSpec.issue());
		assertEquals(-1L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testNoIssue()
		throws Exception {

		var tokenSpec = new RefreshTokenSpec();
		assertFalse(tokenSpec.issue());
		assertEquals(-1L, tokenSpec.getLifetime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertEquals("{\"issue\":false}", tokenSpec.toString());
		tokenSpec = RefreshTokenSpec.parse(tokenSpec.toJSONObject());
		assertNull(tokenSpec.getAudience());
		assertFalse(tokenSpec.issue());
		assertEquals(-1L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testIssue_defaultLifetime()
		throws Exception {

		var tokenSpec = new RefreshTokenSpec(true, -1L);
		assertTrue(tokenSpec.issue());
		assertEquals(-1L, tokenSpec.getLifetime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertEquals("{\"issue\":true}", tokenSpec.toString());
		tokenSpec = RefreshTokenSpec.parse(tokenSpec.toJSONObject());
		assertNull(tokenSpec.getAudience());
		assertTrue(tokenSpec.issue());
		assertEquals(-1L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testIssue_permanentLifetime()
		throws Exception {

		var tokenSpec = new RefreshTokenSpec(true, 0L);
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(0L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals(2, jsonObject.size());
		tokenSpec = RefreshTokenSpec.parse(jsonObject);
		assertNull(tokenSpec.getAudience());
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testIssueWithLifetime()
		throws Exception {

		var tokenSpec = new RefreshTokenSpec(true, 900L);
		assertTrue(tokenSpec.issue());
		assertEquals(900L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		tokenSpec = RefreshTokenSpec.parse(tokenSpec.toJSONObject());
		assertNull(tokenSpec.getAudience());
		assertTrue(tokenSpec.issue());
		assertEquals(900L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().isEmpty());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testIssueWithMaxIdle()
		throws Exception {

		var tokenSpec = new RefreshTokenSpec(true, 0L, 60L, Optional.of(false));
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(60L, tokenSpec.getMaxIdleTime());
		assertFalse(tokenSpec.getRotate().get());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(0L, jsonObject.get("lifetime"));
		assertEquals(60L, jsonObject.get("max_idle"));
		assertFalse(JSONObjectUtils.getBoolean(jsonObject, "rotate"));
		assertEquals(4, jsonObject.size());
		tokenSpec = RefreshTokenSpec.parse(jsonObject);
		assertNull(tokenSpec.getAudience());
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(60L, tokenSpec.getMaxIdleTime());
		assertFalse(tokenSpec.getRotate().get());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testIssueWithLifetimeAndMaxIdle()
		throws Exception {

		var tokenSpec = new RefreshTokenSpec(true, 900L, 60L, Optional.of(true));
		assertTrue(tokenSpec.issue());
		assertEquals(900L, tokenSpec.getLifetime());
		assertEquals(60L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().get());
		tokenSpec = RefreshTokenSpec.parse(tokenSpec.toJSONObject());
		assertNull(tokenSpec.getAudience());
		assertTrue(tokenSpec.issue());
		assertEquals(900L, tokenSpec.getLifetime());
		assertEquals(60L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().get());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testIssueWithRotateTrue() throws ParseException {
		
		var tokenSpec = new RefreshTokenSpec(true, 0L, Optional.of(true));
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().get());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(0L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "rotate"));
		assertEquals(3, jsonObject.size());
		tokenSpec = RefreshTokenSpec.parse(tokenSpec.toJSONObject());
		assertNull(tokenSpec.getAudience());
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertTrue(tokenSpec.getRotate().get());
		assertNull(tokenSpec.getAudience());
	}


        @Test
        public void testIssueWithRotateFalse() throws ParseException {
		
		var tokenSpec = new RefreshTokenSpec(true, 0L, Optional.of(false));
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertFalse(tokenSpec.getRotate().get());
		JSONObject jsonObject = tokenSpec.toJSONObject();
		assertTrue(JSONObjectUtils.getBoolean(jsonObject, "issue"));
		assertEquals(0L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertFalse(JSONObjectUtils.getBoolean(jsonObject, "rotate"));
		assertEquals(3, jsonObject.size());
		tokenSpec = RefreshTokenSpec.parse(tokenSpec.toJSONObject());
		assertNull(tokenSpec.getAudience());
		assertTrue(tokenSpec.issue());
		assertEquals(0L, tokenSpec.getLifetime());
		assertEquals(0L, tokenSpec.getMaxIdleTime());
		assertFalse(tokenSpec.getRotate().get());
		assertNull(tokenSpec.getAudience());
	}
}

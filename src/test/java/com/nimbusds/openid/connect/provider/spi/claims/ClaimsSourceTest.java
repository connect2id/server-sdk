package com.nimbusds.openid.connect.provider.spi.claims;


import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Tests the claims source interface.
 */
public class ClaimsSourceTest extends TestCase {


	public void testRun()
		throws Exception {

		var props = new Properties();
		props.setProperty("mockClaimsSource.enabled", "true");
		var stream = new ByteArrayOutputStream();
		props.store(stream, null);
		
		InitContext mockInitContext = mock(InitContext.class);
		when(mockInitContext.getResourceAsStream("/config.properties")).thenReturn(new ByteArrayInputStream(stream.toByteArray()));

		ClaimsSource source = new MockClaimsSource();

		assertTrue(source instanceof CommonClaimsSource);

		source.init(mockInitContext);

		assertTrue(source.isEnabled());

		assertTrue(source.supportedClaims().contains("email"));
		assertTrue(source.supportedClaims().contains("name"));
		assertEquals(2, source.supportedClaims().size());

		assertNull(source.getClaims(new Subject("no-such-user"),
			new HashSet<>(Arrays.asList("name", "email")),
			null));

		UserInfo alice = source.getClaims(new Subject("alice"), Set.of("name", "email"), null);

		assertEquals("alice", alice.getSubject().getValue());
		assertEquals("Alice Adams", alice.getName());
		assertEquals("alice@wonderland.net", alice.getEmailAddress());

		source.shutdown();
	}
}

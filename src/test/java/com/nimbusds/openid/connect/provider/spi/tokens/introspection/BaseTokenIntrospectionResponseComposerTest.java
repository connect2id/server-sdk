package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;
import net.minidev.json.JSONObject;

import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.dpop.JWKThumbprintConfirmation;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.oauth2.sdk.token.AccessTokenType;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;
import com.nimbusds.openid.connect.provider.spi.tokens.MutableAccessTokenAuthorization;
import com.nimbusds.openid.connect.provider.spi.tokens.SampleX509Certificate;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;


public class BaseTokenIntrospectionResponseComposerTest {
	
	
	private static final TokenIntrospectionResponseComposer COMPOSER = new BaseTokenIntrospectionResponseComposer() { };


        @Test
        public void testInactive() {
		
		TokenIntrospectionSuccessResponse response = COMPOSER.compose(null, null);
		
		assertFalse(response.isActive());
		
		assertEquals(1, response.toJSONObject().size());
	}


        @Test
        public void testActiveMinimal() {
		
		TokenIntrospectionSuccessResponse response = COMPOSER.compose(new MutableAccessTokenAuthorization(), null);
		
		assertTrue(response.isActive());
		
		assertEquals(AccessTokenType.BEARER, response.getTokenType());
		
		assertEquals(2, response.toJSONObject().size());
	}


        @Test
        public void testActiveBearerFullySpecced()
		throws Exception {
		
		var sub = new Subject("alice");
		var act = new Actor(new Subject("bob"));
		var clientID = new ClientID("123");
		var scope = new Scope("openid", "email");
		Instant exp = Instant.ofEpochSecond(123000L);
		Instant iat = Instant.ofEpochSecond(100000L);
		var iss = new Issuer("https://c2id.com");
		List<Audience> audList = new Audience("resource-server-1").toSingleAudienceList();
		var jti = new JWTID();
		Set<String> claimNames = new HashSet<>(Arrays.asList("email", "email_verified"));
		List<LangTag> claimsLocales = Arrays.asList(LangTag.parse("en-GB"), LangTag.parse("de-DE"));
		var presetClaims = new JSONObject();
		presetClaims.put("name", "Alice Adams");
		var sessionKey = "WYqFXK7Q4HFnJv0hiT3Fgw";
		var data = new JSONObject();
		data.put("ip", "192.168.0.1");
		var x5tCnf = new X509CertificateConfirmation(SampleX509Certificate.X5T_SHA256);
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withSubject(sub)
			.withActor(act)
			.withClientID(clientID)
			.withScope(scope)
			.withExpirationTime(exp)
			.withIssueTime(iat)
			.withIssuer(iss)
			.withAudienceList(audList)
			.withJWTID(jti)
			.withClaimNames(claimNames)
			.withClaimsLocales(claimsLocales)
			.withPresetClaims(presetClaims)
			.withSubjectSessionkey(sessionKey)
			.withData(data)
			.withClientCertificateConfirmation(x5tCnf);
		
		assertNotNull(tokenAuthz.getClientCertificateConfirmation());
		
		TokenIntrospectionSuccessResponse response = COMPOSER.compose(tokenAuthz, null);
		
		assertTrue(response.isActive());
		assertEquals(scope, response.getScope());
		assertEquals(clientID, response.getClientID());
		assertEquals(AccessTokenType.BEARER, response.getTokenType());
		assertEquals(exp, response.getExpirationTime().toInstant());
		assertEquals(iat, response.getIssueTime().toInstant());
		assertEquals(sub, response.getSubject());
		assertEquals(audList, response.getAudience());
		assertEquals(iss, response.getIssuer());
		assertEquals(jti, response.getJWTID());
		assertEquals(x5tCnf.getValue(), response.getX509CertificateConfirmation().getValue());
		assertNull(response.getJWKThumbprintConfirmation());
		
		assertEquals(11, response.getParameters().size());
	}


        @Test
        public void testActiveDPoPFullySpecced()
		throws Exception {
		
		var sub = new Subject("alice");
		var act = new Actor(new Subject("bob"));
		var clientID = new ClientID("123");
		var scope = new Scope("openid", "email");
		Instant exp = Instant.ofEpochSecond(123000L);
		Instant iat = Instant.ofEpochSecond(100000L);
		var iss = new Issuer("https://c2id.com");
		List<Audience> audList = new Audience("resource-server-1").toSingleAudienceList();
		var jti = new JWTID();
		Set<String> claimNames = new HashSet<>(Arrays.asList("email", "email_verified"));
		List<LangTag> claimsLocales = Arrays.asList(LangTag.parse("en-GB"), LangTag.parse("de-DE"));
		var presetClaims = new JSONObject();
		presetClaims.put("name", "Alice Adams");
		var sessionKey = "WYqFXK7Q4HFnJv0hiT3Fgw";
		var data = new JSONObject();
		data.put("ip", "192.168.0.1");
		var jktCnf = new JWKThumbprintConfirmation(new Base64URL("0ZcOCORZNYy-DWpqq30jZyJGHTN0d2HglBV3uiguA4I"));
		
		AccessTokenAuthorization tokenAuthz = new MutableAccessTokenAuthorization()
			.withSubject(sub)
			.withActor(act)
			.withClientID(clientID)
			.withScope(scope)
			.withExpirationTime(exp)
			.withIssueTime(iat)
			.withIssuer(iss)
			.withAudienceList(audList)
			.withJWTID(jti)
			.withClaimNames(claimNames)
			.withClaimsLocales(claimsLocales)
			.withPresetClaims(presetClaims)
			.withSubjectSessionkey(sessionKey)
			.withData(data)
			.withClientCertificateConfirmation(null)
			.withJWKThumbprintConfirmation(jktCnf);
		
		TokenIntrospectionSuccessResponse response = COMPOSER.compose(tokenAuthz, null);
		
		assertTrue(response.isActive());
		assertEquals(scope, response.getScope());
		assertEquals(clientID, response.getClientID());
		assertEquals(AccessTokenType.DPOP, response.getTokenType());
		assertEquals(exp, response.getExpirationTime().toInstant());
		assertEquals(iat, response.getIssueTime().toInstant());
		assertEquals(sub, response.getSubject());
		assertEquals(audList, response.getAudience());
		assertEquals(iss, response.getIssuer());
		assertEquals(jti, response.getJWTID());
		assertNull(response.getX509CertificateConfirmation());
		assertEquals(jktCnf.getValue(), response.getJWKThumbprintConfirmation().getValue());
		
		assertEquals(11, response.getParameters().size());
	}
}

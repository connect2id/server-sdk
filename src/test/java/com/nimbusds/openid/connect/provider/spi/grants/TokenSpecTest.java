package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Tests the base token specification.
 */
public class TokenSpecTest {


        @Test
        public void testFullConstructor()
		throws Exception {

		final long lifetime = 3600L;
		List<Audience> audList = Arrays.asList(new Audience("A"), new Audience("B"));
		Subject impersonatedSub = new Subject("claire");

		var tokenSpec = new TokenSpec(lifetime, audList, impersonatedSub);

		assertEquals(lifetime, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(impersonatedSub, tokenSpec.getImpersonatedSubject());

		String json = tokenSpec.toJSONObject().toJSONString();

		// {"lifetime":3600,"audience":["A","B"]}

		JSONObject jsonObject = JSONObjectUtils.parse(json);

		assertEquals(3600L, JSONObjectUtils.getLong(jsonObject, "lifetime"));
		assertEquals("A", JSONObjectUtils.getStringArray(jsonObject, "audience")[0]);
		assertEquals("B", JSONObjectUtils.getStringArray(jsonObject, "audience")[1]);
		assertEquals(2, JSONObjectUtils.getStringArray(jsonObject, "audience").length);
		assertEquals("claire", JSONObjectUtils.getString(jsonObject, "impersonated_sub"));
		assertEquals(3, jsonObject.size());

		tokenSpec = TokenSpec.parse(jsonObject);

		assertEquals(lifetime, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertEquals(impersonatedSub, tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testZeroLifetime()
		throws Exception {

		var tokenSpec = new TokenSpec(0L);
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());
		String json = tokenSpec.toJSONObject().toJSONString();
		// {}
		assertEquals("{\"lifetime\":0}", json);
		JSONObject jsonObject = JSONObjectUtils.parse(json);
		tokenSpec = TokenSpec.parse(jsonObject);
		assertEquals(0L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testPositiveLifetime()
		throws Exception {

		var tokenSpec = new TokenSpec(600L);
		assertEquals(600L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());
		String json = tokenSpec.toJSONObject().toJSONString();
		assertEquals("{\"lifetime\":600}", json);
		JSONObject jsonObject = JSONObjectUtils.parse(json);
		tokenSpec = TokenSpec.parse(jsonObject);
		assertEquals(600L, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testZeroLifetimeToJSONObject() {

		assertEquals("{\"lifetime\":0}", new TokenSpec(0L).toJSONObject().toJSONString());
	}


        @Test
        public void testAudienceOnly()
		throws Exception {

		List<Audience> audList = Arrays.asList(new Audience("A"), new Audience("B"));

		var tokenSpec = new TokenSpec(-1L, audList, null);

		assertEquals(-1L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());

		String json = tokenSpec.toJSONObject().toJSONString();

		// {"audience":["A","B"]}
		assertEquals("{\"audience\":[\"A\",\"B\"]}", json);

		tokenSpec = TokenSpec.parse(JSONObjectUtils.parse(json));

		assertEquals(-1L, tokenSpec.getLifetime());
		assertEquals(audList, tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testNullAudience()
		throws Exception {

		final long lifetime = 3600L;

		var tokenSpec = new TokenSpec(lifetime, null, null);

		assertEquals(lifetime, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());

		String json = tokenSpec.toJSONObject().toJSONString();
		assertEquals("{\"lifetime\":3600}", json);

		JSONObject jsonObject = JSONObjectUtils.parse(json);

		tokenSpec = TokenSpec.parse(jsonObject);

		assertEquals(lifetime, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testEmptyAudience()
		throws Exception {

		final long lifetime = 3600L;
		List<Audience> audList = new ArrayList<>();

		var tokenSpec = new TokenSpec(lifetime, audList, null);

		assertEquals(lifetime, tokenSpec.getLifetime());
		assertTrue(tokenSpec.getAudience().isEmpty());
		assertNull(tokenSpec.getImpersonatedSubject());

		String json = tokenSpec.toJSONObject().toJSONString();
		assertEquals("{\"lifetime\":3600}", json);

		JSONObject jsonObject = JSONObjectUtils.parse(json);

		tokenSpec = TokenSpec.parse(jsonObject);

		assertEquals(lifetime, tokenSpec.getLifetime());
		assertNull(tokenSpec.getAudience());
		assertNull(tokenSpec.getImpersonatedSubject());
	}


        @Test
        public void testParseDefault()
		throws Exception {

		assertEquals(-1L, TokenSpec.parse(new JSONObject()).getLifetime());
		assertNull(TokenSpec.parse(new JSONObject()).getAudience());
	}
}

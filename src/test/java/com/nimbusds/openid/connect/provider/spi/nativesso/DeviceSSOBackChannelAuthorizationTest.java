package com.nimbusds.openid.connect.provider.spi.nativesso;

import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.openid.connect.provider.spi.grants.AccessTokenSpec;
import com.nimbusds.openid.connect.provider.spi.grants.ClaimsSpec;
import com.nimbusds.openid.connect.provider.spi.grants.IDTokenSpec;
import com.nimbusds.openid.connect.provider.spi.grants.RefreshTokenSpec;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;

public class DeviceSSOBackChannelAuthorizationTest {


        @Test
        public void testConstructor() {

                var scope = new Scope("openid", "device_sso", "email");
                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var idTokenSpec = new IDTokenSpec(true, 0, null);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                var authZ = new DeviceSSOBackChannelAuthorization(
                        scope,
                        accessTokenSpec,
                        refreshTokenSpec,
                        idTokenSpec,
                        claimsSpec,
                        null);

                assertEquals(scope, authZ.getScope());
                assertEquals(accessTokenSpec, authZ.getAccessTokenSpec());
                assertEquals(refreshTokenSpec, authZ.getRefreshTokenSpec());
                assertEquals(idTokenSpec, authZ.getIDTokenSpec());
                assertEquals(claimsSpec, authZ.getClaimsSpec());
                assertNull(authZ.getData());
        }


        @Test
        public void testConstructor_withDate() {

                var scope = new Scope("openid", "device_sso", "email");
                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var idTokenSpec = new IDTokenSpec(true, 0, null);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));
                var data = new JSONObject();
                data.put("x", 123);

                var authZ = new DeviceSSOBackChannelAuthorization(
                        scope,
                        accessTokenSpec,
                        refreshTokenSpec,
                        idTokenSpec,
                        claimsSpec,
                        data);

                assertEquals(scope, authZ.getScope());
                assertEquals(accessTokenSpec, authZ.getAccessTokenSpec());
                assertEquals(refreshTokenSpec, authZ.getRefreshTokenSpec());
                assertEquals(idTokenSpec, authZ.getIDTokenSpec());
                assertEquals(claimsSpec, authZ.getClaimsSpec());
                assertEquals(data, authZ.getData());
        }


        @Test(expected = NullPointerException.class)
        public void testConstructor_rejectNullScope() {

                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var idTokenSpec = new IDTokenSpec(true, 0, null);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                new DeviceSSOBackChannelAuthorization(
                        null,
                        accessTokenSpec,
                        refreshTokenSpec,
                        idTokenSpec,
                        claimsSpec,
                        null);
        }


        @Test
        public void testConstructor_allowScopeWithoutOpenID() {

                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var idTokenSpec = new IDTokenSpec(true, 0, null);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                var authZ = new DeviceSSOBackChannelAuthorization(
                        new Scope("device_sso"),
                        accessTokenSpec,
                        refreshTokenSpec,
                        idTokenSpec,
                        claimsSpec,
                        null);

                assertEquals(new Scope("device_sso"), authZ.getScope());
        }


        @Test
        public void testConstructor_allowScopeWithoutDeviceSSO() {

                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var idTokenSpec = new IDTokenSpec(true, 0, null);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                var authZ = new DeviceSSOBackChannelAuthorization(
                        new Scope("openid"),
                        accessTokenSpec,
                        refreshTokenSpec,
                        idTokenSpec,
                        claimsSpec,
                        null);

                assertEquals(new Scope("openid"), authZ.getScope());
        }


        @Test(expected = NullPointerException.class)
        public void testConstructor_rejectNullAccessTokenSpec() {

                var scope = new Scope("openid", "device_sso", "email");
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var idTokenSpec = new IDTokenSpec(true, 0, null);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                new DeviceSSOBackChannelAuthorization(
                        scope,
                        null,
                        refreshTokenSpec,
                        idTokenSpec,
                        claimsSpec,
                        null);
        }


        @Test(expected = NullPointerException.class)
        public void testConstructor_rejectNullRefreshTokenSpec() {

                var scope = new Scope("openid", "device_sso", "email");
                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var idTokenSpec = new IDTokenSpec(true, 0, null);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                new DeviceSSOBackChannelAuthorization(
                        scope,
                        accessTokenSpec,
                        null,
                        idTokenSpec,
                        claimsSpec,
                        null);
        }


        @Test(expected = NullPointerException.class)
        public void testConstructor_rejectNullIDTokenSpec() {

                var scope = new Scope("openid", "device_sso", "email");
                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                new DeviceSSOBackChannelAuthorization(
                        scope,
                        accessTokenSpec,
                        refreshTokenSpec,
                        null,
                        claimsSpec,
                        null);
        }


        @Test
        public void testConstructor_openIDScope_rejectIDTokenIssueDisabled() {

                var scope = new Scope("openid");
                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var claimsSpec = new ClaimsSpec(Set.of("email", "email_verified"));

                try {
                        new DeviceSSOBackChannelAuthorization(
                                scope,
                                accessTokenSpec,
                                refreshTokenSpec,
                                new IDTokenSpec(),
                                claimsSpec,
                                null);
                        fail();
                } catch (IllegalArgumentException e) {
                        assertEquals("ID token issue must be authorized for scope openid", e.getMessage());
                }
        }


        @Test(expected = NullPointerException.class)
        public void testConstructor_rejectNullClaimsSpec() {

                var scope = new Scope("openid", "device_sso", "email");
                var accessTokenSpec = new AccessTokenSpec(10 * 60, null, TokenEncoding.SELF_CONTAINED, Optional.empty());
                var refreshTokenSpec = new RefreshTokenSpec(true, 0);
                var idTokenSpec = new IDTokenSpec(true, 0, null);

                new DeviceSSOBackChannelAuthorization(
                        scope,
                        accessTokenSpec,
                        refreshTokenSpec,
                        idTokenSpec,
                        null,
                        null);
        }
}
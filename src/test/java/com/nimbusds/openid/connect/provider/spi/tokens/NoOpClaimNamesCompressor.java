package com.nimbusds.openid.connect.provider.spi.tokens;

import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;


public class NoOpClaimNamesCompressor implements ClaimNamesCompressor {


        @Override
        public @Nullable List<String> compress(@Nullable Collection<String> claimNames) {
                return claimNames != null ? new LinkedList<>(claimNames) : null;
        }


        @Override
        public @Nullable List<String> decompress(@Nullable List<String> compressedList) {
                return compressedList;
        }
}

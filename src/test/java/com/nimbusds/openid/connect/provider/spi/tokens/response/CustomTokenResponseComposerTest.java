package com.nimbusds.openid.connect.provider.spi.tokens.response;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.AccessTokenResponse;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.rar.AuthorizationDetail;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.Tokens;
import com.nimbusds.oauth2.sdk.util.JSONArrayUtils;
import com.nimbusds.openid.connect.provider.spi.claims.CommonClaimsSource;
import com.nimbusds.openid.connect.provider.spi.claims.MockClaimsSource;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import com.nimbusds.openid.connect.provider.spi.tokens.SampleKeys;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.Test;

import java.net.URI;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class CustomTokenResponseComposerTest {


        @Test
        public void testSuccessOAuthWithRAR() throws ParseException {
		
		var rarComposer = new RARTokenResponseComposer();
		
		JSONArray rarDetails = JSONArrayUtils.parse(
			"[" +
			"  {" +
			"     \"type\": \"https://www.someorg.com/payment_initiation\"," +
			"     \"actions\": [" +
			"        \"initiate\"," +
			"        \"status\"," +
			"        \"cancel\"" +
			"     ]," +
			"     \"locations\": [" +
			"        \"https://example.com/payments\"" +
			"     ]," +
			"     \"instructedAmount\": {" +
			"        \"currency\": \"EUR\"," +
			"        \"amount\": \"123.50\"" +
			"     }," +
			"     \"creditorName\": \"Merchant123\"," +
			"     \"creditorAccount\": {" +
			"        \"iban\": \"DE02100100109307118603\"" +
			"     }," +
			"     \"remittanceInformationUnstructured\": \"Ref Number Merchant\"" +
			"  }" +
			"]"
		);
		
		var authzData = new JSONObject();
		authzData.put("rar_details", rarDetails);
		
		var origResponse = new AccessTokenResponse(new Tokens(new BearerAccessToken(3600L, null), null));
		
		TokenResponse out = rarComposer.compose(origResponse, new TokenResponseContext() {
			@Override
			public OIDCClientInformation getOIDCClientInformation() {
				var metadata = new OIDCClientMetadata();
				metadata.setRedirectionURI(URI.create("https://example.com/cb"));
				metadata.applyDefaults();
				return new OIDCClientInformation(new ClientID("123"), new Date(), metadata, null);
			}
			
			
			@Override
			public JSONObject getAuthorizationData() {
				return authzData;
			}


			@Override
			public CommonClaimsSource getCommonClaimsSource() {
				return new MockClaimsSource();
			}

			@Override
			public JWTSigner getJWTSigner() {
				return (typ, jwtClaimsSet) -> {
                                        var jwt = new SignedJWT(
                                                new JWSHeader.Builder(JWSAlgorithm.RS256)
                                                        .type(typ)
                                                        .keyID(SampleKeys.RSA_JWK_2048.getKeyID())
                                                        .build(),
                                                jwtClaimsSet
                                        );
                                        try {
                                                jwt.sign(new RSASSASigner(SampleKeys.RSA_JWK_2048.toPrivateKey()));
                                        } catch (JOSEException e) {
                                                throw new RuntimeException(e);
                                        }
                                        return jwt;
                                };
			}

			@Override
			public Issuer getIssuer() {
				return new Issuer("https://c2id.com");
			}
		});
		
		AccessTokenResponse finalResponse = out.toSuccessResponse();
		
		assertEquals(origResponse.getTokens().getBearerAccessToken(), finalResponse.getTokens().getBearerAccessToken());
		assertEquals(rarDetails, AuthorizationDetail.toJSONArray(finalResponse.getTokens().getBearerAccessToken().getAuthorizationDetails()));
		assertNull(finalResponse.getTokens().getRefreshToken());
	}
}

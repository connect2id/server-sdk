package com.nimbusds.openid.connect.provider.spi;

import jakarta.servlet.ServletContext;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the abstract servlet init context.
 */
public class ServletInitContextTest {


        static class ConcreteContext extends ServletInitContext {

                public ConcreteContext(final ServletContext servletCtx) {
                        super(servletCtx);
                }
        }


        @Test
        public void testMock() {

                ServletContext servletCtx = mock(ServletContext.class);

                InputStream is = new ByteArrayInputStream(new byte[0]);
                when(servletCtx.getResourceAsStream("/path")).thenReturn(is);

                var ctx = new ConcreteContext(servletCtx);

                assertEquals(servletCtx, ctx.getServletContext());
                assertEquals(is, ctx.getResourceAsStream("/path"));
        }
}

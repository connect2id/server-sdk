package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.crypto.HMACComputer;
import com.nimbusds.openid.connect.provider.spi.crypto.JWSVerifier;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;

import java.security.SecureRandom;
import java.util.Properties;


public class MockTokenCodecContext implements TokenCodecContext {
	
	
	private final static SecureRandom SECURE_RANDOM = new SecureRandom();
	
	
	private JWTSigner jwtSigner;
	
	
	private JWSVerifier jwsVerifier;
	
	
	private final Properties codecProperties = new Properties();
	
	
	private Issuer issuer;
	
	
	public void createJWTSigner(final RSAKey rsaJWK) {
		
		jwtSigner = (typ, jwtClaimsSet) -> {
			SignedJWT jwt = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256)
					.type(typ)
					.keyID(rsaJWK.getKeyID())
					.build(),
				jwtClaimsSet
			);
			try {
				jwt.sign(new RSASSASigner(rsaJWK));
			} catch (JOSEException e) {
				throw new RuntimeException(e);
			}
			return jwt;
		};
	}
	
	
	public void createJWSVerifier(final RSAKey rsaJWK) {
		
		jwsVerifier = jwsObject -> {
			if (! jwsObject.getHeader().getAlgorithm().equals(JWSAlgorithm.RS256)) {
				return false;
			}
			
			String keyID = jwsObject.getHeader().getKeyID();
			
			if (! rsaJWK.getKeyID().equals(keyID)) {
				return false;
			}
			
			try {
				return jwsObject.verify(new RSASSAVerifier(rsaJWK));
			} catch (JOSEException e) {
				throw new RuntimeException(e);
			}
		};
	}
	
	
	@Override
	public SecureRandom getSecureRandom() {
		return SECURE_RANDOM;
	}
	
	
	@Override
	public JWTSigner getJWTSigner() {
		return jwtSigner;
	}
	
	
	@Override
	public JWSVerifier getJWSVerifier() {
		return jwsVerifier;
	}
	
	
	@Override
	public HMACComputer getHMACComputer() {
		return null;
	}
	
	
	@Override
	public Properties getCodecProperties() {
		return codecProperties;
	}


	@Override
	public ClaimNamesCompressor getClaimNamesCompressor() {
		return new NoOpClaimNamesCompressor();
	}


	public void setIssuer(final Issuer issuer) {
		this.issuer = issuer;
	}
	
	
	@Override
	public Issuer getIssuer() {
		return issuer;
	}
}

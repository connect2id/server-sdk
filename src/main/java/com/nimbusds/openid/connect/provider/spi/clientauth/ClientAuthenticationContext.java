package com.nimbusds.openid.connect.provider.spi.clientauth;


import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;


/**
 * Client authentication context.
 */
public interface ClientAuthenticationContext extends InvocationContext {
	
	
	/**
	 * Returns the registered client information for the {@code client_id}
	 * in the client authentication.
 	 *
	 * @return The registered client information. Not {@code null}.
	 */
	OIDCClientInformation getOIDCClientInformation();
	
	
	/**
	 * Returns a unique identifier for the client authentication.
	 *
	 * @return The client authentication ID.
	 */
	ClientAuthenticationID getID();
}

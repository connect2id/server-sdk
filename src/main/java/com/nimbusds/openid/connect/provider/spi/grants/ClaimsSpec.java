package com.nimbusds.openid.connect.provider.spi.grants;


import java.util.List;
import java.util.Set;

import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.langtag.LangTagException;
import com.nimbusds.langtag.LangTagUtils;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;


/**
 * OpenID claims specification.
 */
@Immutable
public class ClaimsSpec extends BasicClaimsSpec {


	/**
	 * None (empty) claims specification.
	 */
	public static final ClaimsSpec NONE = new ClaimsSpec();


	/**
	 * The preferred claims locales, {@code null} if not specified.
	 */
	private final @Nullable List<LangTag> locales;


	/**
	 * The preferred claims transport.
	 */
	private final ClaimsTransport transport;


	/**
	 * Creates a new default OpenID claims specification (empty).
	 */
	public ClaimsSpec() {

		this(null, null, null, null, ClaimsTransport.getDefault());
	}


	/**
	 * Creates a new OpenID claims specification.
	 *
	 * @param names The names of the authorised OpenID claims, {@code null}
	 *              if none.
	 */
	public ClaimsSpec(final @Nullable Set<String> names) {

		this(names, null, null, null, ClaimsTransport.getDefault());
	}


	/**
	 * Creates a new OpenID claims specification.
	 * 
	 * @param names                The names of the authorised OpenID
	 *                             claims, {@code null} if none. The
	 *                             preferred claims locales, {@code null}
	 *                             if not specified.
	 * @param locales              The preferred claims locales,
	 *                             {@code null} if not specified.
	 * @param presetIDTokenClaims  Additional or preset claims to be
	 *                             included in the ID token, {@code null}
	 *                             if none.
	 * @param presetUserInfoClaims Additional or preset claims to be
	 *                             included in the UserInfo response,
	 *                             {@code null} if none.
	 * @param transport            The preferred claims transport. Must not
	 *                             be {@code null}.
	 */
	public ClaimsSpec(final @Nullable Set<String> names,
			  final @Nullable List<LangTag> locales,
			  final @Nullable JSONObject presetIDTokenClaims,
			  final @Nullable JSONObject presetUserInfoClaims,
			  final ClaimsTransport transport) {

		this(names, locales, null, presetIDTokenClaims, presetUserInfoClaims, transport);
	}


	/**
	 * Creates a new OpenID claims specification.
	 *
	 * @param names                The names of the authorised OpenID
	 *                             claims, {@code null} if none.
	 * @param locales              The preferred claims locales,
	 *                             {@code null} if not specified.
	 * @param data                 Optional claims fulfillment data,
	 *                             {@code null} if not specified.
	 * @param presetIDTokenClaims  Additional or preset claims to be
	 *                             included in the ID token, {@code null}
	 *                             if none.
	 * @param presetUserInfoClaims Additional or preset claims to be
	 *                             included in the UserInfo response,
	 *                             {@code null} if none.
	 * @param transport            The preferred claims transport. Must not
	 *                             be {@code null}.
	 */
	public ClaimsSpec(final @Nullable Set<String> names,
			  final @Nullable List<LangTag> locales,
			  final @Nullable JSONObject data,
			  final @Nullable JSONObject presetIDTokenClaims,
			  final @Nullable JSONObject presetUserInfoClaims,
			  final ClaimsTransport transport) {

		super(names, data, new PresetClaims(presetIDTokenClaims, presetUserInfoClaims));

		this.locales = locales;
		this.transport = transport;
	}


	/**
	 * Returns the preferred OpenID claims locales.
	 *
	 * @return The preferred OpenID claims locales, {@code null} if
	 *         not specified.
	 */
	public @Nullable List<LangTag> getLocales() {

		return locales;
	}


	/**
	 * Returns the preferred claims transport.
	 *
	 * @return The preferred claims transport.
	 */
	public ClaimsTransport getTransport() {
		
		return transport;
	}


	/**
	 * Returns a JSON object representation of this claims specification.
	 *
	 * @return The JSON object.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = super.toJSONObject();

		if (locales != null) {
			o.put("claims_locales", LangTagUtils.toStringList(locales));
		}

		o.put("claims_transport", transport.toString());

		return o;
	}


	/**
	 * Parses an OpenID claims specification from the specified
	 * JSON object.
	 *
	 * @param o The JSON object. Must not be {@code null}.
	 *
	 * @return The OpenID claims specification.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static ClaimsSpec parse(final JSONObject o)
		throws ParseException {

		BasicClaimsSpec basicSpec = BasicClaimsSpec.parse(o);

		List<LangTag> claimsLocales = null;

		if (JSONObjectUtils.containsKey(o, "claims_locales")) {

			try {
				claimsLocales = LangTagUtils.parseLangTagList(JSONObjectUtils.getStringArray(o, "claims_locales"));

			} catch (LangTagException e) {

				throw new ParseException("Invalid claims locales value: " + e.getMessage(), e);
			}
		}

		ClaimsTransport claimsTransport;

		if (o.containsKey("claims_transport")) {
			String c = JSONObjectUtils.getString(o, "claims_transport");

			try {
				claimsTransport = ClaimsTransport.valueOf(c.toUpperCase());
			} catch (IllegalArgumentException e) {
				throw new ParseException("Invalid claims transport");
			}
		} else {
			// Defaults to UserInfo if not specified
			claimsTransport = ClaimsTransport.getDefault();
		}

		return new ClaimsSpec(
			basicSpec.getNames(),
			claimsLocales,
			basicSpec.getData(),
			basicSpec.getPresetIDTokenClaims(),
			basicSpec.getPresetUserInfoClaims(),
			claimsTransport);
	}
}

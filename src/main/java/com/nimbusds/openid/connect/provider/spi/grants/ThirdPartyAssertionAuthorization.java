package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;


/**
 * Authorisation produced by a {@link GrantHandler grant handler} of assertions
 * (SAML 2.0 or JWT bearer) issued by a third-party security token service.
 *
 * <p>Required authorisation details:
 *
 * <ul>
 *     <li>The subject (end-user).
 *     <li>The client identifier, must be registered with the Connect2id
 *         server.
 *     <li>The authorised scope.
 * </ul>
 *
 * <p>All other parameters are optional or have suitable defaults.
 */
@Immutable
public class ThirdPartyAssertionAuthorization extends SubjectAuthorization {


	/**
	 * The authorised client identifier, {@code null} if not specified.
	 */
	private final ClientID clientID;


	/**
	 * Creates a new authorisation for a third-party issued assertion grant
	 * where the client acts on behalf of a user.
	 *
	 * <p>See RFC 7521, section 6.3.
	 *
	 * @param subject  The subject (end-user). Must not be {@code null}.
	 * @param clientID The client identifier. Must be registered with the
	 *                 Connect2id server. Must not be {@code null}.
	 * @param scope    The authorised scope values. Must not be
	 *                 {@code null}.
	 */
	public ThirdPartyAssertionAuthorization(final Subject subject,
						final ClientID clientID,
						final Scope scope) {

		this(subject, clientID, scope, AccessTokenSpec.DEFAULT, IDTokenSpec.NONE, ClaimsSpec.NONE, null);
	}


	/**
	 * Creates a new authorisation for a third-party issued assertion grant
	 * where the client acts on behalf of a user.
	 *
	 * <p>See RFC 7521, section 6.3.
	 *
	 * @param subject         The subject (end-user). Must not be
	 *                        {@code null}.
	 * @param clientID        The client identifier. Must be registered
	 *                        with the Connect2id server. Must not be
	 *                        {@code null}.
	 * @param scope           The authorised scope values. Must not be
	 *                        {@code null}.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param idTokenSpec     The ID token specification. Must not be
	 *                        {@code null}.
	 * @param claimsSpec      The OpenID claims specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	public ThirdPartyAssertionAuthorization(final Subject subject,
						final ClientID clientID,
						final Scope scope,
						final AccessTokenSpec accessTokenSpec,
						final IDTokenSpec idTokenSpec,
						final ClaimsSpec claimsSpec,
						final @Nullable JSONObject data) {

		super(subject, scope, accessTokenSpec, idTokenSpec, claimsSpec, data);
		this.clientID = Objects.requireNonNull(clientID);
	}


	/**
	 * Creates a new authorisation for a third-party issued assertion grant
	 * where the client acts on its own behalf.
	 *
	 * <p>See RFC 7521, section 6.2.
	 *
	 * @param subject The client identifier. Must be registered with the
	 *                Connect2id server. Must not be {@code null}.
	 * @param scope   The authorised scope values. Must not be
	 *                {@code null}.
	 */
	public ThirdPartyAssertionAuthorization(final ClientID subject,
						final Scope scope) {

		this(new Subject(subject.getValue()), subject, scope, AccessTokenSpec.DEFAULT, IDTokenSpec.NONE, ClaimsSpec.NONE, null);
	}


	/**
	 * Creates a new authorisation for a third-party issued assertion grant
	 * where the client acts on its own behalf.
	 *
	 * <p>See RFC 7521, section 6.2.
	 *
	 * @param subject         The client identifier. Must be registered
	 *                        with the Connect2id server. Must not be
	 *                        {@code null}.
	 * @param scope           The authorised scope values. Must not be
	 *                        {@code null}.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	public ThirdPartyAssertionAuthorization(final ClientID subject,
						final Scope scope,
						final AccessTokenSpec accessTokenSpec,
						final @Nullable JSONObject data) {

		this(new Subject(subject.getValue()), subject, scope, accessTokenSpec, IDTokenSpec.NONE, ClaimsSpec.NONE, data);
	}


	/**
	 * Returns the authorised client.
	 *
	 * @return The authorised client identifier, {@code null} if not
	 *         specified.
	 */
	public ClientID getClientID() {

		return clientID;
	}


	@Override
	public JSONObject toJSONObject() {

		JSONObject o = super.toJSONObject();
		if (clientID != null) {
			o.put("client_id", clientID.getValue());
		}
		return o;
	}


	/**
	 * Parses a third-party assertion grant authorisation from the
	 * specified JSON object.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static ThirdPartyAssertionAuthorization parse(final JSONObject jsonObject)
		throws ParseException {

		SubjectAuthorization subAuthz = SubjectAuthorization.parse(jsonObject);

		ClientID clientID = null;
		if (jsonObject.containsKey("client_id")) {
			clientID = new ClientID(JSONObjectUtils.getString(jsonObject, "client_id"));
		}

		return new ThirdPartyAssertionAuthorization(
			subAuthz.getSubject(),
			clientID,
			subAuthz.getScope(),
			subAuthz.getAccessTokenSpec(),
			subAuthz.getIDTokenSpec(),
			subAuthz.getClaimsSpec(),
			subAuthz.getData());
	}


	/**
	 * Parses a third-party assertion grant authorisation from the
	 * specified JSON object string.
	 *
	 * @param json The JSON object string to parse. Must not be
	 *             {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static ThirdPartyAssertionAuthorization parse(final String json)
		throws ParseException {

		return parse(JSONObjectUtils.parse(json));
	}
}

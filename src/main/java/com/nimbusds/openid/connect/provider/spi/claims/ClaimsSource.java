package com.nimbusds.openid.connect.provider.spi.claims;


import java.util.List;
import java.util.Set;

import net.jcip.annotations.ThreadSafe;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.langtag.LangTag;

import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;


/**
 * Service Provider Interface (SPI) for sourcing OpenID Connect UserInfo and
 * other claims about a subject (end-user). Implementations must be thread-
 * safe.
 *
 * <p>Claims sources can be:
 *
 * <ul>
 *     <li>LDAP directories
 *     <li>SQL or NoSQL databases
 *     <li>Web services
 *     <li>Files
 * </ul>
 */
@ThreadSafe
public interface ClaimsSource extends CommonClaimsSource {


	/**
	 * Requests claims for the specified subject.
	 *
	 * @param subject       The subject. Must not be {@code null}.
	 * @param claims        The names of the requested claims, with
	 *                      optional language tags. Must not be
	 *                      {@code null}.
	 * @param claimsLocales The preferred languages and scripts for the
	 *                      claims to return, {@code null} if not
	 *                      specified.
	 *
	 * @return The claims, {@code null} if the subject wasn't found or the
	 *         claims source is {@link #isEnabled disabled}.
	 *
	 * @throws Exception If retrieval of the claims failed.
	 */
	UserInfo getClaims(final Subject subject,
			   final Set<String> claims,
			   final @Nullable List<LangTag> claimsLocales)
		throws Exception;
}


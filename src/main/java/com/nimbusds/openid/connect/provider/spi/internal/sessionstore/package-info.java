/**
 * Session store object interfaces.
 */
package com.nimbusds.openid.connect.provider.spi.internal.sessionstore;
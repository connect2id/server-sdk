package com.nimbusds.openid.connect.provider.spi;


/**
 * Service Provider Interface (SPI) lifecycle.
 */
public interface Lifecycle {


	/**
	 * Initialises the SPI implementation after it is loaded by the
	 * Connect2id Server.
	 *
	 * @param initContext The initialisation context. Can be used to
	 *                    configure and set up the SPI implementation. Not
	 *                    {@code null}.
	 *
	 * @throws Exception If initialisation failed.
	 */
	default void init(final InitContext initContext) throws Exception {}


	/**
	 * Checks if the SPI implementation is enabled and can handle requests.
	 * This can be controlled by a configuration setting or otherwise.
	 *
	 * @return {@code true} if the SPI implementation is enabled, else
	 *         {@code false}.
	 */
	default boolean isEnabled() {
		return true;
	}


	/**
	 * Shuts down the SPI implementation. This method is called on
	 * Connect2id Server shutdown.
	 *
	 * @throws Exception If proper shutdown failed.
	 */
	default void shutdown() throws Exception {}
}

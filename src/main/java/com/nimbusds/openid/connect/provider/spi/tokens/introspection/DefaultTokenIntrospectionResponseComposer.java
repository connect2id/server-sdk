package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;


/**
 * The default Connect2id server implementation of the SPI for composing token
 * introspection (RFC 7662) responses.
 *
 * <p>Outputs the following parameters, in addition to those output by the
 * parent {@link BaseTokenIntrospectionResponseComposer}:
 *
 * <ul>
 *     <li>"act" actor, in impersonation and delegation scenarios
 *     <li>"dat" additional data
 *     <li>custom top-level parameters
 * </ul>
 *
 * <p>The following OpenID claims related access token parameters are not
 * output as they are intended for the internal use, such as at the UserInfo
 * endpoint, and therefore should not be exposed to external resources.
 *
 * <ul>
 *     <li>{@link AccessTokenAuthorization#getClaimNames() consented OpenID claim names}
 *     <li>{@link AccessTokenAuthorization#getClaimsLocales() preferred claims locales}
 *     <li>{@link AccessTokenAuthorization#getClaimsData() claims fullfilment data}
 *     <li>{@link AccessTokenAuthorization#getPresetClaims() preset OpenID claims}
 *     <li>{@link AccessTokenAuthorization#getSubjectSessionKey() subject session key}
 * </ul>
 */
@ThreadSafe
public class DefaultTokenIntrospectionResponseComposer extends BaseTokenIntrospectionResponseComposer {
	
	
	@Override
	public TokenIntrospectionSuccessResponse compose(final AccessTokenAuthorization tokenAuthz,
							 final TokenIntrospectionContext context) {
		
		TokenIntrospectionSuccessResponse response = super.compose(tokenAuthz, context);
		
		if (! response.isActive()) {
			// Token invalid or expired
			return response;
		}

		TokenIntrospectionSuccessResponse.Builder builder = new TokenIntrospectionSuccessResponse.Builder(response);
		
		if (tokenAuthz.getActor() != null) {
			builder.parameter("act", tokenAuthz.getActor().toJSONObject());
		}
		
		if (tokenAuthz.getData() != null) {
			builder.parameter("dat", tokenAuthz.getData());
		}
		
		if (tokenAuthz.getOtherTopLevelParameters() != null) {
			tokenAuthz.getOtherTopLevelParameters().forEach(builder::parameter);
		}
		
		return builder.build();
	}
}

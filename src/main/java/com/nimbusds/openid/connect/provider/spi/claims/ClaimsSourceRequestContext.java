package com.nimbusds.openid.connect.provider.spi.claims;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSessionID;
import com.nimbusds.openid.connect.provider.spi.tokens.TokenEncoderContext;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * OpenID Connect claims request context. The supplied context parameters can
 * be used in the processing and accounting of a claims request.
 */
public interface ClaimsSourceRequestContext extends InvocationContext {
	
	
	/**
	 * Returns the claims transport, if applicable.
	 *
	 * @return {@link ClaimsTransport#USERINFO UserInfo} or
	 *         {@link ClaimsTransport#ID_TOKEN ID token}, {@code null} if
	 *         the claims source SPI is invoked for another purpose (e.g.
	 *         in a {@link TokenEncoderContext}).
	 */
	ClaimsTransport getClaimsTransport();
	
	
	/**
	 * Returns the optional claims fulfillment data.
	 *
	 * @return The claims fulfillment data, {@code null} if not specified.
	 */
	@Nullable JSONObject getClaimsData();


	/**
	 * Returns the identifier of the OAuth 2.0 client (client_id).
	 *
	 * @return The client ID. May be {@code null} for a claims source
	 *         request triggered within an SPI request context where the
	 *         {@code client_id} cannot be resolved.
	 */
	@Nullable ClientID getClientID();


	/**
	 * Returns the registered client information.
	 *
	 * @return The registered client information. May be {@code null} for a
	 *         claims source request triggered within an SPI request
	 *         context where the {@code client_id} cannot be resolved.
	 */
	@Nullable OIDCClientInformation getOIDCClientInformation();


	/**
	 * Returns the registered client information for the specified
	 * {@code client_id}.
	 *
	 * @param clientID The client ID.
	 *
	 * @return The registered client information, {@code null} if the
	 *         {@code client_id} is invalid.
	 */
	@Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID);
	
	
	/**
	 * Returns the client IP address.
	 *
	 * @return The client IP address, {@code null} if not available.
	 */
	@Nullable String getClientIPAddress();
	
	
	/**
	 * Returns the received and successfully validated UserInfo access
	 * token for the claims request. If a claims request is triggered in a
	 * OpenID Connect implicit and hybrid flows, where the claims are
	 * returned as part of the ID token, an access token is not involved
	 * and hence not returned by this method.
	 *
	 * <p>The claims source may use the UserInfo access token for the
	 * retrieval of aggregated and distributed claims, where the same token
	 * is recognised by the upstream claims providers. See OpenID Connect
	 * Core 1.0, section 5.6.
	 *
	 * @return The UserInfo access token, {@code null} if the claims
	 *         request wasn't triggered by a UserInfo request.
	 */
	@Nullable AccessToken getUserInfoAccessToken();


	/**
	 * Returns the ID of the associated subject (end-user) session where
	 * the claims sourcing was authorised.
	 *
	 * @return The subject session ID, {@code null} if closed or expired,
	 *         or not available (due to the session key not being encoded
	 *         into the access token where applicable, or other reasons).
	 */
	default @Nullable SubjectSessionID getSubjectSessionID() {
		return null;
	}


	/**
	 * Returns the associated subject (end-user) session where the claims
	 * sourcing was authorised.
	 *
	 * <p>The subject session is supplied in the following cases:
	 *
	 * <ul>
	 *     <li>Claims sourcing for the UserInfo endpoint where the subject
	 *         session where the claims consent occurred is still present
	 *         (not expired or closed)
	 *     <li>Claims sourcing for ID token issue in response to an OAuth
	 *         2.0 authorisation code, implicit (including OpenID Connect
	 *         hybrid response type) and refresh token grants.
	 *     <li>Claims sourcing for a direct authorisation request where a
	 *         valid subject session ID was supplied, or a new subject
	 *         session was created.
	 * </ul>
	 *
	 * @return The subject session, {@code null} if closed or expired, or
	 *         not available (due to the session key not being encoded into
	 *         the access token where applicable, or other reasons).
	 */
	default @Nullable SubjectSession getSubjectSession() {
		return null;
	}


	/**
	 * Returns the associated consented scope.
	 *
	 * <ul>
	 *     <li>When sourcing claims for a UserInfo endpoint response this
	 *         is the scope of the access token.
	 *     <li>When sourcing claims for an ID token to be returned at the
	 *         token endpoint this is the scope of the OAuth 2.0 grant
	 *         (such as an {@code authorization_code} or
	 *         {@code refresh_token} grant).
	 *     <li>When sourcing claims for an ID token to be returned at the
	 *         authorisation endpoint (for a {@code response_type} that
	 *         contains the {@code id_token} value) this is the scope of
	 *         the end-user consent.
	 *     <li>When sourcing claims for an ID token returned at the
	 *         Connect2id server direct authorisation endpoint.
	 * </ul>
	 *
	 * <p>In all other cases the scope is not provided and will be
	 * {@code null}.
	 *
	 * @return The associated consented scope, {@code null} if not
	 *         applicable.
	 */
	default @Nullable Scope getScope() {
		return null;
	}
}

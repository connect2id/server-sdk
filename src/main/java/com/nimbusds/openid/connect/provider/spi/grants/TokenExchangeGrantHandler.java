package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.tokenexchange.TokenExchangeGrant;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import net.jcip.annotations.ThreadSafe;


/**
 * Service Provider Interface (SPI) for handling token exchange grants. Returns
 * a {@link TokenExchangeAuthorization token exchange authorisation} on
 * success. Must throw a {@link GeneralException} with an
 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_GRANT invalid_grant}
 * error code if the {@code subject_token} or the optional {@code actor_token}
 * are invalid.
 *
 * <p>Implementations must be thread-safe.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OAuth 2.0 Token Exchange (RFC 8693).
 * </ul>
 */
@ThreadSafe
public interface TokenExchangeGrantHandler extends GrantHandler {
	
	
	/**
	 * The handled grant type.
	 */
	GrantType GRANT_TYPE = GrantType.TOKEN_EXCHANGE;
	
	
	@Override
	default GrantType getGrantType() {
		return GRANT_TYPE;
	}
	
	
	/**
	 * Handles a token exchange request from a client registered with the
	 * Connect2id server.
	 *
	 * @param grant              The token exchange grant. Not
	 *                           {@code null}.
	 * @param tokenRequestParams The token request parameters, such as the
	 *                           requested scope. Not {@code null}.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param confidentialClient {@code true} if the client is
	 *                           confidential and has been authenticated,
	 *                           else {@code false}.
	 * @param clientMetadata     The OAuth 2.0 client / OpenID relying
	 *                           party metadata. Not {@code null}.
	 * @param tokenIntrospection Token introspection interface for locally
	 *                           issued subject tokens. Not {@code null}.
	 * @param tokenIssueHelpers  Token issue helpers. Not {@code null}.
	 * @param invocationCtx      The invocation context. Not {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	@Deprecated
	default TokenExchangeAuthorization processGrant(final TokenExchangeGrant grant,
						        final TokenRequestParameters tokenRequestParams,
						        final ClientID clientID,
						        final boolean confidentialClient,
						        final OIDCClientMetadata clientMetadata,
						        final TokenIntrospection tokenIntrospection,
						        final TokenIssueHelpers tokenIssueHelpers,
						        final InvocationContext invocationCtx)
		throws GeneralException {

		return null;
	}



	/**
	 * Handles a token exchange request from a client registered with the
	 * Connect2id server.
	 *
	 * @param grant              The token exchange grant. Not
	 *                           {@code null}.
	 * @param tokenRequestParams The token request parameters, such as the
	 *                           requested scope. Not {@code null}.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param confidentialClient {@code true} if the client is
	 *                           confidential and has been authenticated,
	 *                           else {@code false}.
	 * @param clientMetadata     The OAuth 2.0 client / OpenID relying
	 *                           party metadata. Not {@code null}.
	 * @param tokenIntrospection Token introspection interface for locally
	 *                           issued subject tokens. Not {@code null}.
	 * @param tokenIssueHelpers  Token issue helpers. Not {@code null}.
	 * @param handlerCtx         The handler context. Not {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	default TokenExchangeAuthorization processGrant(final TokenExchangeGrant grant,
						        final TokenRequestParameters tokenRequestParams,
						        final ClientID clientID,
						        final boolean confidentialClient,
						        final OIDCClientMetadata clientMetadata,
						        final TokenIntrospection tokenIntrospection,
						        final TokenIssueHelpers tokenIssueHelpers,
						        final GrantHandlerContext handlerCtx)
		throws GeneralException {

		return processGrant(grant, tokenRequestParams, clientID, confidentialClient, clientMetadata, tokenIntrospection, tokenIssueHelpers, (InvocationContext) handlerCtx);
	}
}

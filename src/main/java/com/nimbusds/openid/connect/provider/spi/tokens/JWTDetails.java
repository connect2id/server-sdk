package com.nimbusds.openid.connect.provider.spi.tokens;


import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jwt.JWTClaimsSet;


/**
 * JSON Web Token (JWT) encoder / decoder details.
 */
public interface JWTDetails {
	
	
	/**
	 * Returns the JWT "typ" (type) header value to use.
	 *
	 * @return The "typ" (type) header value, {@code null} if none.
	 */
	@Nullable JOSEObjectType getType();
	
	
	/**
	 * Returns the JWT claims set.
	 *
	 * @return The JWT claims set.
	 */
	JWTClaimsSet getJWTClaimsSet();
}

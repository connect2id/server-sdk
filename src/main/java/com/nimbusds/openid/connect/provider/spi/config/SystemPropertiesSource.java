package com.nimbusds.openid.connect.provider.spi.config;


import java.util.Properties;


/**
 * Service Provider Interface (SPI) for sourcing Java system properties at
 * Connect2id server startup. The system properties can be used to override
 * selected or all Connect2id server configuration properties.
 */
public interface SystemPropertiesSource {
	
	
	/**
	 * Returns properties to be merged into the existing Java system
	 * properties at Connect2id server startup.
	 *
	 * @return The properties.
	 */
	Properties getProperties();
}

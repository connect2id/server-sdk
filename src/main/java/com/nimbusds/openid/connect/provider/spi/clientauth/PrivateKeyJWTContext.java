package com.nimbusds.openid.connect.provider.spi.clientauth;


import com.nimbusds.jose.JWSHeader;


/**
 * {@code private_key_jwt} client authentication context.
 */
public interface PrivateKeyJWTContext extends ClientAuthenticationContext {
	
	
	/**
	 * Returns the {@code private_key_jwt} header.
	 *
	 * @return The header.
	 */
	JWSHeader getJWSHeader();
}

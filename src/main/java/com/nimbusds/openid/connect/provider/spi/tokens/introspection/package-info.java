/**
 * OAuth 2.0 token introspection related SPI.
 */
package com.nimbusds.openid.connect.provider.spi.tokens.introspection;
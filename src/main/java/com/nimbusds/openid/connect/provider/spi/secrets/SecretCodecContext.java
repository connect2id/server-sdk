package com.nimbusds.openid.connect.provider.spi.secrets;


import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * Client secret codec context.
 */
public interface SecretCodecContext extends InvocationContext {
	
	
	/**
	 * Returns the issuer JWK set. Can be used to obtain configured server
	 * key(s) to perform symmetric encryption on the client secret.
	 *
	 * @return The issuer JWK set.
	 */
	JWKSet getIssuerJWKSet();
	
	
	/**
	 * Returns the metadata of the associated OAuth 2.0 / OpenID Connect
	 * client.
	 *
	 * @return The client metadata.
	 */
	OIDCClientMetadata getOIDCClientMetadata();
}

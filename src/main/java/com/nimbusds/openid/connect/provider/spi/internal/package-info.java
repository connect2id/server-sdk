/**
 * Interfaces representing common internal Connect2id server objects.
 */
package com.nimbusds.openid.connect.provider.spi.internal;
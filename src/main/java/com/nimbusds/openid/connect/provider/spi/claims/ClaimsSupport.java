package com.nimbusds.openid.connect.provider.spi.claims;


import java.util.Set;

import net.jcip.annotations.ThreadSafe;


/**
 * Interface for querying OpenID Connect claims support. Implementations must
 * be thread-safe.
 */
@ThreadSafe
public interface ClaimsSupport {


	/**
	 * Returns the names of the supported OpenID Connect claims.
	 *
	 * <p>Wildcard character use:
	 *
	 * <ul>
	 *     <li>Support for a pattern of claims can be indicated with the
	 *         '*' wildcard character, e.g. "https://idp.example.com/*" for
	 *         a set of claims having a common URI prefix.
	 *     <li>Returning a single claim set to "*" indicates support for
	 *         all requested claims (for a catch-all claim source).
	 * </ul>
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * name
	 * email
	 * email_verified
	 * </pre>
	 *
	 * @return The supported claim names. Should not include the reserved
	 *         {@code sub} (subject) claim name.
	 */
	Set<String> supportedClaims();
}

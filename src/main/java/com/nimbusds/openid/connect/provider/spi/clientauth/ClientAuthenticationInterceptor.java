package com.nimbusds.openid.connect.provider.spi.clientauth;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Service Provider Interface (SPI) for intercepting successful and failed
 * client authentications at all Connect2id server endpoints where such
 * authentication occurs, such as the token, token introspection, token
 * revocation and pushed authorisation request (PAR) endpoints.
 *
 * <p>Successful client authentications can be subjected to additional checks
 * and rejected with an {@link InvalidClientException} to produce an OAuth 2.0
 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_CLIENT invalid_client}
 * error.
 *
 * <p>Implementations must be thread-safe. Interceptors that emit events should
 * use a separate thread for blocking operations.
 */
@ThreadSafe
public interface ClientAuthenticationInterceptor extends Lifecycle {
	
	
	/**
	 * Intercepts a successful client authentication. The default
	 * implementation takes no action.
	 *
	 * @param clientAuth The client authentication. Not {@code null}.
	 * @param ctx        The client authentication context. Not
	 *                   {@code null}.
	 *
	 * @throws InvalidClientException To reject the client authentication
	 * with an {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_CLIENT
	 * invalid_client} error. The exception message will be logged
	 * internally and will not be exposed to the client in the
	 * {@code invalid_client} error description.
	 */
	default void interceptSuccess(final ClientAuthentication clientAuth,
				      final ClientAuthenticationContext ctx)
		throws InvalidClientException {
	}
	
	
	/**
	 * Intercepts a failed client authentication. The default
	 * implementation takes no action.
	 *
	 * @param clientAuth The client authentication. Not {@code null}.
	 * @param exception  The invalid client exception. Not {@code null}.
	 * @param ctx        The client authentication context. Not
	 *                   {@code null}.
	 */
	default void interceptError(final ClientAuthentication clientAuth,
				    final InvalidClientException exception,
				    final ClientAuthenticationContext ctx) {
		
	}
}

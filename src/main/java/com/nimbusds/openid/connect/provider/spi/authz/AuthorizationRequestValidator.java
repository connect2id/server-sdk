package com.nimbusds.openid.connect.provider.spi.authz;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Service Provider Interface (SPI) for performing additional validation of
 * OAuth 2.0 authorisation / OpenID authentication requests.
 *
 * <p>The {@link #validateAuthorizationRequest} method will be called after the
 * Connect2id server has performed standard validation of the OAuth 2.0
 * authorisation / OpenID authentication request, such as checking the
 * {@code client_id} and {@code redirect_uri}. JWT-secured authorisation
 * requests (JAR) will be unwrapped / resolved before that. The original raw
 * request can be obtained from the
 * {@link ValidatorContext#getRawRequest() context}.
 *
 * <p>The validated request can be returned modified. Modifications should be
 * limited to optional parameters. Parameters such as {@code client_id},
 * {@code response_type}, {@code redirect_uri} and {@code state} must not be
 * modified.
 *
 * <p>The {@link #validateAuthorizationRequest} method can reject the request
 * by throwing a {@link InvalidAuthorizationRequestException} with an
 * appropriate error code and optional description. When the request is
 * rejected the redirection back to the OAuth 2.0 client can also optionally be
 * disabled.
 *
 * <p>Example:
 *
 * <pre>
 * throw new InvalidAuthorizationRequestException(
 * 	"Scope not accepted", // will be logged
 * 	OAuth2Error.INVALID_SCOPE.setDescription("Scope not accepted: some_scope"),
 *      false // redirection not disabled
 * );
 * </pre>
 *
 * <p>Example resulting response:
 *
 * <pre>
 * HTTP/1.1 302 Found
 * Location: https://client.example.com/cb?
 *  error=invalid_scope
 *  &amp;error_description=Scope%20not%20accepted%3A%20some_scope
 *  &amp;state=UeFi0Eu3siPaJahl
 * </pre>
 *
 * <p>Implementations must be thread-safe.
 */
@ThreadSafe
public interface AuthorizationRequestValidator extends Lifecycle {
	
	
	/**
	 * Validates the specified OAuth 2.0 authorisation / OpenID
	 * authentication request.
	 *
	 * @param authzRequest The request to perform additional validation on.
	 *                     Can be cast to
	 *                     {@link com.nimbusds.openid.connect.sdk.AuthenticationRequest}
	 *                     for an instance of an OpenID authentication
	 *                     request. Not {@code null}.
	 * @param validatorCtx The authorisation request validator context. Not
	 *                     {@code null}.
	 *
	 * @return The validated OAuth 2.0 authorisation / OpenID
	 * authentication request. It may be modified. Must not be
	 * {@code null}.
	 *
	 * @throws InvalidAuthorizationRequestException If the request is
	 *                                              rejected.
	 */
	AuthorizationRequest validateAuthorizationRequest(final AuthorizationRequest authzRequest,
							  final ValidatorContext validatorCtx)
		throws InvalidAuthorizationRequestException;
}

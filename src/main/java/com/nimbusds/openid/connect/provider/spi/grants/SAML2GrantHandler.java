package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GrantType;


/**
 * Marker interface for SAML 2.0 bearer assertion grant handlers.
 */
public interface SAML2GrantHandler extends GrantHandler {
	

	/**
	 * The handled grant type.
	 */
	GrantType GRANT_TYPE = GrantType.SAML2_BEARER;
	
	
	@Override
	default GrantType getGrantType() {
		return GRANT_TYPE;
	}
}

package com.nimbusds.openid.connect.provider.spi.events;


import java.util.EventListener;

import net.jcip.annotations.ThreadSafe;

import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Service Provider Interface (SPI) for listening to ID token issue events.
 *
 * <p>Implementations must be thread-safe. Listeners that emit events should
 * use a separate thread for blocking operations.
 */
@ThreadSafe
public interface IDTokenIssueEventListener extends Lifecycle, EventListener {
	
	
	/**
	 * This method is called when an ID token is issued.
	 *
	 * @param event        The ID token issue event.
	 * @param eventContext Provides access to additional parameters about
	 *                     the event as well as helpers for its
	 *                     processing. Not {@code null}.
	 */
	void idTokenIssued(final IDTokenIssueEvent event, final EventContext eventContext);
}

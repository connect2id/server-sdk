package com.nimbusds.openid.connect.provider.spi.clientauth;


/**
 * X.509 certificate location in a {@code private_key_jwt} context.
 */
public enum CertificateLocation {
	
	
	/**
	 * JSON Web Signature (JWS) header.
	 */
	JWS_HEADER,
	
	
	/**
	 * JSON Web Key (JWK).
	 */
	JWK
}

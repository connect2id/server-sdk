package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;


/**
 * Token issue helpers.
 */
public interface TokenIssueHelpers {
	
	
	/**
	 * Returns an interface for signing JSON Web Tokens (JWT). May be used
	 * to sign tokens produced by the grant handler.
	 *
	 * @return The JWT signer.
	 */
	JWTSigner getJWTSigner();
}

package com.nimbusds.openid.connect.provider.spi.internal.sessionstore;


/**
 * Subject (end-user) session ID.
 */
public interface SubjectSessionID {


        /**
         * Returns the string value.
         *
         * @return The string value.
         */
        String getValue();
}

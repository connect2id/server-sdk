package com.nimbusds.openid.connect.provider.spi.grants;


import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.Scope;


/**
 * Default token request parameters implementation.
 */
public class DefaultTokenRequestParameters implements TokenRequestParameters{
	
	
	/**
	 * The requested scope.
	 */
	private final @Nullable Scope scope;
	
	
	/**
	 * The requested resource server URI(s).
	 */
	private final @Nullable List<URI> resources;
	
	
	/**
	 * Custom token request parameters.
	 */
	private final Map<String,List<String>> customParams;
	
	
	/**
	 * Creates a new token request parameters instance.
	 *
	 * @param scope The requested scope, {@code null} if not specified.
	 */
	public DefaultTokenRequestParameters(final @Nullable Scope scope) {
		this(scope, null, Collections.emptyMap());
	}
	
	
	/**
	 * Creates a new token request parameters instance.
	 *
	 * @param scope        The requested scope, {@code null} if not
	 *                     specified.
	 * @param resources    The requested resource server URI(s),
	 *                     {@code null} if not specified.
	 * @param customParams Custom parameters, empty map if none.
	 */
	public DefaultTokenRequestParameters(final @Nullable Scope scope,
					     final @Nullable List<URI> resources,
					     final Map<String, List<String>> customParams) {
		this.scope = scope;
		this.resources = resources;
		Objects.requireNonNull(customParams);
		this.customParams = customParams;
	}
	
	
	@Override
	public @Nullable Scope getScope() {
		return scope;
	}
	
	
	@Override
	public @Nullable List<URI> getResources() {
		return resources;
	}
	
	
	@Override
	public Map<String, List<String>> getCustomParameters() {
		return customParams;
	}
}

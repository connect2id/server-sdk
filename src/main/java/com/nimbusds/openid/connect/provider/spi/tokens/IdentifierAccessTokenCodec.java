package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.oauth2.sdk.id.Identifier;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;
import net.jcip.annotations.ThreadSafe;


/**
 * Service Provider Interface (SPI) for generating and decoding
 * identifier-based access tokens. Implementations must be thread-safe.
 */
@ThreadSafe
public interface IdentifierAccessTokenCodec extends Lifecycle {
	
	
	/**
	 * Generates a new identifier-based access token.
	 *
	 * @param tokenAuthz The access token authorisation. Not {@code null}.
	 * @param context    The token encoder context. Not {@code null}.
	 *
	 * @return The identifier-based access token.
	 */
	IdentifierAccessToken generate(final AccessTokenAuthorization tokenAuthz, final TokenEncoderContext context);
	
	
	/**
	 * Decodes the specified bearer access token value to extract the token
	 * identifier.
	 *
	 * @param tokenValue The bearer access token value. Not {@code null}.
	 * @param context    The token codec context. Not {@code null}.
	 *
	 * @return The access token identifier.
	 *
	 * @throws TokenDecodeException If decoding failed.
	 */
	Identifier decode(final String tokenValue, final TokenCodecContext context)
		throws TokenDecodeException;
}

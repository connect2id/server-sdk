package com.nimbusds.openid.connect.provider.spi.events;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.sdk.claims.IDTokenClaimsSet;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.EventObject;
import java.util.Objects;
import java.util.Set;


/**
 * Subject authentication and consent event.
 */
@Immutable
public class SubjectAuthAndConsentEvent extends EventObject {


        /**
         * The authenticated subject.
         */
        private final Subject subject;


        /**
         * The client ID.
         */
        private final ClientID clientID;


        /**
         * The ID token claims set, {@code null} if no ID token was issued.
         */
        private final @Nullable IDTokenClaimsSet idTokenClaimsSet;


        /**
         * The consented scope, {@code null} if none.
         */
        private final @Nullable Scope scope;


        /**
         * The names of the consented claims, {@code null} if none.
         */
        private final @Nullable Set<String> claimNames;


        /**
         * The authorisation data, {@code null} if none.
         */
        private final @Nullable JSONObject authzData;


        /**
         * The associated subject session, {@code null} if the subject session
         * has expired in the time window between the event and the event
         * dispatch.
         */
        private final @Nullable SubjectSession subjectSession;


        /**
         * Creates a new subject authentication and consent event.
         *
         * @param source           The event originator.
         * @param subject          The authenticated subject.
         * @param clientID         The client ID.
         * @param idTokenClaimsSet The ID token JWT claims set, {@code null} if
         *                         no ID token was issued.
         * @param scope            The consented scope, {@code null} if none.
         * @param claimNames       The names of the consented claims,
         *                         {@code null} if none.
         * @param authzData        The authorisation data, {@code null} if
         *                         none.
         * @param subjectSession   The associated subject session,
         *                         {@code null} if the subject session has
         *                         expired in the time window between the event
         *                         and the event dispatch.
         */
        public SubjectAuthAndConsentEvent(final Object source,
                                          final Subject subject,
                                          final ClientID clientID,
                                          final @Nullable IDTokenClaimsSet idTokenClaimsSet,
                                          final @Nullable Scope scope,
                                          final @Nullable Set<String> claimNames,
                                          final @Nullable JSONObject authzData,
                                          final @Nullable SubjectSession subjectSession) {
                super(source);
                this.subject = Objects.requireNonNull(subject);
                this.clientID = Objects.requireNonNull(clientID);
                this.idTokenClaimsSet = idTokenClaimsSet;
                this.scope = scope;
                this.claimNames = claimNames;
                this.authzData = authzData;
                this.subjectSession = subjectSession;
        }


        /**
         * Returns the ID token claims set for the authenticated subject.
         *
         * @return The ID token claims set, {@code null} if no ID token was
         *         issued.
         */
        public IDTokenClaimsSet getIDTokenClaimsSet() {
                return idTokenClaimsSet;
        }


        /**
         * Returns the authenticated subject.
         *
         * @return The subject.
         */
        public Subject getSubject() {
                return subject;
        }


        /**
         * Returns the client ID.
         *
         * @return The client ID.
         */
        public ClientID getClientID() {
                return clientID;
        }


        /**
         * Returns the consented scope.
         *
         * @return The consented scope, {@code null} if none.
         */
        public @Nullable Scope getScope() {
                return scope;
        }


        /**
         * Returns the names of the consented claims.
         *
         * @return The names of the consented claims, {@code null} if none.
         */
        public @Nullable Set<String> getClaimNames() {
                return claimNames;
        }


        /**
         * Returns the authorisation data.
         *
         * @return The authorisation data, {@code null} if none.
         */
        public @Nullable JSONObject getAuthorizationData() {
                return authzData;
        }


        /**
         * Returns the associated subject session.
         *
         * @return The subject session, {@code null} if the subject session has
         *         expired in the time window between the event and the event
         *         dispatch.
         */
        public @Nullable SubjectSession getSubjectSession() {
                return subjectSession;
        }
}

/**
 * Client / relying party registration SPIs.
 */
package com.nimbusds.openid.connect.provider.spi.reg;
package com.nimbusds.openid.connect.provider.spi.grants;


import java.util.ArrayList;
import java.util.List;

import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * Base token specification.
 */
@Immutable
public class TokenSpec {


	/**
	 * The token lifetime, in seconds. For access tokens zero and negative
	 * means not specified (to let the Connect2id server apply the default
	 * configured access token lifetime). For refresh tokens zero means no
	 * lifetime limit and negative means not specified (to let the
	 * Connect2id server apply the default configured refresh token
	 * lifetime).
	 */
	private final long lifetime;


	/**
	 * Explicit list of audiences for the token, {@code null} if none.
	 */
	private final @Nullable List<Audience> audList;


	/**
	 * The subject in impersonation and delegation cases, {@code null} if
	 * not applicable.
	 */
	private final @Nullable Subject impersonatedSubject;


	/**
	 * Creates a new token specification. No explicit token audience is
	 * specified. No subject in impersonation and delegation cases is
	 * specified.
	 *
	 * @param lifetime The token lifetime, in seconds. For access tokens
	 *                 zero and negative means not specified (to let the
	 *                 Connect2id server apply the default configured
	 *                 access token lifetime). For refresh tokens zero
	 *                 means no lifetime limit and negative means not
	 *                 specified (to let the Connect2id server apply the
	 *                 default configured refresh token lifetime).
	 */
	public TokenSpec(final long lifetime) {

		this(lifetime, null, null);
	}


	/**
	 * Creates a new token specification.
	 *
	 * @param lifetime            The token lifetime, in seconds. For
	 *                            access tokens zero and negative means not
	 *                            specified (to let the Connect2id
	 *                            server apply the default configured
	 *                            access token lifetime). For refresh
	 *                            tokens zero means no lifetime limit and
	 *                            negative means not specified (to let the
	 *                            Connect2id server apply the default
	 *                            configured refresh token lifetime).
	 * @param audList             Explicit list of audiences for the token,
	 *                            {@code null} if not specified.
	 * @param impersonatedSubject The subject in impersonation and
	 *                            delegation cases, {@code null} if not
	 *                            applicable.
	 */
	public TokenSpec(final long lifetime, final @Nullable List<Audience> audList, final @Nullable Subject impersonatedSubject) {

		this.lifetime = lifetime;
		this.audList = audList;
		this.impersonatedSubject = impersonatedSubject;
	}


	/**
	 * Returns the token lifetime.
	 *
	 * @return The token lifetime, in seconds. For access tokens zero and
	 *         negative means not specified (to let the Connect2id server
	 *         apply the default configured access token lifetime). For
	 *         refresh tokens zero means no lifetime limit and negative
	 *         means not specified (to let the Connect2id server apply the
	 *         default configured refresh token lifetime).
	 */
	public long getLifetime() {

		return lifetime;
	}


	/**
	 * Returns the explicit list of audiences for the token.
	 *
	 * @return The explicit list of audiences for the token, {@code null}
	 *         if not specified.
	 */
	public @Nullable List<Audience> getAudience() {

		return audList;
	}


	/**
	 * Returns the subject in impersonation and delegation cases.
	 *
	 * @return The subject in impersonation and delegation cases,
	 *         {@code null} if not applicable.
	 */
	public @Nullable Subject getImpersonatedSubject() {

		return impersonatedSubject;
	}


	/**
	 * Returns a JSON object representation of this token specification.
	 *
	 * @return The JSON object.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = new JSONObject();

		if (lifetime >= 0L) {
			o.put("lifetime", lifetime);
		}

		if (CollectionUtils.isNotEmpty(audList)) {

			List<String> sl = new ArrayList<>(audList.size());

			for (Audience aud: audList) {
				sl.add(aud.getValue());
			}

			o.put("audience", sl);
		}

		if (impersonatedSubject != null) {
			o.put("impersonated_sub", impersonatedSubject.getValue());
		}

		return o;
	}


	@Override
	public String toString() {

		return toJSONObject().toJSONString();
	}


	/**
	 * Parses a token specification from the specified JSON object.
	 *
	 * @param jsonObject The JSON object. Must not be {@code null}.
	 *
	 * @return The token specification.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static TokenSpec parse(final JSONObject jsonObject)
		throws ParseException {

		long lifetime = -1L;

		if (jsonObject.containsKey("lifetime")) {
			lifetime = JSONObjectUtils.getLong(jsonObject, "lifetime");
		}

		List<Audience> audList = null;
		
		if (jsonObject.containsKey("audience")) {
			audList = Audience.create(JSONObjectUtils.getStringList(jsonObject, "audience"));
		}

		Subject impersonatedSub = null;

		if (jsonObject.containsKey("impersonated_sub")) {
			try {
				impersonatedSub = new Subject(JSONObjectUtils.getString(jsonObject, "impersonated_sub"));
			} catch (IllegalArgumentException e) {
				// On invalid subject
				throw new ParseException(e.getMessage(), e);
			}
		}

		return new TokenSpec(lifetime, audList, impersonatedSub);
	}
}

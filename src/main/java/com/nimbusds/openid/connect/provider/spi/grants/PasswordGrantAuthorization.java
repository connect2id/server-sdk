package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * Authorisation produced by a {@link PasswordGrantHandler}. Specifies a
 * subject (end-user) and permits ID and refresh token issue.
 *
 * <p>Required authorisation details:
 *
 * <ul>
 *     <li>The authenticated subject (end-user).
 *     <li>The authorised scope.
 * </ul>
 *
 * <p>All other parameters are optional or have suitable defaults.
 */
@Immutable
public class PasswordGrantAuthorization extends SubjectAuthorization {


	/**
	 * Controls the authorisation lifetime, {@code true} for a long-lived
	 * (implies persistence), {@code false} for a short-lived (transient).
	 */
	private final boolean longLived;


	/**
	 * The refresh token specification.
	 */
	private final RefreshTokenSpec refreshTokenSpec;


	/**
	 * Creates a new OAuth 2.0 - only authorisation for a password grant.
	 *
	 * @param subject          The subject (end-user) identifier. Must not
	 *                         be {@code null}.
	 * @param scope            The authorised scope values. Must not be
	 *                         {@code null}.
	 */
	public PasswordGrantAuthorization(final Subject subject,
					  final Scope scope) {

		this (subject, null, null, null, scope, null, false, AccessTokenSpec.DEFAULT, RefreshTokenSpec.DEFAULT,
			IDTokenSpec.NONE, ClaimsSpec.NONE, null);
	}


	/**
	 * Creates a new OAuth 2.0 - only authorisation for a password grant.
	 *
	 * @param subject          The subject (end-user) identifier. Must not
	 *                         be {@code null}.
	 * @param scope            The authorised scope values. Must not be
	 *                         {@code null}.
	 * @param audList          Explicit list of audiences for the access
	 *                         token, {@code null} if not specified.
	 * @param longLived        Controls the authorisation lifetime,
	 *                         {@code true} for a long-lived (implies
	 *                         persistence), {@code false} for a
	 *                         short-lived (transient).
	 * @param accessTokenSpec  The access token specification. Must not
	 *                         be {@code null}.
	 * @param refreshTokenSpec The refresh token specification. Must not
	 *                         be {@code null}.
	 * @param data             Additional data as a JSON object,
	 *                         {@code null} if not specified.
	 */
	@Deprecated
	public PasswordGrantAuthorization(final Subject subject,
					  final Scope scope,
					  final @Nullable List<Audience> audList,
					  final boolean longLived,
					  final AccessTokenSpec accessTokenSpec,
					  final RefreshTokenSpec refreshTokenSpec,
					  final @Nullable JSONObject data) {

		this (subject, null, null, null, scope, audList, longLived, accessTokenSpec, refreshTokenSpec,
			IDTokenSpec.NONE, ClaimsSpec.NONE, data);
	}


	/**
	 * Creates a new OAuth 2.0 - only authorisation for a password grant.
	 *
	 * @param subject          The subject (end-user) identifier. Must not
	 *                         be {@code null}.
	 * @param scope            The authorised scope values. Must not be
	 *                         {@code null}.
	 * @param longLived        Controls the authorisation lifetime,
	 *                         {@code true} for a long-lived (implies
	 *                         persistence), {@code false} for a
	 *                         short-lived (transient).
	 * @param accessTokenSpec  The access token specification. Must not
	 *                         be {@code null}.
	 * @param refreshTokenSpec The refresh token specification. Must not
	 *                         be {@code null}.
	 * @param data             Additional data as a JSON object,
	 *                         {@code null} if not specified.
	 */
	public PasswordGrantAuthorization(final Subject subject,
					  final Scope scope,
					  final boolean longLived,
					  final AccessTokenSpec accessTokenSpec,
					  final RefreshTokenSpec refreshTokenSpec,
					  final @Nullable JSONObject data) {

		this(subject, scope, longLived, accessTokenSpec, refreshTokenSpec, IDTokenSpec.NONE, ClaimsSpec.NONE, data);
	}


	/**
	 * Creates a new OpenID Connect / OAuth 2.0 authorisation for a
	 * password grant.
	 *
	 * @param subject          The subject (end-user) identifier. Must not
	 *                         be {@code null}.
	 * @param authTime         The time of the subject authentication. If
	 *                         {@code null} it will be set to now.
	 *                         Applies only if an ID token is issued.
	 * @param acr              The Authentication Context Class Reference
	 *                         (ACR), {@code null} if not specified.
	 *                         Applies only if an ID token is issued.
	 * @param amrList          The Authentication Methods Reference (AMR)
	 *                         list, {@code null} if not specified. Applies
	 *                         only if an ID token is issued.
	 * @param scope            The authorised scope values. Must not be
	 *                         {@code null}.
	 * @param audList          Explicit list of audiences for the access
	 *                         token, {@code null} if not specified.
	 * @param longLived        Controls the authorisation lifetime.
	 *                         {@code true} for a long-lived (implies
	 *                         persistence), {@code false} for a
	 *                         short-lived (transient).
	 * @param accessTokenSpec  The access token specification. Must not be
	 *                         {@code null}.
	 * @param refreshTokenSpec The refresh token specification. Must not be
	 *                         {@code null}.
	 * @param idTokenSpec      The ID token specification. Must not be
	 *                         {@code null}.
	 * @param claimsSpec       The OpenID claims specification.
	 * @param data             Additional data as a JSON object,
	 *                         {@code null} if not specified.
	 */
	@Deprecated
	public PasswordGrantAuthorization(final Subject subject,
					  final @Nullable Date authTime,
					  final @Nullable ACR acr,
					  final @Nullable List<AMR> amrList,
					  final Scope scope,
					  final @Nullable List<Audience> audList,
					  final boolean longLived,
					  final AccessTokenSpec accessTokenSpec,
					  final RefreshTokenSpec refreshTokenSpec,
					  final IDTokenSpec idTokenSpec,
					  final ClaimsSpec claimsSpec,
					  final @Nullable JSONObject data) {

		this(
			subject,
			scope,
			longLived,
			new AccessTokenSpec(
				accessTokenSpec.getLifetime(),
				audList,
				accessTokenSpec.getEncoding(),
				accessTokenSpec.getImpersonatedSubject(),
				accessTokenSpec.getEncryptSelfContained(),
				accessTokenSpec.getSubjectType()),
			refreshTokenSpec,
			new IDTokenSpec(
				idTokenSpec.issue(),
				idTokenSpec.getLifetime(),
				authTime,
				acr,
				amrList,
				idTokenSpec.getImpersonatedSubject()),
			claimsSpec,
			data);
	}


	/**
	 * Creates a new OpenID Connect / OAuth 2.0 authorisation for a
	 * password grant.
	 *
	 * @param subject          The subject (end-user) identifier. Must not
	 *                         be {@code null}.
	 * @param scope            The authorised scope values. Must not be
	 *                         {@code null}.
	 * @param longLived        Controls the authorisation lifetime.
	 *                         {@code true} for a long-lived (implies
	 *                         persistence), {@code false} for a
	 *                         short-lived (transient).
	 * @param accessTokenSpec  The access token specification. Must not be
	 *                         {@code null}.
	 * @param refreshTokenSpec The refresh token specification. Must not be
	 *                         {@code null}.
	 * @param idTokenSpec      The ID token specification. Must not be
	 *                         {@code null}.
	 * @param claimsSpec       The OpenID claims specification.
	 * @param data             Additional data as a JSON object,
	 *                         {@code null} if not specified.
	 */
	public PasswordGrantAuthorization(final Subject subject,
					  final Scope scope,
					  final boolean longLived,
					  final AccessTokenSpec accessTokenSpec,
					  final RefreshTokenSpec refreshTokenSpec,
					  final IDTokenSpec idTokenSpec,
					  final ClaimsSpec claimsSpec,
					  final @Nullable JSONObject data) {

		super(subject, scope, accessTokenSpec, idTokenSpec, claimsSpec, data);
		this.longLived = longLived;
		this.refreshTokenSpec = Objects.requireNonNull(refreshTokenSpec);
	}


	/**
	 * Returns the authorisation lifetime.
	 *
	 * @return {@code true} for a long-lived authorisation (implies
	 *         persistence), {@code false} for a short-lived (transient).
	 */
	public boolean isLongLived() {

		return longLived;
	}


	/**
	 * Returns the refresh token specification.
	 *
	 * @return The refresh token specification.
	 */
	public RefreshTokenSpec getRefreshTokenSpec() {

		return refreshTokenSpec;
	}


	@Override
	public JSONObject toJSONObject() {

		JSONObject o = super.toJSONObject();

		o.put("long_lived", longLived);

		if (refreshTokenSpec.issue()) {
			o.put("refresh_token", refreshTokenSpec.toJSONObject());
		}

		return o;
	}


	/**
	 * Parses a password grant authorisation from the specified JSON
	 * object.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The password grant authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static PasswordGrantAuthorization parse(final JSONObject jsonObject)
		throws ParseException {

		SubjectAuthorization authz = SubjectAuthorization.parse(jsonObject);

		boolean longLived = JSONObjectUtils.getBoolean(jsonObject, "long_lived", false);

		RefreshTokenSpec rtSpec;
		if (jsonObject.containsKey("refresh_token")) {
			rtSpec = RefreshTokenSpec.parse(JSONObjectUtils.getJSONObject(jsonObject, "refresh_token"));
		} else {
			rtSpec = RefreshTokenSpec.DEFAULT;
		}

		return new PasswordGrantAuthorization(
			authz.getSubject(),
			authz.getScope(),
			longLived,
			authz.getAccessTokenSpec(),
			rtSpec,
			authz.getIDTokenSpec(),
			authz.getClaimsSpec(),
			authz.getData());
	}


	/**
	 * Parses a password grant authorisation from the specified JSON
	 * object string.
	 *
	 * @param json The JSON object string to parse. Must not be
	 *             {@code null}.
	 *
	 * @return The password grant authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static PasswordGrantAuthorization parse(final String json)
		throws ParseException {

		return parse(JSONObjectUtils.parse(json));
	}
}

package com.nimbusds.openid.connect.provider.spi.events;


import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.id.Subject;
import net.jcip.annotations.Immutable;

import java.util.Objects;


/**
 * ID token issue event.
 */
@Immutable
public class IDTokenIssueEvent extends JWTIssueEvent {


	/**
	 * The local subject.
	 */
	private final Subject localSubject;
	
	
	/**
	 * Creates a new ID token issue event.
	 *
	 * @param source       The event originator.
	 * @param jwtClaimsSet The ID token claims set.
	 * @param localSubject The local subject.
	 */
	public IDTokenIssueEvent(final Object source,
				 final JWTClaimsSet jwtClaimsSet,
				 final Subject localSubject) {
		super(source, jwtClaimsSet);
		this.localSubject = Objects.requireNonNull(localSubject);
	}


	/**
	 * Creates a new ID token issue event.
	 *
	 * @param source       The event originator.
	 * @param jwtClaimsSet The ID token claims set.
	 */
	@Deprecated
	public IDTokenIssueEvent(final Object source, final JWTClaimsSet jwtClaimsSet) {
		super(source, jwtClaimsSet);
		localSubject = null;
	}


	/**
	 * Returns the ID token local subject. Equals the JWT {@code sub}
	 * claim value unless the subject type is pairwise.
	 *
	 * <p>Use this method if there is a need to get the local (system)
	 * subject for an ID token which subject was made pairwise for its
	 * audience (OpenID relying party).
	 *
	 * @return The local subject.
	 */
	public Subject getLocalSubject() {
		return localSubject;
	}
}

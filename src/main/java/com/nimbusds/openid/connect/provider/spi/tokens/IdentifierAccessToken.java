package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.oauth2.sdk.id.Identifier;
import net.jcip.annotations.Immutable;

import java.util.Objects;


/**
 * Identifier-based access token. The identifier must be sufficiently long and
 * random to make brute force guessing impractical. The value of the access
 * token value may be a direct string representation of the identifier, have
 * some other encoding, or include additional security protection (e.g. HMAC to
 * detect illegal / fake tokens).
 *
 * <p>Sample access token that is a 128 bit random identifier:
 *
 * <pre>NNEYDTdMd2qRiwq-GS6UiQ</pre>
 *
 * <p>Sample access token with the same 128 bit random identifier, protected
 * with HMAC SHA-256 truncated to 128 bits:
 *
 * <pre>NNEYDTdMd2qRiwq-GS6UiQ.ZTOq370aTUQbpljYhJPbHw</pre>
 */
@Immutable
public final class IdentifierAccessToken {
	
	
	/**
	 * The token identifier.
	 */
	private final Identifier id;
	
	
	/**
	 * The token value.
	 */
	private final String tokenValue;
	
	
	/**
	 * Creates a new identifier-based access token. The token value will be
	 * set to the string representation of the specified identifier.
	 *
	 * @param id The identifier to use as unique key for the token
	 *           authorisation in the Connect2id server. Must be
	 *           sufficiently long and random to make brute force guessing
	 *           impractical. Must not be {@code null}.
	 */
	public IdentifierAccessToken(final Identifier id) {
		
		this(id, id.getValue());
	}
	
	
	/**
	 * Creates a new identifier-based access token.
	 *
	 * @param id         The identifier to use as unique key for the token
	 *                   authorisation in the Connect2id server. Must be
	 *                   sufficiently long and random to make brute force
	 *                   guessing impractical. Must not be {@code null}.
	 * @param tokenValue The value of the bearer access token. May
	 *                   represent the string representation of the
	 *                   specified identifier, some other encoding, or
	 *                   include additional security protection (e.g. HMAC
	 *                   to detect illegal / fake tokens). Must not be
	 *                   {@code null}.
	 */
	public IdentifierAccessToken(final Identifier id, final String tokenValue) {
		
		this.id = Objects.requireNonNull(id);
		this.tokenValue = Objects.requireNonNull(tokenValue);
	}
	
	
	/**
	 * Returns the access token identifier.
	 *
	 * @return The access token identifier.
	 */
	public Identifier getIdentifier() {
		return id;
	}
	
	
	/**
	 * Returns the value of the bearer access token.
	 *
	 * @return The token value.
	 */
	public String getTokenValue() {
		return tokenValue;
	}
	
	
	@Override
	public String toString() {
		return getIdentifier().getValue();
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof IdentifierAccessToken)) return false;
		IdentifierAccessToken that = (IdentifierAccessToken) o;
		return Objects.equals(id, that.id);
	}
	
	
	@Override
	public int hashCode() {
		
		return Objects.hash(id);
	}
}

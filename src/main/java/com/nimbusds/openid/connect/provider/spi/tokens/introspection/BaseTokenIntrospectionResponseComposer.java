package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import java.sql.Date;

import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.oauth2.sdk.token.AccessTokenType;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;


/**
 * Base implementation of the SPI for composing token introspection (RFC 7662)
 * responses.
 *
 * <p>Outputs the introspection details specified in:
 *
 * <ul>
 *    <li>OAuth 2.0 Token Introspection (RFC 7662), section 2.2;
 *    <li>OAuth 2.0 Mutual TLS Client Authentication and Certificate Bound
 *        Access Tokens (RFC 8705), section 3.2;
 *    <li>OAuth 2.0 Demonstrating Proof-of-Possession at the Application Layer
 *        (DPoP) (draft-ietf-oauth-dpop-16), section 6.
 * </ul>
 *
 * <p>Parameters:
 *
 * <ul>
 *     <li>"active"
 *     <li>"scope"
 *     <li>"client_id"
 *     <li>"token_type"
 *     <li>"exp"
 *     <li>"iat"
 *     <li>"sub"
 *     <li>"aud"
 *     <li>"iss"
 *     <li>"jti"
 *     <li>"cnf.x5t#S256"
 *     <li>"cnf.jkt"
 * </ul>
 *
 * <p>The following non-standard access token parameters are not output by this
 * base implementation:
 *
 * <ul>
 *     <li>{@link AccessTokenAuthorization#getClaimNames() consented OpenID claim names}
 *     <li>{@link AccessTokenAuthorization#getClaimsLocales() preferred claims locales}
 *     <li>{@link AccessTokenAuthorization#getClaimsData() claims fullfilment data}
 *     <li>{@link AccessTokenAuthorization#getPresetClaims() preset OpenID claims}
 *     <li>{@link AccessTokenAuthorization#getSubjectSessionKey() subject session key}
 *     <li>{@link AccessTokenAuthorization#getActor() actor, in impersonation and delegation scenarios}
 *     <li>{@link AccessTokenAuthorization#getData() additional data}
 *     <li>{@link AccessTokenAuthorization#getOtherTopLevelParameters() custom top-level parameters}
 * </ul>
 *
 * <p>The extending class may implement output of the above non-standard
 * parameters. It may also choose not to output parameters if they are not
 * required by the client (resource server), e.g. for privacy and data
 * minimisation purposes.
 */
@ThreadSafe
public abstract class BaseTokenIntrospectionResponseComposer implements TokenIntrospectionResponseComposer {
	
	
	@Override
	public TokenIntrospectionSuccessResponse compose(final AccessTokenAuthorization tokenAuthz,
							 final TokenIntrospectionContext context) {
		
		if (tokenAuthz == null) {
			// Access token was found invalid or expired
			return new TokenIntrospectionSuccessResponse.Builder(false)
				.build();
		}
		
		AccessTokenType tokenType = tokenAuthz.getJWKThumbprintConfirmation() != null ?
			AccessTokenType.DPOP : AccessTokenType.BEARER;
		
		
		TokenIntrospectionSuccessResponse.Builder builder = new TokenIntrospectionSuccessResponse.Builder(true)
			.tokenType(tokenType)
			.subject(tokenAuthz.getSubject())
			.clientID(tokenAuthz.getClientID())
			.scope(tokenAuthz.getScope())
			.expirationTime(tokenAuthz.getExpirationTime() != null ? Date.from(tokenAuthz.getExpirationTime()) : null)
			.issueTime(tokenAuthz.getIssueTime() != null ? Date.from(tokenAuthz.getIssueTime()) : null)
			.issuer(tokenAuthz.getIssuer())
			.audience(tokenAuthz.getAudienceList())
			.jwtID(tokenAuthz.getJWTID());
		
		if (tokenAuthz.getClientCertificateConfirmation() != null) {
			builder = builder.x509CertificateConfirmation(tokenAuthz.getClientCertificateConfirmation());
		}
		
		if (tokenAuthz.getJWKThumbprintConfirmation() != null) {
			builder = builder.jwkThumbprintConfirmation(tokenAuthz.getJWKThumbprintConfirmation());
		}
		
		return builder.build();
	}
}

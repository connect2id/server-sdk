package com.nimbusds.openid.connect.provider.spi.crypto;


import net.jcip.annotations.ThreadSafe;


/**
 * Interface exposed by the Connect2id server for computing Hash-based Message
 * Authentication Codes (HMAC).
 */
@ThreadSafe
public interface HMACComputer {
	
	
	/**
	 * Computes the SHA-256 based HMAC for the specified input. Uses the
	 * secret key with JWK ID "hmac" configured for the OpenID Provider /
	 * Authorisation Server.
	 *
	 * @param input The input.
	 *
	 * @return The HMAC SHA-256.
	 */
	byte[] computeHMACSHA256(final byte[] input);
}

/**
 * Web SSO eligibility checker SPI.
 */
package com.nimbusds.openid.connect.provider.spi.websso;
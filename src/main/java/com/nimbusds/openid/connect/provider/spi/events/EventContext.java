package com.nimbusds.openid.connect.provider.spi.events;


import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;


/**
 * Event context.
 */
public interface EventContext extends InvocationContext {
	
	
	/**
	 * Returns the registered information for the client associated with
	 * the event.
	 *
	 * @return The registered client information.
	 */
	OIDCClientInformation getOIDCClientInformation();
	
	
	/**
	 * Returns an interface for signing JSON Web Tokens (JWT). May be used
	 * to sign security event tokens (SET) produced by the event listener.
	 *
	 * @return The JWT signer.
	 */
	JWTSigner getJWTSigner();
}

package com.nimbusds.openid.connect.provider.spi.par;


import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;


/**
 * Invalid Pushed Authorisation Request (PAR) exception.
 */
public class InvalidPushedAuthorizationRequestException extends Exception {
	
	
	/**
	 * The error object.
	 */
	private final ErrorObject errorObject;
	
	
	/**
	 * Creates a new invalid Pushed Authorisation Request (PAR) exception.
	 * The HTTP status is set to 400 and error code in the JSON object is
	 * set to {@link OAuth2Error#INVALID_REQUEST invalid_request}.
	 *
	 * @param message The exception message, will be logged. Should not be
	 *                {@code null}.
	 */
	public InvalidPushedAuthorizationRequestException(final @Nullable String message) {
		this(message, OAuth2Error.INVALID_REQUEST);
	}
	
	
	/**
	 * Creates a new invalid OAuth 2.0 authorisation / OpenID
	 * authentication request exception.
	 *
	 * @param message     The exception message, will be logged. Should not
	 *                    be {@code null}.
	 * @param errorObject The error object, with HTTP status, error code
	 *                    and optional error description and error URI.
	 *                    Must not be {@code null}.
	 */
	public InvalidPushedAuthorizationRequestException(final @Nullable String message,
							  final ErrorObject errorObject) {
		super(message);
		this.errorObject = Objects.requireNonNull(errorObject);
	}
	
	
	/**
	 * Returns the error object.
	 *
	 * @return The error object with HTTP status, error code and optional
	 *         error description and error URI.
	 */
	public ErrorObject getErrorObject() {
		return errorObject;
	}
}

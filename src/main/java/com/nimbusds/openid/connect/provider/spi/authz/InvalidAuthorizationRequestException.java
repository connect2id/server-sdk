package com.nimbusds.openid.connect.provider.spi.authz;


import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;


/**
 * Invalid OAuth 2.0 authorisation / OpenID authentication request exception.
 */
public class InvalidAuthorizationRequestException extends Exception {
	
	
	/**
	 * The error object.
	 */
	private final ErrorObject errorObject;
	
	
	/**
	 * {@code true} if redirection to the client is disabled.
	 */
	private final boolean redirectDisabled;
	
	
	/**
	 * Creates a new invalid OAuth 2.0 authorisation / OpenID
	 * authentication request exception. The error code is set to
	 * {@link OAuth2Error#INVALID_REQUEST invalid_request}. The exception
	 * will result in redirection back to the OAuth 2.0 client with the
	 * error.
	 *
	 * @param message The exception message, will be logged. Should not be
	 *                {@code null}.
	 */
	public InvalidAuthorizationRequestException(final @Nullable String message) {
		this(message, OAuth2Error.INVALID_REQUEST, false);
	}
	
	
	/**
	 * Creates a new invalid OAuth 2.0 authorisation / OpenID
	 * authentication request exception.
	 *
	 * @param message          The exception message, will be logged.
	 *                         Should not be {@code null}.
	 * @param errorObject      The error object, with code and optional
	 *                         description and URI. Must not be
	 *                         {@code null}.
	 * @param redirectDisabled {@code true} if redirection back to the
	 *                         OAuth 2.0 client with the error is disabled,
	 *                         {@code false} to perform the regular
	 *                         redirection to {@code redirect_uri} with
	 *                         the error.
	 */
	public InvalidAuthorizationRequestException(final @Nullable String message,
						    final ErrorObject errorObject,
						    final boolean redirectDisabled) {
		super(message);
		this.errorObject = Objects.requireNonNull(errorObject);
		this.redirectDisabled = redirectDisabled;
	}
	
	
	/**
	 * Returns the error object with code and optional description and URI.
	 *
	 * @return The error object.
	 */
	public ErrorObject getErrorObject() {
		return errorObject;
	}
	
	
	/**
	 * Returns {@code true} if redirection back to the OAuth 2.0 client
	 * with the error is disabled.
	 *
	 * @return {@code true} if redirection is disabled, {@code false} to
	 *         perform the regular redirection to {@code redirect_uri} with
	 *         the error.
	 */
	public boolean isRedirectDisabled() {
		return redirectDisabled;
	}
}

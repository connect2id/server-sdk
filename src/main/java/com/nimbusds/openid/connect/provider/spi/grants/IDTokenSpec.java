package com.nimbusds.openid.connect.provider.spi.grants;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;


/**
 * Identity (ID) token specification.
 */
@Immutable
public class IDTokenSpec extends OptionalTokenSpec {


	/**
	 * None (no issue) ID token specification.
	 */
	public static final IDTokenSpec NONE = new IDTokenSpec();


	/**
	 * The time of the subject authentication. If {@code null} it will be
	 * set to now.
	 */
	private final @Nullable Date authTime;


	/**
	 * The Authentication Context Class Reference (ACR), {@code null} if
	 * not specified.
	 */
	private final @Nullable ACR acr;


	/**
	 * The Authentication Methods Reference (AMR) list, {@code null} if not
	 * specified.
	 */
	private final @Nullable List<AMR> amrList;
	
	
	/**
	 * Allow refresh setting.
	 */
	private final boolean allowRefresh;


	/**
	 * Creates a new default ID token specification (no issue).
	 */
	public IDTokenSpec() {

		this(false, 0L, null, null, null, null);
	}


	/**
	 * Creates a new ID token specification.
	 *
	 * @param issue               Controls the ID token issue. If
	 *                            {@code true} an ID token must be issued,
	 *                            {@code false} to prohibit issue.
	 * @param lifetime            The ID token lifetime, in seconds, zero
	 *                            if not specified (to let the Connect2id
	 *                            server apply the default configured
	 *                            lifetime for ID tokens).
	 * @param impersonatedSubject The subject in impersonation and
	 *                            delegation cases, {@code null} if not
	 *                            applicable.
	 */
	public IDTokenSpec(final boolean issue,
			   final long lifetime,
			   final @Nullable Subject impersonatedSubject) {

		this(issue, lifetime, null, null, null, impersonatedSubject);
	}


	/**
	 * Creates a new ID token specification.
	 *
	 * @param issue               Controls the ID token issue. If
	 *                            {@code true} an ID token must be issued,
	 *                            {@code false} to prohibit issue.
	 * @param lifetime            The ID token lifetime, in seconds, zero
	 *                            if not specified (to let the Connect2id
	 *                            server apply the default configured
	 *                            lifetime for ID tokens).
	 * @param authTime            The time of the subject authentication.
	 *                            If {@code null} it will be set to now.
	 *                            Applies only if an ID token is issued.
	 * @param acr                 The Authentication Context Class
	 *                            Reference (ACR), {@code null} if not
	 *                            specified. Applies only if an ID token is
	 *                            issued.
	 * @param amrList             The Authentication Methods Reference
	 *                            (AMR) list, {@code null} if not
	 *                            specified. Applies only if an ID token is
	 *                            issued.
	 * @param impersonatedSubject The subject in impersonation and
	 *                            delegation cases, {@code null} if not
	 *                            applicable.
	 */
	public IDTokenSpec(final boolean issue,
			   final long lifetime,
			   final @Nullable Date authTime,
			   final @Nullable ACR acr,
			   final @Nullable List<AMR> amrList,
			   final @Nullable Subject impersonatedSubject) {

		this(issue, lifetime, authTime, acr, amrList, false, impersonatedSubject);
	}


	/**
	 * Creates a new ID token specification.
	 *
	 * @param issue               Controls the ID token issue. If
	 *                            {@code true} an ID token must be issued,
	 *                            {@code false} to prohibit issue.
	 * @param lifetime            The ID token lifetime, in seconds, zero
	 *                            if not specified (to let the Connect2id
	 *                            server apply the default configured
	 *                            lifetime for ID tokens).
	 * @param authTime            The time of the subject authentication.
	 *                            If {@code null} it will be set to now.
	 *                            Applies only if an ID token is issued.
	 * @param acr                 The Authentication Context Class
	 *                            Reference (ACR), {@code null} if not
	 *                            specified. Applies only if an ID token is
	 *                            issued.
	 * @param amrList             The Authentication Methods Reference
	 *                            (AMR) list, {@code null} if not
	 *                            specified. Applies only if an ID token is
	 *                            issued.
	 * @param allowRefresh        {@code true} to allow ID token refresh
	 *                            until the subject session is closed or
	 *                            expires.
	 * @param impersonatedSubject The subject in impersonation and
	 *                            delegation cases, {@code null} if not
	 *                            applicable.
	 */
	public IDTokenSpec(final boolean issue,
			   final long lifetime,
			   final @Nullable Date authTime,
			   final @Nullable ACR acr,
			   final @Nullable List<AMR> amrList,
			   final boolean allowRefresh,
			   final @Nullable Subject impersonatedSubject) {

		super(issue, lifetime, null, impersonatedSubject);

		if (issue) {
			// Applies only if a token is issued
			this.authTime = authTime;
			this.acr = acr;
			this.amrList = amrList;
			this.allowRefresh = allowRefresh;
		} else {
			this.authTime = null;
			this.acr = null;
			this.amrList = null;
			this.allowRefresh = false;
		}
	}


	/**
	 * Returns the time of the subject authentication.
	 *
	 * @return The time of the subject authentication. If {@code null} it
	 *         will be set to now. Applies only if an ID token is issued.
	 */
	public @Nullable Date getAuthTime() {

		return authTime;
	}


	/**
	 * Returns the Authentication Context Class Reference (ACR).
	 *
	 * @return The Authentication Context Class Reference (ACR),
	 *         {@code null} if not specified. Applies only if an ID token
	 *         is issued.
	 */
	public @Nullable ACR getACR() {

		return acr;
	}


	/**
	 * Returns The Authentication Methods Reference (AMR) list.
	 *
	 * @return The Authentication Methods Reference (AMR) list,
	 *         {@code null} if not specified. Applies only if an ID token
	 *         is issued.
	 */
	public @Nullable List<AMR> getAMRList() {

		return amrList;
	}
	
	
	/**
	 * Returns the ID token refresh setting.
	 *
	 * @return {@code true} to allow ID token refresh until the subject
	 *         session is closed or expires.
	 */
	public boolean isAllowRefresh() {
		return allowRefresh;
	}
	
	
	@Override
	public JSONObject toJSONObject() {

		JSONObject o = super.toJSONObject();
		
		if (getLifetime() <= 0) {
			// Implies not specified - remove
			o.remove("lifetime");
		}

		if (authTime != null) {
			o.put("auth_time", authTime.getTime() / 1000L);
		}

		if (acr != null) {
			o.put("acr", acr.getValue());
		}

		if (CollectionUtils.isNotEmpty(amrList)) {

			List<String> sl = new ArrayList<>(amrList.size());

			for (AMR amr: amrList) {
				sl.add(amr.getValue());
			}

			o.put("amr", sl);
		}
		
		if (allowRefresh) {
			o.put("allow_refresh", true);
		}

		return o;
	}


	/**
	 * Parses an ID token specification from the specified JSON object.
	 *
	 * @param jsonObject The JSON object. Must not be {@code null}.
	 *
	 * @return The ID token specification.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static IDTokenSpec parse(final JSONObject jsonObject)
		throws ParseException {

		OptionalTokenSpec optionalTokenSpec = OptionalTokenSpec.parse(jsonObject);
		
		// Adjust lifetime value for not specified (0)
		long lifetime = optionalTokenSpec.getLifetime() > 0 ? optionalTokenSpec.getLifetime() : 0;

		Date authTime = null;

		if (jsonObject.containsKey("auth_time")) {
			authTime = new Date(JSONObjectUtils.getLong(jsonObject, "auth_time") * 1000L);
		}

		ACR acr = null;

		if (jsonObject.containsKey("acr")) {
			acr = new ACR(JSONObjectUtils.getString(jsonObject, "acr"));
		}

		List<AMR> amrList = null;

		if (jsonObject.containsKey("amr")) {
			String[] sa = JSONObjectUtils.getStringArray(jsonObject, "amr");
			amrList = new ArrayList<>(sa.length);
			for (String s: sa) {
				amrList.add(new AMR(s));
			}
		}
		
		boolean allowRefresh = false;
		if (jsonObject.containsKey("allow_refresh")) {
			allowRefresh = JSONObjectUtils.getBoolean(jsonObject, "allow_refresh");
		}

		return new IDTokenSpec(
			optionalTokenSpec.issue(),
			lifetime,
			authTime,
			acr,
			amrList,
			allowRefresh,
			optionalTokenSpec.getImpersonatedSubject());
	}
}

package com.nimbusds.openid.connect.provider.spi.clientauth;


import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;

import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;


/**
 * X.509 certificate verification for a {@code private_key_jwt} client
 * authentication.
 */
public interface CertificateVerification {
	
	
	/**
	 * The X.509 certificate verification context.
	 */
	interface Context {
		
		
		/**
		 * Returns the locations where the X.509 certificate to be
		 * verified was found - the JWS header of the
		 * {@code private_key_jwt} assertion, in a registered client
		 * JWK, or both. Can be used to enforce a particular policy on
		 * how a client must pass or reference the certificate when one
		 * is required for a {@code private_key_jwt} authentication.
		 *
		 * @return The certificate location(s). Includes at least one
		 *         location.
		 */
		Set<CertificateLocation> getCertificateLocations();
	}
	
	
	/**
	 * Called to verify the specified X.509 certificate for a
	 * {@code private_key_jwt} client authentication.
	 *
	 * @param x5c The X.509 certificate, with optional chain. Not
	 *            {@code null} or empty.
	 * @param ctx The certificate verification context. Not {@code null}.
	 *
	 * @throws InvalidClientException If the X.509 certificate is invalid.
	 * Throwing an {@link ExposedInvalidClientException} will override the
	 * default Connect2id server {@code error_description} and
	 * {@code error_uri} in the HTTP 401 Unauthorized error response.
	 */
	void verify(final List<X509Certificate> x5c, final Context ctx)
		throws InvalidClientException;
}

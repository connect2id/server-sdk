package com.nimbusds.openid.connect.provider.spi.clientauth;


import java.net.URI;

import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;


/**
 * {@linkplain InvalidClientException} with {@code error_description} and
 * {@code error_uri} details exposed in the {@code HTTP 401 Unauthorized} error
 * response.
 */
public class ExposedInvalidClientException extends InvalidClientException {
	
	
	/**
	 * The error description.
	 */
	private final String errorDescription;
	
	
	/**
	 * The error URI.
	 */
	private final URI errorURI;
	
	
	/**
	 * Creates a new exposed invalid client exception.
	 *
	 * @param message          The exception message (will be logged by the
	 *                         Connect2id server and not exposed in the
	 *                         HTTP 401 Unauthorized error response),
	 *                         {@code null} if not specified.
	 * @param errorDescription The {@code error_description} to return in
	 *                         the HTTP 401 Unauthorized response,
	 *                         {@code null} if not specified.
	 */
	public ExposedInvalidClientException(final String message, final String errorDescription) {
		this(message, errorDescription, null);
	}
	
	
	/**
	 * Creates a new exposed invalid client exception.
	 *
	 * @param message          The exception message (will be logged by the
	 *                         Connect2id server and not exposed in the
	 *                         HTTP 401 Unauthorized error response),
	 *                         {@code null} if not specified.
	 * @param errorDescription The {@code error_description} to return in
	 *                         the HTTP 401 Unauthorized response,
	 *                         {@code null} if not specified.
	 * @param errorURI         The {@code error_uri} to return in the HTTP
	 *                         401 Unauthorized response, {@code null} if
	 *                         not specified.
	 */
	public ExposedInvalidClientException(final String message, final String errorDescription, final URI errorURI) {
		super(message);
		this.errorDescription = errorDescription;
		this.errorURI = errorURI;
	}
	
	
	@Override
	public ErrorObject getErrorObject() {
		return super.getErrorObject().setDescription(errorDescription).setURI(errorURI);
	}
}

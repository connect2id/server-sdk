package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import com.nimbusds.oauth2.sdk.TokenIntrospectionSuccessResponse;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;
import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;
import net.jcip.annotations.ThreadSafe;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Service Provider Interface (SPI) for composing token introspection (RFC
 * 7662) responses. Implementations must be thread-safe.
 *
 * <p>The SPI may be used to respond differently to different resource servers
 * making the same request. For instance, an authorisation server may limit
 * which scopes from a given token are returned for each resource server to
 * prevent a resource server from learning more about the larger network than
 * is necessary for its operation.
 *
 * <p>See OAuth 2.0 Token Introspection (RFC 7662), section 2.2.
 */
@ThreadSafe
public interface TokenIntrospectionResponseComposer extends Lifecycle {
	
	
	/**
	 * Composes a token introspection response.
	 *
	 * <p>If the access token was found to be invalid or expired the
	 * method should simply return
	 *
	 * <pre>
	 * if (tokenAuthz == null) {
	 * 	return new TokenIntrospectionSuccessResponse.Builder(false)
	 * 	    .build();
	 * }
	 * </pre>
	 *
	 * @param tokenAuthz The access token authorisation, {@code null} if
	 *                   the token was found to be invalid or expired
	 *                   (implies {@code "active":false}).
	 * @param context    The token introspection context. Not {@code null}.
	 *
	 * @return The token introspection success response (for
	 *         {@code "active":true} as well as {@code "active":false}
	 *         access tokens.
	 */
	TokenIntrospectionSuccessResponse compose(final @Nullable AccessTokenAuthorization tokenAuthz,
						  final TokenIntrospectionContext context);
}

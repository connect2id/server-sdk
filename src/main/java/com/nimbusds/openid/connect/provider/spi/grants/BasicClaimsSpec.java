package com.nimbusds.openid.connect.provider.spi.grants;


import java.util.*;

import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.oauth2.sdk.util.MapUtils;


/**
 * Basic OpenID claims specification.
 */
@Immutable
public class BasicClaimsSpec {


	/**
	 * The names of the authorised OpenID claims, {@code null} if none.
	 */
	private final @Nullable Set<String> names;
	
	
	/**
	 * Optional OpenID claims fulfillment data, {@code null} if not
	 * specified.
	 */
	private final @Nullable JSONObject data;


	/**
	 * Additional or preset OpenID claims to be included in the ID token
	 * and UserInfo response.
	 */
	private final PresetClaims presetClaims;


	/**
	 * Creates a new default basic OpenID claims specification (empty).
	 */
	public BasicClaimsSpec() {

		this(null);
	}


	/**
	 * Creates a new basic OpenID claims specification.
	 *
	 * @param names The names of the authorised OpenID claims, {@code null}
	 *              if none.
	 */
	public BasicClaimsSpec(final @Nullable Set<String> names) {

		this(names, null, (JSONObject) null);
	}


	/**
	 * Creates a new basic OpenID claims specification.
	 *
	 * @param names                The names of the authorised OpenID
	 *                             claims, empty set or {@code null} if
	 *                             none.
	 * @param presetIDTokenClaims  Additional preset claims to be included
	 *                             in the ID token, {@code null} if none.
	 * @param presetUserInfoClaims Additional preset claims to be included
	 *                             in the UserInfo response, {@code null}
	 *                             if none.
	 */
	public BasicClaimsSpec(final @Nullable Set<String> names,
			       final @Nullable JSONObject presetIDTokenClaims,
			       final @Nullable JSONObject presetUserInfoClaims) {

		this(names, new PresetClaims(presetIDTokenClaims, presetUserInfoClaims));
	}


	/**
	 * Creates a new basic OpenID claims specification.
	 *
	 * @param names        The names of the authorised OpenID claims, empty
	 *                     set or {@code null} if none.
	 * @param presetClaims The additional or preset claims to be included
	 *                     in the ID token and UserInfo response,
	 *                     {@code null} if none.
	 */
	public BasicClaimsSpec(final @Nullable Set<String> names,
			       final @Nullable PresetClaims presetClaims) {

		this(names, null, presetClaims);
	}


	/**
	 * Creates a new basic OpenID claims specification.
	 *
	 * @param names        The names of the authorised OpenID claims, empty
	 *                     set or {@code null} if none.
	 * @param data         Optional OpenID claims fulfillment data,
	 *                     {@code null} if none.
	 * @param presetClaims The additional or preset claims to be included
	 *                     in the ID token and UserInfo response,
	 *                     {@code null} if none.
	 */
	public BasicClaimsSpec(final @Nullable Set<String> names,
			       final @Nullable JSONObject data,
			       final @Nullable PresetClaims presetClaims) {

		if (CollectionUtils.isNotEmpty(names)) {
			this.names = names;
		} else {
			this.names = Collections.emptySet();
		}
		
		this.data = data;

		if (presetClaims == null) {
			this.presetClaims = new PresetClaims();
		} else {
			this.presetClaims = presetClaims;
		}
	}


	/**
	 * Returns the names of the authorised OpenID claims.
	 *
	 * @return The names of the authorised OpenID claims, empty set if
	 *         none.
	 */
	public Set<String> getNames() {

		return names;
	}
	
	
	/**
	 * Returns the optional OpenID claims fulfillment data.
	 *
	 * @return The OpenID claims fulfillment data, {@code null} if not
	 *         specified.
	 */
	public @Nullable JSONObject getData() {
		
		return data;
	}
	
	
	/**
	 * The additional or preset claims to be included in the ID token and
	 * UserInfo response.
	 *
	 * @return The additional or preset claims.
	 */
	public PresetClaims getPresetClaims() {

		return presetClaims;
	}


	/**
	 * Returns the additional preset claims to be included in the ID token.
	 *
	 * @return The additional preset claims to be included in the ID token,
	 *         {@code null} if none.
	 */
	public @Nullable JSONObject getPresetIDTokenClaims() {

		return presetClaims.getPresetIDTokenClaims();
	}


	/**
	 * Returns the additional preset claims to be included in the UserInfo
	 * response.
	 *
	 * @return The additional or preset claims to be included in the
	 *         UserInfo response, {@code null} if none.
	 */
	public @Nullable JSONObject getPresetUserInfoClaims() {

		return presetClaims.getPresetUserInfoClaims();
	}


	/**
	 * Returns a JSON object representation of this basic claims
	 * specification.
	 *
	 * @return The JSON object.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = new JSONObject();

		if (CollectionUtils.isNotEmpty(names)) {
			o.put("claims", new ArrayList<>(names));
		}
		
		if (MapUtils.isNotEmpty(data)) {
			o.put("claims_data", data);
		}

		if (! presetClaims.isEmpty()) {
			o.put("preset_claims", presetClaims.toJSONObject());
		}

		return o;
	}


	/**
	 * Parses a basic OpenID claims specification from the specified JSON
	 * object.
	 *
	 * @param o The JSON object. Must not be {@code null}.
	 *
	 * @return The basic OpenID claims specification.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static BasicClaimsSpec parse(final JSONObject o)
		throws ParseException {

		Set<String> claims = null;

		if (o.containsKey("claims")) {
			claims = new HashSet<>(Arrays.asList(JSONObjectUtils.getStringArray(o, "claims")));
		}
		
		JSONObject data = null;
		
		if (o.containsKey("claims_data")) {
			data = JSONObjectUtils.getJSONObject(o, "claims_data");
		}

		PresetClaims presetClaims = null;

		if (o.containsKey("preset_claims")) {
			presetClaims = PresetClaims.parse(JSONObjectUtils.getJSONObject(o, "preset_claims"));
		}

		return new BasicClaimsSpec(claims, data, presetClaims);
	}
}

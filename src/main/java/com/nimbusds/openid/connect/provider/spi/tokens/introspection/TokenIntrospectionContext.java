package com.nimbusds.openid.connect.provider.spi.tokens.introspection;


import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSource;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Token introspection context.
 */
public interface TokenIntrospectionContext extends InvocationContext {
	
	
	/**
	 * If the requesting client authenticated at the introspection
	 * endpoint returns its registered information.
	 *
	 * @return The client information, {@code null} if the introspection
	 *         request was authorised with an access token.
	 */
	@Nullable OIDCClientInformation getOIDCClientInformation();


	/**
	 * Returns the registered client information for the specified
	 * {@code client_id}.
	 *
	 * @param clientID The client ID.
	 *
	 * @return The registered client information, {@code null} if the
	 *         {@code client_id} is invalid.
	 */
	@Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID);
	
	
	/**
	 * Returns the OpenID claims source.
	 *
	 * @return The OpenID claims source.
	 */
	ClaimsSource getClaimsSource();


	/**
	 * Returns the associated subject (end-user) session where the token
	 * issue was authorised.
	 *
	 * @return The subject session, {@code null} if closed or expired, or
	 *         not available due to the session key not being encoded into
	 *         the token.
	 */
	@Nullable SubjectSession getSubjectSession();
}

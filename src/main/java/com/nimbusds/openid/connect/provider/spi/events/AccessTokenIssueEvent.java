package com.nimbusds.openid.connect.provider.spi.events;


import com.nimbusds.jwt.JWTClaimsSet;
import net.jcip.annotations.Immutable;


/**
 * Access token issue event.
 */
@Immutable
public class AccessTokenIssueEvent extends JWTIssueEvent {
	
	
	/**
	 * Creates a new access token issue event.
	 *
	 * @param source       The event originator.
	 * @param jwtClaimsSet The access token claims set.
	 */
	public AccessTokenIssueEvent(final Object source, final JWTClaimsSet jwtClaimsSet) {
		super(source, jwtClaimsSet);
	}
}

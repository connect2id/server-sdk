package com.nimbusds.openid.connect.provider.spi.crypto;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.jose.JWSObject;


/**
 * Interface exposed by the Connect2id server for verifying JSON Web Signatures
 * (JWS), such as for JSON Web Tokens (JWT) created by the {@link JWTSigner}.
 */
@ThreadSafe
public interface JWSVerifier {
	
	
	/**
	 * Verifies the signature of the specified JWS object.
	 *
	 * @param jwsObject The JWS object. Must not be {@code null}.
	 *
	 * @return {@code true} if the signature is valid, else {@code false}.
	 */
	boolean verifySignature(final JWSObject jwsObject);
}

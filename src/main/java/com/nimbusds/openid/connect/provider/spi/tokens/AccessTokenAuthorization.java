package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.dpop.JWKThumbprintConfirmation;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.openid.connect.sdk.SubjectType;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Access token authorisation.
 */
public interface AccessTokenAuthorization {
	
	
	/**
	 * Returns the access token subject.
	 *
	 * @return The subject, {@code null} if not specified.
	 */
	@Nullable Subject getSubject();
	
	
	/**
	 * Returns the access token actor, in impersonation and delegation
	 * scenarios.
	 *
	 * @return The actor, {@code null} if not specified.
	 */
	@Nullable Actor getActor();
	
	
	/**
	 * Returns the identifier of the client to which the access token is
	 * issued.
	 *
	 * @return The client identifier, {@code null} if not specified.
	 */
	@Nullable ClientID getClientID();
	
	
	/**
	 * Returns the scope of the access token.
	 *
	 * @return The scope, {@code null} if not specified.
	 */
	@Nullable Scope getScope();
	
	
	/**
	 * Returns the expiration time of the access token.
	 *
	 * @return The expiration time, {@code null} if not specified.
	 */
	@Nullable Instant getExpirationTime();
	
	
	/**
	 * Returns the issue time of the access token.
	 *
	 * @return The issue time, {@code null} if not specified.
	 */
	@Nullable Instant getIssueTime();
	
	
	/**
	 * Returns the issuer of the access token.
	 *
	 * @return The issuer, {@code null} if not specified.
	 */
	@Nullable Issuer getIssuer();
	
	
	/**
	 * Returns the audience list of the access token, which may be the
	 * logical names of the intended resource servers.
	 *
	 * @return The audience list, {@code null} if not specified.
	 */
	@Nullable List<Audience> getAudienceList();
	
	
	/**
	 * Returns the access token subject type.
	 *
	 * @return The subject type, {@code null} if not specified (may imply
	 *         {@link SubjectType#PUBLIC public}).
	 */
	default @Nullable SubjectType getSubjectType() {
		return null;
	}
	
	
	/**
	 * Returns the access token local subject. Equals the
	 * {@link #getSubject()} value unless the {@link #getSubjectType()
	 * subject type} is pairwise.
	 *
	 * <p>Use this method if there is a need to get the local (system)
	 * subject for an access token which subject was made pairwise for its
	 * audience (resource server).
	 *
	 * <p>Note, an access token which subject is made pairwise must not
	 * have its local subject exposed in introspection responses intended
	 * for the token audience!
	 *
	 * @return The local subject, {@code null} if not specified or for a
	 *         pairwise {@link #getSubjectType() subject type} that
	 *         couldn't be reversed.
	 */
	default @Nullable Subject getLocalSubject() {
		if (SubjectType.PUBLIC == getSubjectType()) {
			return getSubject();
		} else {
			return null;
		}
	}
	
	
	/**
	 * Returns the JSON Web Token (JWT) identifier of the access token.
	 *
	 * @return The JWT ID, {@code null} if not specified or applicable.
	 */
	@Nullable JWTID getJWTID();
	
	
	/**
	 * Returns the names of the consented OpenID claims to be accessed at
	 * the UserInfo endpoint.
	 *
	 * @return The claim names, {@code null} if not specified.
	 */
	@Nullable Set<String> getClaimNames();
	
	
	/**
	 * Returns the preferred locales for the consented OpenID claims.
	 *
	 * @return The preferred claims locales, {@code null} if not specified.
	 */
	@Nullable List<LangTag> getClaimsLocales();
	
	
	/**
	 * Returns the preset OpenID claims to be included in the UserInfo
	 * response.
	 *
	 * @return The preset OpenID claims, {@code null} if not specified.
	 */
	@Nullable JSONObject getPresetClaims();
	
	
	/**
	 * Returns the optional data for the access token.
	 *
	 * @return The optional data, represented as a JSON object,
	 *         {@code null} if not specified.
	 */
	@Nullable JSONObject getData();
	
	
	/**
	 * Returns the client X.509 certificate confirmation (SHA-256
	 * thumbprint) for mutual TLS.
	 *
	 * @return The client X.509 certificate confirmation, {@code null} if
	 *         none.
	 */
	@Nullable X509CertificateConfirmation getClientCertificateConfirmation();
	
	
	/**
	 * Returns the JWK SHA-256 thumbprint confirmation for DPoP.
	 *
	 * @return The JWK thumbprint confirmation, {@code null} if none.
	 */
	default @Nullable JWKThumbprintConfirmation getJWKThumbprintConfirmation() {
		return null;
	}
	
	
	/**
	 * Returns a map of other top-level parameters.
	 *
	 * @return Other top-level parameters, the values should map to JSON
	 *         entities, {@code null} if none.
	 */
	default @Nullable Map<String,Object> getOtherTopLevelParameters() {
		return null;
	}
	
	
	/**
	 * Returns the optional OpenID claims fulfillment data.
	 *
	 * @return The OpenID claims fulfillment data, {@code null} if not
	 *         specified.
	 */
	default @Nullable JSONObject getClaimsData() {
		return null;
	}


	/**
	 * Returns the associated subject (end-user) session key (session ID
	 * with omitted HMAC).
	 *
	 * @return The subject session key, {@code null} if not available.
	 */
	default @Nullable String getSubjectSessionKey() {
		return null;
	}
}

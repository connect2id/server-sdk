package com.nimbusds.openid.connect.provider.spi.grants;


import net.jcip.annotations.Immutable;

import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * Authorisation produced by a {@link GrantHandler grant handler} of
 * self-issued assertions (SAML 2.0 or JWT bearer).
 *
 * <p>Required authorisation details:
 *
 * <ul>
 *     <li>The subject (end-user).
 *     <li>The authorised scope.
 * </ul>
 *
 * <p>All other parameters are optional or have suitable defaults.
 */
@Immutable
public class SelfIssuedAssertionAuthorization extends SubjectAuthorization {


	/**
	 * Creates a new authorisation for a self-issued assertion grant where
	 * the client acts on behalf of a user.
	 *
	 * <p>See RFC 7521, section 6.3.
	 *
	 * @param subject The subject (end-user). Must not be {@code null}.
	 * @param scope   The authorised scope values. Must not be
	 *                {@code null}.
	 */
	public SelfIssuedAssertionAuthorization(final Subject subject,
						final Scope scope) {

		super(subject, scope, AccessTokenSpec.DEFAULT, IDTokenSpec.NONE, ClaimsSpec.NONE, null);
	}


	/**
	 * Creates a new authorisation for a self-issued assertion grant where
	 * the client acts on behalf of a user.
	 *
	 * <p>See RFC 7521, section 6.3.
	 *
	 * @param subject         The subject (end-user). Must not be
	 *                        {@code null}.
	 * @param scope           The authorised scope values. Must not be
	 *                        {@code null}.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param idTokenSpec     The ID token specification. Must not be
	 *                        {@code null}.
	 * @param claimsSpec      The OpenID claims specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	public SelfIssuedAssertionAuthorization(final Subject subject,
						final Scope scope,
						final AccessTokenSpec accessTokenSpec,
						final IDTokenSpec idTokenSpec,
						final ClaimsSpec claimsSpec,
						final @Nullable JSONObject data) {

		super(subject, scope, accessTokenSpec, idTokenSpec, claimsSpec, data);
	}


	/**
	 * Creates a new authorisation for a self-issued assertion grant where
	 * the client acts on its own behalf.
	 *
	 * <p>See RFC 7521, section 6.2.
	 *
	 * @param subject The client identifier. Must not be {@code null}.
	 * @param scope   The authorised scope values. Must not be
	 *                {@code null}.
	 */
	public SelfIssuedAssertionAuthorization(final ClientID subject,
						final Scope scope) {

		this(new Subject(subject.getValue()), scope, AccessTokenSpec.DEFAULT, IDTokenSpec.NONE, ClaimsSpec.NONE, null);
	}


	/**
	 * Creates a new authorisation for a self-issued assertion grant where
	 * the client acts on its own behalf.
	 *
	 * <p>See RFC 7521, section 6.2.
	 *
	 * @param subject         The client identifier. Must not be
	 *                        {@code null}.
	 * @param scope           The authorised scope values. Must not be
	 *                        {@code null}.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	public SelfIssuedAssertionAuthorization(final ClientID subject,
						final Scope scope,
						final AccessTokenSpec accessTokenSpec,
						final @Nullable JSONObject data) {

		this(new Subject(subject.getValue()), scope, accessTokenSpec, IDTokenSpec.NONE, ClaimsSpec.NONE, data);
	}


	/**
	 * Parses a self-issued assertion grant authorisation from the
	 * specified JSON object.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static SelfIssuedAssertionAuthorization parse(final JSONObject jsonObject)
		throws ParseException {

		SubjectAuthorization subAuthz = SubjectAuthorization.parse(jsonObject);

		return new SelfIssuedAssertionAuthorization(
			subAuthz.getSubject(),
			subAuthz.getScope(),
			subAuthz.getAccessTokenSpec(),
			subAuthz.getIDTokenSpec(),
			subAuthz.getClaimsSpec(),
			subAuthz.getData());
	}


	/**
	 * Parses a self-issued assertion grant authorisation from the
	 * specified JSON object string.
	 *
	 * @param json The JSON object string to parse. Must not be
	 *             {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static SelfIssuedAssertionAuthorization parse(final String json)
		throws ParseException {

		return parse(JSONObjectUtils.parse(json));
	}
}

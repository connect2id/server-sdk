package com.nimbusds.openid.connect.provider.spi.reg;


import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;


/**
 * Exception with a wrapped HTTP response.
 */
public class WrappedHTTPResponseException extends Exception {
	
	
	/**
	 * The wrapped HTTP response.
	 */
	private final HTTPResponse httpResponse;
	
	
	/**
	 * Creates a new wrapped HTTP response exception.
	 *
	 * @param message      Optional message to log, {@code null} if not
	 *                     specified.
	 * @param httpResponse The wrapped HTTP response, must not be
	 *                     {@code null}.
	 */
	public WrappedHTTPResponseException(final @Nullable String message, final HTTPResponse httpResponse) {
		super(message);
		this.httpResponse = Objects.requireNonNull(httpResponse);
	}
	
	
	/**
	 * Returns the wrapped HTTP response.
	 *
	 * @return The HTTP response.
	 */
	public HTTPResponse getHTTPResponse() {
		return httpResponse;
	}
}

package com.nimbusds.openid.connect.provider.spi.reg;


import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.client.RegistrationError;


/**
 * Invalid client registration exception.
 */
public class InvalidRegistrationException extends Exception {


	/**
	 * The error object.
	 */
	private final ErrorObject errorObject;
	
	
	/**
	 * Creates a new invalid client registration exception with a general
	 * {@code invalid_client_metadata} error code and description that
	 * doesn't specify the cause.
	 *
	 * <p>This will result in the following error response:
	 *
	 * <pre>
	 * HTTP/1.1 400 Bad Request
	 * Content-Type: application/json
	 * Cache-Control: no-store
	 * Pragma: no-cache
	 *
	 * {
	 *   "error"             : "invalid_client_metadata",
	 *   "error_description" : "Invalid client metadata field"
	 * }
	 * </pre>
	 */
	public InvalidRegistrationException() {
		
		this(RegistrationError.INVALID_CLIENT_METADATA);
	}


	/**
	 * Creates a new invalid client registration exception with the
	 * specified error code and description.
	 *
	 * <p>The error code should be one of the following:
	 *
	 * <ul>
	 *     <li>{@link RegistrationError#INVALID_REDIRECT_URI invalid_redirect_uri}</li>
	 *     <li>{@link RegistrationError#INVALID_CLIENT_METADATA invalid_client_metadata}</li>
	 *     <li>{@link RegistrationError#INVALID_SOFTWARE_STATEMENT invalid_software_statement}</li>
	 *     <li>{@link RegistrationError#UNAPPROVED_SOFTWARE_STATEMENT unapproved_software_statement}</li>
	 * </ul>
	 *
	 * <p>To construct an exception for a general
	 * {@code invalid_client_metadata} error with a description:
	 *
	 * <pre>
	 * new InvalidRegistrationException(RegistrationError.INVALID_CLIENT_METADATA
	 * 	.setDescription("The policy_uri must be on a redirect_uris domain"));
	 * </pre>
	 *
	 * <p>This will result in the following error response:
	 *
	 * <pre>
	 * HTTP/1.1 400 Bad Request
	 * Content-Type: application/json
	 * Cache-Control: no-store
	 * Pragma: no-cache
	 *
	 * {
	 *   "error"             : "invalid_client_metadata",
	 *   "error_description" : "The policy_uri must be on a redirect_uris domain"
	 * }
	 * </pre>
	 *
	 * @param errorObject The associated error object. If {@code null} will
	 *                    be set to {@code invalid_client_metadata}.
	 */
	public InvalidRegistrationException(final @Nullable ErrorObject errorObject) {

		if (errorObject == null)
			this.errorObject = RegistrationError.INVALID_CLIENT_METADATA;
		else
			this.errorObject = errorObject;
	}


	/**
	 * Creates a new invalid client registration exception, with the error
	 * code set to {@code invalid_client_metadata} and a description
	 * specifying the name of the invalid field and cause.
	 *
	 * <p>Example:
	 *
	 * <pre>
	 * new InvalidRegistrationException("policy_uri", "Must be on a redirect_uris domain");
	 * </pre>
	 *
	 * <p>This will result in the following error response:
	 *
	 * <pre>
	 * HTTP/1.1 400 Bad Request
	 * Content-Type: application/json
	 * Cache-Control: no-store
	 * Pragma: no-cache
	 *
	 * {
	 *   "error"             : "invalid_client_metadata",
	 *   "error_description" : "Invalid client metadata field policy_uri: Must be on a redirect_uris domain"
	 * }
	 * </pre>
	 *
	 * @param field The name of the invalid client metadata field. Must not
	 *              be {@code null}.
	 * @param cause The cause, {@code null} if not specified.
	 */
	public InvalidRegistrationException(final String field, final @Nullable String cause) {

		errorObject = RegistrationError.INVALID_CLIENT_METADATA
			.setDescription("Invalid client metadata field " + field +
				((cause != null) ? ": " + cause : ""));
	}


	/**
	 * Returns the associated error object.
	 *
	 * @return The associated error object.
	 */
	public ErrorObject getErrorObject() {

		return errorObject;
	}
}

package com.nimbusds.openid.connect.provider.spi.grants;


import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.oauth2.sdk.util.MapUtils;


/**
 * Additional or preset OpenID Connect claims. These may be included in the ID
 * token or in the UserInfo response.
 */
@Immutable
public final class PresetClaims {


	/**
	 * Additional or preset claims to be included in the ID token,
	 * {@code null} if none.
	 */
	private final @Nullable JSONObject idTokenClaims;


	/**
	 * Additional or preset claims to be included in the UserInfo response,
	 * {@code null} if none.
	 */
	private final @Nullable JSONObject userInfoClaims;


	/**
	 * Creates a new empty preset claims instance.
	 */
	public PresetClaims() {

		this(null, null);
	}


	/**
	 * Creates a new preset claims instance.
	 *
	 * @param idTokenClaims  Additional or preset claims to be included in
	 *                       the ID token, {@code null} if none.
	 * @param userInfoClaims Additional or preset claims to be included in
	 *                       the UserInfo response, {@code null} if none.
	 */
	public PresetClaims(final @Nullable JSONObject idTokenClaims,
			    final @Nullable JSONObject userInfoClaims) {

		this.idTokenClaims = idTokenClaims;
		this.userInfoClaims = userInfoClaims;
	}


	/**
	 * Returns {@code true} if there are no preset claims specified.
	 *
	 * @return {@code true} if there are no preset claims specified, else
	 *         {@code false}.
	 */
	public boolean isEmpty() {

		return
			MapUtils.isEmpty(idTokenClaims) && MapUtils.isEmpty(userInfoClaims);
	}


	/**
	 * Returns the additional or preset claims to be included in the ID
	 * token.
	 *
	 * @return The preset ID token claims, {@code null} if none.
	 */
	public @Nullable JSONObject getPresetIDTokenClaims() {
		return idTokenClaims;
	}


	/**
	 * Returns the additional or preset claims to be returned in the
	 * UserInfo response.
	 *
	 * @return The preset UserInfo claims, {@code null} if none.
	 */
	public @Nullable JSONObject getPresetUserInfoClaims() {
		return userInfoClaims;
	}


	/**
	 * Returns a JSON object representation of this preset claims instance.
	 *
	 * @return The JSON object.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = new JSONObject();

		if (MapUtils.isNotEmpty(idTokenClaims)) {
			o.put("id_token", idTokenClaims);
		}

		if (MapUtils.isNotEmpty(userInfoClaims)) {
			o.put("userinfo", userInfoClaims);
		}

		return o;
	}


	@Override
	public String toString() {

		return toJSONObject().toString();
	}


	/**
	 * Parses a preset claims representation from the specified JSON
	 * object.
	 *
	 * @param o The JSON object. Must not be {@code null}.
	 *
	 * @return The preset claims.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static PresetClaims parse(final JSONObject o)
		throws ParseException {

		JSONObject idTokenClaims = null;

		if (o.containsKey("id_token")) {
			idTokenClaims = JSONObjectUtils.getJSONObject(o, "id_token");
		}

		JSONObject userInfoClaims = null;

		if (o.containsKey("userinfo")) {
			userInfoClaims = JSONObjectUtils.getJSONObject(o, "userinfo");
		}

		return new PresetClaims(idTokenClaims, userInfoClaims);
	}
}

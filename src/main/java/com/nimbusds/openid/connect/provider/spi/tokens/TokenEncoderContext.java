package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSource;
import com.nimbusds.openid.connect.provider.spi.claims.CommonClaimsSource;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Token encoder context.
 */
public interface TokenEncoderContext extends TokenCodecContext {
	
	
	/**
	 * Returns the registered client information for the {@code client_id}.
	 *
	 * @return The registered client information, {@code null} if not
	 *         available (in direct authorisation store web API calls).
	 */
	@Nullable OIDCClientInformation getOIDCClientInformation();
	
	
	
	/**
	 * Returns the OpenID claims source.
	 *
	 * @return The OpenID claims source. Not {@code null}.
	 */
	@Deprecated
	ClaimsSource getClaimsSource();
	
	
	/**
	 * Returns the OpenID claims source.
	 *
	 * @return The OpenID claims source. Not {@code null}.
	 */
	default CommonClaimsSource getCommonClaimsSource() {
		return getClaimsSource();
	}
}

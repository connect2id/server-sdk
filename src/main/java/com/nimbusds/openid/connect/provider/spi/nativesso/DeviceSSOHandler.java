package com.nimbusds.openid.connect.provider.spi.nativesso;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;
import com.nimbusds.openid.connect.provider.spi.grants.TokenRequestParameters;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.sdk.claims.IDTokenClaimsSet;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import net.jcip.annotations.ThreadSafe;


/**
 * Service Provider Interface (SPI) for handling device Single Sign-On (SSO).
 *
 * <p>Implementations must be thread-safe.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OpenID Connect Native SSO for Mobile Apps 1.0
 * </ul>
 */
@ThreadSafe
public interface DeviceSSOHandler extends Lifecycle {


        /**
         * Handles a device SSO back-channel request, represented by a token
         * exchange request (RFC 8693), from a client registered with the
         * Connect2id server. Prior to calling this method the Connect2id
         * server has successfully identified / authenticated the client and
         * validated the received ID token ({@code subject_token}), device
         * secret ({@code device_secret}) and their binding.
         *
         * <p>If the requested scope is invalid, unknown, malformed, or exceeds
         * the scope granted by the resource owner the handler must throw a
         * {@link GeneralException} with an
         * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
         * invalid_scope} error code.
         *
         * <p>If the requested scope requires a higher end-user authentication
         * level (ACR) than recorded in the device session, or explicit consent
         * by the end-user, the handler must throw a {@link GeneralException}
         * with an
         * {@link com.nimbusds.openid.connect.sdk.OIDCError#INTERACTION_REQUIRED
         * interaction_required} error code.
         *
         * @param subject            The local subject (end-user) of the
         *                           validated ID token to sign-in. If the
         *                           ID token was issued with a pairwise
         *                           subject identifier, this is the resolved
         *                           (decrypted) identifier value. Not
         *                           {@code null}.
         * @param idTokenClaimsSet   The claims set of the received and
         *                           successfully validated ID token. Note that
         *                           the ID token subject identifier may be
         *                           pairwise (encrypted). Use the subject
         *                           argument which provides the local
         *                           identifier value. Not {@code null}.
         * @param deviceSession      The device session corresponding to the
         *                           received and successfully validated
         *                           device secret. Not {@code null}.
         * @param tokenRequestParams The token request parameters, such as the
         *                           requested scope. Not {@code null}.
         * @param clientID           The client identifier. Not {@code null}.
         * @param confidentialClient {@code true} if the client is
         *                           confidential and has been authenticated,
         *                           else {@code false}.
         * @param clientMetadata     The OAuth 2.0 client / OpenID relying
         *                           party metadata. Not {@code null}.
         * @param handlerCtx         The handler context. Not {@code null}.
         *
         * @return The authorisation.
         *
         * @throws GeneralException If the request was denied, or another
         *                          exception was encountered.
         */
        DeviceSSOBackChannelAuthorization processBackChannelRequest(final Subject subject,
                                                                    final IDTokenClaimsSet idTokenClaimsSet,
                                                                    final SubjectSession deviceSession,
                                                                    final TokenRequestParameters tokenRequestParams,
                                                                    final ClientID clientID,
                                                                    final boolean confidentialClient,
                                                                    final OIDCClientMetadata clientMetadata,
                                                                    final DeviceSSOHandlerContext handlerCtx)
                throws GeneralException;
}

package com.nimbusds.openid.connect.provider.spi.events;


import java.util.EventObject;
import java.util.Objects;

import com.nimbusds.jose.util.JSONObjectUtils;
import com.nimbusds.jwt.JWTClaimsSet;


/**
 * JSON Web Token (JWT) issue event.
 */
abstract class JWTIssueEvent extends EventObject {
	
	
	/**
	 * The JWT claims set.
	 */
	private final JWTClaimsSet jwtClaimsSet;
	
	
	/**
	 * Creates a new JWT issue event.
	 *
	 * @param source       The event originator.
	 * @param jwtClaimsSet The JWT claims set.
	 */
	public JWTIssueEvent(final Object source, final JWTClaimsSet jwtClaimsSet) {
		super(source);
		this.jwtClaimsSet = Objects.requireNonNull(jwtClaimsSet);
	}
	
	
	/**
	 * Returns the claims set of the issued JWT.
	 *
	 * @return The JWT claims set.
	 */
	public JWTClaimsSet getJWTClaimsSet() {
		return jwtClaimsSet;
	}
	
	
	@Override
	public String toString() {
		return "JWT issue event: " + JSONObjectUtils.toJSONString(jwtClaimsSet.toJSONObject());
	}
}

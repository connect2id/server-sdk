package com.nimbusds.openid.connect.provider.spi.crypto;


import net.jcip.annotations.ThreadSafe;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;


/**
 * Interface exposed by the Connect2id server for signing JSON Web Tokens (JWT)
 * created by SPI implementations, for example Security Event Tokens (SET).
 */
@ThreadSafe
public interface JWTSigner {
	
	
	/**
	 * Signs the specified JWT claims. The issuer (iss) claim will be set
	 * to the OpenID Provider / Authorisation Server issuer URL. The JWT
	 * will be signed with the private key (RSA or EC) used for signing
	 * self-contained access tokens. Recipients can validate the JWT
	 * signature using the published JWK set.
	 *
	 * @param jwtClaimsSet The JWT claims. Must not be {@code null}.
	 *
	 * @return The signed JWT.
	 */
	default SignedJWT sign(final JWTClaimsSet jwtClaimsSet) {
		return sign(null, jwtClaimsSet);
	}
	
	
	/**
	 * Signs the specified JWT claims. The issuer (iss) claim will be set
	 * to the OpenID Provider / Authorisation Server issuer URL. The JWT
	 * will be signed with the private key (RSA or EC) used for signing
	 * self-contained access tokens. Recipients can validate the JWT
	 * signature using the published JWK set.
	 *
	 * @param typ          The JOSE object type ("typ") header parameter,
	 *                     {@code null} if none.
	 * @param jwtClaimsSet The JWT claims. Must not be {@code null}.
	 *
	 * @return The signed JWT.
	 */
	SignedJWT sign(final @Nullable JOSEObjectType typ, final JWTClaimsSet jwtClaimsSet);
}

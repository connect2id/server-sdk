package com.nimbusds.openid.connect.provider.spi.websso;


import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;


/**
 * Service Provider Interface (SPI) for plugging additional checks whether an
 * OAuth 2.0 authorisation / OpenID authentication request is eligible for
 * single sign-on (SSO), after the Connect2id server has completed its own
 * checks.
 *
 * <p>Prior to calling this SPI the Connect2id server ensures the following
 * conditions are met for a request to be eligible for SSO:
 *
 * <ul>
 *     <li>A subject (end-user) session is present.
 *     <li>The subject session authentication lifetime ({@code auth_life}), if
 *         specified for the session, has not expired.
 *     <li>If the request is an OpenID authentication request with a
 *         maximum authentication age ({@code max_age}) or an ACR level
 *         ({@code acr_values}), that the subject session satisfies them.
 *     <li>If a particular user identity is required (via an
 *         {@code id_token_hint}), that it matches session subject.
 *     <li>The request doesn't specify a prompt {@code login},
 *         {@code select_account} or {@code create}.
 *     <li>A Connect2id server configuration doesn't trigger an authentication
 *         prompt.
 * </ul>
 *
 * <p>If the OAuth 2.0 authorisation / OpenID authentication request is
 * eligible for SSO the {@link #isEligible check method} returns {@code true}.
 * Else the method returns {@code false}, to cause the Connect2id server to
 * prompt the end-user for authentication.
 *
 * <p>Implementations must be thread-safe.
 */
public interface WebSSOEligibilityChecker {


        /**
         * Checks whether the specified OAuth 2.0 authorisation / OpenID
         * authentication request is eligible for SSO.
         *
         * @param authzRequest   The authorisation request. Can be cast to
         *                       {@link com.nimbusds.openid.connect.sdk.AuthenticationRequest}
         *                       for an instance of an OpenID authentication
         *                       request. Not {@code null}.
         * @param clientInfo     The registered client information for the
         *                       {@code client_id}. Not {@code null}.
         * @param subjectSession The subject session. Not {@code null}.
         * @param checkerCtx     The checker context. Not {@code null}.
         *
         * @return {@code true} if the request is eligible for SSO,
         *         {@code false} if not and cause the Connect2id server to
         *         prompt the end-user for authentication.
         */
        boolean isEligible(final AuthorizationRequest authzRequest,
                           final OIDCClientInformation clientInfo,
                           final SubjectSession subjectSession,
                           final CheckerContext checkerCtx);
}

package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.provider.spi.crypto.HMACComputer;
import com.nimbusds.openid.connect.provider.spi.crypto.JWSVerifier;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;

import java.security.SecureRandom;
import java.util.Properties;


/**
 * Token encoder and decoder context.
 */
public interface TokenCodecContext extends InvocationContext {
	
	
	/**
	 * Returns an initialised secure random generator.
	 *
	 * @return The secure random generator.
	 */
	SecureRandom getSecureRandom();
	
	
	/**
	 * Returns the JSON Web Token (JWT) signer.
	 *
	 * @return The JWT signer.
	 */
	JWTSigner getJWTSigner();
	
	
	/**
	 * Returns the JSON Web Signature (JWS) verifier.
	 *
	 * @return The JWS verifier.
	 */
	JWSVerifier getJWSVerifier();
	
	
	/**
	 * Returns the Hash-based Message Authentication Code (HMAC) computer.
	 *
	 * @return The HMAC computer.
	 */
	HMACComputer getHMACComputer();
	
	
	/**
	 * The access token encoder and decoder properties prefix.
	 */
	String CODEC_PROPERTIES_PREFIX = "authzStore.accessToken.codec.";
	
	
	/**
	 * Returns the token encoder and decoder properties, if set in the
	 * Connect2id server configuration with prefix
	 * {@link #CODEC_PROPERTIES_PREFIX authzStore.accessToken.codec.*}.
	 *
	 * @return The properties, empty if none.
	 */
	Properties getCodecProperties();


	/**
	 * Returns the claim names compressor. Intended to reduce the size of
	 * the consented claims array in self-contained (JWT-encoded) access
	 * tokens.
	 *
	 * @return The claim names compressor.
	 */
	ClaimNamesCompressor getClaimNamesCompressor();
}

package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;
import net.jcip.annotations.ThreadSafe;


/**
 * Service Provider Interface (SPI) for encoding and decoding authorisations
 * for self-contained access tokens into JWT claims sets. Implementations must
 * be thread-safe.
 *
 * <p>Sample JWT claims set for a self-contained access token:
 *
 * <pre>
 * {
 *  "sub" : "alice",
 *  "cid" : "65564eb0058d",
 *  "scp" : [ "openid", "email", "app:write" ],
 *  "iss" : "https://c2id.com",
 *  "iat" : 1360050000,
 *  "exp" : 1360050795,
 *  "aud" : [ "https://resource-1.example.com", "https://resource-2.example.com" ]
 * }
 * </pre>
 *
 * <p>Implementations should extend {@link BaseSelfContainedAccessTokenClaimsCodec}
 * which encodes all token parameters for which there is an appropriate
 * standard JWT claim, such as for the subject, issuer and expiration time. The
 * implementation only needs to specify encodings for the remaining parameters,
 * such as scope and client ID.
 */
@ThreadSafe
public interface SelfContainedAccessTokenClaimsCodec extends Lifecycle {
	
	
	/**
	 * Encodes the specified access token authorisation into a JWT claims
	 * set.
	 *
	 * @param tokenAuthz The access token authorisation. Not {@code null}.
	 * @param context    The token encoder context. Not {@code null}.
	 *
	 * @return The JWT claims set.
	 */
	JWTClaimsSet encode(final AccessTokenAuthorization tokenAuthz, final TokenEncoderContext context);
	
	
	/**
	 * Encodes the specified access token authorisation into a JWT.
	 *
	 * @param tokenAuthz The access token authorisation. Not {@code null}.
	 * @param context    The token encoder context. Not {@code null}.
	 *
	 * @return The JWT claims set and other details.
	 */
	default JWTDetails advancedEncode(final AccessTokenAuthorization tokenAuthz, final TokenEncoderContext context) {
		return new JWTDetails() {
			@Override
			public JOSEObjectType getType() {
				return null;
			}
			
			
			@Override
			public JWTClaimsSet getJWTClaimsSet() {
				return encode(tokenAuthz, context);
			}
		};
	}
	
	
	/**
	 * Decodes the specified JWT claims set into an access token
	 * authorisation.
	 *
	 * @param claimsSet The JWT claims set. Not {@code null}.
	 * @param context   The token codec context. Not {@code null}.
	 *
	 * @return The access token authorisation.
	 *
	 * @throws TokenDecodeException If decoding failed.
	 */
	AccessTokenAuthorization decode(final JWTClaimsSet claimsSet, final TokenCodecContext context)
		throws TokenDecodeException;
	
	
	/**
	 * Decodes the specified JWT details into an access token
	 * authorisation.
	 *
	 * @param jwtDetails The JWT claims set and other details. Not
	 *                   {@code null}.
	 * @param context    The token codec context. Not {@code null}.
	 *
	 * @return The access token authorisation.
	 *
	 * @throws TokenDecodeException If decoding failed.
	 */
	default AccessTokenAuthorization advancedDecode(final JWTDetails jwtDetails, final TokenCodecContext context)
		throws TokenDecodeException {
		
		return decode(jwtDetails.getJWTClaimsSet(), context);
	}
}

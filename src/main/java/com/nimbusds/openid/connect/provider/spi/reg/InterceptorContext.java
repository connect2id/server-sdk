package com.nimbusds.openid.connect.provider.spi.reg;


import com.nimbusds.openid.connect.provider.spi.InvocationContext;


/**
 * Registration interceptor context.
 */
public interface InterceptorContext extends InvocationContext {
	
	
	/**
	 * Returns {@code true} if open registration is allowed.
	 *
	 * @return {@code true} if open registration is allowed, else
	 *         {@code false}.
	 */
	boolean openRegistrationIsAllowed();
}

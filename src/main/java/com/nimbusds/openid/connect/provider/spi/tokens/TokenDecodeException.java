package com.nimbusds.openid.connect.provider.spi.tokens;


import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Token decode exception.
 */
public class TokenDecodeException extends Exception {
	
	
	/**
	 * Creates a new token decode exception.
	 *
	 * @param message The exception message.
	 */
	public TokenDecodeException(final @Nullable String message) {
		
		super(message);
	}
	
	
	/**
	 * Creates a new token decode exception.
	 *
	 * @param message The exception message.
	 * @param cause   The exception cause, {@code null} if not specified.
	 */
	public TokenDecodeException(final @Nullable String message, final @Nullable Throwable cause) {
		
		super(message, cause);
	}
}

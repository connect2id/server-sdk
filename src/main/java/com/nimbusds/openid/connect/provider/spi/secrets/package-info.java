/**
 * Client secret encoding and decoding SPI.
 */
package com.nimbusds.openid.connect.provider.spi.secrets;
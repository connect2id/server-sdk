package com.nimbusds.openid.connect.provider.spi.tokens;


import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.dpop.JWKThumbprintConfirmation;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.openid.connect.sdk.SubjectType;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;


/**
 * Mutable access token authorisation.
 */
public final class MutableAccessTokenAuthorization implements AccessTokenAuthorization {
	
	
	private @Nullable Subject sub;
	
	
	private @Nullable Actor act;
	
	
	private @Nullable ClientID clientID;
	
	
	private @Nullable Scope scope;
	
	
	private @Nullable Instant exp;
	
	
	private @Nullable Instant iat;
	
	
	private @Nullable Issuer iss;
	
	
	private @Nullable List<Audience> audList;
	
	
	private @Nullable SubjectType subjectType;
	
	
	private @Nullable Subject localSubject;
	
	
	private @Nullable JWTID jti;
	
	
	private @Nullable Set<String> claimNames;
	
	
	private @Nullable List<LangTag> claimsLocales;
	
	
	private @Nullable JSONObject presetClaims;
	
	
	private @Nullable JSONObject claimsData;


	private @Nullable String subjectSessionKey;
	
	
	private @Nullable JSONObject data;
	
	
	private @Nullable X509CertificateConfirmation cnfX5t;
	
	
	private @Nullable JWKThumbprintConfirmation cnfJkt;
	
	
	private @Nullable Map<String, Object> otherTopLevelParams;
	
	
	/**
	 * Creates a new empty mutable access token authorisation.
	 */
	public MutableAccessTokenAuthorization() {
	}
	
	
	/**
	 * Creates a new mutable access token authorisation from the specified
	 * one.
	 *
	 * @param source The source access token authorisation. Must not be
	 *               {@code null}.
	 */
	public MutableAccessTokenAuthorization(final AccessTokenAuthorization source) {
		sub = source.getSubject();
		act = source.getActor();
		clientID = source.getClientID();
		scope = source.getScope();
		exp = source.getExpirationTime();
		iat = source.getIssueTime();
		iss = source.getIssuer();
		audList = source.getAudienceList();
		subjectType = source.getSubjectType();
		localSubject = source.getLocalSubject();
		jti = source.getJWTID();
		claimNames = source.getClaimNames();
		claimsLocales = source.getClaimsLocales();
		presetClaims = source.getPresetClaims();
		claimsData = source.getClaimsData();
		subjectSessionKey = source.getSubjectSessionKey();
		data = source.getData();
		cnfX5t = source.getClientCertificateConfirmation();
		cnfJkt = source.getJWKThumbprintConfirmation();
		otherTopLevelParams = source.getOtherTopLevelParameters();
	}
	
	
	/**
	 * Sets the access token subject.
	 *
	 * @param sub The subject, {@code null} if not specified.
	 *            
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withSubject(final @Nullable Subject sub) {
		this.sub = sub;
		return this;
	}
	
	
	@Override
	public @Nullable Subject getSubject() {
		return sub;
	}
	
	
	/**
	 * Sets the access token actor, in impersonation and delegation
	 * scenarios.
	 *
	 * @param act The actor, {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withActor(final @Nullable Actor act) {
		this.act = act;
		return this;
	}
	
	
	@Override
	public @Nullable Actor getActor() {
		return act;
	}
	
	
	/**
	 * Sets the identifier of the client to which the access token is
	 * issued.
	 *
	 * @param clientID The client identifier, {@code null} if not
	 *                 specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withClientID(final @Nullable ClientID clientID) {
		this.clientID = clientID;
		return this;
	}
	
	
	@Override
	public @Nullable ClientID getClientID() {
		return clientID;
	}
	
	
	/**
	 * Sets the scope of the access token.
	 *
	 * @param scope The scope, {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withScope(final @Nullable Scope scope) {
		this.scope = scope;
		return this;
	}
	
	
	@Override
	public @Nullable Scope getScope() {
		return scope;
	}
	
	
	/**
	 * Sets the expiration time of the access token.
	 *
	 * @param exp The expiration time, {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withExpirationTime(final @Nullable Instant exp) {
		this.exp = exp;
		return this;
	}
	
	
	@Override
	public @Nullable Instant getExpirationTime() {
		return exp;
	}
	
	
	/**
	 * Sets the issue time of the access token.
	 *
	 * @param iat The issue time, {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withIssueTime(final @Nullable Instant iat) {
		this.iat = iat;
		return this;
	}
	
	
	@Override
	public @Nullable Instant getIssueTime() {
		return iat;
	}
	
	
	/**
	 * Sets the issuer of the access token.
	 *
	 * @param iss The issuer, {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withIssuer(final @Nullable Issuer iss) {
		this.iss = iss;
		return this;
	}
	
	
	@Override
	public @Nullable Issuer getIssuer() {
		return iss;
	}
	
	
	/**
	 * Sets the audience list of the access token, which may be the logical
	 * names of the intended resource servers.
	 *
	 * @param audList The audience list, {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withAudienceList(final @Nullable List<Audience> audList) {
		this.audList = audList;
		return this;
	}
	
	
	@Override
	public @Nullable List<Audience> getAudienceList() {
		return audList;
	}
	
	
	/**
	 * Sets the access token subject type.
	 *
	 * @param subjectType The subject type, {@code null} if not specified
	 *                    (may imply {@link SubjectType#PUBLIC public}).
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withSubjectType(final @Nullable SubjectType subjectType) {
		this.subjectType = subjectType;
		return this;
	}
	
	
	@Override
	public @Nullable SubjectType getSubjectType() {
		return subjectType;
	}
	
	
	/**
	 * Sets the access token local (system) subject.
	 *
	 * @param localSubject The local (system) subject, {@code null} if not
	 *                     specified or for a pairwise
	 *                     {@link #getSubjectType() subject type} that
	 *                     couldn't be reversed.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withLocalSubject(final @Nullable Subject localSubject) {
		this.localSubject = localSubject;
		return this;
	}
	
	
	@Override
	public @Nullable Subject getLocalSubject() {
		if (SubjectType.PUBLIC == getSubjectType()) {
			return getSubject();
		} else {
			return localSubject;
		}
	}
	
	
	/**
	 * Sets the JSON Web Token (JWT) identifier of the access token.
	 *
	 * @param jti The JWT ID, {@code null} if not specified or applicable.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withJWTID(final @Nullable JWTID jti) {
		this.jti = jti;
		return this;
	}
	
	
	@Override
	public @Nullable JWTID getJWTID() {
		return jti;
	}
	
	
	/**
	 * Sets the names of the consented OpenID claims to be accessed at
	 * the UserInfo endpoint.
	 *
	 * @param claimNames The claim names, {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withClaimNames(final @Nullable Set<String> claimNames) {
		this.claimNames = claimNames;
		return this;
	}
	
	
	@Override
	public @Nullable Set<String> getClaimNames() {
		return claimNames;
	}
	
	
	/**
	 * Sets the preferred locales for the consented OpenID claims.
	 *
	 * @param claimsLocales The preferred claims locales, {@code null} if
	 *                      not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withClaimsLocales(final @Nullable List<LangTag> claimsLocales) {
		this.claimsLocales = claimsLocales;
		return this;
	}
	
	
	@Override
	public @Nullable List<LangTag> getClaimsLocales() {
		return claimsLocales;
	}
	
	
	/**
	 * Sets the preset OpenID claims to be included in the UserInfo
	 * response.
	 *
	 * @param presetClaims The preset OpenID claims, {@code null} if not
	 *                     specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withPresetClaims(final @Nullable JSONObject presetClaims) {
		this.presetClaims = presetClaims;
		return this;
	}
	
	
	@Override
	public @Nullable JSONObject getPresetClaims() {
		return presetClaims;
	}
	
	
	/**
	 * Sets the OpenID claims fulfillment data for the claims source at the
	 * UserInfo endpoint.
	 *
	 * @param claimsData The OpenID claims fulfillment data, {@code null}
	 *                   if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withClaimsData(final @Nullable JSONObject claimsData) {
		this.claimsData = claimsData;
		return this;
	}
	
	
	@Override
	public @Nullable JSONObject getClaimsData() {
		return claimsData;
	}


	/**
	 * Sets the associated subject (end-user) session key (session ID with
	 * omitted HMAC).
	 *
	 * @param subjectSessionKey The subject session key, {@code null} if
	 *                          not available.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withSubjectSessionkey(final @Nullable String subjectSessionKey) {
		this.subjectSessionKey = subjectSessionKey;
		return this;
	}


	@Override
	public @Nullable String getSubjectSessionKey() {
		return subjectSessionKey;
	}


	/**
	 * Sets the optional data for the access token.
	 *
	 * @param data The optional data, represented as a JSON object,
	 *             {@code null} if not specified.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withData(final @Nullable JSONObject data) {
		this.data = data;
		return this;
	}
	
	
	@Override
	public @Nullable JSONObject getData() {
		return data;
	}
	
	
	/**
	 * Sets the client X.509 certificate confirmation (SHA-256 thumbprint)
	 * for mutual TLS.
	 *
	 * @param cnfX5t The client X.509 certificate confirmation,
	 *               {@code null} if none.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withClientCertificateConfirmation(final @Nullable X509CertificateConfirmation cnfX5t) {
		this.cnfX5t = cnfX5t;
		return this;
	}
	
	
	@Override
	public @Nullable X509CertificateConfirmation getClientCertificateConfirmation() {
		return cnfX5t;
	}
	
	
	/**
	 * Sets the JWK SHA-256 thumbprint confirmation for DPoP.
	 *
	 * @param cnfJkt The JWK thumbprint confirmation, {@code null} if none.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withJWKThumbprintConfirmation(final @Nullable JWKThumbprintConfirmation cnfJkt) {
		this.cnfJkt = cnfJkt;
		return this;
	}
	
	
	@Override
	public @Nullable JWKThumbprintConfirmation getJWKThumbprintConfirmation() {
		return cnfJkt;
	}
	
	
	/**
	 * Sets the other top-level parameters.
	 *
	 * @param params Other top-level parameters, the values should map to
	 *               JSON entities, {@code null} if none.
	 *
	 * @return This object.
	 */
	public MutableAccessTokenAuthorization withOtherTopLevelParameters(final @Nullable Map<String, Object> params) {
		otherTopLevelParams = params;
		return this;
	}
	
	
	@Override
	public @Nullable Map<String, Object> getOtherTopLevelParameters() {
		return otherTopLevelParams;
	}
	
	
	@Override
	public String toString() {
		return new StringJoiner(", ", MutableAccessTokenAuthorization.class.getSimpleName() + "[", "]")
			.add("sub=" + sub)
			.add("act=" + act)
			.add("clientID=" + clientID)
			.add("scope=" + scope)
			.add("exp=" + exp)
			.add("iat=" + iat)
			.add("iss=" + iss)
			.add("audList=" + audList)
			.add("subType=" + subjectType)
			.add("localSub=" + localSubject)
			.add("jti=" + jti)
			.add("claimNames=" + claimNames)
			.add("claimsLocales=" + claimsLocales)
			.add("presetClaims=" + presetClaims)
			.add("claimsData=" + claimsData)
			.add("subjectSessionKey=" + subjectSessionKey)
			.add("data=" + data)
			.add("cnfX5t=" + cnfX5t)
			.add("cnfJkt=" + cnfJkt)
			.add("otherTopLevelParams=" + otherTopLevelParams)
			.toString();
	}
}

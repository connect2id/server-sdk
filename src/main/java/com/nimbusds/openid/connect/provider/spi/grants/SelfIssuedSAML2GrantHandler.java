package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import net.jcip.annotations.ThreadSafe;
import org.opensaml.saml.saml2.core.Assertion;


/**
 * Service Provider Interface (SPI) for handling self-issued SAML 2.0 bearer
 * assertion grants. Returns the matching
 * {@link SelfIssuedAssertionAuthorization authorisation} on success.
 *
 * <p>The handler should not specify access token lifetimes that exceed the
 * validity period of the SAML 2.0 assertion by a significant period. The issue
 * of refresh tokens is not permitted. Clients can refresh an expired access
 * token by requesting a new one using the same assertion, if it is still
 * valid, or with a new assertion.
 *
 * <p>Implementations must be thread-safe.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>Assertion Framework for OAuth 2.0 Client Authentication and
 *         Authorization Grants (RFC 7521), section 4.1.
 *     <li>Security Assertion Markup Language (SAML) 2.0 Profile for OAuth 2.0
 *         Client Authentication and Authorization Grants (RFC 7522), sections
 *         2.1, 3 and 3.1.
 * </ul>
 */
@ThreadSafe
public interface SelfIssuedSAML2GrantHandler extends SAML2GrantHandler {
	

	/**
	 * Handles a self-issued SAML 2.0 bearer assertion grant by a client
	 * registered with the Connect2id server.
	 *
	 * <p>This method is called for SAML 2.0 assertion grants which fulfil
	 * all the following conditions:
	 *
	 * <ol>
	 *     <li>Are issued by a client which is registered with the
	 *         Connect2id server, i.e. the assertion issuer matches a
	 *         registered client_id;
	 *     <li>The client is registered for the
	 *         {@code urn:ietf:params:oauth:grant-type:saml2-bearer} grant;
	 *     <li>The client is successfully authenticated, by means of
	 *         separate client authentication included in the token request
	 *         (client_secret_basic, client_secret_post, client_secret_jwt
	 *         or private_key_jwt), and / or with the SAML 2.0 assertion
	 *         grant itself;
	 *     <li>The SAML 2.0 assertion MAC or signature was successfully
	 *         verified using with a registered {@code client_secret} or
	 *         {@code jwks} / {@code jwks_uri};
	 *     <li>The assertion audience, expiration and not-before time are
	 *         verify successfully.
	 * </ol>
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @param assertion      The SAML 2.0 assertion. The audience,
	 *                       expiration, not-before time and XML signature
	 *                       are verified by the Connect2id server.
	 *                       The issuer will equal the client_id. Not
	 *                       {@code null}.
	 * @param scope          The requested scope, {@code null} if not
	 *                       specified.
	 * @param clientID       The identifier of the authenticated client.
	 *                       Not {@code null}.
	 * @param clientMetadata The OAuth 2.0 client / OpenID relying party
	 *                       metadata. Not {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	@Deprecated
	default SelfIssuedAssertionAuthorization processSelfIssuedGrant(final Assertion assertion,
								        final Scope scope,
								        final ClientID clientID,
								        final OIDCClientMetadata clientMetadata)
		throws GeneralException {
		
		return null;
	}
	

	/**
	 * Handles a self-issued SAML 2.0 bearer assertion grant by a client
	 * registered with the Connect2id server.
	 *
	 * <p>This method is called for SAML 2.0 assertion grants which fulfil
	 * all the following conditions:
	 *
	 * <ol>
	 *     <li>Are issued by a client which is registered with the
	 *         Connect2id server, i.e. the assertion issuer matches a
	 *         registered client_id;
	 *     <li>The client is registered for the
	 *         {@code urn:ietf:params:oauth:grant-type:saml2-bearer} grant;
	 *     <li>The client is successfully authenticated, by means of
	 *         separate client authentication included in the token request
	 *         (client_secret_basic, client_secret_post, client_secret_jwt
	 *         or private_key_jwt), and / or with the SAML 2.0 assertion
	 *         grant itself;
	 *     <li>The SAML 2.0 assertion MAC or signature was successfully
	 *         verified using with a registered {@code client_secret} or
	 *         {@code jwks} / {@code jwks_uri};
	 *     <li>The assertion audience, expiration and not-before time are
	 *         verify successfully.
	 * </ol>
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @param assertion          The SAML 2.0 assertion. The audience,
	 *                           expiration, not-before time and XML
	 *                           signature are verified by the Connect2id
	 *                           server. The issuer will equal the
	 *                           client_id. Not {@code null}.
	 * @param tokenRequestParams The token request parameters, such as the
	 * 	                     requested scope. Not {@code null}.
	 * @param clientID           The identifier of the authenticated client.
	 *                           Not {@code null}.
	 * @param clientMetadata     The OAuth 2.0 client / OpenID relying
	 *                           party metadata. Not {@code null}.
	 * @param invocationCtx      The invocation context. Not {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	@Deprecated
	default SelfIssuedAssertionAuthorization processSelfIssuedGrant(final Assertion assertion,
									final TokenRequestParameters tokenRequestParams,
									final ClientID clientID,
									final OIDCClientMetadata clientMetadata,
									final InvocationContext invocationCtx)
		throws GeneralException {
		
		return processSelfIssuedGrant(assertion, tokenRequestParams.getScope(), clientID, clientMetadata);
	}


	/**
	 * Handles a self-issued SAML 2.0 bearer assertion grant by a client
	 * registered with the Connect2id server.
	 *
	 * <p>This method is called for SAML 2.0 assertion grants which fulfil
	 * all the following conditions:
	 *
	 * <ol>
	 *     <li>Are issued by a client which is registered with the
	 *         Connect2id server, i.e. the assertion issuer matches a
	 *         registered client_id;
	 *     <li>The client is registered for the
	 *         {@code urn:ietf:params:oauth:grant-type:saml2-bearer} grant;
	 *     <li>The client is successfully authenticated, by means of
	 *         separate client authentication included in the token request
	 *         (client_secret_basic, client_secret_post, client_secret_jwt
	 *         or private_key_jwt), and / or with the SAML 2.0 assertion
	 *         grant itself;
	 *     <li>The SAML 2.0 assertion MAC or signature was successfully
	 *         verified using with a registered {@code client_secret} or
	 *         {@code jwks} / {@code jwks_uri};
	 *     <li>The assertion audience, expiration and not-before time are
	 *         verify successfully.
	 * </ol>
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @param assertion          The SAML 2.0 assertion. The audience,
	 *                           expiration, not-before time and XML
	 *                           signature are verified by the Connect2id
	 *                           server. The issuer will equal the
	 *                           client_id. Not {@code null}.
	 * @param tokenRequestParams The token request parameters, such as the
	 * 	                     requested scope. Not {@code null}.
	 * @param clientID           The identifier of the authenticated client.
	 *                           Not {@code null}.
	 * @param clientMetadata     The OAuth 2.0 client / OpenID relying
	 *                           party metadata. Not {@code null}.
	 * @param handlerCtx         The handler context. Not {@code null}.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	default SelfIssuedAssertionAuthorization processSelfIssuedGrant(final Assertion assertion,
									final TokenRequestParameters tokenRequestParams,
									final ClientID clientID,
									final OIDCClientMetadata clientMetadata,
									final GrantHandlerContext handlerCtx)
		throws GeneralException {

		return processSelfIssuedGrant(assertion, tokenRequestParams, clientID, clientMetadata, (InvocationContext) handlerCtx);
	}
}

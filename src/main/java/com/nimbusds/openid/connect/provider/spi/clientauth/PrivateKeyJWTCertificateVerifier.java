package com.nimbusds.openid.connect.provider.spi.clientauth;


import java.util.Optional;

import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Service Provider Interface (SPI) for verifying an X.509 certificate (x5c) in
 * {@link com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod#PRIVATE_KEY_JWT
 * private_key_jwt} client authentications. This can be used to enable
 * {@code private_key_jwt} authentication based on qualified certificates and
 * without a prior client JWK set registration (via the "jwks" or "jwks_uri"
 * client metadata parameters).
 *
 * <p>The SPI enables implementation of policies where only selected
 * clients are allowed or required to include a certificate for the
 * {@code private_key_jwt}, based on the client's registered metadata or other
 * criteria.
 *
 * <p>A client can {@linkplain CertificateLocation place} the certificate in
 * the {@code private_key_jwt} "x5c" header. Alternatively, the certificate
 * can be put in the "x5c" parameter of a matching public JWK and have the key
 * pre-registered via the "jwks" or "jwks_uri" client metadata parameter.
 *
 * <p>Implementations must be thread-safe.
 */
@ThreadSafe
public interface PrivateKeyJWTCertificateVerifier extends Lifecycle {
	
	
	/**
	 * Checks the X.509 certificate requirement for the specified
	 * {@code private_key_jwt} client authentication. If the client must
	 * use a certificate as part of the {@code private_key_jwt}
	 * authentication, included by value in the JWS "x5c" header parameter,
	 * or included in a registered client JWK in the client's "jwks" or
	 * "jwks_uri", the method returns a certificate verification callback.
	 *
	 * @param context The {@code private_key_jwt} client authentication
	 *                context. Not {@code null}.
	 *
	 * @return A certificate verification callback if a certificate is
	 *         required for the {@code private_key_jwt} client
	 *         authentication. If a certificate isn't required none is
	 *         returned.
	 *
	 * @throws InvalidClientException To reject the authentication with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_CLIENT
	 * invalid_client} error, due to an unmet authentication requirement.
	 * Throwing an {@link ExposedInvalidClientException} will override the
	 * default Connect2id server {@code error_description} and
	 * {@code error_uri} in the HTTP 401 Unauthorized error response.
	 */
	Optional<CertificateVerification> checkCertificateRequirement(final PrivateKeyJWTContext context)
		throws InvalidClientException;
}

package com.nimbusds.openid.connect.provider.spi;


import com.nimbusds.oauth2.sdk.id.Issuer;


/**
 * Common Service Provider Interface (SPI) invocation context.
 */
public interface InvocationContext {
	
	
	/**
	 * Returns the OpenID Provider / Authorisation Server issuer URL.
	 *
	 * @return The OpenID Provider / Authorisation Server issuer.
	 */
	Issuer getIssuer();
}

package com.nimbusds.openid.connect.provider.spi.claims;


import java.util.List;
import java.util.Set;

import net.jcip.annotations.ThreadSafe;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;


/**
 * Advanced Service Provider Interface (SPI) for sourcing OpenID Connect
 * UserInfo and other claims about a subject (end-user). This interface is
 * intended for claims sources that require access to additional parameters,
 * such as the client identifier (client_id); if that's not needed stick to the
 * {@link ClaimsSource basic interface}. Implementations must be thread-safe.
 *
 * <p>Claims sources can be:
 *
 * <ul>
 *     <li>LDAP directories
 *     <li>SQL or NoSQL databases
 *     <li>Web services
 *     <li>Files
 * </ul>
 */
@ThreadSafe
public interface AdvancedClaimsSource extends CommonClaimsSource {
	
	
	/**
	 * The {@code verified:} prefix for names of verified claims.
	 */
	String VERIFIED_CLAIM_NAME_PREFIX = "verified:";

	
	/**
	 * Requests claims for the specified subject.
	 *
	 * @param subject        The subject. Must not be {@code null}.
	 * @param claims         The names of the requested claims, with
	 *                       optional language tags. The names of verified
	 *                       claims (OpenID Connect for Identity Assurance
	 *                       1.0) are prefixed with "verified:". Not
	 *                       {@code null}.
	 * @param claimsLocales  The preferred languages and scripts for the
	 *                       claims to return, {@code null} if not
	 *                       specified.
	 * @param requestContext Provides access to additional parameters
	 *                       about the request. Not {@code null}.
	 *
	 * @return The claims, {@code null} if the subject wasn't found or the
	 *         claims source is {@link #isEnabled disabled}.
	 *
	 * @throws Exception If retrieval of the claims failed.
	 */
	UserInfo getClaims(final Subject subject,
			   final Set<String> claims,
			   final @Nullable List<LangTag> claimsLocales,
			   final ClaimsSourceRequestContext requestContext)
		throws Exception;
}

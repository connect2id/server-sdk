package com.nimbusds.openid.connect.provider.spi.nativesso;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.openid.connect.provider.spi.grants.AccessTokenSpec;
import com.nimbusds.openid.connect.provider.spi.grants.ClaimsSpec;
import com.nimbusds.openid.connect.provider.spi.grants.IDTokenSpec;
import com.nimbusds.openid.connect.provider.spi.grants.RefreshTokenSpec;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;


/**
 * Device SSO back-channel authorisation.
 *
 * <p>Required authorisation details:
 *
 * <ul>
 *     <li>The authorised scope. Must include the {@code openid} scope value if
 *         an ID token is issued.
 *     <li>If an ID token is issued, the authorised scope must include the
 *         {@code openid} scope value.
 * </ul>
 *
 * <p>All other parameters are optional or have suitable defaults.
 */
@Immutable
public class DeviceSSOBackChannelAuthorization {


        /**
         * The authorised scope.
         */
        private final Scope scope;


        /**
         * The access token specification.
         */
        private final AccessTokenSpec accessTokenSpec;


        /**
         * The refresh token specification.
         */
        private final RefreshTokenSpec refreshTokenSpec;


        /**
         * The ID token specification.
         */
        private final IDTokenSpec idTokenSpec;


        /**
         * The OpenID Connect claims spec.
         */
        private final ClaimsSpec claimsSpec;


        /**
         * Optional authorisation data as a JSON object, {@code null} if not
         * specified.
         */
        private final @Nullable JSONObject data;


        /**
         * Creates a new device SSO back-channel authorisation for a subject
         * (end-user).
         *
         * @param scope            The authorised scope. Must not be
         *                         {@code null}.
         * @param accessTokenSpec  The access token specification. Must not be
         *                         {@code null}.
         * @param refreshTokenSpec The refresh token specification. Must not be
         *                         {@code null}.
         * @param idTokenSpec      The ID token specification. Must not be
         *                         {@code null}.
         * @param claimsSpec       The OpenID claims specification. Must not be
         *                         {@code null}.
         * @param data             Additional data as a JSON object,
         *                         {@code null} if not specified.
         */
        public DeviceSSOBackChannelAuthorization(final Scope scope,
                                                 final AccessTokenSpec accessTokenSpec,
                                                 final RefreshTokenSpec refreshTokenSpec,
                                                 final IDTokenSpec idTokenSpec,
                                                 final ClaimsSpec claimsSpec,
                                                 final @Nullable JSONObject data) {

                this.scope = Objects.requireNonNull(scope);
                this.accessTokenSpec = Objects.requireNonNull(accessTokenSpec);
                this.refreshTokenSpec = Objects.requireNonNull(refreshTokenSpec);

                if (scope.contains(OIDCScopeValue.OPENID) && ! idTokenSpec.issue()) {
                        throw new IllegalArgumentException("ID token issue must be authorized for scope openid");
                }
                this.idTokenSpec = idTokenSpec;

                this.claimsSpec = Objects.requireNonNull(claimsSpec);
                this.data = data;
        }


        /**
         * Returns the authorised scope.
         *
         * @return The authorised scope.
         */
        public Scope getScope() {
                return scope;
        }


        /**
         * Returns the access token specification.
         *
         * @return The access token specification.
         */
        public AccessTokenSpec getAccessTokenSpec() {
                return accessTokenSpec;
        }


        /**
         * Returns the refresh token specification.
         *
         * @return The refresh token specification.
         */
        public RefreshTokenSpec getRefreshTokenSpec() {
                return refreshTokenSpec;
        }


        /**
         * Returns the ID token specification.
         *
         * @return The ID token specification.
         */
        public IDTokenSpec getIDTokenSpec() {
                return idTokenSpec;
        }


        /**
         * Returns the OpenID claims specification.
         *
         * @return The OpenID claims specification.
         */
        public ClaimsSpec getClaimsSpec() {
                return claimsSpec;
        }


        /**
         * Returns the additional data as a JSON object.
         *
         * @return The additional data, {@code null} if not specified.
         */
        public @Nullable JSONObject getData() {
                return data;
        }
}

package com.nimbusds.openid.connect.provider.spi.grants;


import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.oauth2.sdk.Scope;


/**
 * Common token request parameters for an OAuth 2.0 grant.
 */
public interface TokenRequestParameters {
	
	
	/**
	 * Returns the requested scope.
	 *
	 * @return The scope, {@code null} if not specified.
	 */
	default @Nullable Scope getScope() {
		return null;
	}
	
	
	/**
	 * Returns the resource server URI(s).
	 *
	 * @return The resource server URI(s), {@code null} if not specified.
	 */
	default @Nullable List<URI> getResources() {
		return null;
	}
	
	
	/**
	 * Returns the custom token request parameters.
	 *
	 * @return The custom parameters, empty map if none.
	 */
	default Map<String,List<String>> getCustomParameters() {
		return Collections.emptyMap();
	}
}

package com.nimbusds.openid.connect.provider.spi.tokens;


import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collection;
import java.util.List;


/**
 * Claim names compressor. Intended to reduce the size of the consented claims
 * array in self-contained (JWT-encoded) access tokens.
 */
public interface ClaimNamesCompressor {


        /**
         * Compresses the specified claim names. Names for which no compression
         * map is specified will be left unmodified.
         *
         * @param claimNames The claim names, {@code null} if none.
         *
         * @return The potentially compressed claim names, {@code null} if
         *         none.
         */
        @Nullable List<String> compress(final @Nullable Collection<String> claimNames);


        /**
         * Decompresses the specified potentially compressed list of claim
         * names.
         *
         * @param compressedList A potentially compressed list of claim names,
         *                       {@code null} if none.
         *
         * @return The original claim names, {@code null} if none.
         */
        @Nullable List<String> decompress(final @Nullable List<String> compressedList);
}

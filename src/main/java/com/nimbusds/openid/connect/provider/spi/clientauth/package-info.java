/**
 * Client authentication SPIs.
 */
package com.nimbusds.openid.connect.provider.spi.clientauth;
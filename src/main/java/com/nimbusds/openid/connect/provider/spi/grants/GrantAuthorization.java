package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.oauth2.sdk.util.MapUtils;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


/**
 * Basic OAuth 2.0 authorisation produced by a {@link GrantHandler}.
 *
 * <p>Required authorisation details:
 *
 * <ul>
 *     <li>The authorised scope.
 * </ul>
 *
 * <p>All other parameters are optional or have suitable defaults.
 */
@Immutable
public class GrantAuthorization {


	/**
	 * The authorised scope.
	 */
	private final Scope scope;


	/**
	 * The access token specification.
	 */
	private final AccessTokenSpec accessTokenSpec;
	
	
	/**
	 * The OpenID Connect claims spec.
	 */
	private final ClaimsSpec claimsSpec;


	/**
	 * Optional authorisation data as a JSON object, {@code null} if not
	 * specified.
	 */
	private final @Nullable JSONObject data;


	/**
	 * Creates a new basic authorisation.
	 *
	 * @param scope The authorised scope values. Must not be {@code null}.
	 */
	public GrantAuthorization(final Scope scope) {

		this(scope, AccessTokenSpec.DEFAULT, new ClaimsSpec(), null);
	}


	/**
	 * Creates a new basic authorisation.
	 *
	 * @param scope           The authorised scope. Must not be
	 *                        {@code null}.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	public GrantAuthorization(final Scope scope,
				  final AccessTokenSpec accessTokenSpec,
				  final @Nullable JSONObject data) {
		
		this(scope, accessTokenSpec, new ClaimsSpec(), data);
	}


	/**
	 * Creates a new basic authorisation.
	 *
	 * @param scope           The authorised scope. Must not be
	 *                        {@code null}.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param claimsSpec      The OpenID claims specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	public GrantAuthorization(final Scope scope,
				  final AccessTokenSpec accessTokenSpec,
				  final ClaimsSpec claimsSpec,
				  final @Nullable JSONObject data) {

		this.scope = Objects.requireNonNull(scope);
		this.accessTokenSpec = Objects.requireNonNull(accessTokenSpec);
		this.claimsSpec = Objects.requireNonNull(claimsSpec);
		this.data = data;
	}


	/**
	 * Creates a new basic authorisation.
	 *
	 * @param scope           The authorised scope. Must not be
	 *                        {@code null}.
	 * @param audList         Explicit audience of the access token,
	 *                        {@code null} if not specified.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	@Deprecated
	public GrantAuthorization(final Scope scope,
				  final @Nullable List<Audience> audList,
				  final AccessTokenSpec accessTokenSpec,
				  final @Nullable JSONObject data) {

		this(scope,
			new AccessTokenSpec(
				accessTokenSpec.getLifetime(),
				audList, // override with top-level parameter, backward compat API
				accessTokenSpec.getEncoding(),
				accessTokenSpec.getImpersonatedSubject(),
				accessTokenSpec.encrypt(),
				accessTokenSpec.getSubjectType()),
			data);
	}


	/**
	 * Returns the authorised scope.
	 *
	 * @return The authorised scope.
	 */
	public Scope getScope() {

		return scope;
	}


	/**
	 * Returns the explicit audience of the access token.
	 *
	 * @return The audience of the access token, {@code null} if not
	 *         specified.
	 */
	@Deprecated
	public @Nullable List<Audience> getAudience() {

		return getAccessTokenSpec().getAudience();
	}


	/**
	 * Returns the access token specification.
	 *
	 * @return The access token specification.
	 */
	public AccessTokenSpec getAccessTokenSpec() {

		return accessTokenSpec;
	}
	
	
	/**
	 * Returns the OpenID claims specification.
	 *
	 * @return The OpenID claims specification.
	 */
	public ClaimsSpec getClaimsSpec() {
		
		return claimsSpec;
	}


	/**
	 * Returns the additional data as a JSON object.
	 *
	 * @return The additional data, {@code null} if not specified.
	 */
	public @Nullable JSONObject getData() {

		return data;
	}


	/**
	 * Returns a JSON object representation of this authorisation.
	 *
	 * @return The JSON object representation.
	 */
	public JSONObject toJSONObject() {

		JSONObject o = new JSONObject();

		o.put("scope", scope.toStringList());

		JSONObject accessTokenSpecJSONObject = accessTokenSpec.toJSONObject();

		// Backward API compat
		// TODO remove in future major server version, deprecated in server v12.0
		if (CollectionUtils.isNotEmpty(getAccessTokenSpec().getAudience())) {
			o.put("audience", accessTokenSpecJSONObject.get("audience"));
		}

		o.put("access_token", accessTokenSpecJSONObject);
		
		o.putAll(claimsSpec.toJSONObject());

		if (MapUtils.isNotEmpty(data)) {
			o.put("data", data);
		}

		return o;
	}


	/**
	 * Parses a basic authorisation from the specified JSON object.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The basic authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static GrantAuthorization parse(final JSONObject jsonObject)
		throws ParseException {

		Scope scope = Scope.parse(Arrays.asList(JSONObjectUtils.getStringArray(jsonObject, "scope")));

		// Backward API compat
		List<Audience> topLevelAudList = null;
		if (jsonObject.containsKey("audience")) {
			String[] sa = JSONObjectUtils.getStringArray(jsonObject, "audience");
			topLevelAudList = new ArrayList<>(sa.length);
			for (String s: sa) {
				topLevelAudList.add(new Audience(s));
			}
		}

		AccessTokenSpec accessTokenSpec;

		if (jsonObject.get("access_token") != null) {
			// Parse
			JSONObject accessTokenJSONObject = JSONObjectUtils.getJSONObject(jsonObject, "access_token");
			if (topLevelAudList != null) {
				accessTokenJSONObject.put("audience", Audience.toStringList(topLevelAudList));
			}
			accessTokenSpec = AccessTokenSpec.parse(accessTokenJSONObject);
			if (topLevelAudList != null) {
				accessTokenSpec = new AccessTokenSpec(
					accessTokenSpec.getLifetime(),
					topLevelAudList, // Backward API compat
					accessTokenSpec.getEncoding(),
					accessTokenSpec.getImpersonatedSubject(),
					accessTokenSpec.encrypt(),
					accessTokenSpec.getSubjectType());
			}
		} else {
			// Apply default settings
			accessTokenSpec = new AccessTokenSpec();
		}
		
		ClaimsSpec claimsSpec = ClaimsSpec.parse(jsonObject);

		JSONObject data = null;

		if (jsonObject.containsKey("data")) {
			data = JSONObjectUtils.getJSONObject(jsonObject, "data");
		}

		return new GrantAuthorization(scope, accessTokenSpec, claimsSpec, data);
	}


	/**
	 * Parses a basic authorisation from the specified JSON object string.
	 *
	 * @param json The JSON object string to parse. Must not be
	 *             {@code null}.
	 *
	 * @return The basic authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static GrantAuthorization parse(final String json)
		throws ParseException {

		return parse(JSONObjectUtils.parse(json));
	}
}

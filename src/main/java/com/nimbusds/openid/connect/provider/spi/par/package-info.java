/**
 * Pushed authorisation request (PAR) validator SPI.
 */
package com.nimbusds.openid.connect.provider.spi.par;
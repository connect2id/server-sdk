package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GrantType;


/**
 * Marker interface for JSON Web Token (JWT) bearer assertion grants handlers.
 */
public interface JWTGrantHandler extends GrantHandler {


	/**
	 * The handled grant type.
	 */
	GrantType GRANT_TYPE = GrantType.JWT_BEARER;
	
	
	@Override
	default GrantType getGrantType() {
		return GRANT_TYPE;
	}
}

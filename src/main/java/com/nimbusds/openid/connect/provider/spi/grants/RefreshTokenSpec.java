package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;

import java.util.Optional;


/**
 * Refresh token specification.
 */
@Immutable
public class RefreshTokenSpec extends OptionalTokenSpec {


	/**
	 * Default refresh token specification (no issue).
	 */
	public static final RefreshTokenSpec DEFAULT = new RefreshTokenSpec();


	/**
	 * The maximum idle time, in seconds. Zero means no idle time
	 * expiration.
	 */
	private final long maxIdleTime;
	
	
	/**
	 * The rotation preference.
	 */
	private final Optional<Boolean> rotate;


	/**
	 * Creates a new default refresh token specification (no issue).
	 */
	public RefreshTokenSpec() {

		this(false, -1L);
	}


	/**
	 * Creates a new refresh token specification.
	 *
	 * @param issue    Controls the refresh token issue. If {@code true}
	 *                 a refresh token must be issued (requires a
	 *                 long-lived authorisation), {@code false} to prohibit
	 *                 issue.
	 * @param lifetime The refresh token maximum lifetime, in seconds. Zero
	 *                 means no lifetime limit. Negative means not
	 *                 specified (to let the Connect2id server apply the
	 *                 default configured refresh token lifetime). Applies
	 *                 only when a refresh token is issued.
	 */
	public RefreshTokenSpec(final boolean issue, final long lifetime) {

		this(issue, lifetime, Optional.empty());
	}


	/**
	 * Creates a new refresh token specification.
	 *
	 * @param issue    Controls the refresh token issue. If {@code true}
	 *                 a refresh token must be issued (requires a
	 *                 long-lived authorisation), {@code false} to prohibit
	 *                 issue.
	 * @param lifetime The refresh token maximum lifetime, in seconds. Zero
	 *                 means no lifetime limit. Negative means not
	 *                 specified (to let the Connect2id server apply the
	 *                 default configured refresh token lifetime). Applies
	 *                 only when a refresh token is issued.
	 * @param rotate   The optional rotation preference, if none the
	 *                 default refresh token rotation policy will apply.
	 */
	public RefreshTokenSpec(final boolean issue, final long lifetime, final Optional<Boolean> rotate) {

		this(issue, lifetime, 0L, rotate);
	}


	/**
	 * Creates a new refresh token specification.
	 *
	 * @param issue       Controls the refresh token issue. If {@code true}
	 *                    a refresh token must be issued (requires a
	 *                    long-lived authorisation), {@code false} to
	 *                    prohibit issue.
	 * @param lifetime    The refresh token maximum lifetime, in seconds.
	 *                    Zero means no lifetime limit. Negative means not
	 *                    specified (to let the Connect2id server apply the
	 *                    default configured refresh token lifetime).
	 *                    Applies only when a refresh token is issued.
	 * @param maxIdleTime The refresh token maximum idle time, in seconds.
	 *                    Zero means no idle time expiration.
	 * @param rotate      The optional rotation preference, if none the
	 *                    default refresh token rotation policy will apply.
	 */
	public RefreshTokenSpec(final boolean issue,
				final long lifetime,
				final long maxIdleTime,
				final Optional<Boolean> rotate) {

		super(issue, lifetime, null, null);
		this.maxIdleTime = maxIdleTime;
		this.rotate = rotate;
	}


	/**
	 * Returns the refresh token maximum idle time.
	 *
	 * @return The maximum idle time, in seconds. Zero means no idle time
	 *         expiration.
	 */
	public long getMaxIdleTime() {
		return maxIdleTime;
	}


	/**
	 * Returns the optional refresh token rotation preference.
	 *
	 * @return The rotation preference, if none the default refresh token
	 *         rotation policy will apply.
	 */
	public Optional<Boolean> getRotate() {
		return rotate;
	}
	
	
	@Override
	public JSONObject toJSONObject() {
		
		JSONObject o = super.toJSONObject();
		if (maxIdleTime > 0L) {
			o.put("max_idle", maxIdleTime);
		}
		rotate.ifPresent(aBoolean -> o.put("rotate", aBoolean));
		return o;
	}
	
	
	/**
	 * Parses a refresh token specification from the specified JSON object.
	 *
	 * @param o The JSON object. Must not be {@code null}.
	 *
	 * @return The refresh token specification.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static RefreshTokenSpec parse(final JSONObject o)
		throws ParseException {
		
		OptionalTokenSpec optionalTokenSpec = OptionalTokenSpec.parse(o);

		long maxIdleTime = 0L;
		if (o.containsKey("max_idle")) {
			maxIdleTime = JSONObjectUtils.getLong(o, "max_idle");
		}
		
		Optional<Boolean> rotate;
		if (o.get("rotate") != null) {
			rotate = Optional.of(JSONObjectUtils.getBoolean(o, "rotate"));
		} else {
			rotate = Optional.empty();
		}
		
		return new RefreshTokenSpec(
			optionalTokenSpec.issue(),
			optionalTokenSpec.getLifetime(),
			maxIdleTime,
			rotate);
	}
}

package com.nimbusds.openid.connect.provider.spi.claims;


import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Common interface shared by the OpenID Connect claims source SPIs. Developers
 * should implement one of the extending interfaces:
 *
 * <ul>
 *     <li>{@link ClaimsSource} Intended for most situations when a claims
 *         source connector is required.
 *     <li>{@link AdvancedClaimsSource} Provides additional parameters to aid
 *         servicing of claims source requests.
 * </ul>
 */
public interface CommonClaimsSource extends Lifecycle, ClaimsSupport {


}

package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.openid.connect.provider.spi.tokens.AccessTokenAuthorization;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Introspection of a subject token in a token exchange grant.
 */
public interface TokenIntrospection {
	
	
	/**
	 * Returns the access token authorisation for a subject token that is
	 * a locally issued access token.
	 *
	 * @return The access token authorisation, {@code null} if the subject
	 *         token is not a locally issued access token, invalid or
	 *         expired.
	 */
	@Nullable AccessTokenAuthorization getAccessTokenAuthorization();


	/**
	 * Returns the metadata of the OAuth 2.0 client / OpenID relying party
	 * for a subject token that is a locally issued access token.
	 *
	 * <p>The client metadata is retrieved using the client ID from
	 * {@link #getAccessTokenAuthorization()}.
	 *
	 * @return The client metadata, {@code null} if the subject token is
	 *         not a locally issued access token, invalid or expired.
	 */
	@Nullable OIDCClientMetadata getOIDCClientMetadata();
}

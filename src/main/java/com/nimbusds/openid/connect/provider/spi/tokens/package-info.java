/**
 * Token generation, encoding and decoding SPIs.
 */
package com.nimbusds.openid.connect.provider.spi.tokens;
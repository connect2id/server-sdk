/**
 * Cryptographic services exposed by the Connect2id server to selected SPIs.
 */
package com.nimbusds.openid.connect.provider.spi.crypto;
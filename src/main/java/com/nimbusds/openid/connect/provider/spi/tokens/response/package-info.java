/**
 * Token response customisation SPI.
 */
package com.nimbusds.openid.connect.provider.spi.tokens.response;
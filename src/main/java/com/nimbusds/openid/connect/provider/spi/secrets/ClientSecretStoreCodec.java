package com.nimbusds.openid.connect.provider.spi.secrets;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.auth.Secret;


/**
 * Service Provider Interface (SPI) for encoding OAuth client secrets before
 * persisting them to storage. Can be used to symmetrically encrypt or to hash
 * secrets (e.g. with SCrypt, BCrypt) before committing them to storage. Note,
 * OAuth clients registered for {@code client_secret_jwt} authentication where
 * the secret must be available in plaintext to perform HMAC must not be
 * hashed. This also applies to secrets which may otherwise require the plain
 * secret to be available for decoding, for example to facilitate symmetric
 * encryption of ID tokens or UserInfo.
 *
 * <p>The supplied {@link SecretCodecContext context} provides access to the
 * Connect2id server JWK set to retrieve any configured symmetric keys for the
 * client secret encryption, as well as the client metadata to determine the
 * registered client authentication method.
 *
 * <p>Implementations must be thread-safe.
 */
@ThreadSafe
public interface ClientSecretStoreCodec {
	
	
	/**
	 * Encodes the specified client secret before storing it. Encoding can
	 * be applied for selected clients only, based on their metadata or
	 * other criteria.
	 *
	 * @param secret The client secret. To obtain its value use the
	 *               {@link Secret#getValue()} or
	 *               {@link Secret#getValueBytes()} methods. Note, the
	 *               secret's expiration, if any, need not be encoded, it is
	 *               persisted separately. Not {@code null}.
	 * @param ctx    The codec context. Not {@code null}.
	 *
	 * @return The encoded secret. The default method returns the secret
	 *         value unencoded.
	 */
	default String encode(final Secret secret, final SecretCodecContext ctx) {
		return secret.getValue();
	}
	
	
	/**
	 * Encodes a client secret imported via the custom
	 * {@code preferred_client_secret} client metadata field before storing
	 * it. Encoding can be applied for selected clients only, based on
	 * their metadata or other criteria.
	 *
	 * @param secret The client secret as set by the custom
	 *               {@code preferred_client_secret} metadata field. To
	 *               obtain its value use the {@link Secret#getValue()} or
	 *               {@link Secret#getValueBytes()} methods. Note, the
	 *               secret's expiration, if any, need not be encoded, it is
	 *               persisted separately. Not {@code null}.
	 * @param ctx    The codec context. Not {@code null}.
	 *
	 * @return The encoded secret. The default method returns the secret
	 *         value unencoded.
	 */
	default String encodeImported(final Secret secret, final SecretCodecContext ctx) {
		return secret.getValue();
	}
	
	
	/**
	 * Decodes a client secret after retrieving it from the store.
	 *
	 * <p>If the secret is decoded to plain value the returned
	 * {@link DecodedSecret} must specify it.
	 *
	 * <p>If the secret is stored in a hashed form and cannot be decoded,
	 * the returned {@link DecodedSecret} instance must specify a
	 * {@link SecretVerifier}.
	 *
	 * @param storedValue The stored client secret value. Not {@code null}.
	 * @param ctx         The codec context. Not {@code null}.
	 *
	 * @return The decoded secret. The default method returns the stored
	 *         secret value.
	 */
	default DecodedSecret decode(final String storedValue, final SecretCodecContext ctx) {
		return DecodedSecret.createForPlainSecret(storedValue);
	}
}

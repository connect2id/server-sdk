package com.nimbusds.openid.connect.provider.spi.clientauth;


/**
 * Client authentication identifier, intended for audit and logging purposes.
 */
public interface ClientAuthenticationID {
	
	
	/**
	 * Returns the client authentication identifier value.
	 *
	 * @return The identifier value.
	 */
	String getValue();
}

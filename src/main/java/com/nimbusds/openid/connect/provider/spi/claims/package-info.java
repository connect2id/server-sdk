/**
 * OpenID Connect claims source SPIs.
 */
package com.nimbusds.openid.connect.provider.spi.claims;

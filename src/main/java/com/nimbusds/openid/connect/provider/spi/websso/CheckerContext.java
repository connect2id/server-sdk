package com.nimbusds.openid.connect.provider.spi.websso;

import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Web SSO eligibility checker context.
 */
public interface CheckerContext extends InvocationContext {


        /**
         * Returns the registered client information for the specified
         * {@code client_id}.
         *
         * @param clientID The client ID.
         *
         * @return The registered client information, {@code null} if the
         *         {@code client_id} is invalid.
         */
        @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID);
}

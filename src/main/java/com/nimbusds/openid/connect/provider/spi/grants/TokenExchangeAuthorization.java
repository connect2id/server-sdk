package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.TokenTypeURI;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;


/**
 * Authorisation produced by a {@link TokenExchangeGrantHandler}.
 *
 * <p>Supported issued token types:
 *
 * <ul>
 *     <li>Access token ({@link TokenTypeURI#ACCESS_TOKEN})
 * </ul>
 */
@Immutable
public class TokenExchangeAuthorization extends SubjectAuthorization {
	
	
	/**
	 * The type of the issued token.
	 */
	private final TokenTypeURI tokenType = TokenTypeURI.ACCESS_TOKEN;
	
	
	/**
	 * Controls the authorisation lifetime, {@code true} for a long-lived
	 * (implies persistence), {@code false} for a short-lived (transient).
	 */
	private final boolean longLived;
	
	
	/**
	 * The refresh token specification.
	 */
	private final RefreshTokenSpec refreshTokenSpec;
	
	
	/**
	 * Creates a new token exchange authorisation specifying the issue of
	 * an own (local) {@link TokenTypeURI#ACCESS_TOKEN access token}.
	 *
	 * @param subject         The subject (end-user) identifier. Must not
	 *                        be {@code null}.
	 * @param scope           The authorised scope. Must not be
	 *                        {@code null}.
	 * @param accessTokenSpec The access token specification. Must not be
	 *                        {@code null}.
	 * @param claimsSpec      The OpenID claims specification. Must not be
	 *                        {@code null}.
	 * @param data            Additional data as a JSON object,
	 *                        {@code null} if not specified.
	 */
	public TokenExchangeAuthorization(final Subject subject,
					  final Scope scope,
					  final AccessTokenSpec accessTokenSpec,
					  final ClaimsSpec claimsSpec,
					  final @Nullable JSONObject data) {
		
		this(subject, scope, false, accessTokenSpec, RefreshTokenSpec.DEFAULT, claimsSpec, data);
	}
	
	
	/**
	 * Creates a new token exchange authorisation specifying the issue of
	 * an own (local) {@link TokenTypeURI#ACCESS_TOKEN access token} with
	 * an optional refresh token.
	 *
	 * @param subject          The subject (end-user) identifier. Must not
	 *                         be {@code null}.
	 * @param scope            The authorised scope. Must not be
	 *                         {@code null}.
	 * @param longLived        Controls the authorisation lifetime.
	 *                         {@code true} for a long-lived (implies
	 *                         persistence), {@code false} for a
	 *                         short-lived (transient).
	 * @param accessTokenSpec  The access token specification. Must not be
	 *                         {@code null}.
	 * @param refreshTokenSpec The refresh token specification. Must not be
	 *                         {@code null}.
	 * @param claimsSpec       The OpenID claims specification. Must not be
	 *                         {@code null}.
	 * @param data             Additional data as a JSON object,
	 *                         {@code null} if not specified.
	 */
	public TokenExchangeAuthorization(final Subject subject,
					  final Scope scope,
					  final boolean longLived,
					  final AccessTokenSpec accessTokenSpec,
					  final RefreshTokenSpec refreshTokenSpec,
					  final ClaimsSpec claimsSpec,
					  final @Nullable JSONObject data) {
		
		this(subject, scope, longLived, accessTokenSpec, refreshTokenSpec, IDTokenSpec.NONE, claimsSpec, data);
	}
	
	
	/**
	 * Creates a new token exchange authorisation specifying the issue of
	 * an own (local) {@link TokenTypeURI#ACCESS_TOKEN access token} with
	 * an optional refresh token and ID token.
	 *
	 * @param subject          The subject (end-user) identifier. Must not
	 *                         be {@code null}.
	 * @param scope            The authorised scope. Must not be
	 *                         {@code null}.
	 * @param longLived        Controls the authorisation lifetime.
	 *                         {@code true} for a long-lived (implies
	 *                         persistence), {@code false} for a
	 *                         short-lived (transient).
	 * @param accessTokenSpec  The access token specification. Must not be
	 *                         {@code null}.
	 * @param refreshTokenSpec The refresh token specification. Must not be
	 *                         {@code null}.
	 * @param idTokenSpec      The ID token specification. Must not be
	 *                         {@code null}.
	 * @param claimsSpec       The OpenID claims specification. Must not be
	 *                         {@code null}.
	 * @param data             Additional data as a JSON object,
	 *                         {@code null} if not specified.
	 */
	public TokenExchangeAuthorization(final Subject subject,
					  final Scope scope,
					  final boolean longLived,
					  final AccessTokenSpec accessTokenSpec,
					  final RefreshTokenSpec refreshTokenSpec,
					  final IDTokenSpec idTokenSpec,
					  final ClaimsSpec claimsSpec,
					  final @Nullable JSONObject data) {
		
		super(subject, scope, accessTokenSpec, idTokenSpec, claimsSpec, data);
		this.longLived = longLived;
		this.refreshTokenSpec = Objects.requireNonNull(refreshTokenSpec);
	}
	
	
	/**
	 * Returns the type of the issued token.
	 *
	 * @return The type of the issued token.
	 */
	public TokenTypeURI getTokenType() {
		return tokenType;
	}
	
	
	/**
	 * Returns the authorisation lifetime.
	 *
	 * @return {@code true} for a long-lived authorisation (implies
	 *         persistence), {@code false} for a short-lived (transient).
	 */
	public boolean isLongLived() {
		return longLived;
	}
	
	
	/**
	 * Returns the refresh token specification.
	 *
	 * @return The refresh token specification.
	 */
	public RefreshTokenSpec getRefreshTokenSpec() {
		return refreshTokenSpec;
	}
	
	
	@Override
	public JSONObject toJSONObject() {
		
		JSONObject o =  super.toJSONObject();
		o.put("long_lived", longLived);
		o.put("issued_token_type", tokenType.toString());
		if (refreshTokenSpec.issue()) {
			o.put("refresh_token", refreshTokenSpec.toJSONObject());
		}
		return o;
	}
	
	
	/**
	 * Parses a token exchange authorisation from the specified JSON
	 * object.
	 *
	 * @param jsonObject The JSON object to parse. Must not be
	 *                   {@code null}.
	 *
	 * @return The token exchange authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static TokenExchangeAuthorization parse(final JSONObject jsonObject)
		throws ParseException {
		
		TokenTypeURI tokenTypeURI = TokenTypeURI.parse(JSONObjectUtils.getURI(jsonObject, "issued_token_type").toString());
		
		if (! TokenTypeURI.ACCESS_TOKEN.equals(tokenTypeURI)) {
			throw new ParseException("The issued_token_type must be " + TokenTypeURI.ACCESS_TOKEN);
		}
		
		boolean longLived = JSONObjectUtils.getBoolean(jsonObject, "long_lived", false);
		
		RefreshTokenSpec rtSpec;
		if (jsonObject.containsKey("refresh_token")) {
			rtSpec = RefreshTokenSpec.parse(JSONObjectUtils.getJSONObject(jsonObject, "refresh_token"));
		} else {
			rtSpec = new RefreshTokenSpec();
		}
		
		SubjectAuthorization subjectAuthz = SubjectAuthorization.parse(jsonObject);
		
		return new TokenExchangeAuthorization(
			subjectAuthz.getSubject(),
			subjectAuthz.getScope(),
			longLived,
			subjectAuthz.getAccessTokenSpec(),
			rtSpec,
			subjectAuthz.getIDTokenSpec(),
			subjectAuthz.getClaimsSpec(),
			subjectAuthz.getData());
	}
	
	
	/**
	 * Parses a token exchange authorisation from the specified JSON object
	 * string.
	 *
	 * @param json The JSON object string to parse. Must not be
	 *             {@code null}.
	 *
	 * @return The token exchange authorisation.
	 *
	 * @throws ParseException If parsing failed.
	 */
	public static TokenExchangeAuthorization parse(final String json)
		throws ParseException {
		
		return parse(JSONObjectUtils.parse(json));
	}
}

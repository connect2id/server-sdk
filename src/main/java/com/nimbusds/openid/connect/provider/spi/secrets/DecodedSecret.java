package com.nimbusds.openid.connect.provider.spi.secrets;


import com.nimbusds.oauth2.sdk.auth.Secret;
import net.jcip.annotations.Immutable;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Date;
import java.util.Objects;


/**
 * Decoded secret.
 */
@Immutable
public class DecodedSecret extends Secret {
	
	
	/**
	 * Verifier for hashed secrets.
	 */
	private final SecretVerifier verifier;
	
	
	/**
	 * The encoded (stored) value, {@code null} if not specified.
	 */
	private final @Nullable String encodedValue;
	
	
	/**
	 * Private constructor.
	 */
	private DecodedSecret(final String decodedValue,
			      final @Nullable Date expDate,
			      final SecretVerifier verifier,
			      final @Nullable String encodedValue) {
		super(decodedValue, expDate);
		this.verifier = verifier;
		this.encodedValue = encodedValue;
	}
	
	
	/**
	 * Returns a new decoded secret with the specified expiration date.
	 *
	 * @param expDate The expiration date, {@code null} if not specified.
	 *
	 * @return The new decoded secret.
	 */
	public DecodedSecret withExpiration(final @Nullable Date expDate) {
		if (getValue() != null) {
			return new DecodedSecret(getValue(), expDate, null, encodedValue);
		} else {
			DecodedSecret decodedSecret = new DecodedSecret("erase", expDate, verifier, encodedValue);
			decodedSecret.erase();
			return decodedSecret;
		}
	}
	
	
	/**
	 * Returns the encoded (stored) value.
	 *
	 * @return The encoded value, {@code null} if not specified.
	 */
	public @Nullable String getEncodedValue() {
		return encodedValue;
	}
	
	
	/**
	 * Returns a new decoded secret associating the specified encoded
	 * (stored) value with it.
	 *
	 * @param encodedValue The encoded (stored) value, {@code null} if not
	 *                     specified.
	 *
	 * @return The new decoded secret.
	 */
	public DecodedSecret withEncodedValue(final @Nullable String encodedValue) {
	
		if (getValue() != null) {
			return new DecodedSecret(getValue(), getExpirationDate(), null, encodedValue);
		} else {
			DecodedSecret decodedSecret = new DecodedSecret("erase", getExpirationDate(), verifier, encodedValue);
			decodedSecret.erase();
			return decodedSecret;
		}
	}
	
	
	@Override
	public boolean equals(Object o) {
		
		if (verifier != null && o instanceof Secret) {
			return verifier.verify((Secret) o);
		}
		
		return super.equals(o);
	}
	
	
	/**
	 * Creates a new decoded plain secret.
	 *
	 * @param plainValue The plain secret value. Must not be empty or
	 *                   {@code null}.
	 *
	 * @return The decoded secret.
	 */
	public static DecodedSecret createForPlainSecret(final String plainValue) {
		
		if (plainValue == null || plainValue.trim().isEmpty()) {
			throw new IllegalArgumentException("The plain secret value must not be null or empty");
		}
		
		return new DecodedSecret(plainValue, null, null, null);
	}
	
	
	/**
	 * Creates a new decoded hashed secret.
	 *
	 * @param verifier Verifier to check secrets against the hash. Must not
	 *                 be {@code null}.
	 *
	 * @return The decoded secret.
	 */
	public static DecodedSecret createForHashedSecret(final SecretVerifier verifier) {
		
		DecodedSecret decodedSecret = new DecodedSecret("erase", null, Objects.requireNonNull(verifier), null);
		decodedSecret.erase();
		return decodedSecret;
	}
}

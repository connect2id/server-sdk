package com.nimbusds.openid.connect.provider.spi.internal.sessionstore;

import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;
import java.util.List;


/**
 * Subject (end-user) authentication details.
 */
public interface SubjectAuthentication {


        /**
         * Returns the subject.
         *
         * @return The subject.
         */
        Subject getSubject();


        /**
         * Returns the time of the subject authentication.
         *
         * @return The time of the subject authentication.
         */
        Instant getTime();


        /**
         * Returns the Authentication Context Class Reference (ACR) for the
         * subject.
         *
         * @return The ACR, {@code null} if not specified.
         */
        @Nullable ACR getACR();


        /**
         * Returns the Authentication Method Reference (AMR) list for the
         * subject.
         *
         * @return The AMR list, {@code null} if not specified.
         */
        @Nullable List<AMR> getAMRList();
}

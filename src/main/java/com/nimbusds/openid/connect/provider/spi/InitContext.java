package com.nimbusds.openid.connect.provider.spi;


import jakarta.servlet.ServletContext;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.infinispan.manager.EmbeddedCacheManager;

import java.io.InputStream;


/**
 * Context for the initialisation of SPI implementations.
 *
 * <p>Provides:
 *
 * <ul>
 *     <li>Access to the web application context.
 *     <li>A method to retrieve a configuration or another file in the web
 *         application package.
 *     <li>Access to the Infinispan cache manager.
 * </ul>
 */
public interface InitContext {
	
	
	/**
	 * Returns the servlet context.
	 *
	 * @return The servlet context.
	 */
	ServletContext getServletContext();


	/**
	 * Returns the resource located at the named path as an input stream.
	 * Has the same behaviour as
	 * {@link jakarta.servlet.ServletContext#getResourceAsStream}.
	 *
	 * @param path The path to the resource, must begin with a '/' and is
	 *             interpreted as relative to the web application root.
	 *             Must not be {@code null}.
	 *
	 * @return The resource as an input stream, or {@code null} if no
	 *         resource exists at the specified path.
	 */
	@Nullable InputStream getResourceAsStream(final String path);
	
	
	/**
	 * Returns the Infinispan cache manager.
	 *
	 * @return The Infinispan cache manager.
	 */
	EmbeddedCacheManager getInfinispanCacheManager();
}
package com.nimbusds.openid.connect.provider.spi.events;


import com.nimbusds.openid.connect.provider.spi.Lifecycle;
import net.jcip.annotations.ThreadSafe;

import java.util.EventListener;


/**
 * Service Provider Interface (SPI) for listening to subject authentication and
 * consent events.
 *
 * <p>Implementations must be thread-safe. Listeners that emit events should
 * use a separate thread for blocking operations.
 */
@ThreadSafe
public interface SubjectAuthAndConsentEventListener extends Lifecycle, EventListener {
	
	
	/**
	 * This method is called when a subject is authenticated and consents.
	 *
	 * <p>For clients using the code flow ({@code response_type=code}), the
	 * event is dispatched when the client submits a valid authorisation
	 * code at the token endpoint of the Connect2id server.
	 *
	 * <p>For clients using an implicit ({@code response_type=token},
	 * {@code response_type=id_token}, {@code id_token token}) or hybrid
	 * flow ({@code response_type=code id_token},
	 * {@code response_type=code token},
	 * {@code response_type=code id_token token}, the event is dispatched
	 * when the request at the authorisation endpoint of the Connect2id
	 * server successfully completes.
	 *
	 * @param event        The subject authentication and consented event.
	 * @param eventContext Provides access to additional parameters about
	 *                     the event as well as helpers for its
	 *                     processing. Not {@code null}.
	 */
	void subjectAuthenticatedAndConsented(final SubjectAuthAndConsentEvent event, final EventContext eventContext);
}

package com.nimbusds.openid.connect.provider.spi.tokens.response;


import com.nimbusds.openid.connect.provider.spi.claims.CommonClaimsSource;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;


/**
 * The token response context.
 */
public interface TokenResponseContext extends InvocationContext {
	
	
	/**
	 * If the requesting client is successfully authenticated or identified
	 * at the token endpoint returns its registered information.
	 *
	 * @return The client information, {@code null} if not available.
	 */
	@Nullable OIDCClientInformation getOIDCClientInformation();
	
	
	/**
	 * For a successful token response returns the optional authorisation
	 * data.
	 *
	 * @return The authorisation data, {@code null} if not specified.
	 */
	@Nullable JSONObject getAuthorizationData();


	/**
	 * Returns the OpenID claims source.
	 *
	 * @return The OpenID claims source. Not {@code null}.
	 */
	CommonClaimsSource getCommonClaimsSource();


	/**
	 * Returns an interface for signing JSON Web Tokens (JWT).
	 *
	 * @return The JWT signer.
	 */
	JWTSigner getJWTSigner();
}

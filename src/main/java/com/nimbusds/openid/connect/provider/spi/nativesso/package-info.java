/**
 * OpenID Connect Native SSO handler SPI.
 */
package com.nimbusds.openid.connect.provider.spi.nativesso;
package com.nimbusds.openid.connect.provider.spi.tokens.response;


import com.nimbusds.oauth2.sdk.TokenResponse;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;
import net.jcip.annotations.ThreadSafe;


/**
 * Service Provider Interface (SPI) for composing custom token responses.
 * Implementations must be thread-safe.
 *
 * <p>This SPI can be used to include additional parameters in the token
 * response, such as an {@code authorization_details} parameter required in
 * OAuth 2.0 Rich Authorization Requests.
 *
 * <p>The SPI allows a successful access token response to be replaced with a
 * token error response.
 */
@ThreadSafe
public interface CustomTokenResponseComposer extends Lifecycle {
	
	
	/**
	 * Composes a custom token response.
	 *
	 * @param originalResponse The original success or error response. For
	 *                         an OpenID token response an instance of
	 *                         {@link com.nimbusds.openid.connect.sdk.OIDCTokenResponse}.
	 *                         Not {@code null}.
	 * @param context          The token response context. Not
	 *                         {@code null}.
	 *
	 * @return The token response to return to the client. If no
	 *         customisation is required the original response.
	 */
	TokenResponse compose(final TokenResponse originalResponse, final TokenResponseContext context);
}

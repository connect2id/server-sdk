package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.ResourceOwnerPasswordCredentialsGrant;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import net.jcip.annotations.ThreadSafe;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Service Provider Interface (SPI) for handling OAuth 2.0 resource owner
 * password credentials grants. Returns the matching
 * {@link PasswordGrantAuthorization authorisation} on success. Must throw an
 * {@link GeneralException} with an
 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_GRANT invalid_grant}
 * error code if the user credentials are invalid.
 *
 * <p>Implementations must be thread-safe.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OAuth 2.0 (RFC 6749), sections 1.3.3 and 4.3.
 * </ul>
 */
@ThreadSafe
public interface PasswordGrantHandler extends GrantHandler {


	/**
	 * The handled grant type.
	 */
	GrantType GRANT_TYPE = GrantType.PASSWORD;
	
	
	@Override
	default GrantType getGrantType() {
		return GRANT_TYPE;
	}
	
	
	/**
	 * Handles a resource owner password credentials grant.
	 *
	 * @param grant              The resource owner password credentials
	 *                           grant. Not {@code null}.
	 * @param scope              The requested scope, {@code null} if not
	 *                           specified.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param confidentialClient {@code true} if the client is confidential
	 *                           and has been authenticated, else
	 *                           {@code false}.
	 * @param clientMetadata     The OAuth 2.0 client / OpenID relying
	 *                           party metadata. Not {@code null}.
	 *
	 * <p>If the user credentials are invalid the handler must throw a
	 * {@link GeneralException exception} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_GRANT
	 * invalid_grant} error code.
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	@Deprecated
	default PasswordGrantAuthorization processGrant(final ResourceOwnerPasswordCredentialsGrant grant,
						        final @Nullable Scope scope,
						        final ClientID clientID,
						        final boolean confidentialClient,
						        final OIDCClientMetadata clientMetadata)
		throws GeneralException {
		
		return null;
	}
	
	
	/**
	 * Handles a resource owner password credentials grant.
	 *
	 * @param grant              The resource owner password credentials
	 *                           grant. Not {@code null}.
	 * @param tokenRequestParams The token request parameters, such as the
	 *                           requested scope. Not {@code null}.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param confidentialClient {@code true} if the client is confidential
	 *                           and has been authenticated, else
	 *                           {@code false}.
	 * @param clientMetadata     The OAuth 2.0 client / OpenID relying
	 *                           party metadata. Not {@code null}.
	 * @param invocationCtx      The invocation context. Not {@code null}.
	 *
	 * <p>If the user credentials are invalid the handler must throw a
	 * {@link GeneralException exception} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_GRANT
	 * invalid_grant} error code.
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	@Deprecated
	default PasswordGrantAuthorization processGrant(final ResourceOwnerPasswordCredentialsGrant grant,
							final TokenRequestParameters tokenRequestParams,
							final ClientID clientID,
							final boolean confidentialClient,
							final OIDCClientMetadata clientMetadata,
							final InvocationContext invocationCtx)
		throws GeneralException {
		
		return processGrant(grant, tokenRequestParams.getScope(), clientID, confidentialClient, clientMetadata);
	}


	/**
	 * Handles a resource owner password credentials grant.
	 *
	 * @param grant              The resource owner password credentials
	 *                           grant. Not {@code null}.
	 * @param tokenRequestParams The token request parameters, such as the
	 *                           requested scope. Not {@code null}.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param confidentialClient {@code true} if the client is confidential
	 *                           and has been authenticated, else
	 *                           {@code false}.
	 * @param clientMetadata     The OAuth 2.0 client / OpenID relying
	 *                           party metadata. Not {@code null}.
	 * @param handlerCtx         The handler context. Not {@code null}.
	 *
	 * <p>If the user credentials are invalid the handler must throw a
	 * {@link GeneralException exception} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_GRANT
	 * invalid_grant} error code.
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	default PasswordGrantAuthorization processGrant(final ResourceOwnerPasswordCredentialsGrant grant,
							final TokenRequestParameters tokenRequestParams,
							final ClientID clientID,
							final boolean confidentialClient,
							final OIDCClientMetadata clientMetadata,
							final GrantHandlerContext handlerCtx)
		throws GeneralException {

		return processGrant(grant, tokenRequestParams, clientID, confidentialClient, clientMetadata, (InvocationContext) handlerCtx);
	}
}

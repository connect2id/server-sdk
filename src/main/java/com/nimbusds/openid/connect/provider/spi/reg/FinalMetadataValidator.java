package com.nimbusds.openid.connect.provider.spi.reg;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.openid.connect.provider.spi.Lifecycle;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * Service Provider Interface (SPI) for for performing additional validation
 * and / or shaping of OAuth 2.0 client / OpenID relying party metadata, after
 * the Connect2id server has completed its own standard validations. The
 * loaded and {@link #isEnabled() enabled} SPI implementations will be called
 * (in no particular order) when a new client is registered (via HTTP POST
 * request) or updated (via HTTP PUT request).
 *
 * <p>Implementations must be thread-safe.
 */
@ThreadSafe
public interface FinalMetadataValidator extends Lifecycle {
	
	
	/**
	 * Validates the specified OAuth 2.0 client / OpenID relying party
	 * metadata.
	 *
	 * @param metadata     The OAuth 2.0 client / OpenID relying party
	 *                     metadata. Not {@code null}.
	 * @param validatorCtx The validator context. Not {@code null}.
	 *
	 * @return The validated metadata. It may be modified. Must not be
	 *         {@code null}.
	 *
	 * @throws InvalidRegistrationException If validation failed.
	 */
	OIDCClientMetadata validate(final OIDCClientMetadata metadata,
				    final ValidatorContext validatorCtx)
		throws InvalidRegistrationException;
}

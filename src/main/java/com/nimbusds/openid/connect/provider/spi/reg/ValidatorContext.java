package com.nimbusds.openid.connect.provider.spi.reg;


import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * OAuth 2.0 client / OpenID relying party metadata validator context.
 */
public interface ValidatorContext extends InvocationContext {
	
	
	/**
	 * Returns the type of authorisation credential used at the OAuth 2.0
	 * client registration endpoint.
	 *
	 * @return The authorisation credential type. Not {@code null}.
	 */
	AuthorizationCredentialType getAuthorizationCredentialType();
	
	
	/**
	 * Returns the OAuth 2.0 client / OpenID relying party metadata as
	 * received at the client registration endpoint.
	 *
	 * @return The received metadata. Not {@code null}.
	 */
	OIDCClientMetadata getReceivedMetadata();
}

package com.nimbusds.openid.connect.provider.spi.authz;


import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.op.ReadOnlyOIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;


/**
 * The authorisation request validator context.
 */
public interface ValidatorContext extends InvocationContext {
	
	
	/**
	 * Returns the OpenID provider metadata.
	 *
	 * @return The OpenID provider metadata.
	 */
	ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata();
	
	
	/**
	 * Returns the registered client information for the {@code client_id}
	 * in the authorisation request.
	 *
	 * @return The registered client information.
	 */
	OIDCClientInformation getOIDCClientInformation();
	
	
	/**
	 * Returns the original raw OAuth 2.0 authorisation / OpenID
	 * authentication request, as received at the authorisation endpoint
	 * and prior to any JAR unwrapping / resolution if JWT-secured.
	 *
	 * @return The raw request.
	 */
	AuthorizationRequest getRawRequest();
}

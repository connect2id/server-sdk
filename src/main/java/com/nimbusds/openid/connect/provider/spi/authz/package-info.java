/**
 * Authorisation request validator SPI.
 */
package com.nimbusds.openid.connect.provider.spi.authz;
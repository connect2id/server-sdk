/**
 * OAuth 2.0 authorisation grant handler SPIs.
 */
package com.nimbusds.openid.connect.provider.spi.grants;
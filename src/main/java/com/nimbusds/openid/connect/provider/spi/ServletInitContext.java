package com.nimbusds.openid.connect.provider.spi;


import jakarta.servlet.ServletContext;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.infinispan.manager.EmbeddedCacheManager;

import java.io.InputStream;
import java.util.Objects;


/**
 * Servlet-based context for the initialisation of SPI implementations.
 */
public abstract class ServletInitContext implements InitContext {


	/**
	 * The servlet context.
	 */
	private final ServletContext servletContext;


	/**
	 * Creates a new servlet-based SPI initialisation context.
	 *
	 * @param servletContext The servlet context. Must not be {@code null}.
	 */
	public ServletInitContext(final ServletContext servletContext) {
		this.servletContext = Objects.requireNonNull(servletContext);
	}


	/**
	 * Returns the servlet context.
	 *
	 * @return The servlet context.
	 */
	public ServletContext getServletContext() {

		return servletContext;
	}


	@Override
	public @Nullable InputStream getResourceAsStream(final String path) {

		return servletContext.getResourceAsStream(path);
	}
	
	
	@Override
	public EmbeddedCacheManager getInfinispanCacheManager() {
		
		// Constant from com.nimbusds.common.servlet.InfinispanLauncher.INFINISPAN_CTX_ATTRIBUTE_NAME
		return (EmbeddedCacheManager) servletContext.getAttribute("org.infinispan.manager.EmbeddedCacheManager");
	}
}


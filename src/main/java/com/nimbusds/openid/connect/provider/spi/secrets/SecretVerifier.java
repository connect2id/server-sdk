package com.nimbusds.openid.connect.provider.spi.secrets;


import com.nimbusds.oauth2.sdk.auth.Secret;


/**
 * Secret verifier.
 */
public interface SecretVerifier {
	
	
	/**
	 * Verifies the specified secret.
	 *
	 * @param secret The secret. Not {@code null}.
	 *
	 * @return {@code true} if the secret is valid, else {@code false}.
	 */
	boolean verify(final Secret secret);
}

package com.nimbusds.openid.connect.provider.spi.reg;


/**
 * The type of authorisation credential used at the OAuth 2.0 client
 * registration endpoint, for an initial registration with POST or a
 * registration update with PUT.
 */
public enum AuthorizationCredentialType {
	
	
	/**
	 * Master access token for the registration endpoint.
	 */
	MASTER_TOKEN,
	
	
	/**
	 * Regular access token issued by the Connect2id server (for some
	 * OAuth 2.0 grant) for the purpose of initial registration.
	 */
	INITIAL_TOKEN,
	
	
	/**
	 * Access token issued to the client at the registration endpoint to
	 * allow the client to update its own metadata.
	 */
	REGISTRATION_TOKEN,
	
	
	/**
	 * For an initial registration received without an access token when
	 * the Connect2id server is configured for open registration.
	 */
	OPEN
}

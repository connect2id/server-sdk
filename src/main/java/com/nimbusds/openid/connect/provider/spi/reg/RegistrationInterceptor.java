package com.nimbusds.openid.connect.provider.spi.reg;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Service Provider Interface (SPI) for intercepting and optionally modifying
 * HTTP requests at the client registration endpoint. The loaded and
 * {@link #isEnabled() enabled} SPI implementation will be called when an HTTP
 * request is received at the client registration endpoint.
 *
 * <p>An SPI implementation which requires a client X.509 certificate included
 * in the HTTP request and successfully validated by the web server / TLS proxy
 * can retrieve it using the {@link HTTPRequest#getClientX509Certificate()} and
 * related methods.
 *
 * <p>Implementations must be thread-safe.
 */
@ThreadSafe
public interface RegistrationInterceptor extends Lifecycle {
	
	
	/**
	 * Intercepts an HTTP POST request at the client registration endpoint.
	 * Passes the HTTP request unmodified by default.
	 *
	 * @param httpRequest    The HTTP POST request.
	 * @param interceptorCtx The interceptor context.
	 *
	 * @return The HTTP POST request to pass on to the endpoint for further
	 *         processing.
	 *
	 * @throws WrappedHTTPResponseException To return an HTTP (error)
	 *                                      response immediately.
	 */
	default HTTPRequest interceptPostRequest(final HTTPRequest httpRequest,
						 final InterceptorContext interceptorCtx)
		throws WrappedHTTPResponseException {
		
		return httpRequest;
	}
	
	
	/**
	 * Intercepts an HTTP GET request at the client registration endpoint.
	 * Passes the HTTP request unmodified by default.
	 *
	 * @param httpRequest    The HTTP GET request.
	 * @param interceptorCtx The interceptor context.
	 *
	 * @return The HTTP GET request to pass on to the endpoint for further
	 *         processing.
	 *
	 * @throws WrappedHTTPResponseException To return an HTTP (error)
	 *                                      response immediately.
	 */
	default HTTPRequest interceptGetRequest(final HTTPRequest httpRequest,
						final InterceptorContext interceptorCtx)
		throws WrappedHTTPResponseException {
		
		return httpRequest;
	}
	
	
	/**
	 * Intercepts an HTTP PUT request at the client registration endpoint.
	 * Passes the HTTP request unmodified by default.
	 *
	 * @param httpRequest    The HTTP PUT request.
	 * @param interceptorCtx The interceptor context.
	 *
	 * @return The HTTP PUT request to pass on to the endpoint for further
	 *         processing.
	 *
	 * @throws WrappedHTTPResponseException To return an HTTP (error)
	 *                                      response immediately.
	 */
	default HTTPRequest interceptPutRequest(final HTTPRequest httpRequest,
						final InterceptorContext interceptorCtx)
		throws WrappedHTTPResponseException {
		
		return httpRequest;
	}
	
	
	/**
	 * Intercepts an HTTP DELETE request at the client registration
	 * endpoint. Passes the HTTP request unmodified by default.
	 *
	 * @param httpRequest    The HTTP DELETE request.
	 * @param interceptorCtx The interceptor context.
	 *
	 * @return The HTTP DELETE request to pass on to the endpoint for
	 *         further processing.
	 *
	 * @throws WrappedHTTPResponseException To return an HTTP (error)
	 *                                      response immediately.
	 */
	default HTTPRequest interceptDeleteRequest(final HTTPRequest httpRequest,
						   final InterceptorContext interceptorCtx)
		throws WrappedHTTPResponseException {
		
		return httpRequest;
	}
}

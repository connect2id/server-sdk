package com.nimbusds.openid.connect.provider.spi.internal.sessionstore;

import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.time.Instant;
import java.util.Set;


/**
 * Subject (end-user) session.
 */
public interface SubjectSession {

        /**
         * Returns the session subject. Shorthand for
         * {@code getSubjectAuthentication.getSubject()}.
         *
         * @return The session subject.
         */
        Subject getSubject();


        /**
         * Returns the subject authentication.
         *
         * @return The subject authentication.
         */
        SubjectAuthentication getSubjectAuthentication();


        /**
         * Gets the session creation time.
         *
         * @return The creation time.
         */
        Instant getCreationTime();


        /**
         * Gets the maximum session lifetime.
         *
         * @return The maximum lifetime in minutes, unlimited if negative, not
         *         specified if zero.
         */
        long getMaxLifetime();


        /**
         * Gets the authentication lifetime.
         *
         * @return The authentication lifetime in minutes, unlimited if
         *         negative, not specified if zero.
         */
        long getAuthLifetime();


        /**
         * Gets the maximum session idle time.
         *
         * @return The maximum idle time in minutes, unlimited if negative, not
         *         specified if zero.
         */
        long getMaxIdleTime();


        /**
         * Returns the OpenID relying parties logged in during the session.
         *
         * @return The logged in OpenID relying parties, empty set if none.
         */
        Set<ClientID> getRelyingParties();


        /**
         * Returns the optional claims about the subject.
         *
         * @return The optional claims, {@code null} if none.
         */
        @Nullable JSONObject getClaims();


        /**
         * Returns the optional data.
         *
         * @return The optional data, {@code null} if none.
         */
        @Nullable JSONObject getData();
}

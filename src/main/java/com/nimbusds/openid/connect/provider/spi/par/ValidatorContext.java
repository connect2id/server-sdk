package com.nimbusds.openid.connect.provider.spi.par;


import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import com.nimbusds.openid.connect.sdk.op.ReadOnlyOIDCProviderMetadata;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * The PAR validator context.
 */
public interface ValidatorContext extends InvocationContext {
	
	
	/**
	 * Returns the OpenID provider metadata.
	 *
	 * @return The OpenID provider metadata.
	 */
	ReadOnlyOIDCProviderMetadata getReadOnlyOIDCProviderMetadata();
	
	
	/**
	 * Returns the registered client information.
	 *
	 * @return The registered client information.
	 */
	OIDCClientInformation getOIDCClientInformation();


	/**
	 * Returns the registered client information for the specified
	 * {@code client_id}.
	 *
	 * @param clientID The client ID.
	 *
	 * @return The registered client information, {@code null} if the
	 *         {@code client_id} is invalid.
	 */
	@Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID);
	
	
	/**
	 * Returns the original raw OAuth 2.0 authorisation / OpenID
	 * authentication request, as received at the authorisation endpoint
	 * and prior to any JAR unwrapping / resolution if JWT-secured.
	 *
	 * @return The raw request.
	 */
	AuthorizationRequest getRawRequest();
}

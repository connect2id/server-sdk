package com.nimbusds.openid.connect.provider.spi.grants;


import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import net.jcip.annotations.ThreadSafe;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Service Provider Interface (SPI) for handling OAuth 2.0 client credentials
 * grants. Returns the matching {@link GrantAuthorization authorisation} on
 * success.
 *
 * <p>Implementations must be thread-safe.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OAuth 2.0 (RFC 6749), sections 1.3.4 and 4.4.
 * </ul>
 */
@ThreadSafe
public interface ClientCredentialsGrantHandler extends GrantHandler {


	/**
	 * The handled grant type.
	 */
	GrantType GRANT_TYPE = GrantType.CLIENT_CREDENTIALS;
	
	
	@Override
	default GrantType getGrantType() {
		return GRANT_TYPE;
	}
	
	
	/**
	 * Handles a client credentials grant. The client is confidential and
	 * always authenticated.
	 *
	 * @param scope          The requested scope, {@code null} if not
	 *                       specified.
	 * @param clientID       The client identifier. Not {@code null}.
	 * @param clientMetadata The OAuth 2.0 client metadata. Not
	 *                       {@code null}.
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	@Deprecated
	default GrantAuthorization processGrant(final @Nullable Scope scope,
						final ClientID clientID,
						final ClientMetadata clientMetadata)
		throws GeneralException {
		
		return null;
	}
	
	
	/**
	 * Handles a client credentials grant. The client is confidential and
	 * always authenticated.
	 *
	 * @param tokenRequestParams The token request parameters, such as the
	 *                           requested scope. Not {@code null}.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param clientMetadata     The OAuth 2.0 client metadata. Not
	 *                           {@code null}.
	 * @param invocationCtx      The invocation context. Not {@code null}.
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	@Deprecated
	default GrantAuthorization processGrant(final TokenRequestParameters tokenRequestParams,
					        final ClientID clientID,
					        final ClientMetadata clientMetadata,
					        final InvocationContext invocationCtx)
		throws GeneralException {
		
		return processGrant(tokenRequestParams.getScope(), clientID, clientMetadata);
	}


	/**
	 * Handles a client credentials grant. The client is confidential and
	 * always authenticated.
	 *
	 * @param tokenRequestParams The token request parameters, such as the
	 *                           requested scope. Not {@code null}.
	 * @param clientID           The client identifier. Not {@code null}.
	 * @param clientMetadata     The OAuth 2.0 client metadata. Not
	 *                           {@code null}.
	 * @param handlerCtx         The handler context. Not {@code null}.
	 *
	 * <p>If the requested scope is invalid, unknown, malformed, or exceeds
	 * the scope granted by the resource owner the handler must throw a
	 * {@link GeneralException} with an
	 * {@link com.nimbusds.oauth2.sdk.OAuth2Error#INVALID_SCOPE
	 * invalid_scope} error code.
	 *
	 * @return The authorisation.
	 *
	 * @throws GeneralException If the grant is invalid, or another
	 *                          exception was encountered.
	 */
	default GrantAuthorization processGrant(final TokenRequestParameters tokenRequestParams,
					        final ClientID clientID,
					        final ClientMetadata clientMetadata,
					        final GrantHandlerContext handlerCtx)
		throws GeneralException {

		return processGrant(tokenRequestParams, clientID, clientMetadata, (InvocationContext) handlerCtx);
	}
}

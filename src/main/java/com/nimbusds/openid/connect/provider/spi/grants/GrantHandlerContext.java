package com.nimbusds.openid.connect.provider.spi.grants;

import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.openid.connect.provider.spi.InvocationContext;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Set;


/**
 * OAuth 2.0 authorisation grant handler context.
 */
public interface GrantHandlerContext extends InvocationContext {


        /**
         * Resolves the claim names for all scope values that expand to claims.
         * Recognises all standard OpenID Connect scope values as well as any
         * custom mappings configured in the Connect2id server.
         *
         * @param scope The scope, {@code null} if not specified.
         *
         * @return The resolved claim names, as an unmodifiable set, empty set
         *         if none.
         */
        Set<String> resolveClaimNames(final @Nullable Scope scope);
}

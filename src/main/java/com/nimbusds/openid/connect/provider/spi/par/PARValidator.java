package com.nimbusds.openid.connect.provider.spi.par;


import net.jcip.annotations.ThreadSafe;

import com.nimbusds.oauth2.sdk.AuthorizationRequest;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.openid.connect.provider.spi.Lifecycle;


/**
 * Service Provider Interface (SPI) for performing additional validation of
 * Pushed Authorisation Requests (PAR).
 *
 * <p>The {@link #validatePushedAuthorizationRequest} method will be called
 * after the Connect2id server has performed standard validation of the OAuth
 * 2.0 authorisation / OpenID authentication request, such as checking the
 * {@code client_id} and ensuring the client is authorised to use the OAuth 2.0
 * grant. JWT-secured authorisation requests (JAR) will be unwrapped / resolved
 * before that. The original raw request can be obtained from the
 * {@link ValidatorContext#getRawRequest() context}.
 *
 * <p>The validated request can be returned modified. Modifications should be
 * limited to optional parameters. Parameters such as {@code client_id},
 * {@code response_type}, {@code redirect_uri} and {@code state} must not be
 * modified.
 *
 * <p>The {@link #validatePushedAuthorizationRequest} method can reject the
 * request by throwing an {@link InvalidPushedAuthorizationRequestException}
 * with an appropriate HTTP status code and error code. The exception message
 * will be logged and not output to the client.
 *
 * <p>Example:
 *
 * <pre>
 * throw new InvalidPARException("Scope not accepted scope", // will be logged
 * 	OAuth2Error.INVALID_SCOPE
 * 	.setHTTPStatusCode(400)
 * 	.setDescription("Scope not accepted: some_scope"));
 * </pre>
 *
 * The resulting HTTP response:
 *
 * <pre>
 * HTTP/1.1 400 Bad Request
 * Content-Type: application/json;charset=UTF-8
 * Cache-Control: no-store
 * Pragma: no-cache
 *
 * {
 *   "error"             : "invalid_scope",
 *   "error_description" : "Scope not accepted: some_scope"
 * }
 * </pre>
 *
 * <p>Implementations must be thread-safe.
 */
@ThreadSafe
public interface PARValidator extends Lifecycle {
	
	
	/**
	 * Validates the specified OAuth 2.0 authorisation / OpenID
	 * authentication request.
	 *
	 * <p>Deprecated, use {@link #validatePushedAuthorizationRequest}
	 * instead.
	 *
	 * @param authzRequest The request to perform additional validation on.
	 *                     Can be cast to
	 *                     {@link com.nimbusds.openid.connect.sdk.AuthenticationRequest}
	 *                     for an instance of an OpenID authentication
	 *                     request. Not {@code null}.
	 * @param validatorCtx The PAR validator context. Not {@code null}.
	 *
	 * @throws GeneralException If the request is rejected. Should include
	 *                          an appropriate HTTP status and error code.
	 */
	@Deprecated
	default void validate(final AuthorizationRequest authzRequest, final ValidatorContext validatorCtx)
		throws GeneralException {
		
		try {
			validatePushedAuthorizationRequest(authzRequest, validatorCtx);
		} catch (InvalidPushedAuthorizationRequestException e) {
			throw new GeneralException(e.getMessage(), e.getErrorObject());
		}
	}
	
	
	/**
	 * Validates the specified OAuth 2.0 authorisation / OpenID
	 * authentication request.
	 *
	 * @param authzRequest The request to perform additional validation on.
	 *                     Can be cast to
	 *                     {@link com.nimbusds.openid.connect.sdk.AuthenticationRequest}
	 *                     for an instance of an OpenID authentication
	 *                     request. Not {@code null}.
	 * @param validatorCtx The PAR validator context. Not {@code null}.
	 *
	 * @return The validated OAuth 2.0 authorisation / OpenID
	 * authentication request. It may be modified. Must not be
	 * {@code null}.
	 *
	 * @throws InvalidPushedAuthorizationRequestException If the request is
	 *                                                    rejected.
	 */
	default AuthorizationRequest validatePushedAuthorizationRequest(final AuthorizationRequest authzRequest,
									final ValidatorContext validatorCtx)
		throws InvalidPushedAuthorizationRequestException {
		
		try {
			validate(authzRequest, validatorCtx);
		} catch (GeneralException e) {
			throw new InvalidPushedAuthorizationRequestException(e.getMessage(), e.getErrorObject());
		}
		return authzRequest;
	}
}
